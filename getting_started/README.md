# Space Hub
Getting started for first timers on Space Hub.

## Overview
There are two main folders:
- `quantum_code`: the pure game state/logic Quantum C# project
- `quantum_unity`: the Unity project that uses the code in `quantum_code`

## Requirements
Make sure you meet these requirements before you try to work with Space Hub.

- Unity 2019.1.x
- Visual Studio (min version: 2017) with "managed desktop" workload installed

## Building Quantum
Before we start working inside Unity, we need to first build the Quantum solution.
We do that by opening the solution file `quantum_code/quantum_code.sln` inside Visual Studio.

Once Visual Studio is opened, build the entire solution (`F6` or `Ctrl+Shift+B` depending on your locale). Make sure there are no errors showing in the build log!

## Unity Setup
If everything went fine in the last step, there should be no errors showing in Unity's console (after it finishes compiling the imported assemblies).

Find the file `PhotonServerSettings` in the `Project` view; change `Hosting` to `Best Region`; enter your Quantum `AppId` and yout Photon Voice `Voice AppId`; check `Auto-Join Lobby`.

## Testing
You can test the entire game flow by playing from the scene `Assets/_Project/Scenes/Start.unity`.

It is also possible to play directly (with multiplayer disabled) any game scene such as:
- `Assets/_Project/Scenes/Game.unity`
- `Assets/_Project/Scenes/Game_Firecamp.unity`

## Building SpaceHub
There are no extra steps when building SpaceHub. It is enough to just switch to the target platform on `Build Settings` and hitting `Build`. That holds true even for the Oculus Quest target.
