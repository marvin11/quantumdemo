﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("quantum.console.runner")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("quantum.console.runner")]
[assembly: AssemblyCopyright("Copyright © Exit Games 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("b2c620e4-13f8-4ecd-a7f0-88724216e90c")]
[assembly: AssemblyVersion("1.2.4.1")]
[assembly: AssemblyFileVersion("1.2.4.1")]
[assembly: AssemblyInformationalVersion("1.2.4.1.129 F1")]
