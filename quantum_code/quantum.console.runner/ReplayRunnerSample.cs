using Newtonsoft.Json;
using Photon.Deterministic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Quantum {
  public class ReplayRunnerSample {
    public static bool Run(string pathToLUT,string pathToDatabaseFile, string pathToReplayFile, string pathToChecksumFile) {

      FPLut.Init(pathToLUT);

      FileLoader.Init(new DotNetFileLoader(Path.GetDirectoryName(pathToDatabaseFile)));

      SessionContainer container = new SessionContainer();
      container.databaseFile = JsonConvert.DeserializeObject<DatabaseFile>(File.ReadAllText(pathToDatabaseFile), ReplayJsonSerializerSettings.GetSettings());
      container.replayFile = JsonConvert.DeserializeObject<ReplayFile>(File.ReadAllText(pathToReplayFile), ReplayJsonSerializerSettings.GetSettings());
      container.Start();
      container.provider.ImportFromList(container.replayFile.InputHistory);

      var numberOfFrames = container.replayFile.Length;

      Dictionary<int, ChecksumFile.ChecksumEntry> checksums = null;
      if (pathToChecksumFile != null)
        checksums = JsonConvert.DeserializeObject<ChecksumFile>(File.ReadAllText(pathToChecksumFile), ReplayJsonSerializerSettings.GetSettings()).ToDictionary();

      while (container.session.FramePredicted == null || container.session.FramePredicted.Number < numberOfFrames) {
        Thread.Sleep(16);

        // Use smaller delta time to reduce the playback speed.
        container.Service();

        if (container.session.FrameVerified != null) {
          // Cecksums are not platform independent, yet. Will follow shortly.
          var f = container.session.FrameVerified.Number;
          var cs = ChecksumFileHelper.UlongToLong(container.session.FrameVerified.CalculateChecksum());

          Console.Write($"{f,6} {cs, 25} ");

          if (checksums != null) {
            
            if (checksums.ContainsKey(f)) {
              if (cs != checksums[f].ChecksumAsLong) {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("(failed)");
                Console.ReadKey();
              } else {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("(verified)");
              }
            } else {
              Console.Write("(skipped)");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
          }

          //if (container.session.FrameVerified.Number == 100) File.WriteAllText("frame.txt", (container.session.FrameVerified as Frame).DumpFrame());

          Console.Write("\n");
        }

        if (Console.KeyAvailable) {
          if (Console.ReadKey().Key == ConsoleKey.Escape) {
            Console.WriteLine("Stopping replay");
            return false;
          }
        }
      }

      Console.WriteLine($"Ending replay at frame {container.session.FramePredicted.Number}");

      container.Destroy();

      return true;
    }
  }
}
