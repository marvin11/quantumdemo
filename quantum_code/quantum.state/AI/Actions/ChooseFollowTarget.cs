using System;
using Photon.Deterministic;
using Quantum;

[Serializable]
public class ChooseFollowTarget : AIAction
{
	public AIBlackboardValueKey FollowTargetKey = new AIBlackboardValueKey {Key = "PickupCount"};
	public FP FollowPlayerChance;

	public override unsafe void Update(Frame f, Entity* e)
	{
		var randomValue = f.RNG->Next(FP._0, FP._1);
		var nm = f.Map.GetNavMesh("NavMesh");
		var pos = FPVector2.Zero;
		var bb = Entity.GetBlackboardComponent(e);


		if (randomValue >= FollowPlayerChance)
		{
			pos = new FPVector2(f.RNG->Next(-5, 5), f.RNG->Next(-5, 5));
			bb->Board.Set(FollowTargetKey.Key, EntityRef.None, f, bb);
		} 
		else
		{
			var players = f.GetAllPlayers();
			while (players.Next())
			{
				var p = players.Current;

				var flags = f.GetPlayerInputFlags(p->Fields.Owner);
				if ((flags & DeterministicInputFlags.PlayerNotPresent) == 0)
				{
					pos = p->Transform3D.Position.XZ;

					bb->Board.Set(FollowTargetKey.Key, p->EntityRef, f, bb);
				}
			}
		}


		var agent = Entity.GetNavMeshAgent(e);
		if (agent != null) agent->SetTarget(pos, nm, true);
	}
}