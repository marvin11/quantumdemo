using System;
using Photon.Deterministic;
using Quantum;

[Serializable]
public class DecreaseBlackboardByDeltaTime : AIAction
{
	public AIBlackboardValueKey Key;

	public override unsafe void Update(Frame f, Entity* e)
	{
		var bb = Entity.GetBlackboardComponent(e);

		AIBlackboardValue time = bb->Board.GetValue(Key.Key);
		FP timer = time.GetFP(f, bb);

		timer -= f.DeltaTime;

		bb->Board.Set(Key.Key, timer, f, bb);
	}
}