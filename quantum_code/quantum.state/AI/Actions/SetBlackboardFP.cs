using System;
using Photon.Deterministic;
using Quantum;

[Serializable]
public unsafe class SetBlackboardFP : AIAction
{
	public AIBlackboardValueKey Key = new AIBlackboardValueKey { Key = "PickupCount" };
	public FP Value = 0;

	public override unsafe void Update(Frame f, Entity* e)
	{
		var bb = Entity.GetBlackboardComponent(e);

		AIBlackboardValue pickups = bb->Board.GetValue(Key.Key);
		bb->Board.Set(Key.Key, Value, f, bb);
	}
}