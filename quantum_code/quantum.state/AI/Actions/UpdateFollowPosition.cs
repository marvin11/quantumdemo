using System;
using Photon.Deterministic;
using Quantum;

[Serializable]
public class UpdateFollowPosition : AIAction
{
	public AIBlackboardValueKey FollowTargetKey = new AIBlackboardValueKey { Key = "PickupCount" };
	public AIBlackboardValueKey TimerKey = new AIBlackboardValueKey { Key = "PickupCount" };

	public FP UpdateInterval;
	
	public override unsafe void Update(Frame f, Entity* e)
	{
		var nm = f.Map.GetNavMesh("NavMesh");
		var bb = Entity.GetBlackboardComponent(e);
		var followTarget = bb->Board.GetValue(FollowTargetKey.Key).GetEntityRef(f, bb); 
		
		if ( followTarget == EntityRef.None)
			return;

		var timer = bb->Board.GetValue(TimerKey.Key).GetFP(f, bb);
		timer += f.DeltaTime;

		if (timer >= UpdateInterval)
		{
			var t = Entity.GetTransform3D(f.GetEntity(followTarget));
			if (t == null)
				return;
			
			var agent = Entity.GetNavMeshAgent(e);
			if (agent != null)
			{
				agent->SetTarget(t->Position.XZ, nm, true);
			}	
			timer = 0;
		}

		bb->Board.Set(TimerKey.Key, timer, f, bb);
	}
}