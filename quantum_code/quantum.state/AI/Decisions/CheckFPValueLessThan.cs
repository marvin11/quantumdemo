using System;
using Photon.Deterministic;
using Quantum;

[Serializable]
public class CheckFPValueLessThan : HFSMDecision
{
	public AIBlackboardValueKey Key;
	public FP Target;

	public override unsafe bool Decide(Frame f, HFSMData* fsm, Entity* e)
	{
		var bb = Entity.GetBlackboardComponent(e);

		var time = bb->Board.GetValue(Key.Key);

		return time.GetFP(f, bb) <= Target;
	}
}