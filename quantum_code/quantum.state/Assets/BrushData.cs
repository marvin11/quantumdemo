using Photon.Deterministic;

namespace Quantum
{
  public partial class BrushData
  {
    public FP Radius;
    public FP MaxPressureDistance;
    public FPVector3 Offset;
    public FPVector3 Direction;
    public FP RaycastMaxDistance;
    public int Layer;
  }
}