using Photon.Deterministic;
using Quantum.Core;

namespace Quantum
{
  public partial class DrawingBoardData
  {
    public string PrefabName;
    public FP ScaleFactor = 1;
    public FPVector3 Extents;
  }
}