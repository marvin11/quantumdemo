using Photon.Deterministic;
using Quantum.Core;

namespace Quantum
{
  public partial class InteractableObjectData
  {
    public string PrefabName;

    public PhysicsMaterialLink PhysicsMaterial;
    public DynamicShape3DType Shape;
    public FPVector3 Extents;

    public FP ScaleFactor = 1;
    public FP Mass;

    public BrushDataLink BrushData;
    public bool SnapToHand = false;
  }
}