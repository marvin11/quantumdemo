namespace Quantum
{
	public partial class MapInfoData
	{
		public enum TypeOfPlayer
		{
			Robot,
			Human
		}

		public TypeOfPlayer PlayerType;
		public bool HasCompanion;
	}
}