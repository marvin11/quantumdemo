﻿using System;
using System.Collections.Generic;
using Photon.Deterministic;

namespace Quantum {
  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public unsafe partial class GOAPTask : AssetObject {
    public string Label;
    public GOAPState Consequences;
    public GOAPState Conditions;

    public AIActionLink[] Actions;

    [NonSerialized]
    public AIAction[] _actions;

    public override void Loaded() {
      _actions = new AIAction[Actions.Length];
      for (Int32 i = 0; i < _actions.Length; i++) {
        _actions[i] = Actions[i];
      }
    }

    public void Update(Frame f, Entity* e) {
      for (Int32 i = 0; i < _actions.Length; i++) {
        _actions[i].Update(f, e);
      }
    }

    public static GOAPTask[] GetAllTasks() {
      List<GOAPTask> tasks = new List<GOAPTask>();
      tasks.AddRange(DB.FindAllAssets<GOAPTask>());
      return tasks.ToArray();
    }
  }


  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefGOAPTask {
    private Int32 _id;
    public GOAPTask Asset {
      get {
        return DB.FindAsset<GOAPTask>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        } else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefGOAPTask(GOAPTask value) {
      var r = default(AssetRefGOAPTask);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator GOAPTask(AssetRefGOAPTask value) {
      return DB.FindAsset<GOAPTask>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefGOAPTask* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      } else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
  [Quantum.AssetLinkAttribute(typeof(GOAPTask))]
  [System.SerializableAttribute()]
  public unsafe partial struct GOAPTaskLink {
    public String Guid;
    public GOAPTask Instance {
      get {
        return DB.FindAsset<GOAPTask>(Guid);
      }
    }
    public static implicit operator GOAPTask(GOAPTaskLink value) {
      return DB.FindAsset<GOAPTask>(value.Guid);
    }
    public static implicit operator GOAPTaskLink(String guid) {
      return new GOAPTaskLink { Guid = guid };
    }
    public static implicit operator GOAPTaskLink(GOAPTask asset) {
      return new GOAPTaskLink { Guid = asset != null ? asset.Guid : null };
    }
  }

  [System.SerializableAttribute()]
  public partial class GOAPRoot : AssetObject {
    public GOAPTaskLink[] Tasks;
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefGOAPRoot {
    private Int32 _id;
    public GOAPRoot Asset {
      get {
        return DB.FindAsset<GOAPRoot>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        } else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefGOAPRoot(GOAPRoot value) {
      var r = default(AssetRefGOAPRoot);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator GOAPRoot(AssetRefGOAPRoot value) {
      return DB.FindAsset<GOAPRoot>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefGOAPRoot* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      } else {
        ptr->_id = stream.ReadInt();
      }
    }
  }

  [Quantum.AssetLinkAttribute(typeof(GOAPRoot))]
  [System.SerializableAttribute()]
  public unsafe partial struct GOAPRootLink {
    public String Guid;
    public GOAPRoot Instance {
      get {
        return DB.FindAsset<GOAPRoot>(Guid);
      }
    }
    public static implicit operator GOAPRoot(GOAPRootLink value) {
      return DB.FindAsset<GOAPRoot>(value.Guid);
    }
    public static implicit operator GOAPRootLink(String guid) {
      return new GOAPRootLink { Guid = guid };
    }
    public static implicit operator GOAPRootLink(GOAPRoot asset) {
      return new GOAPRootLink { Guid = asset != null ? asset.Guid : null };
    }
  }
}
