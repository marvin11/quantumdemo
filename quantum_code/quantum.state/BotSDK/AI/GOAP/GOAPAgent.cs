﻿using System;
using Photon.Deterministic;

namespace Quantum
{
  [System.SerializableAttribute()]
  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct GOAPState
  {
    public Int64 Positive;
    public Int64 Negative;
    public override Int32 GetHashCode()
    {
      unchecked
      {
        var hash = 43;
        hash = hash * 31 + Positive.GetHashCode();
        hash = hash * 31 + Negative.GetHashCode();
        return hash;
      }
    }
    public static void Serialize(GOAPState* ptr, BitStream stream)
    {
      if (stream.Writing)
      {
        stream.WriteLong(ptr->Positive);
        stream.WriteLong(ptr->Negative);
      } else
      {
        ptr->Positive = stream.ReadLong();
        ptr->Negative = stream.ReadLong();
      }
    }
  }
  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct GOAPAgent
  {
    public Int64 CurrentState;
    public GOAPState Goal;
    private AssetRefGOAPTask Plan0;
    private AssetRefGOAPTask Plan1;
    private AssetRefGOAPTask Plan2;
    private AssetRefGOAPTask Plan3;
    public Int32 CurrentTaskIndex;
    public Int32 TaskCount;

    public void SetState(GOAPWorldState newState)
    {
      var state = (Int64)newState;
      if (!state.Equals(CurrentState))
      {
        CurrentTaskIndex = -1;
        CurrentState = state;
      }
    }

    public Int32 PlanSize
    {
      get
      {
        return 4;
      }
    }
    public void SetGoal(GOAPState goal)
    {
      if (!goal.Equals(Goal))
      {
        CurrentTaskIndex = -1;
        Goal = goal;
      }
    }
    public override Int32 GetHashCode()
    {
      unchecked
      {
        var hash = 47;
        hash = hash * 31 + CurrentState.GetHashCode();
        hash = hash * 31 + Goal.GetHashCode();
        hash = hash * 31 + Plan0.GetHashCode();
        hash = hash * 31 + Plan1.GetHashCode();
        hash = hash * 31 + Plan2.GetHashCode();
        hash = hash * 31 + Plan3.GetHashCode();
        hash = hash * 31 + CurrentTaskIndex.GetHashCode();
        hash = hash * 31 + TaskCount.GetHashCode();
        return hash;
      }
    }
    [Quantum.Core.PatchArrayAccessorAttribute("Plan0", "index", 4)]
    public AssetRefGOAPTask* Plan(Int32 index)
    {
#if DEBUG
      //if (index < 0 || index >= 4) { Quantum.Core.ILHelper.ArrayOutOfRange(); }
      fixed (AssetRefGOAPTask* p = &Plan0) { return p + index; }
#else
      return null;
#endif
    }
    public static void Serialize(GOAPAgent* ptr, BitStream stream)
    {
      if (stream.Writing)
      {
        stream.WriteLong(ptr->CurrentState);
        stream.WriteInt(ptr->CurrentTaskIndex);
        stream.WriteInt(ptr->TaskCount);
      } else
      {
        ptr->CurrentState = stream.ReadLong();
        ptr->CurrentTaskIndex = stream.ReadInt();
        ptr->TaskCount = stream.ReadInt();
      }
      Quantum.GOAPState.Serialize(&ptr->Goal, stream);
      { var arrayPtr = &ptr->Plan0; for (var i = 0; i < 4; ++i) { Quantum.AssetRefGOAPTask.Serialize(&arrayPtr[i], stream); } }
    }
  }
}
