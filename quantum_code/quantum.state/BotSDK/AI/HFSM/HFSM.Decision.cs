﻿using Photon.Deterministic;
using System;

namespace Quantum {
  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public abstract unsafe partial class HFSMDecision : AssetObject {
    public string Label;

    public abstract Boolean Decide(Frame f, HFSMData* fsm, Entity* e);
  }

  [Quantum.AssetLinkAttribute(typeof(HFSMDecision))]
  [System.SerializableAttribute()]
  public unsafe partial struct HFSMDecisionLink {
    public String Guid;
    public HFSMDecision Instance {
      get {
        return DB.FindAsset<HFSMDecision>(Guid);
      }
    }
    public static implicit operator HFSMDecision(HFSMDecisionLink value) {
      return DB.FindAsset<HFSMDecision>(value.Guid);
    }
    public static implicit operator HFSMDecisionLink(String guid) {
      return new HFSMDecisionLink { Guid = guid };
    }
    public static implicit operator HFSMDecisionLink(HFSMDecision asset) {
      return new HFSMDecisionLink { Guid = asset != null ? asset.Guid : null };
    }
  }
}
