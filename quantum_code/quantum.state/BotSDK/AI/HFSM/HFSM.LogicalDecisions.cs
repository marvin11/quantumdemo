﻿using System;

namespace Quantum
{
  public abstract class HFSMLogicalDecision : HFSMDecision
  {
    public HFSMDecisionLink[] Decisions;

    protected HFSMDecision[] _decisions;

    public override void Loaded()
    {
      base.Loaded();

      _decisions = new HFSMDecision[Decisions == null ? 0 : Decisions.Length];
      if (Decisions != null)
      {
        for (Int32 i = 0; i < Decisions.Length; i++)
        {
          _decisions[i] = Decisions[i];
        }
      }
    }
  }

  [Serializable]
  public class HFSMOrDecision : HFSMLogicalDecision
  {
    public override unsafe bool Decide(Frame frame, HFSMData* fsm, Entity* entity)
    {
      foreach (var decision in _decisions)
      {
        if (decision.Decide(frame, fsm, entity))
          return true;
      }
      return false;
    }
  }


  [Serializable]
  public class HFSMAndDecision : HFSMLogicalDecision
  {
    public override unsafe bool Decide(Frame frame, HFSMData* fsm, Entity* entity)
    {
      foreach (var decision in _decisions)
      {
        if (!decision.Decide(frame, fsm, entity))
          return false;
      }
      return true;
    }
  }


  [Serializable]
  public class HFSMNotDecision : HFSMLogicalDecision
  {
    public override unsafe bool Decide(Frame frame, HFSMData* fsm, Entity* entity)
    {
      return !_decisions[0].Decide(frame, fsm, entity);
    }
  }
}
