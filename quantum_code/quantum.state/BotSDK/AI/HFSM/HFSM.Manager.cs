﻿using Photon.Deterministic;
using System;

namespace Quantum {
  public static unsafe class HFSMManager {
    /// <summary>
    /// Initializes the HFSM, making the current state to be equals the initial state
    /// </summary>
    public static unsafe void Init(Frame f, HFSMData* hfsm, Entity* e, HFSMRoot root) {
      hfsm->Root = root;
      if (hfsm->Root != null)
        ChangeState(root.InitialState, f, hfsm, e);
    }

    /// <summary>
    /// Update the state of the HFSM.
    /// This can called in a low or high rate
    /// </summary>
    /// <param name="deltaTime">Usually the current deltaTime so the HFSM accumulates the time stood on the current state</param>
    public static void Update(Frame f, FP deltaTime, HFSMData* hfsmData, Entity* e) {
      hfsmData->CurrentState.UpdateState(f, deltaTime, hfsmData, e);
    }
    
    /// <summary>
    /// Triggers an event if the target HFSM listens to it
    /// </summary>
    public static unsafe void TriggerEvent(Frame f, HFSMData* hfsmData, Entity* e, string eventName) {
      Int32 eventInt = 0;
      if(hfsmData->Root.RegisteredEvents.TryGetValue(eventName, out eventInt)) {
        if (hfsmData->CurrentState != null)
        {
          hfsmData->CurrentState.Event(f, hfsmData, e, eventInt);
        }
      }
    }

    /// <summary>
    /// Executes the On Exit actions for the current state, then changes the current state
    /// </summary>
    internal static void ChangeState(HFSMState nextState, Frame f, HFSMData* hfsmData, Entity* e) {
      hfsmData->CurrentState?.ExitState(f, hfsmData, e);
      hfsmData->CurrentState = nextState;
      nextState.EnterState(f, hfsmData, e);
    }
  }
}
