﻿using System;
using System.Collections.Generic;
using Photon.Deterministic;

namespace Quantum
{
  [System.SerializableAttribute()]
  public partial class HFSMRoot : AssetObject
  {
    public string Label;

    public HFSMStateLink[] StatesLinks;

    public HFSMStateLink InitialState
    {
      get
      {
		if(StatesLinks != null)
        {
          return StatesLinks[0];
        }
        return "";
      }
    }

    public string[] EventsNames;

    [NonSerialized]
    public Dictionary<string, int> RegisteredEvents = new Dictionary<string, int>();

    public override void Loaded()
    {
      RegisteredEvents.Clear();
      for (int i = 0; i < EventsNames.Length; i++)
      {
        RegisteredEvents.Add(EventsNames[i], i + 1);
      }
    }

    public string GetEventName(int eventID)
    {
      foreach (var kvp in RegisteredEvents)
      {
        if (kvp.Value == eventID)
          return kvp.Key;
      }
      return "";
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefHFSMRoot
  {
    private Int32 _id;
    public HFSMRoot Asset
    {
      get
      {
        return DB.FindAsset<HFSMRoot>(this._id);
      }
      set
      {
        if (value == null)
        {
          _id = 0;
        } else
        {
          _id = value.Id;
        }
      }
    }
    public Int32 Id
    {
      get
      {
        return _id;
      }
    }
    public static implicit operator AssetRefHFSMRoot(HFSMRoot value)
    {
      var r = default(AssetRefHFSMRoot);
      if (value != null)
      {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator HFSMRoot(AssetRefHFSMRoot value)
    {
      return DB.FindAsset<HFSMRoot>(value._id);
    }
    public override Int32 GetHashCode()
    {
      return _id;
    }
    public static void Serialize(AssetRefHFSMRoot* ptr, BitStream stream)
    {
      if (stream.Writing)
      {
        stream.WriteInt(ptr->_id);
      } else
      {
        ptr->_id = stream.ReadInt();
      }
    }
  }

  [Quantum.AssetLinkAttribute(typeof(HFSMRoot))]
  [System.SerializableAttribute()]
  public unsafe partial struct HFSMRootLink
  {
    public String Guid;
    public HFSMRoot Instance
    {
      get
      {
        return DB.FindAsset<HFSMRoot>(Guid);
      }
    }
    public static implicit operator HFSMRoot(HFSMRootLink value)
    {
      return DB.FindAsset<HFSMRoot>(value.Guid);
    }
    public static implicit operator HFSMRootLink(String guid)
    {
      return new HFSMRootLink { Guid = guid };
    }
    public static implicit operator HFSMRootLink(HFSMRoot asset)
    {
      return new HFSMRootLink { Guid = asset != null ? asset.Guid : null };
    }
  }
}