﻿using Photon.Deterministic;
using System;
using System.Collections.Generic;

namespace Quantum {
  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public unsafe partial class HFSMState : AssetObject {
    public string Label;
    public AIActionLink[] OnUpdateLinks;
    public AIActionLink[] OnEnterLinks;
    public AIActionLink[] OnExitLinks;
    public HFSMTransition[] Transitions;
    public HFSMTransitionSetTransition[] TransitionSetTransitions;

    public HFSMStateLink[] ChildrenLinks;
    public HFSMStateLink ParentLink;
    
    [NonSerialized]
    public AIAction[] OnUpdate;
    [NonSerialized]
    public AIAction[] OnEnter;
    [NonSerialized]
    public AIAction[] OnExit;
    [NonSerialized]
    public HFSMState[] Children;
    [NonSerialized]
    public HFSMState Parent;
    
    public override void Loaded() {
      OnUpdate = new AIAction[OnUpdateLinks == null ? 0 : OnUpdateLinks.Length];
      if (OnUpdateLinks != null) {
        for (Int32 i = 0; i < OnUpdateLinks.Length; i++) {
          OnUpdate[i] = OnUpdateLinks[i];
        }
      }
      OnEnter = new AIAction[OnEnterLinks == null ? 0 : OnEnterLinks.Length];
      if (OnEnterLinks != null) {
        for (Int32 i = 0; i < OnEnterLinks.Length; i++) {
          OnEnter[i] = OnEnterLinks[i];
        }
      }
      OnExit = new AIAction[OnExitLinks == null ? 0 : OnExitLinks.Length];
      if (OnExitLinks != null) {
        for (Int32 i = 0; i < OnExitLinks.Length; i++) {
          OnExit[i] = OnExitLinks[i];
        }
      }

      Children = new HFSMState[ChildrenLinks == null ? 0 : ChildrenLinks.Length];
      if (ChildrenLinks != null) {
        for (Int32 i = 0; i < ChildrenLinks.Length; i++) {
          Children[i] = ChildrenLinks[i];
        }
      }

      Parent = ParentLink;
      if (Transitions != null) {
        for (int i = 0; i < Transitions.Length; i++) {
          Transitions[i].Setup();
        }
      }
      if (TransitionSetTransitions != null) {
        for (int i = 0; i < TransitionSetTransitions.Length; i++) {
          TransitionSetTransitions[i].Setup();
        }
      }
    }

    internal Boolean UpdateState(Frame f, FP deltaTime, HFSMData* fsm, Entity* e) {
      HFSMState p = Parent;
      Boolean transition = false;
      if (p != null) {
        transition = p.UpdateState(f, deltaTime, fsm, e);
      } else {
        fsm->Time += deltaTime;
      }

      if (transition) {
        return true;
      }

      DoUpdateActions(f, e);
      return CheckTransitions(f, fsm, e, 0);
    }

    internal Boolean Event(Frame f, HFSMData* fsm, Entity* e, Int32 eventInt) {
      HFSMState p = Parent;
      Boolean transition = false;
      if (p != null) {
        transition = p.Event(f, fsm, e, eventInt);
      }

      if (transition) {
        return true;
      }

      return CheckTransitions(f, fsm, e, eventInt);
    }

    private void DoUpdateActions(Frame f, Entity* e) {
      for (int i = 0; i < OnUpdate.Length; i++) {
        OnUpdate[i].Update(f, e);
        int nextAction = OnUpdate[i].NextAction(f, e);
        if (nextAction > i) {
          i = nextAction;
        }
      }
    }
    private void DoEnterActions(Frame f, Entity* e) {
      for (int i = 0; i < OnEnter.Length; i++) {
        OnEnter[i].Update(f, e);
        int nextAction = OnEnter[i].NextAction(f, e);
        if (nextAction > i) {
          i = nextAction;
        }
      }
    }
    private void DoExitActions(Frame f, Entity* e) {
      for (int i = 0; i < OnExit.Length; i++) {
        OnExit[i].Update(f, e);
        int nextAction = OnExit[i].NextAction(f, e);
        if (nextAction > i) {
          i = nextAction;
        }
      }
    }

    private Boolean CheckTransitions(Frame f, HFSMData* fsm, Entity* e, Int32 eventKey = 0) {
      if (Transitions != null) {
        for (int i = 0; i < Transitions.Length; i++) {
          if (Transitions[i].State == null) continue;

          if (Transitions[i].EventKey != eventKey) continue;

          if (Transitions[i].Decision != null && !Transitions[i].Decision.Decide(f, fsm, e)) continue;

          HFSMManager.ChangeState(Transitions[i].State, f, fsm, e);
          return true;
        }
      }

      if (RunTransitionSetTransitions(f, fsm, ref TransitionSetTransitions, e, eventKey)) {
        return true;
      }

      return false;
    }

    internal Boolean RunTransitionSetTransitions(Frame f, HFSMData* fsm, ref HFSMTransitionSetTransition[] transitionSetTransitions, Entity* e, Int32 eventKey = 0, int depth = 0) {
      // Just to avoid accidental loops
      if (depth == 10) return false;

      if (transitionSetTransitions != null) {
        for (int j = 0; j < transitionSetTransitions.Length; ++j) {
          HFSMTransitionSetTransition setTransition = transitionSetTransitions[j];

          if (setTransition.TransitionSet == null) continue;

          if (setTransition.EventKey != eventKey) continue;

          if (setTransition.Decision != null && !setTransition.Decision.Decide(f, fsm, e)) continue;

          var set = transitionSetTransitions[j].TransitionSet;
          if (set.Prerequisite != null && !set.Prerequisite.Decide(f, fsm, e)) continue;

          if (set.Transitions != null) {
            for (int i = 0; i < set.Transitions.Length; i++) {
              if (set.Transitions[i].State == null) continue;

              if (set.Transitions[i].EventKey != eventKey) continue;

              if (set.Transitions[i].Decision != null && !set.Transitions[i].Decision.Decide(f, fsm, e)) continue;

              HFSMManager.ChangeState(set.Transitions[i].State, f, fsm, e);
              return true;
            }
          }

          if (RunTransitionSetTransitions(f, fsm, ref set.TransitionSetTransitions, e, eventKey, depth + 1)) {
            return true;
          }
        }
      }

      return false;
    }

    internal void EnterState(Frame f, HFSMData* fsm, Entity* e) {
      fsm->Time = FP._0;
      DoEnterActions(f, e);
      if (Children != null && Children.Length > 0) {
        HFSMState child = Children[0];
        HFSMManager.ChangeState(child, f, fsm, e);
      }
    }
    internal void ExitState(Frame f, HFSMData* fsm, Entity* e) {
      DoExitActions(f, e);
    }
  }

  [Quantum.AssetLinkAttribute(typeof(HFSMState))]
  [System.SerializableAttribute()]
  public unsafe partial struct HFSMStateLink {
    public String Guid;
    public HFSMState Instance {
      get {
        return DB.FindAsset<HFSMState>(Guid);
      }
    }
    public static implicit operator HFSMState(HFSMStateLink value) {
      return DB.FindAsset<HFSMState>(value.Guid);
    }
    public static implicit operator HFSMStateLink(String guid) {
      return new HFSMStateLink { Guid = guid };
    }
    public static implicit operator HFSMStateLink(HFSMState asset) {
      return new HFSMStateLink { Guid = asset != null ? asset.Guid : null };
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefHFSMState {
    private Int32 _id;
    public HFSMState Asset {
      get {
        return DB.FindAsset<HFSMState>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        } else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefHFSMState(HFSMState value) {
      var r = default(AssetRefHFSMState);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator HFSMState(AssetRefHFSMState value) {
      return DB.FindAsset<HFSMState>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefHFSMState* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      } else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
}
