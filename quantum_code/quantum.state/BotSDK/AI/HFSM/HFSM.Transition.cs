﻿using System;

namespace Quantum {
  [System.Serializable]
  public class HFSMTransition {
    public Int32 EventKey = 0;
    public HFSMDecisionLink DecisionLink;
    public HFSMStateLink StateLink;

    [NonSerialized]
    public HFSMDecision Decision;
    [NonSerialized]
    public HFSMState State;

    public void Setup() {
      Decision = DecisionLink;
      State = StateLink;
    }
  }
}
