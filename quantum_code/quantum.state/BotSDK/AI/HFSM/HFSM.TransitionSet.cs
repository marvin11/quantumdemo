﻿using Photon.Deterministic;
using System;

namespace Quantum {
  [System.Serializable]
  public class HFSMTransitionSetTransition {
    public string Label;

    public Int32 EventKey = 0;
    public HFSMDecisionLink DecisionLink;
    public HFSMTransitionSetLink TransitionSetLink;

    [NonSerialized]
    public HFSMDecision Decision;
    [NonSerialized]
    public HFSMTransitionSet TransitionSet;

    public void Setup() {
      Decision = DecisionLink;
      TransitionSet = TransitionSetLink;
    }
  }

  [System.Serializable]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public class HFSMTransitionSet : AssetObject {
    public string Label;
    public HFSMDecisionLink PrerequisiteLink;

    public HFSMTransition[] Transitions;
    public HFSMTransitionSetTransition[] TransitionSetTransitions;

    [NonSerialized]
    public HFSMDecision Prerequisite;

    public override void Loaded()
    {
      Prerequisite = PrerequisiteLink;

      for (int i = 0; i < Transitions.Length; i++)
      {
        Transitions[i].Setup();
      }

      for (int i = 0; i < TransitionSetTransitions.Length; i++)
      {
        TransitionSetTransitions[i].Setup();
      }
    }
  }
  
  [Quantum.AssetLinkAttribute(typeof(HFSMTransitionSet))]
  [System.SerializableAttribute()]
  public unsafe partial struct HFSMTransitionSetLink {
    public String Guid;
    public HFSMTransitionSet Instance {
      get {
        return DB.FindAsset<HFSMTransitionSet>(Guid);
      }
    }
    public static implicit operator HFSMTransitionSet(HFSMTransitionSetLink value) {
      return DB.FindAsset<HFSMTransitionSet>(value.Guid);
    }
    public static implicit operator HFSMTransitionSetLink(String guid) {
      return new HFSMTransitionSetLink { Guid = guid };
    }
    public static implicit operator HFSMTransitionSetLink(HFSMTransitionSet asset) {
      return new HFSMTransitionSetLink { Guid = asset?.Guid };
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefHFSMTransitionSet {
    private Int32 _id;
    public HFSMTransitionSet Asset {
      get {
        return DB.FindAsset<HFSMTransitionSet>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        } else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefHFSMTransitionSet(HFSMTransitionSet value) {
      var r = default(AssetRefHFSMTransitionSet);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator HFSMTransitionSet(AssetRefHFSMTransitionSet value) {
      return DB.FindAsset<HFSMTransitionSet>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefHFSMTransitionSet* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      } else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
}
