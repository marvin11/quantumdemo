﻿using System;
using Photon.Deterministic;

namespace Quantum {
  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct HFSMAgent {
    public HFSMData Data;
    public override Int32 GetHashCode() {
      unchecked {
        var hash = 47;
        hash = hash * 31 + Data.GetHashCode();
        return hash;
      }
    }

    public static void Serialize(HFSMAgent* ptr, BitStream stream) {
      Quantum.HFSMData.Serialize(&ptr->Data, stream);
    }
  }
}
