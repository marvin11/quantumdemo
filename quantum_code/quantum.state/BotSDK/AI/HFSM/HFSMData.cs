﻿using Photon.Deterministic;

namespace Quantum {
  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct HFSMData {
    private AssetRefHFSMRoot _HFSMRoot;
    private AssetRefHFSMState _CurrentState;
    public FP Time;

    public HFSMRoot Root {
      get {
        return _HFSMRoot.Asset;
      }
      set {
        _HFSMRoot.Asset = value;
      }
    }

    public HFSMState CurrentState {
      get {
        return _CurrentState.Asset;
      }
      set {
        _CurrentState.Asset = value;
      }
    }

    public override System.Int32 GetHashCode() {
      unchecked {
        var hash = 43;
        hash = hash * 31 + _HFSMRoot.GetHashCode();
        hash = hash * 31 + _CurrentState.GetHashCode();
        hash = hash * 31 + Time.GetHashCode();
        return hash;
      }
    }

    public static void Serialize(HFSMData* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteFP(ptr->Time);
      } else {
        ptr->Time = stream.ReadFP();
      }
      Quantum.AssetRefHFSMRoot.Serialize(&ptr->_HFSMRoot, stream);
      Quantum.AssetRefHFSMState.Serialize(&ptr->_CurrentState, stream);
    }
  }
}
