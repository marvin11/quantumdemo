﻿using Photon.Deterministic;
using System;

namespace Quantum {

  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = true, GenerateAssetResetMethod = false)]
  public unsafe partial class AIBlackboard : AssetObject {
  }
  [Quantum.AssetLinkAttribute(typeof(AIBlackboard))]
  [System.SerializableAttribute()]
  public unsafe partial struct AIBlackboardLink {
    public String Guid;
    public AIBlackboard Instance {
      get {
        return DB.FindAsset<AIBlackboard>(Guid);
      }
    }
    public static implicit operator AIBlackboard(AIBlackboardLink value) {
      return DB.FindAsset<AIBlackboard>(value.Guid);
    }
    public static implicit operator AIBlackboardLink(String guid) {
      return new AIBlackboardLink { Guid = guid };
    }
    public static implicit operator AIBlackboardLink(AIBlackboard asset) {
      return new AIBlackboardLink { Guid = asset != null ? asset.Guid : null };
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefAIBlackboard {
    private Int32 _id;
    public AIBlackboard Asset {
      get {
        return DB.FindAsset<AIBlackboard>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        }
        else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefAIBlackboard(AIBlackboard value) {
      var r = default(AssetRefAIBlackboard);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator AIBlackboard(AssetRefAIBlackboard value) {
      return DB.FindAsset<AIBlackboard>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefAIBlackboard* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      }
      else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
}