﻿namespace Quantum {
  public enum AIBlackboardCompareOperator {
    Equal,
    NotEqual,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual
  }
}
