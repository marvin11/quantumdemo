﻿using Photon.Deterministic;
using System;

namespace Quantum {

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AIBlackboardComponent {
    private AssetRefAIBlackboard _Board;
    public Int32 BytesSize;
    public Int32 GenericDataSize;
    public AIBlackboard Board {
      get {
        return _Board.Asset;
      }
      set {
        _Board.Asset = value;
      }
    }
    public override Int32 GetHashCode() {
      unchecked {
        var hash = 61;
        hash = hash * 31 + _Board.GetHashCode();
        hash = hash * 31 + BytesSize.GetHashCode();
        hash = hash * 31 + GenericDataSize.GetHashCode();
        return hash;
      }
    }
    public static void Serialize(AIBlackboardComponent* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->BytesSize);
        stream.WriteInt(ptr->GenericDataSize);
      }
      else {
        ptr->BytesSize = stream.ReadInt();
        ptr->GenericDataSize = stream.ReadInt();
      }
      Quantum.AssetRefAIBlackboard.Serialize(&ptr->_Board, stream);
    }

    [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
    public unsafe partial struct BlackboardFilter {
      public void* Entity;
      public AIBlackboardComponent* Blackboard;
    }
  }
}