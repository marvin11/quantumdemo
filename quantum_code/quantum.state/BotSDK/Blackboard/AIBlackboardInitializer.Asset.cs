﻿using Photon.Deterministic;
using System;

namespace Quantum {

  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = true, GenerateAssetResetMethod = false)]
  public unsafe partial class AIBlackboardInitializer : AssetObject {
  }
  [Quantum.AssetLinkAttribute(typeof( AIBlackboardInitializer ) )]
  [System.SerializableAttribute()]
  public unsafe partial struct AIBlackboardInitializerLink {
    public String Guid;
    public AIBlackboardInitializer Instance {
      get {
        return DB.FindAsset<AIBlackboardInitializer>(Guid);
      }
    }
    public static implicit operator AIBlackboardInitializer( AIBlackboardInitializerLink value ) {
      return DB.FindAsset<AIBlackboardInitializer>(value.Guid);
    }
    public static implicit operator AIBlackboardInitializerLink( String guid) {
      return new AIBlackboardInitializerLink { Guid = guid };
    }
    public static implicit operator AIBlackboardInitializerLink( AIBlackboardInitializer asset ) {
      return new AIBlackboardInitializerLink { Guid = asset != null ? asset.Guid : null };
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefAIBlackboardInitializer {
    private Int32 _id;
    public AIBlackboardInitializer Asset {
      get {
        return DB.FindAsset<AIBlackboardInitializer>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        }
        else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefAIBlackboardInitializer( AIBlackboardInitializer value ) {
      var r = default(AssetRefAIBlackboardInitializer);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator AIBlackboardInitializer( AssetRefAIBlackboardInitializer value) {
      return DB.FindAsset<AIBlackboardInitializer>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefAIBlackboardInitializer* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      }
      else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
}