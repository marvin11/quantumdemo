﻿using Photon.Deterministic;
using System;

namespace Quantum
{
  public unsafe partial class AIBlackboardInitializer
  {
    [Serializable]
    public struct AIBlackboardInitialValue
    {
      public Boolean asBoolean;
      public Byte asByte;
      public Int32 asInteger;
      public FP asFP;
      public FPVector2 asFPVector2;
      public FPVector3 asFPVector3;
      public EntityRef asEntityRef;
    }

    [Serializable]
    public struct AIBlackboardInitialValueEntry
    {
      public string Key;
      public AIBlackboardInitialValue Value;
    }

    public bool ReportMissingEntries = true;

    public AIBlackboardLink AIBlackboard;
    public AIBlackboardInitialValueEntry[] InitialValues;


    public unsafe static void InitializeBlackboard( Frame f, AIBlackboardComponent* bb, AIBlackboardInitializer bbInitialState, AIBlackboardInitialValueEntry[] bbOverrides = null )
    {
      AIBlackboard board = bbInitialState.AIBlackboard.Instance;
      bb->Board = board;

      ApplyEntries( f, bb, bbInitialState, bbInitialState.InitialValues );
      ApplyEntries( f, bb, bbInitialState, bbOverrides );
    }

    public unsafe static void ApplyEntries( Frame f, AIBlackboardComponent* bb, AIBlackboardInitializer bbInitialState, AIBlackboardInitialValueEntry[] values )
    {
      if ( values == null ) return;

      for ( int i = 0; i < values.Length; i++ )
      {
        string key = values[ i ].Key;
        if ( bb->Board.HasEntry( key ) == false )
        {
          if ( bbInitialState.ReportMissingEntries )
          {
            Quantum.Log.Warn( $"Blackboard {bb->Board} does not have an entry with a key called '{key}'" );
          }
          continue;
        }

        AIBlackboardEntry entry = bb->Board.GetEntry( key );
        switch ( entry.Type )
        {
          case AIBlackboardValueType.Boolean:
            bb->Board.Set( key, values[ i ].Value.asBoolean, f, bb );
            break;
          case AIBlackboardValueType.Byte:
            bb->Board.Set( key, values[ i ].Value.asByte, f, bb );
            break;
          case AIBlackboardValueType.EntityRef:
            bb->Board.Set( key, values[ i ].Value.asEntityRef, f, bb );
            break;
          case AIBlackboardValueType.FP:
            bb->Board.Set( key, values[ i ].Value.asFP, f, bb );
            break;
          case AIBlackboardValueType.Integer:
            bb->Board.Set( key, values[ i ].Value.asInteger, f, bb );
            break;
          case AIBlackboardValueType.Vector2:
            bb->Board.Set( key, values[ i ].Value.asFPVector2, f, bb );
            break;
          case AIBlackboardValueType.Vector3:
            bb->Board.Set( key, values[ i ].Value.asFPVector3, f, bb );
            break;
        }
      }
    }
  }
}
