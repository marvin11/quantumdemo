﻿using System;

namespace Quantum{
  public struct AICustomFrameDataSize {
    public Int32 Bytes;
    public Int32 GenericData;

    public AICustomFrameDataSize(Int32 bytes, Int32 genericData) {
      Bytes = bytes;
      GenericData = genericData;
    }

    public AICustomFrameDataSize(AICustomFrameDataSize other) {
      Bytes = other.Bytes;
      GenericData = other.GenericData;
    }

    public void UpdateMax(AICustomFrameDataSize otherSize) {
      Bytes = Math.Max(Bytes, otherSize.Bytes);
      GenericData = Math.Max(GenericData, otherSize.GenericData);
    }

    public void Add(AICustomFrameDataSize otherSize) {
      Bytes += otherSize.Bytes;
      GenericData += otherSize.GenericData;
    }

    public Int32 IncrementBytes() {
      var currentBytes = Bytes;
      Bytes++;
      return currentBytes;
    }

    public Int32 AddBytes(Int32 size) {
      var currentOffset = Bytes;
      Bytes += size;
      return currentOffset;
    }

    public Int32 AddGenericData(Int32 size)
    {
      var currentOffset = RoundUpToAlignment(GenericData, GetAlignment(size));
      GenericData = currentOffset + size;
      return currentOffset;
    }

    static int GetAlignment(int value)
    {
      if (value == 1)
      {
        return 1;
      }
      if (value == 2)
      {
        return 2;
      }
      if (value % 8 == 0) {
        return 8;
      }
      if ((value % 4) == 0)
      {
        return 4;
      }
      return 8;
    }

    public override string ToString() {
      return string.Format("Byte: {0} GenericData: {1}", Bytes, GenericData);
    }

    private static Int32 RoundUpToAlignment(Int32 size, Int32 alignment) {
      switch (alignment) {
        case 1: return size;
        case 2: return ((size + 1) >> 1) * 4;
        case 4: return ((size + 3) >> 2) * 4;
        case 8: return ((size + 7) >> 3) * 8;
        case 16: return ((size + 15) >> 4) * 16;
        case 32: return ((size + 31) >> 5) * 32;
        case 64: return ((size + 63) >> 6) * 64;
      }

      throw new InvalidOperationException(String.Format("Invalid Alignment: {0}", alignment));
    }
  }
}
