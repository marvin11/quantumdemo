﻿using Photon.Deterministic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quantum {
  public unsafe partial class AIBlackboard {

    // The memory reserved for a blackboard has to match every possible one.
    // Doing it this way is just more convenient than going through Frame.GetAllBTBlackboards()
    public static AICustomFrameDataSize MaxDataSize;

    public AIBlackboardEntry[] Entries;

    // The memory required for all blackboard variables of this blackboard. Created once during Load().
    [NonSerialized] public AICustomFrameDataSize DataSize;

    // The value stores the calculated memory offset with it. With the dictionary everyone can
    // retrieve the blackboard variable by it's string key. Created once during Load().
    [NonSerialized] public Dictionary<String, AIBlackboardValue> Map;

    public override void Loaded() {

      Map = new Dictionary<string, AIBlackboardValue>();
      DataSize = new AICustomFrameDataSize();

      for (Int32 i = 0; i < Entries.Length; i++) {
        Map.Add(Entries[i].Key.Key, AIBlackboardValue.Create(Entries[i], ref DataSize, this));
      }

      //Log.Info("Blackboard '{0}' requires '{1}' of data", Guid, DataSize);

      MaxDataSize.UpdateMax(DataSize);
    }

    public bool HasValue( string key )
    {
      return Map.ContainsKey(key);
    }

    public AIBlackboardValue GetValue(string key) {
      return Map[key];
    }

    public bool HasEntry ( string key )
    {
      return Entries.Where(x=>x.Key.Key == key).Count() > 0;
    }

    public AIBlackboardEntry GetEntry( string key )
    {
      return Entries.Where( x => x.Key.Key == key ).FirstOrDefault();
    }

    public void Set(String key, Boolean value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      GetValue(key).Set(value, f, bb);
    }

    public void Set(String key, Byte value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      GetValue(key).Set(value, f, bb);
    }

    public void Set(String key, Int32 value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      GetValue(key).Set(value, f, bb);
    }

    public void Set(String key, FP value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      GetValue(key).Set(value, f, bb);
    }

    public void Set(String key, FPVector2 value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      GetValue(key).Set(value, f, bb);
    }

    public void Set( String key, FPVector3 value, IAIBlackboardMemory f, AIBlackboardComponent* bb )
    {
      GetValue( key ).Set( value, f, bb );
    }

    public void Set( String key, EntityRef value, IAIBlackboardMemory f, AIBlackboardComponent* bb )
    {
      GetValue( key ).Set( value, f, bb );
    }
  }
}
