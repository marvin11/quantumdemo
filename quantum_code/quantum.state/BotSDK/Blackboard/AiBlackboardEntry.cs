﻿using System;

namespace Quantum {
  // This struct is stored on the blackboard asset.
  [Serializable]
  public struct AIBlackboardEntry {
    public AIBlackboardValueType Type;
    public AIBlackboardValueKey Key;
  }
}
