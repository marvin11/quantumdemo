﻿using Photon.Deterministic;
using System;

namespace Quantum {
  public unsafe class AIBlackboardValue {
    private readonly Int32 _relativeOffset;
    private readonly AIBlackboardEntry _entry;
    private readonly AIBlackboard _parent;

    public String Key { get { return _entry.Key.Key; } }
    public AIBlackboardValueType Type { get { return _entry.Type; } }

    public AIBlackboardValue(Int32 relativeOffset, AIBlackboardEntry entry, AIBlackboard parent) {
      _relativeOffset = relativeOffset;
      _entry = entry;
      _parent = parent;
    }

    public static AIBlackboardValue Create(AIBlackboardEntry entry, ref AICustomFrameDataSize dataSize, AIBlackboard parent) {
      switch (entry.Type) {
        case AIBlackboardValueType.Boolean: { return new AIBlackboardValue(dataSize.IncrementBytes(), entry, parent); }
        case AIBlackboardValueType.Byte: { return new AIBlackboardValue(dataSize.IncrementBytes(), entry, parent); }
        case AIBlackboardValueType.Integer: { return new AIBlackboardValue(dataSize.AddGenericData(sizeof(Int32)), entry, parent); }
        case AIBlackboardValueType.FP: { return new AIBlackboardValue(dataSize.AddGenericData(sizeof(Int64)), entry, parent); }
        case AIBlackboardValueType.Vector2: { return new AIBlackboardValue(dataSize.AddGenericData(sizeof(FPVector2)), entry, parent); }
        case AIBlackboardValueType.Vector3: { return new AIBlackboardValue(dataSize.AddGenericData(sizeof(FPVector3)), entry, parent); }
        case AIBlackboardValueType.EntityRef: { return new AIBlackboardValue(dataSize.AddGenericData(sizeof(EntityRef)), entry, parent); }
      }

      return null;
    }

    public Boolean GetBoolean(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(f.GetBytes() + bb->BytesSize + _relativeOffset) != 0;
    }

    public Byte GetByte(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(f.GetBytes() + bb->BytesSize + _relativeOffset);
    }

    public Int32 GetInteger(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(Int32*)(f.GetData() + bb->GenericDataSize + _relativeOffset);
    }

    public FP GetFP(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return FP.FromRaw(*(Int64*)(f.GetData() + bb->GenericDataSize + _relativeOffset));
    }

    public FPVector2 GetVector2(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(FPVector2*)(f.GetData() + bb->GenericDataSize + _relativeOffset);
    }

    public FPVector3 GetVector3(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(FPVector3*)(f.GetData() + bb->GenericDataSize + _relativeOffset);
    }

    public EntityRef GetEntityRef(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      return *(EntityRef*)(f.GetData() + bb->GenericDataSize + _relativeOffset);
    }

    public void* GetVoidPtr(IAIBlackboardMemory f, AIBlackboardComponent* bb)
    {
      return (void*) (f.GetData() + bb->GenericDataSize + _relativeOffset);
    }

    public void Set(Boolean value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *(f.GetBytes() + bb->BytesSize + _relativeOffset) = (value ? (Byte)1 : (Byte)0);
    }

    public void Set(Byte value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *(f.GetBytes() + bb->BytesSize + _relativeOffset) = value;
    }

    public unsafe void Set(Int32 value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *((Int32*)(f.GetData() + bb->GenericDataSize + _relativeOffset)) = value;
    }

    public unsafe void Set(FP value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *((Int64*)(f.GetData() + bb->GenericDataSize + _relativeOffset)) = value.RawValue;
    }

    public unsafe void Set(FPVector2 value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *((FPVector2*)(f.GetData() + bb->GenericDataSize + _relativeOffset)) = value;
    }

    public unsafe void Set(FPVector3 value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *((FPVector3*)(f.GetData() + bb->GenericDataSize + _relativeOffset)) = value;
    }

    public unsafe void Set(EntityRef value, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      *((EntityRef*)(f.GetData() + bb->GenericDataSize + _relativeOffset)) = value;
    }

    public unsafe Boolean IsSet(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      // [Performance] inlining here?
      switch (Type) {
        case AIBlackboardValueType.Boolean: return GetBoolean(f, bb) == true;
        case AIBlackboardValueType.Byte: return GetByte(f, bb) == 0;
        case AIBlackboardValueType.Integer: return GetInteger(f, bb) != 0;
        case AIBlackboardValueType.FP: return GetFP(f, bb) != FP._0;
        case AIBlackboardValueType.Vector2: return GetVector2(f, bb) != FPVector2.Zero;
        case AIBlackboardValueType.Vector3: return GetVector3(f, bb) != FPVector3.Zero;
        case AIBlackboardValueType.EntityRef: return GetEntityRef(f, bb) != EntityRef.None;

      }
      return false;
    }

    public Boolean Compare(Byte otherValue, AIBlackboardCompareOperator op, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      if (Type != AIBlackboardValueType.Byte) {
        Log.Error("Comparing blackboard value of type {0} with byte", Type);
        return false;
      }
      switch (op) {
        case AIBlackboardCompareOperator.Equal: return GetByte(f, bb) == otherValue;
        case AIBlackboardCompareOperator.NotEqual: return GetByte(f, bb) != otherValue;
        case AIBlackboardCompareOperator.Greater: return GetByte(f, bb) > otherValue;
        case AIBlackboardCompareOperator.GreaterOrEqual: return GetByte(f, bb) >= otherValue;
        case AIBlackboardCompareOperator.Less: return GetByte(f, bb) < otherValue;
        case AIBlackboardCompareOperator.LessOrEqual: return GetByte(f, bb) <= otherValue;
      }

      return false;
    }

    public Boolean Compare(Int32 otherValue, AIBlackboardCompareOperator op, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      if (Type != AIBlackboardValueType.Integer) {
        Log.Error("Comparing blackboard value of type {0} with integer", Type);
        return false;
      }
      switch (op) {
        case AIBlackboardCompareOperator.Equal: return GetInteger(f, bb) == otherValue;
        case AIBlackboardCompareOperator.NotEqual: return GetInteger(f, bb) != otherValue;
        case AIBlackboardCompareOperator.Greater: return GetInteger(f, bb) > otherValue;
        case AIBlackboardCompareOperator.GreaterOrEqual: return GetInteger(f, bb) >= otherValue;
        case AIBlackboardCompareOperator.Less: return GetInteger(f, bb) < otherValue;
        case AIBlackboardCompareOperator.LessOrEqual: return GetInteger(f, bb) <= otherValue;
      }

      return false;
    }

    public unsafe Boolean Compare(FP otherValue, AIBlackboardCompareOperator op, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      if (Type != AIBlackboardValueType.FP) {
        Log.Error("Comparing blackboard value of type {0} with FP", Type);
        return false;
      }
      switch (op) {
        case AIBlackboardCompareOperator.Equal: return GetFP(f, bb).RawValue == otherValue.RawValue;
        case AIBlackboardCompareOperator.NotEqual: return GetFP(f, bb).RawValue != otherValue.RawValue;
        case AIBlackboardCompareOperator.Greater: return GetFP(f, bb).RawValue > otherValue.RawValue;
        case AIBlackboardCompareOperator.GreaterOrEqual: return GetFP(f, bb).RawValue >= otherValue.RawValue;
        case AIBlackboardCompareOperator.Less: return GetFP(f, bb).RawValue < otherValue.RawValue;
        case AIBlackboardCompareOperator.LessOrEqual: return GetFP(f, bb).RawValue <= otherValue.RawValue;
      }

      return false;
    }

    public Boolean Compare(AIBlackboardValue otherValue, AIBlackboardCompareOperator op, IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      if (Type != otherValue.Type) {
        Log.Warn("Comparing two blackboard values of different typ {0} and {1}", Type, otherValue.Type);
        return false;
      }

      switch (Type) {
        case AIBlackboardValueType.Boolean:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetBoolean(f, bb) == otherValue.GetBoolean(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetBoolean(f, bb) != otherValue.GetBoolean(f, bb);
          }
          break;

        case AIBlackboardValueType.Byte:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetByte(f, bb) == otherValue.GetByte(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetByte(f, bb) != otherValue.GetByte(f, bb);
          }
          break;

        case AIBlackboardValueType.Integer:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetInteger(f, bb) == otherValue.GetInteger(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetInteger(f, bb) != otherValue.GetInteger(f, bb);
            case AIBlackboardCompareOperator.Greater: return GetInteger(f, bb) > otherValue.GetInteger(f, bb);
            case AIBlackboardCompareOperator.GreaterOrEqual: return GetInteger(f, bb) >= otherValue.GetInteger(f, bb);
            case AIBlackboardCompareOperator.Less: return GetInteger(f, bb) < otherValue.GetInteger(f, bb);
            case AIBlackboardCompareOperator.LessOrEqual: return GetInteger(f, bb) <= otherValue.GetInteger(f, bb);
          }
          break;

        case AIBlackboardValueType.FP:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetFP(f, bb).RawValue == GetFP(f, bb).RawValue;
            case AIBlackboardCompareOperator.NotEqual: return GetFP(f, bb).RawValue != GetFP(f, bb).RawValue;
            case AIBlackboardCompareOperator.Greater: return GetFP(f, bb).RawValue > GetFP(f, bb).RawValue;
            case AIBlackboardCompareOperator.GreaterOrEqual: return GetFP(f, bb).RawValue >= GetFP(f, bb).RawValue;
            case AIBlackboardCompareOperator.Less: return GetFP(f, bb).RawValue < GetFP(f, bb).RawValue;
            case AIBlackboardCompareOperator.LessOrEqual: return GetFP(f, bb).RawValue <= GetFP(f, bb).RawValue;
          }
          break;

        case AIBlackboardValueType.Vector2:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetVector2(f, bb) == otherValue.GetVector2(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetVector2(f, bb) != otherValue.GetVector2(f, bb);
          }
          break;

        case AIBlackboardValueType.Vector3:
          switch (op) {
            case AIBlackboardCompareOperator.Equal: return GetVector3(f, bb) == otherValue.GetVector3(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetVector3(f, bb) != otherValue.GetVector3(f, bb);
          }
          break;

        case AIBlackboardValueType.EntityRef:
          switch (op)
          {
            case AIBlackboardCompareOperator.Equal: return GetEntityRef(f, bb) == otherValue.GetEntityRef(f, bb);
            case AIBlackboardCompareOperator.NotEqual: return GetEntityRef(f, bb) != otherValue.GetEntityRef(f, bb);
          }
          break;
      }

      return false;
    }

    public String ToString(IAIBlackboardMemory f, AIBlackboardComponent* bb) {
      switch (Type) {
        case AIBlackboardValueType.Boolean: return string.Format("'{0} ({1}): {2}'", Key, Type, GetBoolean(f, bb));
        case AIBlackboardValueType.Byte: return string.Format("'{0} ({1}): {2}'", Key, Type, GetByte(f, bb));
        case AIBlackboardValueType.Integer: return string.Format("'{0} ({1}): {2}'", Key, Type, GetInteger(f, bb));
        case AIBlackboardValueType.FP: return string.Format("'{0} ({1}): {2}'", Key, Type, GetFP(f, bb));
        case AIBlackboardValueType.Vector2: return string.Format("'{0} ({1}): {2}'", Key, Type, GetVector2(f, bb));
        case AIBlackboardValueType.Vector3: return string.Format("'{0} ({1}): {2}'", Key, Type, GetVector3(f, bb));
        case AIBlackboardValueType.EntityRef: return string.Format("'{0} ({1}): {2}'", Key, Type, GetEntityRef(f, bb));
      }

      return base.ToString();
    }
  }
}
