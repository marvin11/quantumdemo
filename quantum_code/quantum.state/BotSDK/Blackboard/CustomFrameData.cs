﻿using System;

namespace Quantum {
  public unsafe struct CustomFrameData {

    public Int32 DataSizeTotal { get { return BlackboardCount * DataSizePerBlackboard; } }
    public Int32 ByteSizeTotal { get { return BlackboardCount * ByteSizePerBlackboard; } }
    public Int32 MapSize { get { return (BlackboardCount + 7) >> 3; } }

    public Int32 BlackboardCount;
    public Int32 DataSizePerBlackboard;
    public Int32 ByteSizePerBlackboard;

    public Byte* Data;
    public Byte* Bytes;
    public Byte* DataMap;
    public Byte* ByteMap;
  }
}
