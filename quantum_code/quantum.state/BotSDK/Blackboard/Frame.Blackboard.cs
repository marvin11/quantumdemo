﻿﻿using System;
using System.Runtime.InteropServices;

namespace Quantum
{
  unsafe partial class Frame : IAIBlackboardMemory
  {
    CustomFrameData customFrameData;

    public CustomFrameData CustomFrameData
    {
      get
      {
        return customFrameData;
      }
    }

    public void CreateBlackboardMemory(AIBlackboardComponent* bb)
    {

      // Frame.Bytes                                                            Blackboard A
      // [Blackboard A - Byte 1] <-- offset = index * max bytes (BytesSize) <-- [Byte 1]
      // [Blackboard A - Byte 2]                                                [Byte 2]
      // [Blackboard B - Byte 1] <-- offset = index * max bytes
      // [Blackboard B - Byte 2]

      if (customFrameData.ByteSizePerBlackboard > 0)
      {
        Int32 offset = -1;

        for (Int32 i = 0; i < customFrameData.BlackboardCount; i++)
        {
          if ((customFrameData.ByteMap[i / 8] & (1 << (i % 8))) == 0u)
          {
            offset = i * customFrameData.ByteSizePerBlackboard;
            // Potentially ByteMap and DataMap can be combined as we reserve the maximum amount of memory for each bb anyway
            customFrameData.ByteMap[i / 8] |= (byte)(1 << (i % 8));
            break;
          }
        }

        if (offset >= 0)
        {
          bb->BytesSize = offset;
        } else
        {
          Log.Error("Failed to allocate memory for blackboard bytes.");
        }
      }

      if (customFrameData.DataSizePerBlackboard > 0)
      {
        Int32 offset = -1;

        for (Int32 i = 0; i < customFrameData.BlackboardCount; i++)
        {
          if ((customFrameData.DataMap[i / 8] & (1 << (i % 8))) == 0u)
          {
            offset = i * customFrameData.DataSizePerBlackboard;
            customFrameData.DataMap[i / 8] |= (byte)(1 << (i % 8));
            break;
          }
        }

        if (offset >= 0)
        {
          bb->GenericDataSize = offset;
        } else
        {
          Log.Error("Failed to allocate memory for blackboard data.");
        }
      }
    }

    public void DestroyBlackboardMemory(AIBlackboardComponent* bb)
    {
      if (customFrameData.ByteSizePerBlackboard > 0)
      {
        var index = bb->BytesSize / customFrameData.ByteSizePerBlackboard;
        if (index >= 0 && index < customFrameData.BlackboardCount)
        {
          customFrameData.ByteMap[index / 8] &= (byte)~(1 << (index % 8));
        }
      }

      if (customFrameData.DataSizePerBlackboard > 0)
      {
        var index = bb->GenericDataSize / customFrameData.DataSizePerBlackboard;
        if (index >= 0 && index < customFrameData.BlackboardCount)
        {
          customFrameData.DataMap[index / 8] &= (byte)~(1 << (index % 8));
        }
      }
    }

#if USE_BLACKBOARD_FRAME_METHODS
    public void InitBlackboard()
#else
    partial void InitUser()
#endif
    {
      // Save these values, theoretically they could change when loading different databases.
      customFrameData.ByteSizePerBlackboard = AIBlackboard.MaxDataSize.Bytes;
      customFrameData.DataSizePerBlackboard = AIBlackboard.MaxDataSize.GenericData;
      customFrameData.BlackboardCount = 0;

      // Count the total number of blackboards.
      var allEntities = GetAllEntitiesUnsafe();
      for (Int32 i = 0; i < allEntities.Length; ++i)
      {
        var e = allEntities[i];
        var bb = Entity.GetBlackboardComponent(e.Entity);
        if (bb != null)
        {
          customFrameData.BlackboardCount++;
        }
      }

      if (customFrameData.ByteSizeTotal > 0)
      {
        // Create memory chunck for all bytes in all blackboards
        customFrameData.Bytes = (Byte*)Native.Alloc(customFrameData.ByteSizeTotal);
        Native.Zero(customFrameData.Bytes, customFrameData.ByteSizeTotal);

        // Create a bitmap that remembers which byte set is in use
        customFrameData.ByteMap = (Byte*)Native.Alloc(customFrameData.MapSize);
        Native.Zero(customFrameData.ByteMap, customFrameData.MapSize);
      }

      if (customFrameData.DataSizeTotal > 0)
      {
        customFrameData.Data = (Byte*)Native.Alloc(customFrameData.DataSizeTotal);
        Native.Zero(customFrameData.Data, customFrameData.DataSizeTotal);

        customFrameData.DataMap = (Byte*)Native.Alloc(customFrameData.MapSize);
        Native.Zero(customFrameData.DataMap, customFrameData.MapSize);
      }
    }

#if USE_BLACKBOARD_FRAME_METHODS
    void FreeBlackboard()
#else
    partial void FreeUser()
#endif
    {
      if (customFrameData.Bytes != null)
        Native.Free(new IntPtr(customFrameData.Bytes));
      if (customFrameData.Data != null)
        Native.Free(new IntPtr(customFrameData.Data));
      if (customFrameData.ByteMap != null)
        Native.Free(new IntPtr(customFrameData.ByteMap));
      if (customFrameData.DataMap != null)
        Native.Free(new IntPtr(customFrameData.DataMap));
    }

#if USE_BLACKBOARD_FRAME_METHODS
    void AllocBlackboard()
#else
    partial void AllocUser()
#endif
    {
      customFrameData = new CustomFrameData();
    }

#if USE_BLACKBOARD_FRAME_METHODS
    void CopyFromUserBlackboard(Frame frame)
#else
    partial void CopyFromUser(Frame frame)
#endif
    {
      if (customFrameData.ByteSizeTotal < frame.customFrameData.ByteSizeTotal)
      {
        Native.Zero(customFrameData.Bytes, customFrameData.ByteSizeTotal);
        Native.Free(new IntPtr(customFrameData.Bytes));
        customFrameData.Bytes = (Byte*)Native.Alloc(frame.customFrameData.ByteSizeTotal);

        if (customFrameData.MapSize < frame.customFrameData.MapSize)
        {
          Native.Zero(customFrameData.ByteMap, customFrameData.MapSize);
          Native.Free(new IntPtr(customFrameData.ByteMap));
          customFrameData.ByteMap = (Byte*)Native.Alloc(frame.customFrameData.MapSize);
        }
      }

      if (customFrameData.DataSizeTotal < frame.customFrameData.DataSizeTotal)
      {
        Native.Zero(customFrameData.Data, customFrameData.DataSizeTotal);
        Native.Free(new IntPtr(customFrameData.Data));
        customFrameData.Data = (Byte*)Native.Alloc(frame.customFrameData.DataSizeTotal);

        if (customFrameData.MapSize < frame.customFrameData.MapSize)
        {
          Native.Zero(customFrameData.DataMap, customFrameData.MapSize);
          Native.Free(new IntPtr(customFrameData.DataMap));
          customFrameData.DataMap = (Byte*)Native.Alloc(frame.customFrameData.MapSize);
        }
      }

      customFrameData.BlackboardCount = frame.customFrameData.BlackboardCount;
      customFrameData.DataSizePerBlackboard = frame.customFrameData.DataSizePerBlackboard;
      customFrameData.ByteSizePerBlackboard = frame.customFrameData.ByteSizePerBlackboard;

      if (frame.customFrameData.Bytes != null && customFrameData.Bytes != null)
        Native.Copy(customFrameData.Bytes, frame.customFrameData.Bytes, (ulong)frame.customFrameData.ByteSizeTotal);
      if (frame.customFrameData.Data != null && customFrameData.Data != null)
        Native.Copy(customFrameData.Data, frame.customFrameData.Data, (ulong)frame.customFrameData.DataSizeTotal);
      if (frame.customFrameData.ByteMap != null && customFrameData.ByteMap != null)
        Native.Copy(customFrameData.ByteMap, frame.customFrameData.ByteMap, (ulong)frame.customFrameData.MapSize);
      if (frame.customFrameData.DataMap != null && customFrameData.DataMap != null)
        Native.Copy(customFrameData.DataMap, frame.customFrameData.DataMap, (ulong)frame.customFrameData.MapSize);
    }

    public byte* GetData()
    {
      return customFrameData.Data;
    }

    public byte* GetBytes()
    {
      return customFrameData.Bytes;
    }

#if USE_BLACKBOARD_FRAME_METHODS
    void SerializeBlackboard(ref byte[] data)
#else
    partial void SerializeUser(ref byte[] data)
#endif
    {
      var size = 12 + // BlackboardCount(4) + DataSizePerBlackboard(4) + ByteSizePerBlackboard(4)
        customFrameData.DataSizeTotal + customFrameData.ByteSizeTotal +
        customFrameData.MapSize + customFrameData.MapSize;

      data = new byte[size];

      var stream = new Photon.Deterministic.BitStream(data);
      stream.Writing = true;

      stream.WriteInt(customFrameData.BlackboardCount);
      stream.WriteInt(customFrameData.DataSizePerBlackboard);
      stream.WriteInt(customFrameData.ByteSizePerBlackboard);

      var bufferSize = Math.Max(customFrameData.DataSizeTotal, Math.Max(customFrameData.ByteSizeTotal, customFrameData.MapSize));
      byte[] buffer = new byte[bufferSize];

      if (customFrameData.DataSizeTotal != 0)
      {
        Array.Clear(buffer, 0, bufferSize);
        Marshal.Copy((IntPtr)customFrameData.Data, buffer, 0, customFrameData.DataSizeTotal);
        stream.WriteByteArray(buffer, customFrameData.DataSizeTotal);

        if (customFrameData.MapSize != 0)
        {
          Array.Clear(buffer, 0, bufferSize);
          Marshal.Copy((IntPtr)customFrameData.DataMap, buffer, 0, customFrameData.MapSize);
          stream.WriteByteArray(buffer, customFrameData.MapSize);
        }
      }

      if (customFrameData.ByteSizeTotal != 0)
      {
        Array.Clear(buffer, 0, bufferSize);
        Marshal.Copy((IntPtr)customFrameData.Bytes, buffer, 0, customFrameData.ByteSizeTotal);
        stream.WriteByteArray(buffer, customFrameData.ByteSizeTotal);

        if (customFrameData.MapSize != 0)
        {
          Array.Clear(buffer, 0, bufferSize);
          Marshal.Copy((IntPtr)customFrameData.ByteMap, buffer, 0, customFrameData.MapSize);
          stream.WriteByteArray(buffer, customFrameData.MapSize);
        }
      }
    }

#if USE_BLACKBOARD_FRAME_METHODS
    void DeserializeBlackboard(byte[] data)
#else
    partial void DeserializeUser(byte[] data)
#endif
    {
      var stream = new Photon.Deterministic.BitStream(data);

      customFrameData.BlackboardCount = stream.ReadInt();
      customFrameData.DataSizePerBlackboard = stream.ReadInt();
      customFrameData.ByteSizePerBlackboard = stream.ReadInt();

      var currentPosition = stream.Position / 8;

      if (customFrameData.DataSizeTotal > 0)
      {
        customFrameData.Data = (Byte*)Native.Alloc(customFrameData.DataSizeTotal);
        fixed (byte* b = &data[currentPosition])
        {
          Native.Copy(customFrameData.Data, b, (ulong)customFrameData.DataSizeTotal);
        }
        currentPosition += customFrameData.DataSizeTotal;

        if (customFrameData.MapSize > 0)
        {
          customFrameData.DataMap = (Byte*)Native.Alloc(customFrameData.MapSize);
          fixed (byte* b = &data[currentPosition])
          {
            Native.Copy(customFrameData.DataMap, b, (ulong)customFrameData.MapSize);
          }
          currentPosition += customFrameData.MapSize;
        }
      }

      if (customFrameData.ByteSizeTotal > 0)
      {
        customFrameData.Bytes = (Byte*)Native.Alloc(customFrameData.ByteSizeTotal);
        fixed (byte* b = &data[currentPosition])
        {
          Native.Copy(customFrameData.Bytes, b, (ulong)customFrameData.ByteSizeTotal);
        }
        currentPosition += customFrameData.ByteSizeTotal;

        if (customFrameData.MapSize > 0)
        {
          customFrameData.ByteMap = (Byte*)Native.Alloc(customFrameData.MapSize);
          fixed (byte* b = &data[currentPosition])
          {
            Native.Copy(customFrameData.ByteMap, b, (ulong)customFrameData.MapSize);
          }
          currentPosition += customFrameData.MapSize;
        }
      }
    }
  }
}
