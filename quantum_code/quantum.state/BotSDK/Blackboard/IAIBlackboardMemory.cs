﻿using System;

namespace Quantum {
  public unsafe interface IAIBlackboardMemory {
    Byte* GetData();
    Byte* GetBytes();
  }
}
