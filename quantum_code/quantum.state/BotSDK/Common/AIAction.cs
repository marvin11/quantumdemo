﻿using System;
using Photon.Deterministic;

namespace Quantum {

  [System.SerializableAttribute()]
  [AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public abstract unsafe partial class AIAction : AssetObject {
    public const int NEXT_ACTION_DEFAULT = -1;
    public string Label;

    public abstract void Update(Frame f, Entity* e);
    public virtual int NextAction(Frame f, Entity* e) { return NEXT_ACTION_DEFAULT; }
  }

  [Quantum.AssetLinkAttribute(typeof(AIAction))]
  [System.SerializableAttribute()]
  public unsafe partial struct AIActionLink {
    public String Guid;
    public AIAction Instance {
      get {
        return DB.FindAsset<AIAction>(Guid);
      }
    }
    public static implicit operator AIAction(AIActionLink value) {
      return DB.FindAsset<AIAction>(value.Guid);
    }
    public static implicit operator AIActionLink(String guid) {
      return new AIActionLink { Guid = guid };
    }
    public static implicit operator AIActionLink(AIAction asset) {
      return new AIActionLink { Guid = asset != null ? asset.Guid : null };
    }
  }

  [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = Quantum.Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct AssetRefAIAction {
    private Int32 _id;
    public AIAction Asset {
      get {
        return DB.FindAsset<AIAction>(this._id);
      }
      set {
        if (value == null) {
          _id = 0;
        } else {
          _id = value.Id;
        }
      }
    }
    public Int32 Id {
      get {
        return _id;
      }
    }
    public static implicit operator AssetRefAIAction(AIAction value) {
      var r = default(AssetRefAIAction);
      if (value != null) {
        r._id = value.Id;
      }
      return r;
    }
    public static implicit operator AIAction(AssetRefAIAction value) {
      return DB.FindAsset<AIAction>(value._id);
    }
    public override Int32 GetHashCode() {
      return _id;
    }
    public static void Serialize(AssetRefAIAction* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->_id);
      } else {
        ptr->_id = stream.ReadInt();
      }
    }
  }
}
