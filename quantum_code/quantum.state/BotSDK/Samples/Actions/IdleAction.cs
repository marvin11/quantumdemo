﻿using System;
using Photon.Deterministic;

namespace Quantum
{
  [Serializable]
  public class IdleAction : AIAction
  {
    public override unsafe void Update(Frame f, Entity* e)
    {
    }
  }
}
