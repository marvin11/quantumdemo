﻿using System;

namespace Quantum
{
  [Serializable]
  public unsafe class IncreaseBlackboardInt : AIAction
  {
    public AIBlackboardValueKey Key = new AIBlackboardValueKey { Key = "PickupCount" };
    public int AmountToIncrement = 1;

    public override unsafe void Update(Frame f, Entity* e)
    {
      var bb = Entity.GetBlackboardComponent(e);

      AIBlackboardValue pickups = bb->Board.GetValue(Key.Key);
      int pickupCount = pickups.GetInteger(f, bb);
      pickupCount += AmountToIncrement;

      bb->Board.Set(Key.Key, pickupCount, f, bb);
    }
  }
}