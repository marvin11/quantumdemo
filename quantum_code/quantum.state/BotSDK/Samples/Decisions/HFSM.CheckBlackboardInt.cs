﻿using System;
using Photon.Deterministic;

namespace Quantum
{
  [Serializable]
  public class CheckBlackboardInt : HFSMDecision
  {
    public AIBlackboardValueKey Key = new AIBlackboardValueKey { Key = "PickupCount" };
    public AIBlackboardValueKey DesiredValueKey = new AIBlackboardValueKey { Key = "" };

    public int DesiredValue = 1;
    public override unsafe bool Decide(Frame f, HFSMData* fsm, Entity* e)
    {
      var bb = Entity.GetBlackboardComponent(e);

      AIBlackboardValue pickups = bb->Board.GetValue(Key.Key);
      int pickupCount = pickups.GetInteger(f, bb);
      int desiredValue = DesiredValue;

      if (string.IsNullOrEmpty(DesiredValueKey.Key) == false)
      {
        desiredValue = bb->Board.GetValue(DesiredValueKey.Key).GetInteger(f, bb);
      }

      return pickupCount >= desiredValue;
    }
  }
}