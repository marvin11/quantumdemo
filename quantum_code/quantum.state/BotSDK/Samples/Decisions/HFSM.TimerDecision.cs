﻿using System;
using Photon.Deterministic;

namespace Quantum
{
  [Serializable]
  public class TimerDecision : HFSMDecision
  {
    public FP TimeToTrueState;
    public override unsafe bool Decide(Frame frame, HFSMData* fsm, Entity* entity)
    {
      return (fsm->Time.RawValue >= TimeToTrueState.RawValue) ? true : false;
    }
  }
}
