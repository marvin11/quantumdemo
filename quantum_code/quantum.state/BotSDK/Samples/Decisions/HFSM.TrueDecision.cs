﻿using System;
using Photon.Deterministic;

namespace Quantum
{
  [Serializable]
  public class TrueDecision : HFSMDecision
  {
    public override unsafe bool Decide(Frame frame, HFSMData* fsm, Entity* entity)
    {
      return true;
    }
  }
}
