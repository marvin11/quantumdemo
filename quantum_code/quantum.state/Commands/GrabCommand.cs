using Photon.Deterministic;

namespace Quantum.Commands
{
  public class GrabCommand : DeterministicCommand
  {
    public bool LeftHand;

    public override void Serialize(BitStream stream)
    {
      stream.Serialize(ref LeftHand);
    }
  }
}