using Photon.Deterministic;

namespace Quantum.Commands
{
	public class ReactionCommand : DeterministicCommand
  {
    public string ReactionId;

    public override void Serialize(BitStream stream)
    {
      stream.Serialize(ref ReactionId);
    }
  }
}