using Photon.Deterministic;

namespace Quantum.Commands
{
  public class ThrowCommand : DeterministicCommand
  {
    public bool LeftHand;
    public FPVector3 Velocity;
    
    public override void Serialize(BitStream stream)
    {
      stream.Serialize(ref LeftHand);
      stream.Serialize(ref Velocity);
    }
  }
}