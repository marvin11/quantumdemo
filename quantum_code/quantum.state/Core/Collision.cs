﻿using Photon.Deterministic;
using System;

namespace Quantum {
  public interface ISignalOnCollisionDynamic {
    Int32 RuntimeIndex { get; }
    void OnCollisionDynamic(Frame f, DynamicCollisionInfo info);
  }

  public interface ISignalOnCollisionStatic {
    Int32 RuntimeIndex { get; }
    void OnCollisionStatic(Frame f, StaticCollisionInfo info);
  }

  public interface ISignalOnTriggerDynamic {
    Int32 RuntimeIndex { get; }
    void OnTriggerDynamic(Frame f, DynamicCollisionInfo info);
  }

  public interface ISignalOnTriggerStatic {
    Int32 RuntimeIndex { get; }
    void OnTriggerStatic(Frame f, StaticCollisionInfo info);
  }

  public unsafe struct StaticCollisionInfo {
    public Entity* Entity;
    public StaticColliderData StaticData;
    public FPVector2 ContactPoint;
    public FPVector2 ContactNormal;
    public FPVector3 ContactPoint3D;
    public FPVector3 ContactNormal3D;
    public FP Penetration;

    public Boolean IgnoreCollision {
      get { return _m != null ? _m.Ignore : _m3D.Ignore; }
      set { if (_m != null) _m.Ignore = value; if (_m3D != null) _m3D.Ignore = value; }
    }

    Core.DynamicScene.Manifold _m;
    Core.DynamicScene3D.Manifold3D _m3D;

    public StaticCollisionInfo(Core.DynamicScene.Manifold m) {
      this = default(StaticCollisionInfo);
      _m = m;
    }

    public StaticCollisionInfo(Core.DynamicScene3D.Manifold3D m)
    {
      this = default(StaticCollisionInfo);
      _m3D = m;
    }
  }

  public unsafe struct DynamicCollisionInfo {
    public Entity* EntityA;
    public Entity* EntityB;
    public FPVector2 ContactPoint;
    public FPVector2 ContactNormal;
    public FPVector3 ContactPoint3D;
    public FPVector3 ContactNormal3D;
    public FP Penetration;

    public Boolean IgnoreCollision
    {
      get { return _m != null ? _m.Ignore : _m3D.Ignore; }
      set { if (_m != null) _m.Ignore = value; if (_m3D != null) _m3D.Ignore = value; }
    }

    Core.DynamicScene.Manifold _m;
    Core.DynamicScene3D.Manifold3D _m3D;

    public DynamicCollisionInfo(Core.DynamicScene.Manifold m) {
      this = default(DynamicCollisionInfo);
      _m = m;
    }

    public DynamicCollisionInfo(Core.DynamicScene3D.Manifold3D m)
    {
      this = default(DynamicCollisionInfo);
      _m3D = m;
    }
  }
}
