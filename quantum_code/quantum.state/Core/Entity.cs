﻿using Photon.Deterministic;
using System;
using System.Runtime.InteropServices;

namespace Quantum {

  public unsafe interface ISignalOnEntityCreated {
    Int32 RuntimeIndex { get; }
    void OnEntityCreated(Frame f, Entity* entity);
  }

  public unsafe interface ISignalOnEntityDestroy {
    Int32 RuntimeIndex { get; }
    void OnEntityDestroy(Frame f, Entity* entity);
  }

  [Flags]
  public enum EntityFlags : int {
    Active = 1 << 0,
    DestroyPending = 1 << 1,
    Culled = 1 << 2,
    NotCullable = 1 << 3
  }
  [StructLayout(LayoutKind.Sequential, Pack = Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe struct EntityCollectionHeader {
    public Int32 FreePtr;
    public Int32 UsedPtr;
    public Int32 Count;

    public static void Serialize(EntityCollectionHeader* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt(ptr->Count);
        stream.WriteInt(ptr->FreePtr);
        stream.WriteInt(ptr->UsedPtr);
      } else {
        ptr->Count = stream.ReadInt();
        ptr->FreePtr = stream.ReadInt();
        ptr->UsedPtr = stream.ReadInt();
      }
    }
  }

  [StructLayout(LayoutKind.Sequential, Pack = Core.CodeGenConstants.STRUCT_PACK)]
  public unsafe partial struct Entity {
    internal EntityFlags _flags;
    internal EntityRef _ref;
    internal Int32 _prev;
    internal Int32 _next;

    public EntityTypes Type {
      get { return _ref._type; }
    }

    public EntityRef EntityRef {
      get { return _ref; }
    }

    public EntityFlags Flags {
      get { return _flags; }
    }

    public static void Serialize(Entity* ptr, BitStream stream) {
      if (stream.Writing) {
        stream.WriteInt((Int32)ptr->_flags);
        EntityRef.Serialize(&ptr->_ref, stream);
        stream.WriteInt(ptr->_next);
        stream.WriteInt(ptr->_prev);
      } else {
        ptr->_flags = (EntityFlags)stream.ReadInt();
        EntityRef.Serialize(&ptr->_ref, stream);
        ptr->_next = stream.ReadInt();
        ptr->_prev = stream.ReadInt();
      }
    }
  }
}
