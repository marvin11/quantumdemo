﻿using System;

namespace Quantum {
  [Serializable]
  public struct EntityTypeSelection {
    EntityTypes? _cached;

    public String StringValue;

    public EntityTypes SelectedType {
      get {
        if (_cached.HasValue == false) {
          try {
            _cached = (EntityTypes)Enum.Parse(typeof(EntityTypes), StringValue);
          } catch {
            _cached = default(EntityTypes);
          }
        }

        return _cached.Value;
      }
    }

    public static implicit operator EntityTypes(EntityTypeSelection selection) {
      return selection.SelectedType;
    }
  }
}
