﻿using Photon.Deterministic;
using Quantum.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Quantum {
#if DEBUG
  [DebugBuild]
#endif
  public unsafe partial class Frame : Core.FrameBase, Core.IFrameInternal, INavMeshRegionMask {

    struct RuntimePlayerData {
      public Int32 ActorId;
      public Byte[] Data;
      public RuntimePlayer Player;
    }

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    _globals_* _globals;

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    _entities_* _entities;

    Int32 _simulationRate;

    // meta classes
    FrameEvents _frameEvents;
    FrameSignals _frameSignals;
    FrameFilters _frameFilters;

    // events
    EventBase _events;

    // animator
    AnimatorUpdater _animator;

    // navmesh
    NavMeshUpdater _navMeshUpdater;

    // configs
    RuntimeConfig _runtimeConfig;
    SimulationConfig _simulationConfig;
    DeterministicSessionConfig _sessionConfig;

    // systems
    SystemBase[] _systems;

    // 
    Queue<EntityRef> _destroy;

    readonly KCCUtils3D _kccUtils3D;
    readonly KCCUtils2D _kccUtils2D;

    // player data
    PersistentMap<Int32, RuntimePlayerData> _playerData;

    IEntityManifoldFilter _entityManifoldFilter;

    ISignalOnPlayerDataSet[] _ISignalOnPlayerDataSet;
    ISignalOnEntityCreated[] _ISignalOnEntityCreateSystems;
    ISignalOnEntityDestroy[] _ISignalOnEntityDestroySystems;
    ISignalOnCollisionStatic[] _ISignalOnCollisionStaticSystems;
    ISignalOnCollisionDynamic[] _ISignalOnCollisionDynamicSystems;

    ISignalOnTriggerStatic[] _ISignalOnTriggerStaticSystems;
    ISignalOnTriggerDynamic[] _ISignalOnTriggerDynamicSystems;
    ISignalOnNavMeshTargetReached[] _ISignalOnNavMeshTargetReachedSystems;
    ISignalOnNavMeshSearchFailed[] _ISignalOnNavMeshSearchFailedSystems;
    ISignalOnNavMeshUpdateSteering[] _ISignalOnNavMeshUpdateSteeringSystems;
    ISignalOnNavMeshUpdateAvoidance[] _ISignalOnNavMeshUpdateAvoidanceSystems;

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    public _globals_* Global {
      get {
        return _globals;
      }
    }

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    public _entities_* Entities {
      get {
        return _entities;
      }
    }

    [DebuggerBrowsable(DebuggerBrowsableState.Never)]
    public RNGSession* RNG {
      get {
        return &_globals->RngSession;
      }
    }

    public FP DeltaTime {
      get {
        return _globals->DeltaTime;
      }
      set {
        _globals->DeltaTime = value;
      }
    }

    public Int32 SimulationRate {
      get {
        return _simulationRate;
      }
    }

    public Int32 PlayerCount {
      get {
        return _sessionConfig.PlayerCount;
      }
    }

    public Map Map {
      get {
        return _globals->Map.Asset;
      }
      set {
        _globals->Map.Asset = value;
      }
    }

    public KCCUtils3D CharacterController3D
    {
      get
      {
        return _kccUtils3D;
      }
    }
    
    public KCCUtils2D CharacterController2D
    {
      get
      {
        return _kccUtils2D;
      }
    }

    public AnimatorUpdater AnimatorUpdater {
      get {
        return _animator;
      }
    }

    public NavMeshUpdater NavMeshUpdater {
      get {
        return _navMeshUpdater;
      }
    }

    public FrameSignals Signals {
      get {
        return _frameSignals;
      }
    }

    public FrameFilters Filters {
      get {
        return _frameFilters;
      }
    }

    public FrameEvents Events {
      get { return _frameEvents; }
    }

    public RuntimeConfig RuntimeConfig {
      get {
        return _runtimeConfig;
      }
    }

    public SimulationConfig SimulationConfig {
      get {
        return _simulationConfig;
      }
    }

    public DeterministicSessionConfig SessionConfig {
      get {
        return _sessionConfig;
      }
    }

    public SystemBase[] Systems {
      get { return _systems; }
    }

    public override IEntityManifoldFilter EntityManifoldFilter {
      get {
        return _entityManifoldFilter;
      }
    }

    public RuntimePlayer GetPlayerData(PlayerRef player) {
      RuntimePlayerData data;

      if (_playerData.TryFind(player, out data)) {
        return data.Player;
      }

      return null;
    }

    public Int32? PlayerToActorId(PlayerRef player) {
      RuntimePlayerData data;

      if (_playerData.TryFind(player, out data)) {
        return data.ActorId;
      }

      return null;
    }


    public PlayerRef? ActorIdToFirstPlayer(Int32 actorId) {
      foreach (var kvp in _playerData.Iterator()) {
        if (kvp.Value.ActorId == actorId) {
          return kvp.Key;
        }
      }

      return null;
    }

    public PlayerRef[] ActorIdToAllPlayers(Int32 actorId) {
      return _playerData.Iterator().Where(x => x.Value.ActorId == actorId).Select(x => (PlayerRef)x.Key).ToArray();
    }

    EventBase IFrameInternal.EventHead {
      get {
        return _events;
      }
    }

    public Frame(FrameContext context, SystemBase[] systems, DeterministicSessionConfig sessionConfig, RuntimeConfig runtimeConfig, SimulationConfig simulationConfig, FP deltaTime, Int32 simulationRate) : base(context) {
      if (context != null) {
        _systems = systems;
        _runtimeConfig = runtimeConfig;
        _simulationConfig = simulationConfig;
        _simulationRate = simulationRate;
        _sessionConfig = sessionConfig;

        _destroy = new Queue<EntityRef>(1024);
        _playerData = new PersistentMap<Int32, RuntimePlayerData>();

        _frameEvents = new FrameEvents(this);
        _frameSignals = new FrameSignals(this);
        _frameFilters = new FrameFilters(this);

        _kccUtils3D = new KCCUtils3D(this);
        _kccUtils2D = new KCCUtils2D(this);

        _entityManifoldFilter = _systems.FirstOrDefault(x => x is IEntityManifoldFilter) as IEntityManifoldFilter;

        // use dummy in case no system implements the filter
        if (_entityManifoldFilter == null) {
          _entityManifoldFilter = new DummyManifoldFilter();
        }
      }

      AllocGen();

      if (context != null) {
        InitGen();

        _ISignalOnPlayerDataSet = BuildSignalsArray<ISignalOnPlayerDataSet>();

        _ISignalOnEntityCreateSystems = BuildSignalsArray<ISignalOnEntityCreated>();
        _ISignalOnEntityDestroySystems = BuildSignalsArray<ISignalOnEntityDestroy>();

        _ISignalOnCollisionStaticSystems = BuildSignalsArray<ISignalOnCollisionStatic>();
        _ISignalOnCollisionDynamicSystems = BuildSignalsArray<ISignalOnCollisionDynamic>();

        _ISignalOnTriggerStaticSystems = BuildSignalsArray<ISignalOnTriggerStatic>();
        _ISignalOnTriggerDynamicSystems = BuildSignalsArray<ISignalOnTriggerDynamic>();

        _ISignalOnNavMeshTargetReachedSystems = BuildSignalsArray<ISignalOnNavMeshTargetReached>();
        _ISignalOnNavMeshSearchFailedSystems = BuildSignalsArray<ISignalOnNavMeshSearchFailed>();
        _ISignalOnNavMeshUpdateSteeringSystems = BuildSignalsArray<ISignalOnNavMeshUpdateSteering>();
        _ISignalOnNavMeshUpdateAvoidanceSystems = BuildSignalsArray<ISignalOnNavMeshUpdateAvoidance>();

        // assign map, rng session, etc.
        _globals->Map.Asset = runtimeConfig.Map.Instance;
        _globals->RngSession = new RNGSession(runtimeConfig.Seed);
        _globals->DeltaTime = deltaTime;

        // set default enabled systems
        for (Int32 i = 0; i < _systems.Length; ++i) {
          if (_systems[i].StartEnabled) {
            BitSet256.Set(&_globals->Systems, i);
          }
        }

        var allEntities = GetAllEntitiesUnsafe();

        // init physics 3D Entities
        //var physicsEntities3D = new List<DynamicScene3D.DynamicScene3DEntity>();

        //Int32 mailboxIndex = 0;
        //for (Int32 i = 0; i < allEntities.Length; ++i) {
        //  var e = allEntities[i];
        //  var d = Entity.GetDynamicBody3D(e.Entity);
        //  var t = Entity.GetTransform3D(e.Entity);

        //  if (d != null) {
        //    physicsEntities3D.Add(new DynamicScene3D.DynamicScene3DEntity {
        //      Entity = (void*)e.Entity,
        //      DynamicBody = d,
        //      Transform3D = t,
        //      MailboxIndex = mailboxIndex++
        //    });
        //  }
        //}

        // init physics
        _globals->PhysicsSettings.Gravity = simulationConfig.Physics.Gravity;
        _globals->PhysicsSettings.SolverIterations = simulationConfig.Physics.SolverIterations;
        _globals->PhysicsSettings.UseAngularVelocity = simulationConfig.Physics.UseAngularVelocity;
        _globals->PhysicsSettings.Substeps = simulationConfig.Physics.Substeps;
        _globals->PhysicsSettings.RaiseCollisionEventsForStatics = simulationConfig.Physics.RaiseCollisionEventsForStatics;
        _globals->PhysicsSettings.IsMultithreaded = simulationConfig.Physics.IsMultithreaded;
        _globals->PhysicsSettings.PenetrationAllowance = simulationConfig.Physics.PenetrationAllowance;
        _globals->PhysicsSettings.PenetrationCorrection = simulationConfig.Physics.PenetrationCorrection;
        _globals->PhysicsSettings.MinLinearIntegration = simulationConfig.Physics.MinLinearIntegration;
        _globals->PhysicsSettings.AllowSleeping = simulationConfig.Physics.AllowSleeping;
        _globals->PhysicsSettings.UseIslandsForSleepDetection = simulationConfig.Physics.UseIslandsForSleepDetection;
        _globals->PhysicsSettings.SleepTimeSec = simulationConfig.Physics.SleepTimeSec;
        _globals->PhysicsSettings.LinearSleepTolerance = simulationConfig.Physics.LinearSleepTolerance;
        _globals->PhysicsSettings.AngularSleepToleranceInRad = simulationConfig.Physics.AngularSleepToleranceInRad;
        _globals->PhysicsSettings.UseVerticalTransform = simulationConfig.Physics.UseVerticalTransform;

        // create scenes (2D and 3D)
        _scene = new DynamicScene(&_globals->PhysicsSettings, context);
        _scene3D = new DynamicScene3D(&_globals->PhysicsSettings, context);

        // init animator
        var animatorEntities = new List<AnimatorUpdater.AnimatorEntity>();

        for (Int32 i = 0; i < allEntities.Length; ++i) {
          var e = allEntities[i];
          var a = Entity.GetAnimator(e.Entity);
          if (a != null) {
            animatorEntities.Add(new AnimatorUpdater.AnimatorEntity {
              Entity = (void*)e.Entity,
              Animator = a,
              Transform = Entity.GetTransform2D(e.Entity),
              DynamicBody = Entity.GetDynamicBody(e.Entity)
            });
          }
        }

        _animator = new AnimatorUpdater(animatorEntities.ToArray());

        NavMeshUpdater.NavMeshUpdaterConfig navMeshConfig = new NavMeshUpdater.NavMeshUpdaterConfig() {
          AvoidanceRange = simulationConfig.NavMeshAgent.AvoidanceRange,
          UpdateInterval = simulationConfig.NavMeshAgent.UpdateInterval,
          UseLegacyAvoidance = simulationConfig.NavMeshAgent.UseLegacyAvoidance,
          IsMultithreaded = simulationConfig.NavMeshAgent.IsMultithreaded,
          EnableCallbacks = simulationConfig.NavMeshAgent.EnableCallbacks
        };

        _navMeshUpdater = new NavMeshUpdater(Map, navMeshConfig, context);
      }

      AllocUser();

      if (context != null) {
        InitUser();
      }
    }

    public void SetPredictionArea(FPVector3 position, FP radius) {
      Context.SetPredictionArea(position, radius);
    }

    public void SetPredictionArea(FPVector2 position, FP radius) {
      Context.SetPredictionArea(position.XOY, radius);
    }

    public Boolean InPredictionArea(FPVector3 position) {
      return Context.InPredictionArea(this, position);
    }

    public Boolean InPredictionArea(FPVector2 position) {
      return Context.InPredictionArea(this, position);
    }

    public override Byte[] Serialize() {
      Byte[] userdata = null;
      SerializeUser(ref userdata);

      if (userdata == null) {
        return ByteUtils.PackByteBlocks(
          SerializeRuntimePlayers(),
          SerializeGlobals(),
          SerializeEntities()
        );
      } else {
        return ByteUtils.PackByteBlocks(
          SerializeRuntimePlayers(),
          SerializeGlobals(),
          SerializeEntities(),
          userdata
        );
      }
    }

    public override void Deserialize(Byte[] data) {
      var blocks = ByteUtils.ReadByteBlocks(data).ToArray();
      DeserializeRuntimePlayers(blocks[0]);
      DeserializeGlobals(blocks[1]);
      DeserializeEntities(blocks[2]);

      if (blocks.Length == 4) {
        DeserializeUser(blocks[3]);
      }
    }

    Byte[] SerializeGlobals() {
      BitStream stream;
      stream = new BitStream(sizeof(_globals_) * 2);
      stream.Writing = true;

      _globals_.Serialize(_globals, stream);

      return stream.ToArray();
    }

    void DeserializeGlobals(Byte[] bytes) {
      BitStream stream;
      stream = new BitStream(bytes);
      stream.Reading = true;

      _globals_.Serialize(_globals, stream);
    }

    Byte[] SerializeEntities() {
      BitStream stream;
      stream = new BitStream(sizeof(_entities_) * 2);
      stream.Writing = true;

      _entities_.Serialize(_entities, stream);

      return stream.ToArray();
    }

    void DeserializeEntities(Byte[] bytes) {
      BitStream stream;
      stream = new BitStream(bytes);
      stream.Reading = true;

      _entities_.Serialize(_entities, stream);
    }

    Byte[] SerializeRuntimePlayers() {
      BitStream stream;
      stream = new BitStream(1024 * 10);
      stream.Writing = true;

      stream.WriteInt(_playerData.Count);

      foreach (var player in _playerData.Iterator()) {
        stream.WriteInt(player.Key);
        stream.WriteInt(player.Value.ActorId);
        stream.WriteByteArrayLengthPrefixed(player.Value.Data);
        player.Value.Player.Serialize(stream);
      }

      return stream.ToArray();
    }

    void DeserializeRuntimePlayers(Byte[] bytes) {
      BitStream stream;
      stream = new BitStream(bytes);
      stream.Reading = true;

      var count = stream.ReadInt();

      for (Int32 i = 0; i < count; ++i) {
        var player = stream.ReadInt();

        RuntimePlayerData data;
        data.ActorId = stream.ReadInt();
        data.Data = stream.ReadByteArrayLengthPrefixed();
        data.Player = new RuntimePlayer();
        data.Player.Serialize(stream);

        _playerData = _playerData.Add(player, data);
      }
    }

    public sealed override void Reset() {
      // release all events
      try {
        var head = _events;

        while (head != null) {
          // copy to tmp
          var tmp = head;

          // grab tail
          head = tmp._tail;

          // release event
          tmp.Release();
        }
      } catch (Exception exn) {
        Log.Exception(exn);

      } finally {
        // clear event head
        _events = null;
      }
    }

    public sealed override String DumpFrame() {
      String userDump = "";

      DumpFrameUser(ref userDump);

      StringBuilder sb = new StringBuilder();
      sb.AppendLine("RuntimeConfig");
      sb.AppendLine(RuntimeConfig.Dump());

      foreach (var kvp in _playerData.Iterator()) {
        sb.AppendLine("RuntimePlayer " + kvp.Key);

        if (kvp.Value.Player != null) {
          sb.AppendLine(kvp.Value.Player.Dump());
        } else {
          sb.AppendLine("NULL");
        }
      }

      return sb.ToString() + FrameDiffer.Dump("Globals", *_globals) + FrameDiffer.Dump("Entities", *_entities) + (userDump ?? "");
    }

    public sealed override UInt64 CalculateChecksum() {
      if (SessionConfig.ChecksumCrossPlatformDeterminism) {
        var data = Serialize();

        fixed (Byte* dataPtr = data) {
          return CRC64.Calculate(0, dataPtr, data.Length);
        }
      } else {
        var globals = CRC64.Calculate(0, (Byte*)_globals, sizeof(_globals_));
        var entities = CRC64.Calculate(0, (Byte*)_entities, sizeof(_entities_));

        unchecked {
          UInt64 hash = 17UL;
          hash = hash * 31UL + globals;
          hash = hash * 31UL + entities;
          return hash;
        }
      }
    }

    public sealed override void CopyFrom(DeterministicFrame frame) {
      var f = (Frame)frame;

      // copy player data
      _playerData = f._playerData;

      // perform native copy
      CopyFromGen(f);
      CopyFromUser(f);
    }

    public sealed override void Free() {
      FreeGen();
      FreeUser();
    }

    public Boolean SystemIsEnabled<T>() where T : SystemBase {
      var system = FindSystem<T>();
      if (system.Item0 == null) {
        return false;
      }

      return BitSet256.IsSet(&_globals->Systems, system.Item1);
    }

    public void SystemEnable<T>() where T : SystemBase {
      var system = FindSystem<T>();
      if (system.Item0 == null) {
        return;
      }

      if (BitSet256.IsSet(&_globals->Systems, system.Item1) == false) {
        // set flag
        BitSet256.Set(&_globals->Systems, system.Item1);

        try {
          system.Item0.OnEnabled(this);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void SystemDisable<T>() where T : SystemBase {
      var system = FindSystem<T>();
      if (system.Item0 == null) {
        return;
      }

      if (BitSet256.IsSet(&_globals->Systems, system.Item1)) {
        // clear flag
        BitSet256.Clear(&_globals->Systems, system.Item1);

        try {
          system.Item0.OnDisabled(this);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void SetCullable(EntityRef entityRef, Boolean cullable) {
      SetCullable(GetEntity(entityRef), cullable);
    }

    public void SetCullable(Entity* entity, Boolean cullable) {
      if (entity == null) {
        return;
      }

      if (cullable) {
        entity->_flags &= ~EntityFlags.NotCullable;
      } else {
        entity->_flags |= EntityFlags.NotCullable;
        entity->_flags &= ~EntityFlags.Culled;
      }
    }

    public void PreSimulatePrepare() {
      _destroy.Clear();

      // perform culling
      CullAllTransform2Ds();
      CullAllTransform3Ds();
    }

    public void PostSimulateCleanup() {
      while (_destroy.Count > 0) {
        DestroyEntityInternal(_destroy.Dequeue());
      }
    }

    public Boolean EntityExists(EntityRef entityRef) {
      return GetEntity(entityRef) != null;
    }

    Photon.Deterministic.Tuple<SystemBase, Int32> FindSystem<T>() {
      for (Int32 i = 0; i < _systems.Length; ++i) {
        if (_systems[i].GetType() == typeof(T)) {
          return Photon.Deterministic.Tuple.Create(_systems[i], i);
        }
      }

      Log.Error("System '{0}' not found, did you forget to add it to SystemSetup.CreateSystems ?", typeof(T).Name);
      return new Photon.Deterministic.Tuple<SystemBase, Int32>(null, -1);
    }

    T[] BuildSignalsArray<T>() {
      return _systems.Where(x => x is T).Cast<T>().ToArray();
    }

    void AddEvent(EventBase evnt) {
      // cons tail
      evnt._tail = _events;

      // overwrite head pointer
      _events = evnt;
    }

    // partial declarations populated from code generator
    partial void InitGen();
    partial void FreeGen();
    partial void AllocGen();
    partial void CopyFromGen(Frame frame);

    partial void InitUser();
    partial void FreeUser();
    partial void AllocUser();
    partial void CopyFromUser(Frame frame);

    partial void SerializeUser(ref Byte[] data);
    partial void DeserializeUser(Byte[] data);

    partial void DumpFrameUser(ref String dump);

    // entity create and destroy methods
    void EntityCreate(Entity* entity) {
      Assert.Check(entity->_flags == default(EntityFlags));
      entity->_ref._version += 1;
      entity->_flags = EntityFlags.Active;
    }

    void EntityDestroy(Entity* entity) {
      const EntityFlags DESTROY_FLAGS = EntityFlags.Active | EntityFlags.DestroyPending;

      Assert.Check((entity->_flags & DESTROY_FLAGS) == DESTROY_FLAGS);
      entity->_ref._version += 1;
      entity->_flags = default(EntityFlags);
    }

    void IFrameInternal.UpdatePlayerData() {
      UInt64 set = 0;

      for (Int32 i = 0; i < PlayerCount; ++i) {
        var rpc = GetRawRpc(i);
        if (rpc != null) {

          var flags = GetPlayerInputFlags(i);
          if ((flags & DeterministicInputFlags.Command) != DeterministicInputFlags.Command) {
            var playerDataOriginal = _playerData;
            
            try {
              // create player data
              RuntimePlayerData data;
              data.Data    = rpc;
              data.ActorId = BitConverter.ToInt32(rpc, rpc.Length - 4);
              data.Player  = Quantum.RuntimePlayer.FromByteArray(rpc);

              // set data
              _playerData = _playerData.AddOrSet(i, data);
              
              // set mask
              set |= 1UL << FPMath.Clamp(i, 0, 63);;
            } catch (Exception) {
              _playerData = playerDataOriginal;
            }
          }
        }
      }

      if (set != 0UL) {
        for (Int32 i = 0; i < PlayerCount; ++i) {
          var b = 1UL << i;
          if ((set & b) == b) {
            Signals.OnPlayerDataSet(i);
          }
        }
      }
    }

    public void SetNavMeshRegionActive(UInt64 flags, bool value) {
      if (flags == 0UL)
        return;

      if (value)
        _globals->NavMeshRegions &= ~flags;
      else
        _globals->NavMeshRegions |= flags;
    }

    public bool IsNavMeshRegionActive(UInt64 flags) {
      if (flags == 0UL)
        return true;

      return (_globals->NavMeshRegions & flags) == 0UL;
    }

    public void ClearAllNavMeshRegions() {
      _globals->NavMeshRegions = 0UL;
    }

    class DummyManifoldFilter : IEntityManifoldFilter {
      public Boolean Filter(FrameBase frame, DynamicScene.Manifold manifold) {
        return true;
      }
    }
  }
}
