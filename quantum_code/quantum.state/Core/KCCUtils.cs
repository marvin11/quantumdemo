﻿using System;
using Photon.Deterministic;

namespace Quantum.Core
{
  public unsafe class KCCUtils3D
  {

    internal KCCUtils3D(Frame f)
    {
      _frame = f;
    }

    Frame _frame;

    public FPVector3 Move(Entity* entity, FPVector3 velocity, IKCCCallbacks callback = null, int layermask = -1)
    {
      var t = Entity.GetTransform3D(entity);
      var kcc = Entity.GetCharacterController3D(entity);
      if (t == null || kcc == null)
        throw new InvalidOperationException("Entity must contain both Transform3D and CharacterController3D");
      return  CharacterController3D.Move(entity, t, kcc, velocity, _frame.Scene3D, _frame.DeltaTime, callback, layermask);
    }

    public CharacterController3DMovement ComputeRawMovement(Entity* entity, FPVector3 velocity, IKCCCallbacks callback = null, int layermask = -1)
    {
      var t = Entity.GetTransform3D(entity);
      var kcc = Entity.GetCharacterController3D(entity);
      if (t == null || kcc == null)
        throw new InvalidOperationException("Entity must contain both Transform3D and CharacterController3D");
      return CharacterController3D.ComputeRawMovement(entity, t, kcc, velocity, _frame.Scene3D, _frame.DeltaTime, callback, layermask);
    }
  }
  
  
  public unsafe class KCCUtils2D
  {

    internal KCCUtils2D(Frame f)
    {
      _frame = f;
    }

    Frame _frame;

    public FPVector2 Move(Entity* entity, FPVector2 velocity, IKCCCallbacks callback = null, int layermask = -1)
    {
      var t = Entity.GetTransform2D(entity);
      var kcc = Entity.GetCharacterController2D(entity);
      if (t == null || kcc == null)
        throw new InvalidOperationException("Entity must contain both Transform2D and CharacterController2D");
      return CharacterController2D.Move(entity, t, kcc, velocity, _frame.Scene, _frame.DeltaTime, callback, layermask);
    }

    public CharacterController2DMovement ComputeRawMovement(Entity* entity, FPVector2 velocity, IKCCCallbacks callback = null, int layermask = -1)
    {
      var t = Entity.GetTransform2D(entity);
      var kcc = Entity.GetCharacterController2D(entity);
      if (t == null || kcc == null)
        throw new InvalidOperationException("Entity must contain both Transform2D and CharacterController2D");
      return CharacterController2D.ComputeRawMovement(entity, t, kcc, velocity, _frame.Scene, _frame.DeltaTime, callback, layermask);
    }
  }
  
}
