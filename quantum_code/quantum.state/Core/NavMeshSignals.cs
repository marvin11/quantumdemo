﻿using System;

namespace Quantum
{
  public unsafe interface ISignalOnNavMeshTargetReached {
    Int32 RuntimeIndex { get; }
    void OnNavMeshTargetReached(Frame f, Core.NavMeshAgent* agent, Entity* entity);
  }

  public unsafe interface ISignalOnNavMeshSearchFailed {
    Int32 RuntimeIndex { get; }
    void OnNavMeshSearchFailed(Frame f, Core.NavMeshAgent* agent, Entity* entity);
  }

  public unsafe interface ISignalOnNavMeshUpdateSteering {
    Int32 RuntimeIndex { get; }
    void OnNavMeshUpdateSteering(Frame f, Core.NavMeshAgent* agent, Entity* entity, Core.NavMeshAgentSteeringData* data);
  }

  public unsafe interface ISignalOnNavMeshUpdateAvoidance
  {
    Int32 RuntimeIndex { get; }
    void OnNavMeshUpdateAvoidance(Frame f, Core.NavMeshAgent* agentA, Entity* entityA, Core.NavMeshAgent* agentB, Entity* entityB, Core.NavMeshAgentAvoidanceData* data);
  }
}
