﻿using System;
using Quantum.Core;

namespace Quantum {
  public unsafe partial class PhysicsSystemBase : SystemBase, Core.ICollisionCallbacks {
    public Frame _f;

    Frame.FrameFilters.DynamicBody2DIterator _2dIterator;
    Frame.FrameFilters.DynamicBody3DIterator _3dIterator;

    public override void Update(Frame f) {
      // hold temp data
      _f = f;
      _2dIterator = f.Filters.DynamicBody2D(true);
      _3dIterator = f.Filters.DynamicBody3D(true);

      // 2d physics
      Profiler.Start("Physics");
      f.Scene.Update(f, f.Map, f.DeltaTime, this);
      Profiler.End();

      // 3d physics
      Profiler.Start("Physics 3D");
      f.Scene3D.Update(f, f.Map, f.DeltaTime, this);
      Profiler.End();

      // clear temp data
      _3dIterator = default(Frame.FrameFilters.DynamicBody3DIterator);
      _2dIterator = default(Frame.FrameFilters.DynamicBody2DIterator);
      _f = null;
    }

    public Boolean Next(out void* entity, out Transform3D* transform, out DynamicBody3D* dynamicBody) {
      if (_3dIterator.Next()) {
        entity = _3dIterator.Current;
        transform = _3dIterator.Transform3D;
        dynamicBody = _3dIterator.DynamicBody3D;
        return true;
      }

      entity = null;
      transform = null;
      dynamicBody = null;
      return false;
    }

    public Boolean Next(out void* entity, out Transform2D* transform, out DynamicBody* dynamicBody, out Transform2DVertical* vertical) {
      if (_2dIterator.Next()) {
        entity = _2dIterator.Current;
        vertical = _2dIterator.Any.Transform2DVertical;
        transform = _2dIterator.Transform2D;
        dynamicBody = _2dIterator.DynamicBody;
        return true;
      }

      entity = null;
      vertical = null;
      transform = null;
      dynamicBody = null;
      return false;
    }

    public void OnCollision(DynamicScene.Manifold manifold) {
      var aIsNull = manifold.A.Entity == null;
      if (aIsNull || manifold.B.Entity == null) {
        StaticCollisionInfo info;
        info = new StaticCollisionInfo(manifold);
        info.Entity = (Entity*)(aIsNull ? manifold.B.Entity : manifold.A.Entity);
        info.StaticData = aIsNull ? manifold.A.StaticData : manifold.B.StaticData;
        info.ContactNormal = manifold.ContactNormal;
        info.ContactPoint = manifold.GetContactPoint(0);
        info.Penetration = manifold.Penetration;
        _f.Signals.OnCollisionStatic(info);
        var typesHash = (Int32)info.Entity->Type;
        OnCollisionInternal(info, typesHash);
      } else {
        DynamicCollisionInfo info = new DynamicCollisionInfo(manifold);
        info.EntityA = (Entity*)manifold.A.Entity;
        info.EntityB = (Entity*)manifold.B.Entity;
        info.ContactNormal = manifold.ContactNormal;
        info.ContactPoint = manifold.GetContactPoint(0);
        info.Penetration = manifold.Penetration;
        _f.Signals.OnCollisionDynamic(info);
        var typesHash = (Int32)info.EntityA->Type | ((Int32)info.EntityB->Type << 16);
        OnCollisionInternal(info, typesHash);
      }
    }

    partial void OnCollisionInternal(StaticCollisionInfo info, Int32 typesHash);
    partial void OnTriggerInternal(StaticCollisionInfo info, Int32 typesHash);
    partial void OnCollisionInternal(DynamicCollisionInfo info, Int32 typesHash);
    partial void OnTriggerInternal(DynamicCollisionInfo info, Int32 typesHash);

    public void OnTrigger(DynamicScene.Manifold manifold) {
      var aIsNull = manifold.A.Entity == null;
      if (aIsNull || manifold.B.Entity == null) {
        StaticCollisionInfo info;
        info = new StaticCollisionInfo(manifold);
        info.Entity = (Entity*)(aIsNull ? manifold.B.Entity : manifold.A.Entity);
        info.StaticData = aIsNull ? manifold.A.StaticData : manifold.B.StaticData;
        _f.Signals.OnTriggerStatic(info);
        var typesHash = (Int32)info.Entity->Type;
        OnTriggerInternal(info, typesHash);
      } else {
        DynamicCollisionInfo info;
        info = new DynamicCollisionInfo(manifold);
        info.EntityA = (Entity*)manifold.A.Entity;
        info.EntityB = (Entity*)manifold.B.Entity;
        _f.Signals.OnTriggerDynamic(info);
        var typesHash = (Int32)info.EntityA->Type | ((Int32)info.EntityB->Type << 16);
        OnTriggerInternal(info, typesHash);
      }
    }

    // TODO: fix this to use new Info and new InternalCallbacks?
    public void OnCollision3D(DynamicScene3D.Manifold3D manifold) {
      var aIsNull = manifold.A.Entity == null;
      if (aIsNull || manifold.B.Entity == null) {
        StaticCollisionInfo info;
        info = new StaticCollisionInfo(manifold);
        info.Entity = (Entity*)(aIsNull ? manifold.B.Entity : manifold.A.Entity);
        info.StaticData = aIsNull ? manifold.A.StaticData : manifold.B.StaticData;
        info.ContactNormal3D = manifold.ContactNormal;
        info.ContactPoint3D = manifold.contactPoints[0].point;
        info.Penetration = manifold.Penetration;
        _f.Signals.OnCollisionStatic(info);
        var typesHash = (Int32)info.Entity->Type;
        OnCollisionInternal(info, typesHash);
      } else {
        DynamicCollisionInfo info = new DynamicCollisionInfo(manifold);
        info.EntityA = (Entity*)manifold.A.Entity;
        info.EntityB = (Entity*)manifold.B.Entity;
        info.ContactNormal3D = manifold.ContactNormal;
        info.ContactPoint3D = manifold.contactPoints[0].point;
        info.Penetration = manifold.Penetration;
        _f.Signals.OnCollisionDynamic(info);
        var typesHash = (Int32)info.EntityA->Type | ((Int32)info.EntityB->Type << 16);
        OnCollisionInternal(info, typesHash);
      }
    }

    public void OnTrigger3D(DynamicScene3D.Manifold3D manifold) {
      var aIsNull = manifold.A.Entity == null;
      if (aIsNull || manifold.B.Entity == null) {
        StaticCollisionInfo info;
        info = new StaticCollisionInfo(manifold);
        info.Entity = (Entity*)(aIsNull ? manifold.B.Entity : manifold.A.Entity);
        info.StaticData = aIsNull ? manifold.A.StaticData : manifold.B.StaticData;
        _f.Signals.OnTriggerStatic(info);
        var typesHash = (Int32)info.Entity->Type;
        OnTriggerInternal(info, typesHash);
      } else {
        DynamicCollisionInfo info;
        info = new DynamicCollisionInfo(manifold);
        info.EntityA = (Entity*)manifold.A.Entity;
        info.EntityB = (Entity*)manifold.B.Entity;
        _f.Signals.OnTriggerDynamic(info);
        var typesHash = (Int32)info.EntityA->Type | ((Int32)info.EntityB->Type << 16);
        OnTriggerInternal(info, typesHash);
      }
    }
  }
}
