﻿public enum RecordingFlags {
  None = 0,
  Input = 1 << 0,
  Checksums = 1 << 1,
  Default = Input | Checksums,
  All = 0xFF
}