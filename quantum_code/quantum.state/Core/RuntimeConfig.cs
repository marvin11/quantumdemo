﻿using Photon.Deterministic;
using System;
using System.Text;

namespace Quantum {
  [Serializable]
  public partial class RuntimeConfig {
    public Int32 Seed;
    public MapLink Map;
    public SimulationConfigLink SimulationConfig;
    public RecordingFlags RecordingFlags;

    public void Serialize(BitStream stream) {
      stream.Serialize(ref Seed);
      stream.Serialize(ref Map.Guid);
      stream.Serialize(ref SimulationConfig.Guid);

      if (stream.Writing)
        stream.WriteInt((int)RecordingFlags);
      else
        RecordingFlags = (RecordingFlags)stream.ReadInt();

      SerializeUserData(stream);
    }

    public String Dump() {
      String dump = "";
      DumpUserData(ref dump);

      StringBuilder sb = new StringBuilder();
      sb.Append(dump);
      sb.Append("\n");
      sb.AppendLine("Seed: " + Seed);
      sb.AppendLine("Map.Guid: " + (Map.Guid ?? ""));
      sb.AppendLine("SimulationConfig.Guid: " + (SimulationConfig.Guid ?? ""));
      sb.AppendLine("RecordingFlags: " + RecordingFlags);

      return sb.ToString();
    }

    partial void DumpUserData(ref String dump);
    partial void SerializeUserData(BitStream stream);

    public static Byte[] ToByteArray(RuntimeConfig config) {
      BitStream stream;
      
      stream = new BitStream(new Byte[1024]);
      stream.Writing = true;

      config.Serialize(stream);

      return stream.ToArray();
    }

    public static RuntimeConfig FromByteArray(Byte[] data) {
      BitStream stream;
      stream = new BitStream(data);
      stream.Reading = true;

      RuntimeConfig config;
      config = new RuntimeConfig();
      config.Serialize(stream);

      return config;
    }
  }
}
