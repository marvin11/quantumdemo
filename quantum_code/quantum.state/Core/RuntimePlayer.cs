﻿using Photon.Deterministic;
using System;

namespace Quantum {

  public interface ISignalOnPlayerDataSet {
    Int32 RuntimeIndex { get; }
    void OnPlayerDataSet(Frame f, PlayerRef player);
  }

  [Serializable]
  public partial class RuntimePlayer {
    public void Serialize(BitStream stream) {
      SerializeUserData(stream);
    }

    public String Dump() {
      String dump = "";
      DumpUserData(ref dump);
      return dump ?? "";
    }

    partial void DumpUserData(ref String dump);
    partial void SerializeUserData(BitStream stream);

    public static Byte[] ToByteArray(RuntimePlayer player) {
      BitStream stream;

      stream = new BitStream(new Byte[8192]);
      stream.Writing = true;

      player.Serialize(stream);

      return stream.ToArray();
    }

    public static RuntimePlayer FromByteArray(Byte[] data) {
      BitStream stream;
      stream = new BitStream(data);
      stream.Reading = true;

      RuntimePlayer player;
      player = new RuntimePlayer();
      player.Serialize(stream);

      return player;
    }
  }
}
