﻿using Photon.Deterministic;
using System;

namespace Quantum {
  [Serializable, AssetObjectConfig(GenerateLinkingScripts = true, GenerateAssetCreateMenu = false, GenerateAssetResetMethod = false)]
  public partial class SimulationConfig : AssetObject {

    [Serializable]
    public class SimulationConfigPhysics {
      public FPVector3 Gravity = FPVector3.Zero;
      public Int32 SolverIterations = 4;
      public Int32 Substeps = 1;
      public Boolean UseAngularVelocity = true;
      public Boolean UseVerticalTransform = false;
      public Boolean RaiseCollisionEventsForStatics = false;
      public Boolean IsMultithreaded = false;
      public FP PenetrationAllowance = FP._0_01;
      public FP PenetrationCorrection = FP._0_50;
      public FP MinLinearIntegration = 0;
      public Boolean AllowSleeping = false;
      public Boolean UseIslandsForSleepDetection = false;
      public FP SleepTimeSec = FP._0_50;
      public FP LinearSleepTolerance = FP._0_02;
      public FP AngularSleepToleranceInRad = FP._2 * FP.Deg2Rad;
      public PhysicsMaterialLink DefaultPhysicsMaterial;
      public CharacterController3DConfigLink DefaultCharacterController3D;
      public CharacterController2DConfigLink DefaultCharacterController2D;
      public String[] Layers;
      public Int32[] LayerMatrix;
    }

    [Serializable]
    public class SimulationConfigNavMeshAgent
    {
      public FP AvoidanceRange = 8;
      public Int32 UpdateInterval = 1;
      public NavMeshAgentConfigLink DefaultNavMeshAgent;
      public Boolean IsMultithreaded = false;
      public Boolean EnableCallbacks = false;
      public Boolean UseLegacyAvoidance = true;
    }

    public SimulationConfigPhysics Physics;
    public SimulationConfigNavMeshAgent NavMeshAgent;
    public Boolean AutoLoadSceneFromMap = true;
    public SimulationUpdateTime DeltaTimeType = SimulationUpdateTime.Default;
    public Boolean UsePredictionArea = false;
  }

  [Serializable]
  public struct SimulationConfigLink {
    public String Guid;

    public SimulationConfig Instance {
      get {
        return DB.FindAsset<SimulationConfig>(Guid);
      }
    }

    public static implicit operator SimulationConfig(SimulationConfigLink value) {
      return DB.FindAsset<SimulationConfig>(value.Guid);
    }
  }
}
