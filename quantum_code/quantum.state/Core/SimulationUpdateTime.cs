﻿namespace Quantum {
  public enum SimulationUpdateTime {
    Default,
    EngineDeltaTime,
    EngineUnscaledDeltaTime
  }
}