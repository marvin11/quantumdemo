﻿using Photon.Deterministic;
using System;

namespace Quantum {
  partial class RuntimeConfig
  {
	  public FPVector3 HandExtents;

	  partial void SerializeUserData(BitStream stream)
	  {
		  stream.Serialize(ref HandExtents);
	  }
  }
}
