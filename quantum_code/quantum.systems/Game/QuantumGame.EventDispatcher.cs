﻿using Quantum;
using Quantum.Core;
using System;
using System.Collections.Generic;

public partial class QuantumGame
{

  public struct EventKey
  {
    int _tick;
    int _id;
    int _hash;

    public EventKey(int tick, int id, int hash)
    {
      _tick = tick;
      _id = id;
      _hash = hash;
    }

    public static implicit operator EventKey(EventBase b)
    {
      EventKey k;
      k._id = ((IEventBaseInternal)b).Id;
      k._hash = b.GetHashCode();
      k._tick = b.Tick;
      return k;
    }

    public override string ToString()
    {
      return "[" + _id + ", " + _tick + ", " + _hash + "]";
    }
  }

  TriggeredSetPool _triggeredSetPool;

#if QUANTUM_FLIP_EVENT_ORDER
    Stack<IEventBaseInternal> _eventsInvokeStack;
#endif
  Dictionary<Int32, Dictionary<Int32, Boolean>[]> _eventsTriggered;

  void InitEventInvoker(Int32 size)
  {
    // allocate dictionary with pre-defined capacity
#if QUANTUM_FLIP_EVENT_ORDER
      _eventsInvokeStack = new Stack<IEventBaseInternal>(512);
#endif
    _eventsTriggered = new Dictionary<Int32, Dictionary<Int32, Boolean>[]>(size);

    // init trigger set pool with empty hashsets
    _triggeredSetPool = new TriggeredSetPool();
    _triggeredSetPool.Init(size);
  }

  void RaiseEvent(IEventBaseInternal evnt)
  {
    try
    {
      evnt.EventRaise(this);
    } catch (Exception exn)
    {
      Log.Error("## Event Callback Threw Exception ##");
      Log.Exception(exn);
    }
  }

  void InvokeEvents(Frame f)
  {
    // store previous frame value so we can restore it
    var previousFrameValue = Frames.Current;

    try
    {
      // set current frame we are invoking the events for
      Frames.Current = f;

      // instead of hashset, Dictionary of Key-hash, Boolean confirmed...
      Dictionary<Int32, Boolean>[] triggered;

      // grab or create new triggered set lookup
      if (_eventsTriggered.TryGetValue(f.Number, out triggered) == false)
      {
        _eventsTriggered.Add(f.Number, triggered = _triggeredSetPool.Alloc());
      }

      // grab event head
      var head = (IEventBaseInternal)(((IFrameInternal)f).EventHead);
      // TODO move somewhere else


#if QUANTUM_FLIP_EVENT_ORDER
      // push events on invoke stack
      while (head != null) {
        ((EventBase)head).Tick = f.Number;
        // put on stack
        _eventsInvokeStack.Push(head);

        // next
        head = head.EventTail;
      }

      // step over invoke stack
      while(_eventsInvokeStack.Count > 0) {
        head = _eventsInvokeStack.Pop();

        if (head.EventIsSynced) {
          if (f.IsVerified) {
            RaiseEvent(head);
          }
        }
        else {
          // calculate hash code
          var hash = head.GetHashCode();

          // if this was already raised, do nothing
          if (triggered[head.Id].ContainsKey(hash) == false)
          {
            // dont trigger this again
            triggered[head.Id].Add(hash, false);

            // trigger event
            RaiseEvent(head);
          }

          // if frame is verified, CONFIRM the event in the temp collection of hashes
          if (f.IsVerified)
          {
            // confirm this event is definitive...
            triggered[head.Id][hash] = true;
          }
        }
      }
#else
      // step over each event
      while (head != null)
      {
        ((EventBase)head).Tick = f.Number;
        if (head.EventIsSynced)
        {
          if (f.IsVerified)
          {
            RaiseEvent(head);
          }
        } else
        {
          // calculate hash code
          var hash = head.GetHashCode();

          // if this was already raised, do nothing
          if (triggered[head.Id].ContainsKey(hash) == false)
          {
            // dont trigger this again
            triggered[head.Id].Add(hash, false);

            // trigger event
            RaiseEvent(head);
          }

          // if frame is verified, CONFIRM the event in the temp collection of hashes
          if (f.IsVerified)
          {
            // confirm this event is definitive...
            triggered[head.Id][hash] = true;
          }

        }

        // next
        head = head.EventTail;
      }
#endif

      // frame is verified?
      if (f.IsVerified)
      {
        // remove triggered set
        _eventsTriggered.Remove(f.Number);

        // check for even cancelations here...
        // all hashes that were NOT confirmed on the verification...
        // traverse all hashes (hashes are only for non synced)

        for (int eventTypeID = 0; eventTypeID < triggered.Length; eventTypeID++)
        {
          var eventCollection = triggered[eventTypeID];
          foreach (var hash in eventCollection.Keys)
          {
            EventKey k = new EventKey(f.Number, eventTypeID, hash);
            var confirmed = eventCollection[hash];
            if (confirmed)
            {
              Callbacks.InvokeOnEventConfirmed(this, k);
            } else
            {
              // call event cancelation, passing: game (this), frame (f), event hash...
              // also pass the index from eventCollection (trhis is the event type ID);
              Callbacks.InvokeOnEventCanceled(this, k);
            }
          }
        }

        // free it
        _triggeredSetPool.Free(triggered);
      }
    } finally
    {
      // restore frame value
      Frames.Current = previousFrameValue;
    }
  }
}