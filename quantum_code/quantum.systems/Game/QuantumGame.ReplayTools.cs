﻿using Photon.Deterministic;
using Quantum;
using Quantum.Core;
using System;
using System.IO;

public partial class QuantumGame {

  public InputProvider RecordedInputs { get; private set; }
  public ChecksumFile RecordedChecksums { get; private set; }

  ChecksumFile _checksumsToVerify;
  RingBuffer<Frame> _snapshots;
  Int32 _snapshotFrequencyPerSec;

  public static void ExportRecordedReplay(QuantumGame game, Stream stream, IReplaySerializer serializer) {
    if (game != null) {
      var replay = game.GetRecordedReplay();
      if (replay == null) {
        Log.Error("Failed to export replay. Use StartRecordingInput to start recording.");
      } else {
        serializer.SaveTo(replay, stream);
      }
    }
  }

  public static void ExportRecordedChecksums(QuantumGame game, Stream stream, IReplaySerializer serializer) {
    if (game != null) {
      if (game.RecordedChecksums == null) {
        Log.Error("Failed to export checksums. Use StartRecodingChecksums to start recording.");
      } else {
        serializer.SaveTo(game.RecordedChecksums, stream);
      }
    }
  }

  public static void ExportDatabase(Stream stream, IReplaySerializer serializer, string folderpath, int serializationBufferSize) {

    var database = new DatabaseFile {
      // First index is null and needs to be removed before serialization.
      // RemoveAtStart copies the array DB.FastUnsafe.
      Database = ArrayUtils.RemoveAtStart(DB.FastUnsafe)
    };

    // Uses supplied serialization.
    serializer.SaveTo(database, stream);

    // Export navmesh files from the memory.
    // They have to reside in the same folder as the database file.
    var navMeshes = DB.FindAllAssets<NavMesh>();
    foreach (var n in navMeshes) {
      // Uses it's own binary serialization code.
      var bytestream = new ByteStream(new System.Byte[serializationBufferSize]);
      n.Serialize(bytestream, true);

      var filePath = Path.Combine(folderpath, n.DataFilepath);
      var directoryName = Path.GetDirectoryName(filePath);
      if (!Directory.Exists(directoryName)) {
        Directory.CreateDirectory(directoryName);
      }

      File.WriteAllBytes(filePath, bytestream.ToArray());
    }

    // Export mesh triangles from each map
    var maps = DB.FindAllAssets<Map>();
    foreach (var map in maps) {
      if (!string.IsNullOrEmpty(map.MeshTrianglesDataPath)) {
        var bytestream = new ByteStream(new System.Byte[serializationBufferSize]);
        map.Serialize(bytestream, true);

        var filePath = Path.Combine(folderpath, map.MeshTrianglesDataPath);
        var directoryName = Path.GetDirectoryName(filePath);
        if (!Directory.Exists(directoryName)) {
          Directory.CreateDirectory(directoryName);
        }

        File.WriteAllBytes(filePath, bytestream.ToArray());
      }
    }
  }

  public static void ExportSavegame(QuantumGame game, Stream stream, IReplaySerializer serializer) {
    if (game != null) {
      ReplayFile savegame = game.CreateSavegame();
      if (savegame == null) {
        Log.Error("Failed to save game.");
      } else {
        serializer.SaveTo(savegame, stream);
      }
    }
  }

  public Frame GetRecordedSnapshot(int frame) {
    if (_snapshots == null) {
      Log.Error("Can't find any recorded snapshots. Use StartRecordingSnapshots to start recording.");
      return null;
    }

    var snapshot = _snapshots.PeekFront();
    for (int i = 1; i < _snapshots.Size; i++) {
      var s = _snapshots.PeekFront(i);
      if (s.Number > frame)
        break;
      snapshot = s;
    }
    return snapshot;
  }

  public ReplayFile CreateSavegame() {
    if (Frames.Verified == null) {
      Log.Error("Cannot create a savegame. Frames verified not found.");
      return null;
    }

    return new ReplayFile {
      DeterministicConfig = Frames.Verified.SessionConfig,
      RuntimeConfig = Frames.Verified.RuntimeConfig,
      InputHistory = null,
      Length = Frames.Verified.Number,
      Frame = Frames.Verified.Serialize()
    };
  }

  public ReplayFile GetRecordedReplay() {
    if (Frames.Current == null || Frames.Verified == null) {
      Log.Error("Cannot create a replay. Frames current or verified are not valid, yet.");
      return null;
    }

    if (RecordedInputs == null) {
      Log.Error("Cannot create a replay, beacuse no recorded input was found. Use StartRecordingInput to start recording or setup RecordingFlags.");
      return null;
    }

    var verifiedFrame = Frames.Verified.Number;

    return new ReplayFile {
      DeterministicConfig = Frames.Current.SessionConfig,
      RuntimeConfig = Frames.Current.RuntimeConfig,
      InputHistory = RecordedInputs.ExportToList(verifiedFrame),
      Length = verifiedFrame
    };
  }

  public void StartRecordingInput() {
    if (Session == null) {
      Log.Error("Can't start input recording, because the session is invalid. Wait for the OnGameStart callback.");
      return;
    }
    if (RecordedInputs == null) {
      RecordedInputs = new InputProvider(Session.PlayerCount);

      Callbacks.OnInputConfirmed += RecordedInputs.OnInputConfirmed;
      Callbacks.OnGameDestroyed += CleanupInputRecording;
      Log.Info("QuantumGame.ReplayTools: Input recording started");
    }
  }

  public void StartRecordingChecksums() {
    if (RecordedChecksums == null) {
      RecordedChecksums = new ChecksumFile();

      Callbacks.OnChecksumComputed += RecordedChecksums.RecordChecksum;
      Callbacks.OnGameDestroyed += CleanupRecordingChecksums;
      Log.Info("QuantumGame.ReplayTools: Checksum recording started");
    }
  }

  public void StartVerifyingChecksums(ChecksumFile checksums) {
    if (_checksumsToVerify == null) {
      _checksumsToVerify = checksums;

      Callbacks.OnChecksumComputed += _checksumsToVerify.VerifyChecksum;
      Callbacks.OnGameDestroyed += CleanupVerifyingChecksums;
      Log.Info("QuantumGame.ReplayTools: Checksum verification started");
    }
  }

  public void StartRecordingSnapshots(float bufferSizeSec, int snapshotFrequencyPerSec) {
    if (Session == null) {
      Log.Error("Can't start input recording, because the session is invalid. Wait for the OnGameStart callback.");
      return;
    }
    if (snapshotFrequencyPerSec <= 0) {
      Log.Error("Can't start input recording, snapshotFrequencyPerSec must be greater than 0.");
      return;
    }

    if (_snapshots == null) {
      var bufferSize = Math.Max(1, FPMath.CeilToInt(FP.FromFloat_UNSAFE(bufferSizeSec) * snapshotFrequencyPerSec));
      _snapshots = new RingBuffer<Frame>(bufferSize);
      _snapshotFrequencyPerSec = snapshotFrequencyPerSec;

      Callbacks.OnSimulateFinished += OnRecordSnapshot;
      Callbacks.OnGameDestroyed += CleanupSnapshotRecording;
      Log.Info("QuantumGame.ReplayTools: Snapshot recording started");
    }
  }

  private void OnRecordSnapshot(QuantumGame game, Frame frame) {
    Profiler.Start("QuantumGame.RecordingSnapshot");
    if (frame.IsVerified) {
      var currentFrame = frame.Number;
      var snapshotFrequencyInFrames = game.Session.SimulationRate / _snapshotFrequencyPerSec;
      if (_snapshots.IsEmpty || 
        currentFrame >= _snapshots.PeekBack().Number + snapshotFrequencyInFrames) {
        Frame frameContainer = null;
        if (_snapshots.IsFull)
          // Reuse the oldest frame object for new data.
          frameContainer = _snapshots.PopFront();
        else
          // Initialize a new frame as "container".
          frameContainer = (Frame)game.CreateFrame(null);

        frameContainer.CopyFrom(frame);
        frameContainer.Number = frame.Number;
        _snapshots.PushBack(frameContainer);
        //Log.Info("Snapshot saved at frame " + currentFrame);
      }
    }
    Profiler.End();
  }

  private void CleanupInputRecording(QuantumGame game) {
    Callbacks.OnGameDestroyed -= CleanupInputRecording;
    Callbacks.OnInputConfirmed -= RecordedInputs.OnInputConfirmed;
    // Maybe we want to access this data after game ended.
    //RecordedInputs = null;
  }

  private void CleanupRecordingChecksums(QuantumGame game) {
    Callbacks.OnGameDestroyed -= CleanupRecordingChecksums;
    Callbacks.OnChecksumComputed -= RecordedChecksums.RecordChecksum;
    // Maybe we want to access this data after game ended.
    //RecordedChecksums = null;
  }

  private void CleanupVerifyingChecksums(QuantumGame game) {
    Callbacks.OnGameDestroyed -= CleanupVerifyingChecksums;
    Callbacks.OnChecksumComputed -= _checksumsToVerify.VerifyChecksum;
    _checksumsToVerify = null;
  }

  private void CleanupSnapshotRecording(QuantumGame game) {
    Callbacks.OnGameDestroyed -= CleanupSnapshotRecording;
    Callbacks.OnSimulateFinished -= OnRecordSnapshot;

    while (!_snapshots.IsEmpty)
      _snapshots.PopBack().Free();
    _snapshots = null;
  }
}