﻿using Photon.Deterministic;
using Quantum;
using Quantum.Core;
using System;
using System.Collections.Generic;
using System.Linq;

public unsafe partial class QuantumGame : IDeterministicGame, IDeterministicReplayProvider {

  public class FramesContainer {
    public Frame Current;
    public Frame Verified;
    public Frame Predicted;
    public Frame PredictedPrevious;

    public Frame PreviousUpdatePredicted;
  }

  // Caveat: Only set after the first CreateFrame() call
  public class ConfigurationsContainer {
    public RuntimeConfig Runtime;
    public SimulationConfig Simulation;
  }

  [Obsolete("Use QuantumRunner.Default.Game instead")]
  static public QuantumGame Instance { get; set; }

  public FramesContainer Frames { get; }
  public ConfigurationsContainer Configurations { get; }
  public DeterministicSession Session { get; private set; }
  public Single InterpolationFactor { get; private set; }
  public QuantumGameCallbacks Callbacks { get; private set; }

  Byte[] _inputStreamReadZeroArray;
  BitStream _inputStreamRead;
  BitStream _inputStreamWrite;
  SystemBase[] _systems;
  FrameContext _context;

  public QuantumGame(QuantumGameCallbacks callbacks) {
#pragma warning disable CS0618
    Instance = this;
#pragma warning restore CS0618
    Frames = new FramesContainer();
    Configurations = new ConfigurationsContainer();
    Callbacks = callbacks;
  }

  public Int32[] GetLocalPlayers() {
    return Session.LocalPlayerIndices;
  }

  public Boolean PlayerIsLocal(PlayerRef playerRef) {
    if (playerRef == PlayerRef.None) {
      return false;
    }

    for (Int32 i = 0; i < Session.LocalPlayerIndices.Length; i++) {
      if (Session.LocalPlayerIndices[i] == playerRef) {
        return true;
      }
    }

    return false;
  }

  public void SendCommand(DeterministicCommand command) {
    var players = GetLocalPlayers();
    if (players.Length > 0) {
      Session.SendCommand(players[0], command);
    } else {
      Log.Error("No local player found to send command for");
    }
  }

  public void SendCommand(Int32 player, DeterministicCommand command) {
    Session.SendCommand(player, command);
  }

  public void SendPlayerData(Int32 player, RuntimePlayer data) {
    Session.SetPlayerData(player, RuntimePlayer.ToByteArray(data));
  }

  [Obsolete("Use SendPlayerData instead")]
  public void SetPlayerData(Int32 player, RuntimePlayer data) {
    Session.SetPlayerData(player, RuntimePlayer.ToByteArray(data));
  }

  public void OnDestroy() {
    Callbacks.InvokeOnGameDestroyed(this);
    Callbacks.Dispose();
    Callbacks = null;
  }

  public DeterministicFrame CreateFrame(IDisposable context) {
    // This will run after the game started, the session has the valid server runtime config.
    if (_systems == null) {
      // de-serialize runtime config, session is the one from the server
      Configurations.Runtime = RuntimeConfig.FromByteArray(Session.RuntimeConfig);
      Configurations.Simulation = Configurations.Runtime.SimulationConfig.Instance;

      // register commands
      Session.CommandSerializer.RegisterPrototypes(CommandSetup.CreateCommands(Configurations.Runtime, Configurations.Simulation));

      // initialize systems
      _systems = SystemSetup.CreateSystems(Configurations.Runtime, Configurations.Simulation);

      // set system runtime indices
      for (Int32 i = 0; i < _systems.Length; ++i) {
        _systems[i].RuntimeIndex = i;
      }
    }

    return new Frame((FrameContext)context, _systems, Session.SessionConfig, Configurations.Runtime, Configurations.Simulation, Session.DeltaTime, Session.SimulationRate);
  }

  public DeterministicFrame CreateFrame(IDisposable context, Byte[] data) {
    Frame f = (Frame)CreateFrame(context);
    f.Deserialize(data);
    return f;
  }

  public IDisposable CreateFrameContext() {
    if (_context == null) {
      _context = new FrameContext(true, Session.SessionConfig.ExposeVerifiedStatusInsideSimulation && RuntimeConfig.FromByteArray(Session.RuntimeConfig).SimulationConfig.Instance.UsePredictionArea, Session.IsLocalPlayer);
    }

    return _context;
  }

  public void SetPredictionArea(FPVector3 position, FP radius) {
    _context.SetPredictionArea(position, radius);
  }

  public void SetPredictionArea(FPVector2 position, FP radius) {
    _context.SetPredictionArea(position.XOY, radius);
  }

  public void OnGameEnded() {
    Callbacks.InvokeOnGameEnded(this);
  }

  public void OnGameStart(DeterministicFrame f) {

    // init event invoker
    InitEventInvoker(Session.RollbackWindow);

    // init systems on latest frame
    InitSystems(f);

    Frames.Current = (Frame)f;
    Frames.Predicted = Frames.Current;
    Frames.PredictedPrevious = Frames.Current;
    Frames.Verified = Frames.Current;
    Frames.PreviousUpdatePredicted = Frames.Current;

    if (Session.InitialTick == 0)
      Callbacks.InvokeOnGameStart(this);
    else
      Callbacks.InvokeOnGameStartFromSnapshot(this, Session.InitialTick);

    if (Session.GameMode != DeterministicGameMode.Replay) {
      if ((Configurations.Runtime.RecordingFlags & RecordingFlags.Checksums) == RecordingFlags.Checksums)
        StartRecordingChecksums();
      if ((Configurations.Runtime.RecordingFlags & RecordingFlags.Input) == RecordingFlags.Input)
        StartRecordingInput();
    }
  }

  public Photon.Deterministic.Tuple<Byte[], DeterministicInputFlags> OnLocalInput(Int32 player) {
    return PollLocalInput(player, 0, false);
  }

  public Photon.Deterministic.Tuple<byte[], DeterministicInputFlags> OnLocalInputLockstep(Int32 frame, Int32 player) {
    return PollLocalInput(player, frame, true);
  }

  Photon.Deterministic.Tuple<Byte[], DeterministicInputFlags> PollLocalInput(Int32 player, Int32 frame, Boolean lockstep) {
    var input = default(Photon.Deterministic.Tuple<Input, DeterministicInputFlags>);

    // poll input
    try {
      if (lockstep) {
        input = Callbacks.InvokeOnPollInputLockstep(this, player, frame);
      } else {
        input = Callbacks.InvokeOnPollInput(this, player);
      }
    } catch (Exception exn) {
      Log.Error("## Input Code Threw Exception ##");
      Log.Exception(exn);
    }

    if (_inputStreamWrite == null) {
      _inputStreamWrite = new BitStream(new Byte[1024]);
    }

    // clear old data
    _inputStreamWrite.Reset();
    _inputStreamWrite.Writing = true;
    _inputStreamWrite.InputMode = true;

    // pack into stream
    Input.Write(_inputStreamWrite, input.Item0);

    // send input via backend
    return Photon.Deterministic.Tuple.Create(_inputStreamWrite.ToArray(), input.Item1);
  }

  public void OnSimulate(DeterministicFrame state) {
    var f = (Frame)state;

    try {
      ApplyInputs(f);

      f.PreSimulatePrepare();

      ((IFrameInternal)state).UpdatePlayerData();

      for (Int32 i = 0; i < _systems.Length; ++i) {
        if (BitSet256.IsSet(&f.Global->Systems, i)) {
          try {
            _systems[i].Update(f);
          } catch (Exception exn) {
            LogSimulationException(exn);
          }
        }
      }

      f.PostSimulateCleanup();
    } catch (Exception exn) {
      LogSimulationException(exn);
    }
  }

  public void OnSimulateFinished(DeterministicFrame state) {
    InvokeEvents((Frame)state);

    Callbacks.InvokeOnSimulateFinished(this, state);
  }

  public void OnUpdateDone() {
    Frames.Current = (Frame)Session.FramePredicted;
    Frames.Predicted = (Frame)Session.FramePredicted;
    Frames.PredictedPrevious = (Frame)Session.FramePredictedPrevious;
    Frames.Verified = (Frame)Session.FrameVerified;
    Frames.PreviousUpdatePredicted = (Frame)Session.PreviousUpdateFramePredicted;

    var f = (float)(Session.AccumulatedTime / Frames.Current.DeltaTime.AsFloat);
    InterpolationFactor = f < 0.0f ? 0.0f : f > 1.0f ? 1.0f : f; // Clamp01

    Callbacks.InvokeOnUpdateView(this);
  }

  public void AssignSession(DeterministicSession session) {
    Session = session;

    Byte[] runtimeConfig;
    DeterministicSessionConfig sessionConfig;

    Session.GetLocalConfigs(out sessionConfig, out runtimeConfig);
    
    _runtimePlayersData = new Dictionary<int, Queue<byte[]>>();
    _commandsData = new Dictionary<int, Queue<byte[]>>();
    for (Int32 p = 0; p < sessionConfig.PlayerCount; p++)
    {
      _runtimePlayersData[p] = new Queue<byte[]>();
      _commandsData[p] = new Queue<byte[]>();
    }

    // verify player count is in correct range
    if (sessionConfig.PlayerCount < 1 || sessionConfig.PlayerCount > Quantum.Input.MAX_COUNT) {
      throw new Exception(String.Format("Invalid player count {0} (needs to be in 1-{1} range)", sessionConfig.PlayerCount, Quantum.Input.MAX_COUNT));
    }
  }

  public void OnChecksumError(DeterministicTickChecksumError error, DeterministicFrame[] frames) {
    Callbacks.InvokeOnChecksumError(this, error, frames);
  }

  public void OnChecksumComputed(Int32 frame, ulong checksum) {
    Callbacks.InvokeOnChecksumComputed(this, frame, checksum);
  }

  public void OnInputConfirmed(Int32 frame, Int32 player, Byte[] rpc, Photon.Deterministic.Tuple<Byte[], DeterministicInputFlags> input) {
    Callbacks.InvokeOnInputConfirmed(this, frame, player, rpc, input);
  }

  public void OnChecksumErrorFrameDump(Int32 actorId, Int32 frameNumber, String frameData) {
    Callbacks.InvokeOnChecksumErrorFrameDump(this, actorId, frameNumber, frameData);
  }

  public Int32 GetFixedInputSize() {
    var stream = new BitStream(1024);
    stream.Writing = true;
    stream.InputMode = true;
    Input.Write(stream, new Input());
    return stream.ToArray().Length;
  }

  void InitSystems(DeterministicFrame f) {
    if (Session.InitialTick != 0)
      return;

    // call init on ALL systems
    for (Int32 i = 0; i < _systems.Length; ++i) {
      try {
        _systems[i].OnInit((Frame)f);
      } catch (Exception exn) {
        LogSimulationException(exn);
      }
    }

    // call OnEnabled on all systems which start enabled
    for (Int32 i = 0; i < _systems.Length; ++i) {
      if (_systems[i].StartEnabled) {
        try {
          _systems[i].OnEnabled((Frame)f);
        } catch (Exception exn) {
          LogSimulationException(exn);
        }
      }
    }

    // invoke events from OnInit/OnEnabled
    InvokeEvents((Frame)f);
  }

  void ApplyInputs(Frame f) {
    for (Int32 i = 0; i < Session.PlayerCount; i++) {
      var data = f.GetRawInput(i);

      if (_inputStreamRead == null) {
        _inputStreamReadZeroArray = new Byte[1024];
        _inputStreamRead = new BitStream(new Byte[1024]);
      }

      // copy into stream
      _inputStreamRead.Reset();

      if (data == null || data.Length == 0) {
        _inputStreamRead.CopyFromArray(_inputStreamReadZeroArray);
      } else {
        _inputStreamRead.CopyFromArray(data);
      }

      _inputStreamRead.Reading = true;
      _inputStreamRead.InputMode = true;

      Input input;

      // try read input and assign it
      if (ReadInputFromStream(out input)) {
        f.SetPlayerInput(i, input);
      } else {
        Log.Trace("Received invalid input data from player {0}, could not deserialize.", i);
        f.SetPlayerInput(i, default(Input));
      }
    }
  }

  Boolean ReadInputFromStream(out Input input) {
    try {
      input = Input.Read(_inputStreamRead);
      return true;
    } catch {
      input = default(Input);
      return false;
    }
  }

  void LogSimulationException(Exception exn) {
    Log.Error("## Simulation Code Threw Exception ##");
    Log.Exception(exn);
  }

  public HashSet<int> LocalPlayerSet { get; }
  public void FrameIsDone(int frame)
  {
  }

  public bool Finished { get; }
  public bool HasAllInputForFrame(int frame, int playerCount)
  {
    return true;
  }

  public Photon.Deterministic.Tuple<byte[], DeterministicInputFlags> GetInputForFrame(int frame, int player)
  {
    var inputTuple = PollLocalInput(player, frame, false);
    if (_commandsData[player].Count > 0)
    {
      inputTuple = Photon.Deterministic.Tuple.Create(inputTuple.Item0, inputTuple.Item1 | DeterministicInputFlags.Command);
    }
    
    return inputTuple;
  }

  public byte[] GetRPCDataForFrame(int frame, int player)
  {
    if (_commandsData[player].Count > 0)
      return _commandsData[player].Dequeue();
    if (_runtimePlayersData[player].Count > 0)
      return _runtimePlayersData[player].Dequeue();
    return null;
  }

  private Dictionary<Int32, Queue<Byte[]>> _commandsData;
  private Dictionary<Int32, Queue<Byte[]>> _runtimePlayersData;

  public void AddPlayerData(int player, Byte[] data)
  {
    _runtimePlayersData[player].Enqueue(data);
  }
  
  public void AddCommand(int player, Byte[] data)
  {
    _commandsData[player].Enqueue(data);
  }
}