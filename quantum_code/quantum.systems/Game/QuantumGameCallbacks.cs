﻿using System;
using System.Collections.Generic;
using System.Linq;
using Photon.Deterministic;

namespace Quantum {

  public sealed class QuantumGameCallbacks : IDisposable {

    public Func<QuantumGame, Int32, Photon.Deterministic.Tuple<Input, DeterministicInputFlags>> OnPollInput;
    public Func<QuantumGame, Int32, Int32, Photon.Deterministic.Tuple<Input, DeterministicInputFlags>> OnPollInputLockstep;
    public Action<QuantumGame> OnGameStart;
    public Action<QuantumGame, int> OnGameStartFromSnapshot;
    public Action<QuantumGame> OnGameDestroyed;
    public Action<QuantumGame> OnUpdateView;
    public Action<QuantumGame, Frame> OnSimulateFinished;
    public Action<QuantumGame, QuantumGame.EventKey> OnEventCanceled;
    public Action<QuantumGame, QuantumGame.EventKey> OnEventConfirmed;
    public Action<QuantumGame, DeterministicTickChecksumError, Frame[]> OnChecksumError;
    public ChecksumErrorFrameDumpDelegate OnChecksumErrorFrameDump;
    public InputConfirmedDelegate OnInputConfirmed;
    public Action<QuantumGame, int, ulong> OnChecksumComputed;
    // public Action<QuantumGame> OnGameEnded;
    // OnEntityCreated/Destroyed Callbacks?

    public delegate void ChecksumErrorFrameDumpDelegate(QuantumGame game, Int32 actorId, Int32 frameNumber, String frameData);
    public delegate void InputConfirmedDelegate(QuantumGame game, Int32 frame, Int32 player, Byte[] rpc, Byte[] data, DeterministicInputFlags flags);

    List<IDisposable> _cleanupObjects = new List<IDisposable>();

    public void AddDisposableObject(IDisposable obj) {
      _cleanupObjects.Add(obj);
    }

    public void Dispose() {
      OnPollInput = null;
      OnPollInputLockstep = null;
      OnGameStart = null;
      OnGameStartFromSnapshot = null;
      OnGameDestroyed = null;
      OnUpdateView = null;
      OnSimulateFinished = null;
      OnChecksumError = null;
      OnInputConfirmed = null;
      OnChecksumComputed = null;
      OnChecksumErrorFrameDump = null;

      foreach (var obj in _cleanupObjects)
        obj.Dispose();
      _cleanupObjects.Clear();
    }

    public Photon.Deterministic.Tuple<Input, DeterministicInputFlags> InvokeOnPollInput(QuantumGame game, Int32 player) {
      if (OnPollInput != null) {
        try {
          return OnPollInput(game, player);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
      return default(Photon.Deterministic.Tuple<Input, DeterministicInputFlags>);
    }

    public Photon.Deterministic.Tuple<Input, DeterministicInputFlags> InvokeOnPollInputLockstep(QuantumGame game, Int32 frame, Int32 player) {
      if (OnPollInputLockstep != null) {
        try {
          return OnPollInputLockstep(game, frame, player);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
      return default(Photon.Deterministic.Tuple<Input, DeterministicInputFlags>);
    }

    public void InvokeOnGameStart(QuantumGame game) {
      //OnGameStart?.Invoke(game)
      if (OnGameStart != null) {
        try {
          OnGameStart(game);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnGameStartFromSnapshot(QuantumGame game, int frameNumber) {
      if (OnGameStartFromSnapshot != null) {
        try {
          OnGameStartFromSnapshot(game, frameNumber);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnGameEnded(QuantumGame game) {
      // Will be implemented in future releases.
    }

    public void InvokeOnGameDestroyed(QuantumGame game) {
      if (OnGameDestroyed != null) {
        try {
          OnGameDestroyed(game);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnChecksumErrorFrameDump(QuantumGame game, Int32 actorId, Int32 frameNumber, String frameData) {
      Profiler.Start("QuantumGame.InvokeOnChecksumErrorFrameDump.OnUpdateView");
      if (OnChecksumErrorFrameDump != null) {
        try {
          OnChecksumErrorFrameDump(game, actorId, frameNumber, frameData);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
      Profiler.End();
    }

    public void InvokeOnUpdateView(QuantumGame game) {
      Profiler.Start("QuantumGame.Invoke.OnUpdateView");
      if (OnUpdateView != null) {
        try {
          OnUpdateView(game);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
      Profiler.End();
    }

    public void InvokeOnSimulateFinished(QuantumGame game, DeterministicFrame state) {
      Profiler.Start("QuantumGame.Invoke.OnSimulateFinished");
      if (OnSimulateFinished != null) {
        try {
          OnSimulateFinished(game, (Frame)state);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
      Profiler.End();
    }

    public void InvokeOnEventCanceled(QuantumGame game, QuantumGame.EventKey eventKey)
    {
      if (OnEventCanceled != null)
      {
        try
        {
          OnEventCanceled(game, eventKey);
        } catch (Exception exn)
        {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnEventConfirmed(QuantumGame game, QuantumGame.EventKey eventKey)
    {
      if (OnEventConfirmed != null)
      {
        try
        {
          OnEventConfirmed(game, eventKey);
        } catch (Exception exn)
        {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnChecksumError(QuantumGame game, DeterministicTickChecksumError error, DeterministicFrame[] frames) {
      if (OnChecksumError != null) {
        var castedFrames = frames.Cast<Frame>().ToArray();
        try {
          OnChecksumError(game, error, castedFrames);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnInputConfirmed(QuantumGame game, Int32 frame, Int32 player, Byte[] rpc, Photon.Deterministic.Tuple<Byte[], DeterministicInputFlags> input) {
      var inputData = input.Item0 ?? new byte[0];

      if (OnInputConfirmed != null) {
        DeterministicTickInput data = new DeterministicTickInput() {
          Tick = frame,
          PlayerIndex = player,
          DataArray = inputData,
          DataLength = inputData.Length,
          Flags = input.Item1,
          Rpc = rpc
        };
        try {
          OnInputConfirmed(game, frame, player, rpc, input.Item0, input.Item1);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }

    public void InvokeOnChecksumComputed(QuantumGame game, int frame, ulong checksum) {
      if (OnChecksumComputed != null) {
        try {
          OnChecksumComputed(game, frame, checksum);
        } catch (Exception exn) {
          Log.Exception(exn);
        }
      }
    }
  }
}