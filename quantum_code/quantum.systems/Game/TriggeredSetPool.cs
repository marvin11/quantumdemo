﻿using System;
using System.Collections.Generic;

namespace Quantum {
  public class TriggeredSetPool {

    Stack<Dictionary<Int32, Boolean>[]> _triggeredPool = new Stack<Dictionary<Int32, Boolean>[]>();

    public void Init(Int32 size) {
      while (_triggeredPool.Count < size) {
        _triggeredPool.Push(CreateNew());
      }
    }

    public Dictionary<Int32, Boolean>[] Alloc() {
      Dictionary<Int32, Boolean>[] set;

      if (_triggeredPool.Count > 0) {
        set = _triggeredPool.Pop();
      } else {
        set = CreateNew();
      }

      return set;
    }

    public void Free(Dictionary<Int32, Boolean>[] set) {
      if (set != null) {
        for (Int32 i = 0; i < Frame.FrameEvents.EVENT_TYPE_COUNT; ++i) {
          set[i].Clear();
        }

        // push on pool
        _triggeredPool.Push(set);
      }
    }

    Dictionary<Int32, Boolean>[] CreateNew() {
      var set = new Dictionary<Int32, Boolean>[Frame.FrameEvents.EVENT_TYPE_COUNT];

      for (Int32 i = 0; i < Frame.FrameEvents.EVENT_TYPE_COUNT; ++i) {
        set[i] = new Dictionary<Int32, Boolean>();
      }

      return set;
    }
  }
}