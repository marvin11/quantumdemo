﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Quantum {

  [Serializable]
  public class ChecksumFile {
    public class ChecksumEntry {
      public int Frame;
      // This is super annoying: Unity JSON cannot read the unsinged long data type. 
      // We can convert on this level, keeping the ULong CalculateChecksum() signature and encode the 
      // checksum as a long for serialization. Any other ideas?
      public long ChecksumAsLong;
    }

    public ChecksumEntry[] Checksums;

    private Int32 writeIndex;

    public Dictionary<int, ChecksumEntry> ToDictionary() {
      return Checksums.Where(item => item != null).ToDictionary(item => item.Frame, item => item);
    }

    internal void RecordChecksum(QuantumGame game, Int32 frame, ulong checksum) {
      Profiler.Start("QuantumGame.RecordingChecksum");
      const Int32 GrowSize = 64;
      var checksumEntry = new ChecksumEntry() {
        Frame = frame,
        ChecksumAsLong = ChecksumFileHelper.UlongToLong(checksum)
      };
      if (Checksums == null) {
        Checksums = new ChecksumEntry[GrowSize];
      }
      if (writeIndex + 1 > Checksums.Length) {
        Array.Resize(ref Checksums, Checksums.Length + GrowSize);
      }
      Checksums[writeIndex++] = checksumEntry;
      Profiler.End();
    }

    internal void VerifyChecksum(QuantumGame game, Int32 frame, ulong checksum) {
      Profiler.Start("QuantumGame.VerifyingChecksum");
      // Pretty bad performance, optimize me
      var checksumItem = Checksums.First(item => item.Frame == frame);
      if (checksumItem != null) {
        var convertedChecksum = ChecksumFileHelper.UlongToLong(checksum);
        if (checksumItem.ChecksumAsLong != convertedChecksum)
          Log.Error("Checksum mismatch in frame {0}: {1} != {2}", frame, checksumItem.ChecksumAsLong, convertedChecksum);
      }
      Profiler.End();
    }
  }

  public static class ChecksumFileHelper {
    public unsafe static long UlongToLong(ulong value) {
      //return unchecked((long)value + long.MinValue);
      return *((long*)&value);
    }

    public unsafe static ulong LongToULong(long value) {
      //return unchecked((ulong)(value - long.MinValue));
      return *((ulong*)&value);
    }
  }
}
