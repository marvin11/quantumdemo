﻿using System;

namespace Quantum {

  [Serializable]
  public class DatabaseFile {
    public AssetObject[] Database;
  }
}
