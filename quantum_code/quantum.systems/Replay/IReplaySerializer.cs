﻿using System.IO;

namespace Quantum {
  public interface IReplaySerializer {
    T LoadFrom<T>(string text);
    T LoadFrom<T>(byte[] buffer);
    T LoadFrom<T>(Stream stream);
    void SaveTo(object obj, Stream stream);
  }
}
