﻿using System;
using System.Linq;
using System.Collections.Generic;
using Photon.Deterministic;

namespace Quantum {
  public class InputProvider : IDeterministicReplayProvider {
    private HashSet<Int32> _localPlayerSet = new HashSet<int>();

    public InputProvider(int playerCount) {
      for (int i = 0; i < playerCount; i++)
        _localPlayerSet.Add(i);
    }

    public bool Finished => _inputSets == null || _inputSets.Count == 0 || lastFrame >= _inputSets.Last().Key;

    public HashSet<int> LocalPlayerSet => _localPlayerSet;

    private int lastFrame;

    public void FrameIsDone(int frame) {
      lastFrame = frame;
    }

    private SortedDictionary<Int32, DeterministicTickInputSet> _inputSets = new SortedDictionary<int, DeterministicTickInputSet>();

    public void ImportFromList(DeterministicTickInputSet[] inputList) {
      for (int i = 0; i < inputList.Length; i++) {
        _inputSets.Add(inputList[i].Tick, inputList[i]);
        inputList[i].Inputs.All(x => x.Sent = true);
      }
    }

    public DeterministicTickInputSet[] ExportToList(int verifiedFrame) {
      var result = _inputSets.Values.ToArray();
      while (result.Length > 0 && result.Last().Inputs.Any(x => x == null || x.Tick > verifiedFrame)) {
        Array.Resize(ref result, result.Length - 1);
      }
      return result;
    }

    public void OnInputConfirmed(QuantumGame game, Int32 frame, Int32 player, Byte[] rpc, Byte[] data, DeterministicInputFlags flags) {
      Profiler.Start("QuantumGame.RecordingInput");

      data = data ?? new byte[0];

      DeterministicTickInput input = new DeterministicTickInput() {
        Tick = frame,
        PlayerIndex = player,
        DataArray = data,
        DataLength = data.Length,
        Flags = flags,
        Rpc = rpc
      };

      InjectInput(input, true, false);

      Profiler.End();
    }

    public void InjectInput(DeterministicTickInput input, bool localReplay, bool pooledInput) {

      if (pooledInput) {
        // We need to clone the input object because it is cached on the calling side.
        // This behaviour is mandatory in the plugin.
        input = input.Clone();
      }

      if (!_inputSets.ContainsKey(input.Tick)) {
        _inputSets.Add(input.Tick, new DeterministicTickInputSet() {
          Tick = input.Tick,
          Inputs = new DeterministicTickInput[LocalPlayerSet.Count]
        });
      }

      _inputSets[input.Tick].Inputs[input.PlayerIndex] = input;

      if (localReplay)
        input.Sent = true;
    }

    public Byte[] GetRPCDataForFrame(int frame, int player)
    {
      return _inputSets[frame].Inputs[player].Rpc;
    }

    public void AddPlayerData(int player, Byte[] data)
    {
      // not used in replays
    }

    public void AddCommand(int player, Byte[] data)
    {
      // not used in replays
    }
    
    public Photon.Deterministic.Tuple<byte[], DeterministicInputFlags> GetInputForFrame(int frame, int player) {
      return Photon.Deterministic.Tuple.Create(_inputSets[frame].Inputs[player].DataArray, _inputSets[frame].Inputs[player].Flags);
    }

    public bool HasAllInputForFrame(int frame, int playerCount) {
      return _inputSets.ContainsKey(frame) && _inputSets[frame].IsFinished();
    }
  }

  public static class DeterministicTickInputSetExtensions {
    public static bool IsComplete(this DeterministicTickInputSet set) {
      return set.Inputs.All(x => ReferenceEquals(x, null) == false);
    }

    public static bool IsFinished(this DeterministicTickInputSet set) {
      return set.IsComplete() && set.Inputs.All(x => x.Sent);
    }
  }
}
