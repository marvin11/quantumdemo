﻿using Photon.Deterministic;
using System;

namespace Quantum {

  [Serializable]
  public class ReplayFile {
    public RuntimeConfig RuntimeConfig;
    public DeterministicSessionConfig DeterministicConfig;
    public DeterministicTickInputSet[] InputHistory;
    public Int32 Length;
    public byte[] Frame;
  }
}
