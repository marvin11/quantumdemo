﻿using Photon.Deterministic;
using Quantum.Core;
using System;

namespace Quantum
{
  public class SessionContainer
  {
    public DeterministicSession session;
    public DeterministicSessionConfig config => replayFile.DeterministicConfig;
    public RuntimeConfig runtimeConfig => replayFile.RuntimeConfig;
    public InputProvider provider;
    public IDeterministicGame game;
    public ReplayFile replayFile;
    public DatabaseFile databaseFile;

    public static Boolean _loadedAllStatics = false;
    public static Object _lock = new Object();

    public void Start()
    {
      

      lock (_lock)
      {
        if (!_loadedAllStatics)
        {
          Log.InitForConsole();
          Native.Impl = Native.NativeImpl.MSCVCRT;
          var transformedArray = ArrayUtils.AddAtStart(databaseFile.Database, null);
          DB.Init(transformedArray);
        }
      }
      var simulationConfig = runtimeConfig.SimulationConfig.Instance;

      lock (_lock)
      {
        if (!_loadedAllStatics)
        {
          Layers.Init(simulationConfig.Physics.Layers, simulationConfig.Physics.LayerMatrix);
          _loadedAllStatics = true;
        }
      }
        

      game = new QuantumGame(new QuantumGameCallbacks());

      provider = new InputProvider(config.PlayerCount);

      session = new DeterministicSession(DeterministicGameMode.Replay, RuntimeConfig.ToByteArray(runtimeConfig), config, game, null, provider, null);
      session.Join("server", config.PlayerCount);
    }

    public void Service(double? dt = null)
    {
      session.Update(dt);
    }

    public void Destroy()
    {
      if (session != null)
        session.Destroy();
      session = null;

      //DB.Dispose();
    }
  }
}
