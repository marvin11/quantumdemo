﻿using Quantum.Core;

namespace Quantum
{
  public static class SystemSetup
  {
    public static SystemBase[] CreateSystems(RuntimeConfig gameConfig, SimulationConfig simulationConfig)
    {
      return new SystemBase[] {
        // pre-defined core systems
        new Core.PhysicsSystemPre(),
        new Core.NavMeshAgentSystem(),

        // user systems go here
        new PlayerSystem(),
        new InteractableObjectSystem(),
        new InteractionSystem(),
        new DrawSystem(),
        new VideoPlayerSystem(),
        new CompanionSystem(), 

        // pre-defined core systems
        new Core.AnimatorSystem(),
      };
    }
  }
}
