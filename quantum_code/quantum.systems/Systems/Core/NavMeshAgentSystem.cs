﻿using Photon.Deterministic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quantum.Core
{
  public unsafe class NavMeshAgentSystem : SystemBase, INavMeshCallbacks
  {
    Frame.FrameFilters.NavMeshAgentIterator _iterator;

    public void OnTargetReached(NavMeshUpdater.NavMeshEntity e)
    {
      _f.Signals.OnNavMeshTargetReached(e.Agent, (Entity*)e.Entity);
    }

    public void OnSearchFailed(NavMeshUpdater.NavMeshEntity e) {
      _f.Signals.OnNavMeshSearchFailed(e.Agent, (Entity*)e.Entity);
    }

    public void OnUpdateSteering(NavMeshUpdater.NavMeshEntity e, NavMeshAgentSteeringData* data) {
      _f.Signals.OnNavMeshUpdateSteering(e.Agent, (Entity*)e.Entity, data);
    }

    public void OnUpdateAvoidance(NavMeshUpdater.NavMeshEntity a, NavMeshUpdater.NavMeshEntity b, Core.NavMeshAgentAvoidanceData* data)
    {
      data->IgnoreInternalAvoidance = false;
      _f.Signals.OnNavMeshUpdateAvoidance(a.Agent, (Entity*)a.Entity, b.Agent, (Entity*)b.Entity, data);
    }

    public bool Next(out void* entity, out Int32 entityIndex, out NavMeshAgent* agent, out Transform2D* transform, out DynamicBody* dynamicBody) {
      if (_iterator.Next()) {
        entity = _iterator.Current;
        agent = _iterator.NavMeshAgent;
        transform = _iterator.Transform2D;
        dynamicBody = _iterator.Any.DynamicBody;
        entityIndex = _iterator.Current->EntityRef.Index;
        return true;
      }

      entity = null;
      agent = null;
      transform = null;
      dynamicBody = null;
      entityIndex = 0;
      return false;
    }

    private Frame _f;
    public override void Update(Frame f)
    {
      _f = f;
      _iterator = f.Filters.NavMeshAgent(true);
      Profiler.Start("NavMeshAgent System");
      f.NavMeshUpdater.Update(f, f.DeltaTime, f.Number, this);
      Profiler.End();
      _iterator = default(Frame.FrameFilters.NavMeshAgentIterator);
      _f = null;
    }
  }
}
