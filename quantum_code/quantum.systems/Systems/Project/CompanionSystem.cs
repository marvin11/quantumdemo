using Photon.Deterministic;

namespace Quantum.Core
{
	public unsafe class CompanionSystem : SystemBase, ISignalOnNavMeshTargetReached
	{
		public override void OnInit(Frame f)
		{
			if (f.Map.UserAsset.Instance is MapInfoData mapAsset && mapAsset.HasCompanion)
			{
				var c = f.CreateCompanion();

				c->Prefab.Current = "Companion";
				c->NavMeshAgent.AgentConfig = DB.FindAllAssets<NavMeshAgentConfig>()[0];
				c->Transform2D.Position = new FPVector2(3, 3);
				c->Transform2D.Rotation = 0;
			}
		}

		public override void Update(Frame f)
		{
			var companions = f.GetAllCompanions();
			while (companions.Next())
			{
				var c = companions.Current;

				if (!c->Fields.Moving)
				{
					c->Fields.WaitTime -= f.DeltaTime;
					if (c->Fields.WaitTime <= FP._0)
					{
						ChooseNextTarget(f, c);
					}
				} 
				else
				{
					if (f.Number % 30 == 0 && c->Fields.FollowTarget != EntityRef.None)
						UpdateTargetPosition(f, c);
				}
			}
		}

		private void UpdateTargetPosition(Frame f, Companion* c)
		{
			var e = f.GetEntity(c->Fields.FollowTarget);
			var t = Entity.GetTransform3D(e);

			if (t == null)
				return;
			
			var nm = f.Map.GetNavMesh("NavMesh");
			c->NavMeshAgent.SetTarget(t->Position.XZ, nm, false);
		}

		private void ChooseNextTarget(Frame f, Companion* c)
		{
			var randomValue = f.RNG->Next(FP._0, FP._1);
			var nm = f.Map.GetNavMesh("NavMesh");
			var pos = FPVector2.Zero;

			if (true)
			{
				pos = new FPVector2(f.RNG->Next(-5, 5), f.RNG->Next(-5, 5));
				c->Fields.FollowTarget = EntityRef.None;
			} 
			else
			{
				var players = f.GetAllPlayers();
				while (players.Next())
				{
					var p = players.Current;

					var flags = f.GetPlayerInputFlags(p->Fields.Owner);
					if ((flags & DeterministicInputFlags.PlayerNotPresent) == 0)
					{
						pos = p->Transform3D.Position.XZ;
						c->Fields.FollowTarget = p->EntityRef;
					}
				}
			}

			c->Fields.Moving = true;
			c->NavMeshAgent.SetTarget(pos, nm, false);
		}

		public void OnNavMeshTargetReached(Frame f, NavMeshAgent* agent, Entity* entity)
		{
			var c = Entity.CastToCompanion(entity);
			if (c == null)
				return;

//			TriggerAiEvent(f, c, "OnTargetReached");

			c->Fields.Moving = false;
			c->Fields.WaitTime = 10;
		}

		private void TriggerAiEvent(Frame f, Companion* c, string eventName)
		{
//			HFSMManager.TriggerEvent(f, &c->Fields.HFSMAgent.Data, (Entity*) c, eventName);
		}
	}
}