using Photon.Deterministic;

namespace Quantum.Core
{
	public unsafe class DrawSystem : SystemBase, ISignalOnContainerGrabbed, ISignalOnContainerReleased, ISignalOnToggleDrawing
  {
    public override void OnInit(Frame f)
    {
      for (var i = 0; i < f.Map.StaticColliders3D.Length; i++)
      {
        var collider = f.Map.StaticColliders3D[i];
        var asset = DB.FindAsset<DrawingBoardData>(collider.StaticData.Asset.Guid);
        if (asset == null)
          continue;

        var drawingBoard = f.CreateDrawingBoard();
        drawingBoard->Prefab.Current = asset.PrefabName;

        drawingBoard->Transform3D.Position = collider.Position;
        drawingBoard->Transform3D.Rotation = collider.Rotation;

        var shape = DynamicShape3D.CreateBox(asset.Extents * (asset.ScaleFactor * FP._0_50));
        drawingBoard->DynamicBody3D.InitKinematic(shape);
      }

      var leftHandBrushData = DB.FindAsset<BrushData>("LeftHandBrushData");
      var rightHandBrushData = DB.FindAsset<BrushData>("RightHandBrushData");

      var players = f.GetAllPlayers();
      while (players.Next())
      {
        var player = players.Current;

        if (leftHandBrushData != null)
        {
          var brush = f.CreateBrush();
          brush->Fields.Enabled = false;
          brush->Fields.Container = EntityRefContainer.None;
          brush->Fields.Hand = player->Fields.LeftHand;
          brush->Fields.BrushData = leftHandBrushData;
        }

        if (rightHandBrushData != null)
        {
          var brush = f.CreateBrush();
          brush->Fields.Enabled = false;
          brush->Fields.Container = EntityRefContainer.None;
          brush->Fields.Hand = player->Fields.RightHand;
          brush->Fields.BrushData = rightHandBrushData;
        }
      }
    }

    public override void Update(Frame f)
    {
      var brushes = f.GetAllBrushes();
      while (brushes.Next())
      {
        var brush = brushes.Current;
        var data = brush->Fields.BrushData;

        if (!brush->Fields.Enabled)
          continue;

        var originPosition = FPVector3.Zero;
        var direction = data.Direction;

        var container = f.GetContainer(brush->Fields.Container);
        if (container != null)
        {
          originPosition = container->Transform3D.Position + container->Transform3D.Rotation * data.Offset;
          direction = container->Transform3D.Rotation * direction;
        }
        var hand = f.GetBodyPart(brush->Fields.Hand);
        if (hand != null)
        {
          originPosition = hand->Transform3D.Position + hand->Transform3D.Rotation * data.Offset;
          direction = hand->Transform3D.Rotation * direction;
        }

        var isDrawing = false;
        using (var hits = f.Scene3D.Raycast(
          originPosition,
          direction.Normalized,
          data.RaycastMaxDistance,
          -1,
          DynamicScene3D.LinecastOptions.SkipStatic
        ))
        {
          foreach (var hit in hits)
          {
            isDrawing = true;
            var hitDistance = FPVector3.Distance(hit.Point, originPosition);
            var pressure = FPMath.Clamp01((data.RaycastMaxDistance - hitDistance) / brush->Fields.BrushData.MaxPressureDistance);

            PaintOnDrawingBoard(f, (DrawingBoard*)hit.Entity, brush, hit.Point, pressure);
          }
        }

        brush->Fields.WasDrawing = isDrawing;

        Draw.Sphere(originPosition, FP._0_01);
        Draw.Sphere(originPosition + direction.Normalized * data.RaycastMaxDistance, FP._0_01);
      }
    }

    private static void PaintOnDrawingBoard(Frame f, DrawingBoard* board, Brush* brush, FPVector3 point, FP pressure)
    {
      var xAxis = board->Transform3D.Rotation * FPVector3.Right;
      var yAxis = board->Transform3D.Rotation * FPVector3.Up;

      var extents = board->DynamicBody3D.GetShape().Box.Extents;
      var offset = point - board->Transform3D.Position;
      var toUv = new FPVector3(
        FPVector3.Dot(xAxis, offset) / (extents.X * FP._2) + FP._0_50,
        FPVector3.Dot(yAxis, offset) / (extents.Y * FP._2) + FP._0_50,
        pressure
      );

      var fromUv = brush->Fields.WasDrawing ? brush->Fields.LastUv : toUv;

      f.Events.DrawOnBoard(board->EntityRef, brush->EntityRef, fromUv, toUv);

      brush->Fields.LastUv = toUv;
    }

    Brush* FindBrushForContainer(Frame f, Container* container)
    {
      var brushes = f.GetAllBrushes();
      while (brushes.Next())
      {
        var brush = brushes.Current;

        if (container->EntityRef == (EntityRef)brush->Fields.Container)
        {
          return brush;
        }
      }
      return null;
    }

    Brush* FindBrushForPlayer(Frame f, Player* p, bool leftHand)
    {
      var brushes = f.GetAllBrushes();
      while (brushes.Next())
      {
        var brush = brushes.Current;

        if (p->Fields.LeftHand == brush->Fields.Hand && leftHand)
          return brush;
        if (p->Fields.RightHand == brush->Fields.Hand && !leftHand)
          return brush;
      }
      return null;
    }


    public void OnContainerGrabbed(Frame f, Container* container, Player* player, BodyPart* bodyPart, bool leftHand)
    {
      var brush = FindBrushForContainer(f, container);
      if (brush != null)
        brush->Fields.Enabled = true;
    }

    public void OnContainerReleased(Frame f, Container* container)
    {
      var brush = FindBrushForContainer(f, container);
      if (brush != null)
        brush->Fields.Enabled = false;
    }

    public void OnToggleDrawing(Frame f, Player* p)
    {
      if (p->Fields.ToggleDrawingCooldown > 0)
        return;

      var leftBrush = FindBrushForPlayer(f, p, true);
      var rightBrush = FindBrushForPlayer(f, p, false);

      p->Fields.ToggleDrawingCooldown = FP._1;
      p->Fields.IsDrawing = !p->Fields.IsDrawing;
      leftBrush->Fields.Enabled = p->Fields.IsDrawing;
      rightBrush->Fields.Enabled = p->Fields.IsDrawing;
    }
  }
}
