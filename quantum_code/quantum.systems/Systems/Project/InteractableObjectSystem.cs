using Photon.Deterministic;

namespace Quantum.Core
{
  public unsafe class InteractableObjectSystem : SystemBase, ISignalOnCollisionDynamic, ISignalOnCollisionStatic
  {
    public override void OnInit(Frame f)
    {
      for (int i = 0; i < f.Map.StaticColliders3D.Length; i++)
      {
        var collider = f.Map.StaticColliders3D[i];
        var asset = DB.FindAsset<InteractableObjectData>(collider.StaticData.Asset.Guid);
        if (asset == null)
          continue;

        var container = f.CreateContainer();

        container->Fields.InitialPosition = collider.Position;
        container->Fields.InitialRotation = collider.Rotation;
        container->Fields.ObjectData = asset;

        ResetContainer(f, container);

        DynamicShape3D shape;
        if (asset.Shape == DynamicShape3DType.Box)
        {
          var scaleVector = asset.ScaleFactor * FP._0_50;
          shape = DynamicShape3D.CreateBox(asset.Extents * scaleVector);
        }
        else
        {
          shape = DynamicShape3D.CreateSphere(asset.Extents.X * asset.ScaleFactor);
        }

        container->DynamicBody3D.InitDynamic(shape, asset.Mass);
        container->Prefab.Current = asset.PrefabName;

        if (!string.IsNullOrEmpty(asset.PhysicsMaterial.Guid))
          container->DynamicBody3D.PhysicsMaterial = asset.PhysicsMaterial;

        // Brush
        if (string.IsNullOrEmpty(asset.BrushData.Guid))
          continue;
        var brushData = asset.BrushData.Instance;
        if (brushData == null)
          continue;

        var brush = f.CreateBrush();
        brush->Fields.Container = container->EntityRef;
        brush->Fields.Hand = EntityRefBodyPart.None;
        brush->Fields.BrushData = brushData;
      }
    }

    public override void Update(Frame f)
    {
      var containers = f.GetAllContainers();
      while (containers.Next())
      {
        var container = containers.Current;

        if (container->Fields.RespawnTimer > 0 && f.Number > container->Fields.RespawnTimer)
        {
          ResetContainer(f, container);
        }

        if (container->Fields.Controller == PlayerRef.None)
          continue;

        var hand = f.GetBodyPart(container->Fields.Hand);

        var objectAsset = container->Fields.ObjectData;

        if (objectAsset == null || !objectAsset.SnapToHand)
        {
          container->Transform3D.Position =
            hand->Transform3D.Position +
            hand->Transform3D.Rotation * hand->Fields.HoldingInfo.PositionOffset;
          container->Transform3D.Rotation = FPQuaternion.Multiply(
            hand->Fields.HoldingInfo.RotationOffset,
            hand->Transform3D.Rotation);
        }
        else
        {
          container->Transform3D.Position = hand->Transform3D.Position;
          container->Transform3D.Rotation = hand->Transform3D.Rotation;
        }
      }
    }

    public void OnCollisionDynamic(Frame f, DynamicCollisionInfo info)
    {
    }

    public void OnCollisionStatic(Frame f, StaticCollisionInfo info)
    {
      var entity = f.GetEntity(info.Entity->EntityRef);
      var first = Entity.CastToContainer(entity);

      if (first == null)
        return;

      // self destruct in 10 seconds
      if (first->Fields.RespawnTimer < 0 && info.StaticData.Tag == "Floor")
        first->Fields.RespawnTimer = f.Number + 10 * f.SimulationRate;

      if (first->Fields.LastCollisionId != info.StaticData.Layer)
      {
        first->Fields.LastCollisionId = info.StaticData.Layer;
        f.Events.ContainerHitStatic(first->EntityRef, info.StaticData.Layer);
      }
    }

    private void ResetContainer(Frame f, Container* container)
    {
      container->Transform3D.Position = container->Fields.InitialPosition;
      container->Transform3D.Rotation = container->Fields.InitialRotation;
    }
  }
}