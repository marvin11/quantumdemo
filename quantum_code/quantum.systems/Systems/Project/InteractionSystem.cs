using System;
using Photon.Deterministic;
using Quantum.Commands;

namespace Quantum.Core
{
  public unsafe class InteractionSystem : SystemBase, ISignalOnContainerGrabbed, ISignalOnContainerReleased, ISignalOnTogglePointer
  {
    public override void Update(Frame f)
    {
      var players = f.GetAllPlayers();
      while (players.Next())
      {
        var p = players.Current;
        var command = f.GetPlayerCommand(p->Fields.Owner);

        var input = f.GetPlayerInput(p->Fields.Owner);

        if (input->LeftButton.WasPressed)
          HandleGrab(f, p, new GrabCommand { LeftHand = true });
        if (input->RightButton.WasPressed)
          HandleGrab(f, p, new GrabCommand { LeftHand = false });

        if (input->LeftButton.WasReleased)
          HandleThrow(f, p, new ThrowCommand { LeftHand = true, Velocity = input->LeftVelocity });
        if (input->RightButton.WasReleased)
          HandleThrow(f, p, new ThrowCommand { LeftHand = false, Velocity = input->RightVelocity });

        if (p->Fields.HiFiveCooldown <= FP._0)
        {
	        HandleHighFive(f, p->Fields.LeftHand);
	        HandleHighFive(f, p->Fields.RightHand);
        } 
        else
        {
	        p->Fields.HiFiveCooldown -= f.DeltaTime;
        }
        
        
        switch (command)
        {
	        case GrabCommand grabCommand:
	          HandleGrab(f, p, grabCommand);
	          break;
	        case ThrowCommand throwCommand:
	          HandleThrow(f, p, throwCommand);
	          break;
        }
      }
    }

    private void HandleGrab(Frame f, Player* player, GrabCommand command)
    {
      var hand = f.GetBodyPart(command.LeftHand ? player->Fields.LeftHand : player->Fields.RightHand);

      using (var hits = f.Scene3D.OverlapShape(hand->Transform3D.Position,
        FPQuaternion.Identity,
        DynamicShape3D.CreateSphere(FP._0_25)))
      {
        if (hits.Count == 0)
          return;

        var entity = (Entity*)hits[0].Entity;
        var container = Entity.CastToContainer(entity);

        if (container == null || (container->Fields.Controller != PlayerRef.None &&
                                   container->Fields.Controller != player->Fields.Owner))
          return;

        if (container->Fields.Controller == player->Fields.Owner)
          return;

        f.Signals.OnContainerGrabbed(container, player, hand, command.LeftHand);
      }
    }

    private void HandleThrow(Frame f, Player* player, ThrowCommand command)
    {
      var hand = command.LeftHand ? player->Fields.LeftHand : player->Fields.RightHand;
      var containers = f.GetAllContainers();
      while (containers.Next())
      {
        var container = containers.Current;

        if (container->Fields.Controller != player->Fields.Owner || hand != container->Fields.Hand)
          continue;

        f.Signals.OnContainerReleased(container);

        container->DynamicBody3D.AddForce(command.Velocity * FP._100);
        return;
      }
    }

    bool IsHandOpen(HandAnimationInfo animationInfo)
    {
	    return
		    animationInfo.Thumb < FP._0_10 &&
		    animationInfo.Index < FP._0_10 &&
		    animationInfo.Middle < FP._0_10 &&
		    animationInfo.Ring < FP._0_10 &&
		    animationInfo.Pinky < FP._0_10;
    }
    
    private void HandleHighFive(Frame f, EntityRefBodyPart handRef)
    {
	    var hand = f.GetBodyPart(handRef);
	    var player = f.GetPlayer(hand->Fields.Owner);
	    var playerInputFlags = f.GetPlayerInputFlags(player->Fields.Owner);

	    if (!IsHandOpen(hand->Fields.AnimationInfo) ||
	        (playerInputFlags & DeterministicInputFlags.PlayerNotPresent) == DeterministicInputFlags.PlayerNotPresent ||
	        hand->Transform3D.Position.Magnitude < FP._1)
		    return;

	    var hiFiveLayerMask = Layers.GetLayerMask("Hands");
	    using (var hits = f.Scene3D.OverlapShape(hand->Transform3D, hand->DynamicBody3D.GetShape(), hiFiveLayerMask,
		    DynamicScene3D.OverlapOptions.SkipStatic))
	    {
		    foreach (var hit in hits)
		    {
			    if (hit.Entity == null)
				    continue;

			    var bodyPart = Entity.CastToBodyPart((Entity*) hit.Entity);
			    if (bodyPart == null ||
			        !bodyPart->Fields.IsHand ||
			        bodyPart->EntityRef == hand->EntityRef ||
			        bodyPart->Fields.Owner == hand->Fields.Owner)
				    continue;

			    if (IsHandOpen(bodyPart->Fields.AnimationInfo))
			    {
				    f.Events.OnHighFive(hand->Transform3D.Position);
				    var p1 = f.GetPlayer(hand->Fields.Owner);
				    var p2 = f.GetPlayer(bodyPart->Fields.Owner);

				    p1->Fields.HiFiveCooldown = FP._10 + FP._10;
				    p2->Fields.HiFiveCooldown = FP._10 + FP._10;
				    return;
			    }
		    }
	    }
    }

    public void OnContainerGrabbed(Frame f, Container* container, Player* player, BodyPart* hand, Boolean leftHand)
    {
      container->DynamicBody3D.InitKinematic(container->DynamicBody3D.GetShape());

      container->Fields.Controller = player->Fields.Owner;
      container->Fields.Hand = hand->EntityRef;
      container->Fields.HeldOnLeft = leftHand;

      container->Fields.RespawnTimer = -1;
      container->Fields.LastCollisionId = -1;

      var inverseHandRotation = FPQuaternion.Inverse(hand->Transform3D.Rotation);
      hand->Fields.HoldingInfo.Holding = true;
      hand->Fields.HoldingInfo.PositionOffset = inverseHandRotation * (container->Transform3D.Position - hand->Transform3D.Position);
      hand->Fields.HoldingInfo.RotationOffset = FPQuaternion.Multiply(container->Transform3D.Rotation, inverseHandRotation);

      container->DynamicBody3D.Velocity = FPVector3.Zero;
      container->DynamicBody3D.AngularVelocity = FPVector3.Zero;

      f.Events.OnContainerPickup(player->Fields.Owner, container->EntityRef);
    }

    public void OnContainerReleased(Frame f, Container* container)
    {
      container->DynamicBody3D.InitDynamic(container->DynamicBody3D.GetShape(), FP._1);
      container->Fields.Controller = PlayerRef.None;

      var hand = f.GetBodyPart(container->Fields.Hand);
      hand->Fields.HoldingInfo.Holding = false;

      container->Fields.Hand = EntityRefBodyPart.None;
      container->DynamicBody3D.Velocity = FPVector3.Zero;
      container->DynamicBody3D.AngularVelocity = FPVector3.Zero;
    }

    public void OnTogglePointer(Frame f, EntityRefPlayer player)
    {
      f.Events.TogglePointer(player);
    }
  }
}