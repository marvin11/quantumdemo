using System;
using Photon.Deterministic;
using Quantum.Commands;

namespace Quantum.Core
{
  public unsafe class PlayerSystem : SystemBase
  {
    public override void OnInit(Frame f)
    {
      for (int i = 0; i < f.Map.StaticColliders3D.Length; i++)
      {
        var collider = f.Map.StaticColliders3D[i];
        var asset = DB.FindAsset<PlayerPivotData>(collider.StaticData.Asset.Guid);

        if (asset != null)
        {
          var point = f.CreatePlayerPivot();

          point->Transform3D.Position = collider.Position;
          point->Transform3D.Rotation = collider.Rotation;
          point->Fields.Occupied = false;
        }
      }

      for (int i = 0; i < f.PlayerCount; i++)
      {
        CreatePlayer(f, i);
      }
    }

    public void CreatePlayer(Frame f, PlayerRef player)
    {
      var p = f.CreatePlayer();
      var head = f.CreateBodyPart();
      var leftHand = f.CreateBodyPart();
      var rightHand = f.CreateBodyPart();

      var points = f.GetAllPlayerPivots();

      PlayerPivot* chosenPoint = null;
      while (points.Next())
      {
        chosenPoint = points.Current;
        if (!chosenPoint->Fields.Occupied)
        {
          chosenPoint->Fields.Occupied = true;
          break;
        }
      }

      p->Transform3D.Position = chosenPoint->Transform3D.Position;
      p->Transform3D.Rotation = chosenPoint->Transform3D.Rotation;

      p->Fields.Owner = player;
      p->Fields.Head = head->EntityRef;

      p->Fields.LeftHand = leftHand->EntityRef;
      p->Fields.RightHand = rightHand->EntityRef;

      p->Fields.HiFiveCooldown = FP._10;
      
      leftHand->Fields.Owner = p->EntityRef;
      rightHand->Fields.Owner = p->EntityRef;
      
      leftHand->Prefab.Current = "QuantumLeftHand";
      rightHand->Prefab.Current = "QuantumRightHand";

      leftHand->Fields.IsHand = true;
      rightHand->Fields.IsHand = true;

      var handExtents = new FPVector3(FP._0_03, FP._0_04 + FP._0_03, FP._0_20);

      head->Transform3D.Rotation = FPQuaternion.Identity;
      leftHand->Transform3D.Rotation = FPQuaternion.Identity;
      rightHand->Transform3D.Rotation = FPQuaternion.Identity;
      
      leftHand->DynamicBody3D.Layer = Layers.GetLayerIndex("Hands");
      rightHand->DynamicBody3D.Layer = Layers.GetLayerIndex("Hands");
      
      leftHand->DynamicBody3D.InitKinematic(DynamicShape3D.CreateBox(handExtents));
      rightHand->DynamicBody3D.InitKinematic(DynamicShape3D.CreateBox(handExtents));

      
      f.Events.PlayerCreated(p->EntityRef);
    }

    public override void Update(Frame f)
    {
      var players = f.GetAllPlayers();
      while (players.Next())
      {
        var p = players.Current;

        var input = f.GetPlayerInput(p->Fields.Owner);

        SetTransformValues(f, p->Transform3D.Position, p->Fields.Head, input->HeadPos, input->HeadRotation);
        SetTransformValues(f, p->Transform3D.Position, p->Fields.LeftHand, input->LeftHandPos, input->LeftHandRotation);
        SetTransformValues(f, p->Transform3D.Position, p->Fields.RightHand, input->RightHandPos, input->RightHandRotation);

        HandlePlayerCommands(f, p);

        DecodeHandAnimations(f, p->Fields.LeftHand, input->LeftHandAnimation);
        DecodeHandAnimations(f, p->Fields.RightHand, input->RightHandAnimation);
        
        // turn pointer on
        if(input->Pad.WasPressed && f.IsVerified)
        {
          f.Signals.OnTogglePointer(p->EntityRef);
          Log.Info("Pressed Pointer Button");
        }

        // Toggle Drwaing cooldown
        if (p->Fields.ToggleDrawingCooldown > FP._0)
	        p->Fields.ToggleDrawingCooldown -= f.DeltaTime;
        
        p->Fields.TeleportData.Teleporting = false;
        HandleTeleport(f, p, p->Fields.LeftHand, ref input->LeftTeleport, ref input->LeftTeleportAngle,
	        ref p->Fields.TeleportData);
        HandleTeleport(f, p, p->Fields.RightHand, ref input->RightTeleport, ref input->RightTeleportAngle,
	        ref p->Fields.TeleportData);
      }
    }

    private void DecodeHandAnimations(Frame f, EntityRefBodyPart handEntity, uint bits)
    {
	    int BITMASK = ((1 << 6) - 1);
	    var hand = f.GetBodyPart(handEntity);

	    if (bits >> 30 == 0)
		    return;

	    hand->Fields.AnimationInfo.Thumb = DecompressFingerData((int) ((bits >> 24) & BITMASK), BITMASK);
	    hand->Fields.AnimationInfo.Index = DecompressFingerData((int) ((bits >> 18) & BITMASK), BITMASK);
	    hand->Fields.AnimationInfo.Middle = DecompressFingerData((int) ((bits >> 12) & BITMASK), BITMASK);
	    hand->Fields.AnimationInfo.Ring = DecompressFingerData((int) ((bits >> 6) & BITMASK), BITMASK);
	    hand->Fields.AnimationInfo.Pinky = DecompressFingerData((int) ((bits >> 0) & BITMASK), BITMASK);
    }

    private void HandlePlayerCommands(Frame f, Player* p)
    {
	    var cmd = f.GetPlayerCommand(p->Fields.Owner);
	    
	    switch (cmd)
	    {
		    case ReactionCommand react:
		    {
			    var reactions = DB.FindAllAssets<ReactionData>();
			    var i = 0;
			    for (; i < reactions.Length; i++)
			    {
				    if (reactions[i].Guid == react.ReactionId)
					    break;
			    }

			    f.Events.OnReactionSent(p->Fields.Owner, i);
			    break;
		    }
		    case PlayVideoCommand _:
			    f.Signals.OnVideoPlay();
			    break;
		    case StopVideoCommand _:
			    f.Signals.OnVideoStop();
			    break;
		    case ToggleDrawingModeCommand _:
			    f.Signals.OnToggleDrawing(p);
			    break;
	    }
    }

    private void SetTransformValues(Frame f, FPVector3 center, EntityRef entity, uint pos, uint rot)
    {
      if (pos == 0)
        return;

      var ent = f.GetBodyPart(entity);

      ent->Transform3D.Position = center + DecompressVector(pos);
      ent->Transform3D.Rotation = DecompressQuaternion(rot);
    }

    private static void HandleTeleport(Frame f, Player* p, EntityRefBodyPart hand, ref Button teleportButton,
	    ref Byte teleportAngle, ref TeleportData teleportData)
    {
	    var h = f.GetBodyPart(hand);
	    var forward = h->Transform3D.Rotation * FPVector3.Forward;

	    bool hasTarget = false;
	    FPVector3 pos = FPVector3.Zero;
	    bool isValid = true;

	    var mask = Layers.GetLayerMask("TeleportMesh", "TeleportBlockerMesh");
	    using (var hits = f.Scene3D.Raycast(h->Transform3D.Position, forward, FP._100, mask,
		    DynamicScene3D.LinecastOptions.SkipDynamic))
	    {
		    for (int i = 0; i < hits.Count; i++)
		    {
			    if (hits[i].Entity == null)
			    {
				    hasTarget = true;
				    pos = hits[i].Point;

				    if (hits[i].StaticData.Layer == Layers.GetLayerIndex("TeleportBlockerMesh"))
				    {
					    isValid = false;
				    }
			    }
		    }
	    }

	    FP totalAngle = 360;
	    var angleStep = totalAngle / Byte.MaxValue;
	    var tpAngle = (teleportAngle) * angleStep;
	    
	    var validTeleport = teleportButton.IsDown && hasTarget;
	    if (validTeleport)
	    {
		    teleportData.Teleporting = true;
		    teleportData.Position = pos;
		    teleportData.Source = hand;
		    teleportData.Valid = isValid;
		    teleportData.Angle = tpAngle;
	    }

	    if (teleportButton.WasReleased && hasTarget && isValid)
	    {
		    p->Transform3D.Position = pos;
		    p->Transform3D.Rotation = p->Transform3D.Rotation * FPQuaternion.AngleAxis(teleportData.Angle, FPVector3.Up);
	    }
    }

    private FP DecompressFingerData(int bits, int maxValue)
    {
	    FP maxFp = maxValue;
	    FP val = bits;

	    return val / maxFp;
    }

    public FPQuaternion DecompressQuaternion(uint rot)
    {
      const uint BITMASK = ((1 << 10) - 1);

      var x = DecompressQuaternionFP((rot >> 20) & BITMASK);
      var y = DecompressQuaternionFP((rot >> 10) & BITMASK);
      var z = DecompressQuaternionFP((rot >> 0) & BITMASK);

      return RebuildQuaternion(rot >> 30, x, y, z);
    }

    private FPQuaternion RebuildQuaternion(uint index, FP x, FP y, FP z)
    {
      var w = FPMath.Sqrt(FP._1 - (x * x + y * y + z * z));

      switch (index)
      {
      case 0:
        return new FPQuaternion(w, x, y, z);
      case 1:
        return new FPQuaternion(x, w, y, z);
      case 2:
        return new FPQuaternion(x, y, w, z);
      case 3:
        return new FPQuaternion(x, y, z, w);
      }
      return FPQuaternion.Identity;
    }

    private FP DecompressQuaternionFP(uint bits)
    {
      const uint BITMASK = ((1 << 9) - 1);
      var ret = FP.FromRaw((bits & BITMASK) << 7);

      if ((bits >> 9) > 0)
        return -ret;
      return ret;
    }

    public FPVector3 DecompressVector(uint pos)
    {
      const uint BITMASK = ((1 << 10) - 1);
      return new FPVector3
      {
        X = DecompressFP((pos >> 20) & BITMASK),
        Y = DecompressFP((pos >> 10) & BITMASK),
        Z = DecompressFP(pos & BITMASK)
      };
    }

    private FP DecompressFP(uint bits)
    {
      var ret = FP.FromRaw(bits << 9);
      return ret - 4;
    }
  }
}