namespace Quantum.Core
{
  public unsafe class VideoPlayerSystem : SystemBase, ISignalOnVideoPlay, ISignalOnVideoStop
  {
    public override void Update(Frame f)
    {
      if (f.Global->VideoPlayer.Playing)
        f.Global->VideoPlayer.Runtime += f.DeltaTime;
    }

    public void OnVideoPlay(Frame f)
    {
      f.Global->VideoPlayer.Playing = !f.Global->VideoPlayer.Playing;
    }

    public void OnVideoStop(Frame f)
    {
      f.Global->VideoPlayer.Playing = false;
      f.Global->VideoPlayer.Runtime = 0;
    }
  }
}