using UnityEngine;

public sealed class AudioLevelWatcher : MonoBehaviour
{
  public AudioSource source;

  [Delayed]
  public int bufferSize = 512;
  public float changeRate = 0.001f;
  public AnimationCurve curve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

  [System.NonSerialized]
  public float level;
  [System.NonSerialized]
  public float maxLevel;

  private float[] buffer;

  private void Reset()
  {
    source = GetComponent<AudioSource>();
  }

  private void OnValidate()
  {
    bufferSize = Mathf.ClosestPowerOfTwo(Mathf.Max(bufferSize, 16));
    if (Application.isPlaying && (buffer == null || bufferSize != buffer.Length))
      buffer = new float[bufferSize];
    changeRate = Mathf.Max(changeRate, 0);
  }

  private void Awake()
  {
    buffer = new float[bufferSize];
  }

  private void Update()
  {
    var mean = 0.0f;
    var max = 0.0f;

    if (source.isPlaying)
    {
      source.GetOutputData(buffer, 0);
      foreach (var sample in buffer)
      {
        mean += sample * sample;
        max = Mathf.Max(max, Mathf.Abs(sample));
      }

      mean = Mathf.Sqrt(mean / buffer.Length);
    }

    mean = curve.Evaluate(mean);
    max = curve.Evaluate(max);

    var maxDelta = changeRate * Time.deltaTime;
    level = Mathf.MoveTowards(level, mean, maxDelta);
    maxLevel = Mathf.MoveTowards(maxLevel, max, maxDelta);
  }
}
