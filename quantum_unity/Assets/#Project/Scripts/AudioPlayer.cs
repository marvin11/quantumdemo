using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class AudioPlayer
{
	private AudioSource audioSource;

	public void Setup(AudioSource source)
	{
		audioSource = source;
	}
	
	public void PlayRandomSound(List<AudioClip> clips)
	{
		var clip = clips[Random.Range(0, clips.Count)];
		audioSource.clip = clip;

		audioSource.Play();
	}
}