using System.Collections;
using UnityEngine;

public class AutoDestruct : MonoBehaviour
{
	public bool timed = true;
	public float destructionDelay;

	IEnumerator Start()
	{
		if(!timed)
			yield break;
		
		yield return new WaitForSeconds(destructionDelay);
		SelfDestruct();
	}

	public void SelfDestruct()
	{
		Destroy(gameObject);
	}
}