using BitStrap;
using UnityEngine;
using Animator = UnityEngine.Animator;

public sealed class CompanionAnimator : MonoBehaviour
{
	public Animator companionAnimator;

	[AnimatorField("companionAnimator")]
	public FloatAnimationParameter speedParameter;

	private EntityPrefabRoot prefabRoot;

	void Start()
	{
		prefabRoot = GetComponentInChildren<EntityPrefabRoot>();
	}
	
	unsafe void Update()
	{
		if (QuantumRunner.Default.Game == null)
			return;

		var companion = QuantumRunner.Default.Game.Frames.Current.GetCompanion(prefabRoot.EntityRef);

		speedParameter.Set(companionAnimator, companion->NavMeshAgent.CurrentVelocity.Magnitude.AsFloat);
	}
}
