using UnityEngine;
using UnityEngine.UI;

public partial class ReactionDataAsset
{
	public GameObject reactionPrefab;
	public Sprite reactionSprite;
}