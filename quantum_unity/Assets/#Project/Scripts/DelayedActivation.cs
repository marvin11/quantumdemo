using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedActivation : MonoBehaviour
{
	public List<GameObject> objects;
	public float delay;

	IEnumerator Start()
	{
		yield return new WaitForSeconds(delay);

		foreach (var o in objects)
		{
			o.SetActive(true);
		}
	}

}