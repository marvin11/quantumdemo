using System.Collections;
using System.Collections.Generic;
using Quantum;
using UnityEngine;
using System.Threading;

public sealed unsafe class DrawingBoardView : MonoBehaviour
{
  private struct DrawEvent
  {
    public Color color;
    public float radius;
    public Vector2 extents;
    public Vector3 fromUv;
    public Vector3 toUv;
  }

  public int textureSize = 512;
  public EntityPrefabRoot root;
  public Renderer boardRenderer;

  public AudioSource audioSource;

  public AudioClip drawStartSfx;
  public AudioClip drawLoopSfx;

  private Texture2D canvas;
  private List<DrawEvent> drawEvents = new List<DrawEvent>();
  private List<DrawEvent> mainThreadDrawEvents = new List<DrawEvent>();

  private float drawingTime = -1;

  private void OnValidate()
  {
    textureSize = Mathf.NextPowerOfTwo(Mathf.Max(textureSize, 64));
  }

  private void Reset()
  {
    root = GetComponent<EntityPrefabRoot>();
    boardRenderer = GetComponent<Renderer>();
  }

  private void Awake()
  {
    var board = QuantumRunner.Default.Game.Frames.Current.GetDrawingBoard(root.EntityRef);
    canvas = new Texture2D(textureSize, textureSize, TextureFormat.RGBA32, false, true);
    var data = canvas.GetRawTextureData<byte>();
    for (var i = 0; i < data.Length; i++)
      data[i] = byte.MaxValue;
    canvas.Apply();
    boardRenderer.material.mainTexture = canvas;

    EventDrawOnBoard.OnRaised += OnDrawOnBoard;
  }

  private void OnDestroy()
  {
    EventDrawOnBoard.OnRaised += OnDrawOnBoard;
  }

  private void OnDrawOnBoard(EventDrawOnBoard ev)
  {
    if (root.EntityRef != ev.drawingBoard)
      return;

    var board = QuantumRunner.Default.Game.Frames.Current.GetDrawingBoard(root.EntityRef);
    var brush = QuantumRunner.Default.Game.Frames.Current.GetBrush(ev.brush);
    var brushDataAsset = UnityDB.FindAsset<BrushDataAsset>(brush->Fields.BrushData.Id);

    var drawEvent = new DrawEvent
    {
      color = brushDataAsset.BrushColor,
      radius = brushDataAsset.Settings.Radius.AsFloat,
      extents = board->DynamicBody3D.GetShape().Box.Extents.XY.ToUnityVector2(),
      fromUv = ev.fromUv.ToUnityVector3(),
      toUv = ev.toUv.ToUnityVector3(),
    };

    lock (drawEvents)
    {
      drawEvents.Add(drawEvent);
    }
  }

  private void Update()
  {
    if (drawingTime > 0)
    {
      drawingTime -= Time.deltaTime;
    }
    if (drawingTime <= 0 && audioSource.isPlaying)
      audioSource.Stop();

    mainThreadDrawEvents.Clear();

    if (!Monitor.TryEnter(drawEvents))
      return;

    foreach (var ev in drawEvents)
      mainThreadDrawEvents.Add(ev);
    drawEvents.Clear();
    Monitor.Exit(drawEvents);

    if (mainThreadDrawEvents.Count > 0)
    {
      var data = canvas.GetPixels();
      foreach (var ev in mainThreadDrawEvents)
        DrawOnBoard(ref data, ev);
      canvas.SetPixels(data);
      canvas.Apply();
    }
  }

  private void DrawOnBoard(ref Color[] textureData, DrawEvent ev)
  {
    var fromBrushRadius = ev.radius * ev.fromUv.z;
    var fromBrushWidth = Mathf.CeilToInt(fromBrushRadius * textureSize / ev.extents.x);
    var fromBrushHeight = Mathf.CeilToInt(fromBrushRadius * textureSize / ev.extents.y);

    var toBrushRadius = ev.radius * ev.toUv.z;
    var toBrushWidth = Mathf.CeilToInt(toBrushRadius * textureSize / ev.extents.x);
    var toBrushHeight = Mathf.CeilToInt(toBrushRadius * textureSize / ev.extents.y);

    var maxIndex = textureSize - 1;

    var fromCenter = new Vector2Int(
      Mathf.RoundToInt(ev.fromUv.x * maxIndex),
      Mathf.RoundToInt(ev.fromUv.y * maxIndex)
    );
    var fromMinX = Mathf.Max(fromCenter.x - fromBrushWidth, 0);
    var fromMaxX = Mathf.Min(fromCenter.x + fromBrushWidth, maxIndex);
    var fromMinY = Mathf.Max(fromCenter.y - fromBrushHeight, 0);
    var fromMaxY = Mathf.Min(fromCenter.y + fromBrushHeight, maxIndex);

    var toCenter = new Vector2Int(
      Mathf.RoundToInt(ev.toUv.x * maxIndex),
      Mathf.RoundToInt(ev.toUv.y * maxIndex)
    );
    var toMinX = Mathf.Max(toCenter.x - toBrushWidth, 0);
    var toMaxX = Mathf.Min(toCenter.x + toBrushWidth, maxIndex);
    var toMinY = Mathf.Max(toCenter.y - toBrushHeight, 0);
    var toMaxY = Mathf.Min(toCenter.y + toBrushHeight, maxIndex);

    var minX = Mathf.Min(fromMinX, toMinX);
    var minY = Mathf.Min(fromMinY, toMinY);
    var maxX = Mathf.Max(fromMaxX, toMaxX);
    var maxY = Mathf.Max(fromMaxY, toMaxY);

    var fromPixelSqrRadius = (fromBrushWidth * fromBrushWidth + fromBrushHeight * fromBrushHeight) / 4;
    var toPixelSqrRadius = (toBrushWidth * toBrushWidth + toBrushHeight * toBrushHeight) / 4;

    var diff = toCenter - fromCenter;
    var distanceSqr = Mathf.Max(Dot(diff, diff), 1);

    for (var x = minX; x <= maxX; x++)
      for (var y = minY; y <= maxY; y++)
      {
        var distanceSquared = DistanceToLineSegmentSquared(
          fromCenter, toCenter,
          new Vector2Int(x, y),
          out var projectionDot
        );

        var pixelSqrRadius = fromPixelSqrRadius + (toPixelSqrRadius - fromPixelSqrRadius) * projectionDot / distanceSqr;

        if (distanceSquared <= pixelSqrRadius)
          textureData[x + textureSize * y] = ev.color;
      }

    if (drawingTime < 0)
    {
      StartCoroutine(StartDrawingSfx());
    }
    drawingTime = 0.33f;
  }

  private IEnumerator StartDrawingSfx()
  {
    audioSource.clip = drawStartSfx;
    audioSource.loop = false;
    audioSource.Play();

    yield return new WaitForSeconds(drawStartSfx.length);

    audioSource.clip = drawLoopSfx;
    audioSource.loop = true;
    audioSource.Play();
  }

  private static int DistanceToLineSegmentSquared(Vector2Int a, Vector2Int b, Vector2Int p, out int projectionDot)
  {
    var t = b - a;

    var t2 = Dot(t, t);
    if (t2 == 0)
    {
      projectionDot = 0;
      return int.MaxValue;
    }

    var ap = p - a;
    var d = Dot(t, ap);
    if (d < 0)
    {
      projectionDot = 0;
      return Dot(ap, ap);
    }

    var pb = b - p;
    if (Dot(t, pb) < 0)
    {
      projectionDot = t2;
      return Dot(pb, pb);
    }

    projectionDot = d;
    var n = ap - Div(Mult(t, d), t2);
    return Dot(n, n);
  }

  private static int Dot(Vector2Int a, Vector2Int b)
  {
    return a.x * b.x + a.y * b.y;
  }

  private static Vector2Int Mult(Vector2Int v, int s)
  {
    return new Vector2Int(v.x * s, v.y * s);
  }

  private static Vector2Int Div(Vector2Int v, int s)
  {
    return new Vector2Int(v.x / s, v.y / s);
  }
}