using UnityEngine;
using UnityEditor;
using BitStrap;

[CustomEditor(typeof(AudioLevelWatcher))]
public sealed class AudioLevelWatcherEditor : Editor
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();

    var self = target as AudioLevelWatcher;

    var rect = GUILayoutUtility.GetRect(GUIContent.none, EditorStyles.label);
    EditorGUI.ProgressBar(rect, self.level, "Audio Level");

    Handles.color = Color.red;
    var x = Mathf.Lerp(rect.xMin, rect.xMax, self.maxLevel);
    Handles.DrawLine(new Vector2(x, rect.yMin), new Vector2(x, rect.yMax));

    using (var v = new GUILayout.VerticalScope(EditorStyles.helpBox))
    using (var d = DisabledGroup.Do(true))
    {
      EditorGUILayout.FloatField("Level", self.level);
      EditorGUILayout.FloatField("Max Level", self.maxLevel);
    }

    if (Application.isPlaying)
      Repaint();
  }
}
