using UnityEngine;

public sealed class GazeTrigger : MonoBehaviour
{
	public static Transform GazeTransform;

	public float angleThreshold = 15;

	public bool isGazing = false;
	
	void Update()
	{
		CheckGazeState();
	}

	private void CheckGazeState()
	{
		if (GazeTransform == null)
		{
			isGazing = false;
			return;
		}

		var lookVector = GazeTransform.forward;
		var targetVector = transform.forward;

		var dot = Vector3.Dot(targetVector, lookVector);
		if (dot < 0)
		{
			isGazing = false;
			return;
		}

		var angle = Vector3.Angle(lookVector, targetVector);
		isGazing = Mathf.Abs(angle) < angleThreshold;
	}
}
