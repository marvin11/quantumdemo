using System.Collections.Generic;
using BitStrap;
using Quantum;
using UnityEngine;

public class HiFiveFxController : MonoBehaviour
{
	public GameObject visualFxPrefab;
	public List<AudioClip> hiFiveSfx;

	public AudioPlayer audioPlayer = new AudioPlayer();
	
	void Start()
	{
		EventOnHighFive.OnRaised += OnHighFive;
		audioPlayer.Setup(GetComponentInChildren<AudioSource>());
	}

	private void OnDestroy()
	{
		EventOnHighFive.OnRaised -= OnHighFive;
	}

	private void OnHighFive(EventOnHighFive ev)
	{
		audioPlayer.PlayRandomSound(hiFiveSfx);
		Instantiate(visualFxPrefab, ev.Position.ToUnityVector3(), Quaternion.identity);
	}
}