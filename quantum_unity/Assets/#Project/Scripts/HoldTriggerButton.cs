using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public sealed class HoldTriggerButton : MonoBehaviour
{
	private const float MinimumTimeToReceiveEvents = 0.5f;

	public float holdTime;
	public LayerMask triggerMask;
	
	public UnityEvent onButtonTriggered;

	public float HoldRatio => timeHolding / holdTime;
	
	private float timeActive = 0;
	private float timeHolding = 0;

	private List<Collider> holders = new List<Collider>();
	private bool triggered = false;
	
	void Update()
	{
		timeActive += Time.deltaTime;

		if (timeActive >= MinimumTimeToReceiveEvents && holders.Count > 0)
		{
			timeHolding += Time.deltaTime;
			if (timeHolding > holdTime && !triggered)
			{
				triggered = true;
				onButtonTriggered.Invoke();
			}
		}

		if (holders.Count == 0)
		{
			timeHolding = 0;
			triggered = false;
		}
	}
	
	void OnEnable()
	{
		timeActive = 0;
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if (((1 << other.gameObject.layer) & triggerMask) == 0) 
			return;

		holders.Add(other);
	}

	private void OnTriggerExit(Collider other)
	{
		if (holders.Contains(other))
			holders.Remove(other);
	}
}