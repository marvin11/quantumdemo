using UnityEngine;
using UnityEngine.UI;

public sealed class HoldTriggerFeedback : MonoBehaviour
{
	public HoldTriggerButton target;

	public Image fillImage;

	void Update()
	{
		fillImage.fillAmount = target.HoldRatio;
	}
}