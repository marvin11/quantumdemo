using System;
using System.Collections.Generic;
using Quantum;
using UnityEngine;

public class InteractableObjectView : MonoBehaviour
{
	public List<AudioClip> hitSoundEffects;

	public AudioPlayer audioPlayer = new AudioPlayer();
	
	private EntityPrefabRoot root;

	private unsafe void Start()
	{
		root = GetComponent<EntityPrefabRoot>();

		var interactable = QuantumRunner.Default.Game.Frames.Current.GetContainer(root.EntityRef);
		var asset = UnityDB.FindAsset<InteractableObjectDataAsset>(interactable->Fields.ObjectData.Guid);

		transform.localScale = Vector3.one * asset.Settings.ScaleFactor.AsFloat;

		audioPlayer.Setup(GetComponentInChildren<AudioSource>());

		EventContainerHitStatic.OnRaised += OnContainerHitStatic;
	}

	private void OnDestroy()
	{
		EventContainerHitStatic.OnRaised -= OnContainerHitStatic;
	}

	private void OnContainerHitStatic(EventContainerHitStatic obj)
	{
		if (obj.target != root.EntityRef)
			return;

		audioPlayer.PlayRandomSound(hitSoundEffects);
	}

	private unsafe void LateUpdate()
	{
		var quantumGame = QuantumRunner.Default.Game;
		var container = QuantumRunner.Default.Game.Frames.Current.GetContainer(root.EntityRef);

		root.blockEngineUpdate = quantumGame.PlayerIsLocal(container->Fields.Controller);
		if (quantumGame.PlayerIsLocal(container->Fields.Controller))
		{
			if (container->Fields.HeldOnLeft)
			{
				transform.position = SandboxPlayerSpawner.LocalPlayer.LeftHandPosition;
				transform.rotation = SandboxPlayerSpawner.LocalPlayer.LeftHandRotation;
			}
			else
			{
				transform.position = SandboxPlayerSpawner.LocalPlayer.RightHandPosition;
				transform.rotation = SandboxPlayerSpawner.LocalPlayer.RightHandRotation;
			}

			if (!container->Fields.ObjectData.SnapToHand)
			{
				var hand = QuantumRunner.Default.Game.Frames.Current.GetBodyPart(container->Fields.Hand);
				transform.position +=
					(hand->Transform3D.Rotation * hand->Fields.HoldingInfo.PositionOffset).ToUnityVector3();
				transform.rotation = transform.rotation * hand->Fields.HoldingInfo.RotationOffset.ToUnityQuaternion();
			}
		}
	}
}