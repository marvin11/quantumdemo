using Quantum;
using Quantum.Core;
using UnityEngine;

public class ObjectPreviewer : MonoBehaviour
{
  private void Awake()
  {
    Destroy(gameObject);
  }

  private void OnDrawGizmos()
  {
    var dataAsset = GetComponent<QuantumStaticBoxCollider3D>();
    var assetGuid = dataAsset.Settings.Asset.Guid;

    {
      var assets = Resources.FindObjectsOfTypeAll<InteractableObjectDataAsset>();
      var brushAssets = Resources.FindObjectsOfTypeAll<BrushDataAsset>();
      foreach (var asset in assets)
      {
        if (asset.Settings.Guid != assetGuid)
          continue;

        var scale = asset.Settings.ScaleFactor.AsFloat;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.matrix *= Matrix4x4.Scale(new Vector3(scale, scale, scale));
        Gizmos.DrawMesh(asset.PreviewMesh);

        var extents = asset.Settings.Extents.ToUnityVector3() * scale;
        switch (asset.Settings.Shape)
        {
        case DynamicShape3DType.Sphere:
          GizmoUtils.DrawGizmosSphere(Vector3.zero, extents.x, false, QuantumEditorSettings.Instance.StaticColliderColor);
          break;
        case DynamicShape3DType.Box:
          GizmoUtils.DrawGizmosBox(transform, extents, false, QuantumEditorSettings.Instance.StaticColliderColor);
          break;
        }

        var brushAssetGuid = asset.Settings.BrushData.Guid;
        if (string.IsNullOrEmpty(brushAssetGuid))
          continue;
        foreach (var brushAsset in brushAssets)
        {
          if (brushAsset.Settings.Guid != brushAssetGuid)
            continue;

          var from = transform.position + transform.rotation * brushAsset.Settings.Offset.ToUnityVector3();
          var direction = transform.rotation * brushAsset.Settings.Direction.ToUnityVector3();
          var to = from + direction.normalized * brushAsset.Settings.RaycastMaxDistance.AsFloat;
          GizmoUtils.DrawGizmosSphere(
            from,
            brushAsset.Settings.Radius.AsFloat,
            false,
            QuantumEditorSettings.Instance.KinematicColliderColor
          );
          GizmoUtils.DrawGizmosSphere(
            to,
            brushAsset.Settings.Radius.AsFloat,
            false,
            QuantumEditorSettings.Instance.KinematicColliderColor
          );

          break;
        }

        break;
      }
    }

    {
      var assets = Resources.FindObjectsOfTypeAll<DrawingBoardDataAsset>();
      foreach (var asset in assets)
      {
        if (asset.Settings.Guid != assetGuid)
          continue;

        var scale = asset.Settings.ScaleFactor.AsFloat;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.matrix *= Matrix4x4.Scale(new Vector3(scale, scale, scale));
        Gizmos.DrawMesh(asset.PreviewMesh);

        var extents = asset.Settings.Extents.ToUnityVector3() * scale;
        GizmoUtils.DrawGizmosBox(transform, extents, false, QuantumEditorSettings.Instance.StaticColliderColor);
      }
    }

    Gizmos.matrix = Matrix4x4.identity;
  }
}