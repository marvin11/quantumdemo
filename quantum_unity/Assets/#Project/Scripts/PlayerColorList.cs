using System;
using System.Collections.Generic;
using Quantum;
using UnityEngine;

[CreateAssetMenu]
public class PlayerColorList : ScriptableObject
{
	[Serializable]
	public class RobotColorInfo
	{
		public Material robotMaterial;
		public Color handColor;
		public Color rimColor;
	}

	public List<RobotColorInfo> robotColors;

	public RobotColorInfo GetRobotColor(PlayerRef controller)
	{
		if (controller == PlayerRef.None)
			return robotColors[0];
		return robotColors[controller];
	}
}