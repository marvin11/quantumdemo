using System.Linq;
using Quantum;
using UnityEngine;

public class ReactionController : MonoBehaviour
{
	public Transform reactionPivot;
	
	private SandboxRemotePlayer remotePlayer;
	
	void Awake()
	{
		remotePlayer = GetComponent<SandboxRemotePlayer>();
	}

	void Start()
	{
		EventOnReactionSent.OnRaised += OnReactionSent;
	}

	private void OnDestroy()
	{
		EventOnReactionSent.OnRaised -= OnReactionSent;
	}

	private void OnReactionSent(EventOnReactionSent evt)
	{
		if (remotePlayer == null || evt.player != remotePlayer.playerReference)
			return;

		var reaction = UnityDB.AllOf<ReactionDataAsset>().ToList()[evt.reactionNumber];
		Instantiate(reaction.reactionPrefab, reactionPivot);
	}
}