using System.Collections;
using UnityEngine;

public sealed class ReactionSfx : MonoBehaviour
{
	public AudioClip[] sfxList;
	public float initialDelay;

	public AudioSource source;
	
	IEnumerator Start()
	{
		yield return new WaitForSeconds(initialDelay);

		source.clip = sfxList[Random.Range(0, sfxList.Length)];
		source.Play();
	}
}
