﻿using System.Collections.Generic;
using System.Linq;
using BitStrap;
using Quantum;
using Quantum.Commands;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Animator = UnityEngine.Animator;

public class SandboxPlayer : MonoBehaviour
{
	[Header("References")]
	public Transform head;
	public Transform leftHand;
	public Transform rightHand;

	[Header("Hands")]
	public VrHandsBase leftHandBaseScript;
	public VrHandsBase rightHandBaseScript;

	public VoiceRecorder voiceRecorderPrefab;

	public MovableUi movableUi;

	public EntityRef playerEntityRef;
	public PlayerRef playerReference;
	
	[Header("Reactions")]
	public Transform selfReactionPivot;
	public List<ReactionDataLink> reactions;

	protected EntityRef leftHandEntity;
	protected EntityRef rightHandEntity;

	protected Vector3 previousLeftPosition;
	protected Vector3 previousRightPosition;
	public Vector3 LeftVelocity { get; set; }
	public Vector3 RightVelocity { get; set; }

	public Vector3 CenterPosition { get; set; }
	
	private EntityPrefabViewUpdater prefabViewUpdater;
	private Transform selfReactionContainer;

	public virtual Vector3 LeftHandPosition => leftHand.position;
	public virtual Quaternion LeftHandRotation => leftHand.rotation;

	public virtual Vector3 RightHandPosition => rightHand.position;
	public virtual Quaternion RightHandRotation => rightHand.rotation;

	private PlayerColorList.RobotColorInfo robotColorInfo;
	
	protected virtual void Start()
	{
		GazeTrigger.GazeTransform = head;

		EventDrawOnBoard.OnRaised += OnBoardDrawn;

		EventOnReactionSent.OnRaised += OnReactionSent;

		selfReactionContainer = new GameObject().GetComponent<Transform>();
	}

	private void OnDestroy()
	{
		EventDrawOnBoard.OnRaised -= OnBoardDrawn;

		EventOnReactionSent.OnRaised -= OnReactionSent;
	}

	private unsafe void OnBoardDrawn(EventDrawOnBoard ev)
	{
		var f = QuantumRunner.Default.Game.Frames.Current;
		var brush = f.GetBrush(ev.brush);

		var container = f.GetContainer(brush->Fields.Container);
		if (container != null && container->Fields.Controller == playerReference)
			Rumble(container->Fields.HeldOnLeft, ev.toUv.Z.AsFloat, 0.15f);

		var player = f.GetPlayer(playerEntityRef);
		if (player->Fields.LeftHand == brush->Fields.Hand)
			Rumble(true, ev.toUv.Z.AsFloat, 0.15f);

		if (player->Fields.RightHand == brush->Fields.Hand)
			Rumble(false, ev.toUv.Z.AsFloat, 0.15f);
	}

	private void OnReactionSent(EventOnReactionSent obj)
	{
		if (obj.player != playerReference)
			return;

		var reactionAsset = UnityDB.AllOf<ReactionDataAsset>().ToList()[obj.reactionNumber];
		var reaction = Create.Prefab(reactionAsset.reactionPrefab, selfReactionContainer);

		selfReactionContainer.position = selfReactionPivot.position;
		selfReactionContainer.rotation = selfReactionPivot.rotation;
		selfReactionContainer.localScale = selfReactionPivot.localScale;
	}

	protected virtual void Update()
	{
		var rPos = RightHandPosition;
		var lPos = LeftHandPosition;

		LeftVelocity = (lPos - previousLeftPosition) / Time.deltaTime;
		RightVelocity = (rPos - previousRightPosition) / Time.deltaTime;

		previousRightPosition = rPos;
		previousLeftPosition = lPos;

		CheckPlayerPosition();

		CheckHands();

		CheckReactions();

		CheckMenu();

		PostUpdate();
	}

	protected virtual void PostUpdate()
	{
		
	}

	private unsafe void CheckPlayerPosition()
	{
		var p = QuantumRunner.Default.Game.Frames.Current.GetPlayer(playerEntityRef);

		CenterPosition = p->Transform3D.Position.ToUnityVector3();
		UpdateLocalPosition(p->Transform3D.Position.ToUnityVector3(), p->Transform3D.Rotation.ToUnityQuaternion());
	}

	protected virtual void UpdateLocalPosition(Vector3 pos, Quaternion rot)
	{
		transform.position = pos;
		transform.rotation = rot;
	}
	
	private void CheckMenu()
	{
		if (GetMenuStateLeft())
		{
			if (movableUi.IsOpen)
			{
				movableUi.HideUi();
			}
			else
			{
				var pos = LeftHandPosition;
				var facing = head.transform.position - pos;

				movableUi.ShowUi(pos, facing);
			}
		}
	}

	private void CheckReactions()
	{
		for (var i = 0; i < reactions.Count; i++)
		{
			if (UnityEngine.Input.GetKeyDown(KeyCode.Alpha1 + i))
			{
				QuantumRunner.Default.Game.SendCommand(new ReactionCommand
				{
					ReactionId = reactions[i].Guid
				});
			}
		}
	}

	private void SearchForHand(VrHandsBase handScript, EntityRef handEntity)
	{
		var hand = prefabViewUpdater.GetPrefab(handEntity);
		if (hand != null)
		{
			var anim = hand.GetComponentInChildren<Animator>();
			handScript.SetupHandModel(anim);
			
			hand.blockEngineUpdate = true;
			
			TrySetupHand(handScript);
		}
	}

	private void UpdateHand(EntityRef handEntity, Transform handTransform)
	{
		if (handTransform == null)
			return;
		
		var hand = prefabViewUpdater.GetPrefab(handEntity);
		hand.transform.position = handTransform.position;
		hand.transform.rotation = handTransform.rotation;
	}

	private unsafe void CheckHands()
	{
		if (prefabViewUpdater != null)
		{
			if (!leftHandBaseScript.IsSetup)
				SearchForHand(leftHandBaseScript, leftHandEntity);
			if (!rightHandBaseScript.IsSetup)
				SearchForHand(rightHandBaseScript, rightHandEntity);

			UpdateHand(leftHandEntity, leftHand);
			UpdateHand(rightHandEntity, rightHand);
		}
	}

	public virtual bool GetMenuStateLeft()
	{
		return false;
	}

	public virtual bool GetGrabStateLeft()
	{
		return false;
	}

	public virtual bool GetGrabStateRight()
	{
		return false;
	}

	public virtual Vector2 GetTeleportStateLeft()
	{
		return Vector2.zero;
	}

	public virtual Vector2 GetTeleportStateRight()
	{
		return Vector2.zero;
	}

	public virtual bool GetPointerToggleState()
	{
		return false;
	}

	public virtual void Rumble(bool left, float intensity, float duration)
	{
	}

	public unsafe void Setup(PlayerRef playerRef, EntityRef entityRef, PlayerColorList.RobotColorInfo color)
	{
		playerReference = playerRef;
		playerEntityRef = entityRef;
		robotColorInfo = color;
		
		var p = QuantumRunner.Default.Game.Frames.Current.GetPlayer(entityRef);

		leftHandEntity = p->Fields.LeftHand;
		rightHandEntity = p->Fields.RightHand;

		transform.position = p->Transform3D.Position.ToUnityVector3();
		transform.rotation = p->Transform3D.Rotation.ToUnityQuaternion();

		prefabViewUpdater = FindObjectOfType<EntityPrefabViewUpdater>();

		TrySetupHand(leftHandBaseScript);
		TrySetupHand(rightHandBaseScript);
		
		var voice = PhotonNetwork.Instantiate("VoiceRecorder", transform.position, transform.rotation, 0);
		if (voice != null)
			PhotonNetwork.RPC(voice.GetPhotonView(), "SetupPlayerRef", PhotonTargets.All, false, (int) playerReference);
	}

	void TrySetupHand(VrHandsBase handScript)
	{
		if (!handScript.IsSetup || robotColorInfo == null)
			return;

		var spaceHubHand = handScript.HandRoot.GetComponentInChildren<SpaceHubHands>();
		if (spaceHubHand != null)
			spaceHubHand.SetColor(robotColorInfo.handColor, robotColorInfo.rimColor);
	}
	
	public uint GetLeftHandAnimationData()
	{
		if (leftHandBaseScript)
			return leftHandBaseScript.GetAnimationData();
		return 0;
	}

	public uint GetRightHandAnimationData()
	{
		if (rightHandBaseScript)
			return rightHandBaseScript.GetAnimationData();
		return 0;
	}
}