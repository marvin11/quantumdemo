using System.Collections;
using System.Linq;
using Quantum.Commands;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SandboxPlayerPcRig : SandboxPlayer
{
	[Header("PC Rig Configurations")]
	public MouseLook mouseLook;
	public ReactionMenu reactionMenu;
	
	public override Vector3 LeftHandPosition => Vector3.zero;
	public override Quaternion LeftHandRotation => Quaternion.identity;
	public override Vector3 RightHandPosition => Vector3.zero;
	public override Quaternion RightHandRotation => Quaternion.identity;

	public float speed = 0.1f;

	//private Transform childObject;

	protected override void Start()
	{
		base.Start();
		DisableLooking();
		
		reactionMenu.onReactionChosen.Register(OnReactionChosen);
		reactionMenu.Setup(reactions.Select(r => r.Instance.GetUnityAsset()).ToList());
	//	childObject = transform.GetChild(0);
	}

	private void OnReactionChosen(ReactionDataAsset asset)
	{
		QuantumRunner.Default.Game.SendCommand(new ReactionCommand
		{
			ReactionId = asset.AssetObject.Guid
		});
	}

	protected override void PostUpdate()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			DisableLooking();
		}
	}

	private void DisableLooking()
	{
		mouseLook.enabled = false;
		Cursor.lockState = CursorLockMode.None;
	}

	public void EnableLooking()
	{
		mouseLook.enabled = true;
		Cursor.lockState = CursorLockMode.Locked;
	}
	
	public void LeaveRoom()
	{
		StartCoroutine(DoLeave());
	}

	private IEnumerator DoLeave()
	{
		if (QuantumRunner.ShutdownAll(true)) {

			// leave room
			PhotonNetwork.LeaveRoom(false);

			// wait one second (or wait for LeaveRoom)
			var startWait = Time.realtimeSinceStartup;
			while (PhotonNetwork.room != null && (Time.realtimeSinceStartup - startWait) < 1f) {
				yield return null;
			}

			PhotonNetwork.Disconnect();
			SceneManager.LoadScene("Start");
		}		
	}
	
		/*
    private void Update()
    {
        if(Input.GetKey(KeyCode.W))
        {
			childObject.position += childObject.forward * speed;
        }
		else if (Input.GetKey(KeyCode.S))
		{
			transform.position += childObject.forward * -speed;
		}
		else if (Input.GetKey(KeyCode.A))
		{
			transform.position += childObject.right * -speed;
		}
		else if (Input.GetKey(KeyCode.D))
		{
			transform.position += childObject.right * speed;
		}
		else if (Input.GetKey(KeyCode.Q))
		{
			transform.position += childObject.up * -speed;
		}
		else if (Input.GetKey(KeyCode.E))
		{
			transform.position += childObject.up * speed;
		}
	}
		*/
	
}
