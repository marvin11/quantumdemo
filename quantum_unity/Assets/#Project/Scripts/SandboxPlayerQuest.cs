using System;
using System.Collections;
using UnityEngine;

public class SandboxPlayerQuest : SandboxPlayer
{
	[Serializable]
	public class QuestRumbleConfig
	{
		public OVRInput.Controller controllerMask;

		public float activeFrequency;
		public float activeIntensity;

		public float timeLeft;
		public float vibrationFrequency = 32;

		public QuestRumbleConfig(OVRInput.Controller controller)
		{
			controllerMask = controller;
		}
		
		public void SetRumble(float intensity, float time)
		{
			activeFrequency = vibrationFrequency;
			activeIntensity = intensity;
			timeLeft = time;
			
			OVRInput.SetControllerVibration(activeFrequency, activeIntensity, controllerMask);
		}
		
		public void UpdateRumble()
		{
			if (timeLeft <= 0)
				return;

			timeLeft -= Time.deltaTime;
			if (timeLeft <= 0)
			{
				OVRInput.SetControllerVibration(0, 0, controllerMask);
			}
		}
	}

	public OVRManager ovrManager;
	public float targetHeightWhenSitting = 1.75f;
	
	public QuestRumbleConfig leftRumble = new QuestRumbleConfig(OVRInput.Controller.LTouch);
	public QuestRumbleConfig rightRumble = new QuestRumbleConfig(OVRInput.Controller.RTouch);

	private bool isRecentering;
	private Vector3 heightAdjustment;
	
	private void OnApplicationFocus(bool hasFocus)
	{
		OnHeadsetRecentered();
	}

	protected override void Update()
	{
		base.Update();
		
		leftRumble.UpdateRumble();
		rightRumble.UpdateRumble();

		if (OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.LTouch))
		{
			OnHeadsetRecentered();
		}
	}

	public override bool GetMenuStateLeft()
	{
		return OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.LTouch);
	}

	private void OnHeadsetRecentered()
	{
		if (OVRPlugin.GetBoundaryConfigured()) 
			return;

		StartCoroutine(DoRecenter());
	}

	protected override void UpdateLocalPosition(Vector3 pos, Quaternion rot)
	{
		if (OVRPlugin.GetBoundaryConfigured())
		{
			base.UpdateLocalPosition(pos, rot);
		}
		else
		{
			transform.position = pos + heightAdjustment;
		}
	}

	private IEnumerator DoRecenter()
	{
		heightAdjustment = Vector3.zero;

		yield return null;
		
		var height = head.position.y;
		var diff = targetHeightWhenSitting - height;

		heightAdjustment = new Vector3(0, diff, 0);
	}

	public override bool GetGrabStateLeft()
	{
		return OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
	}

	public override bool GetGrabStateRight()
	{
		return OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
	}

	public override Vector2 GetTeleportStateLeft()
	{
		return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.LTouch);
	}

	public override Vector2 GetTeleportStateRight()
	{
		return OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.RTouch);
	}

	public override bool GetPointerToggleState()
	{
		return OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.All);
	}

	public override void Rumble(bool left, float intensity, float duration)
	{
		if (left)
		{
			leftRumble.SetRumble(intensity, duration);
		}
		else
		{
			rightRumble.SetRumble(intensity, duration);
		}
	}
}