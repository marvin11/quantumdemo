﻿using System.Collections.Generic;
using Quantum;
using QuantumReconnect;
using UnityEngine;
using UnityEngine.XR;

public unsafe class SandboxPlayerSpawner : QuantumCallbacks
{
  public SandboxPlayer localPlayerPrefab;
  public SandboxPlayerQuest localPlayerPrefabQuest;
  public SandboxPlayerPcRig localPlayerPcRig;
  public SandboxRemotePlayer remotePlayerPrefabHuman;
  public SandboxRemotePlayer remotePlayerPrefabRobot;

  public PlayerColorList colorList;

  public List<PlayerRef> spawnedPlayers = new List<PlayerRef>();

  public static SandboxPlayer LocalPlayer { get; set; }
  
  void Start()
  {
    QuantumRunner.Init();
    UnityDB.Init();
  }

  void Update()
  {
    var game = QuantumRunner.Default.Game;
    if (game == null || game.Frames.Current == null)
      return;

    var players = game.Frames.Current.GetAllPlayers();
    while (players.Next())
    {
      var player = players.Current;

      if (spawnedPlayers.Contains(player->Fields.Owner))
        continue;

      UnityEngine.Debug.Log("Spawning player for " + player->Fields.Owner);
      var id = player->Fields.Owner;

      var spawnLocalPlayer = game.PlayerIsLocal(id);
      if (spawnLocalPlayer)
      {
        if (string.IsNullOrEmpty(XRSettings.loadedDeviceName))
        {
          var p = Instantiate(localPlayerPcRig);
          p.Setup(id, player->EntityRef, colorList.GetRobotColor(id));

          LocalPlayer = p;
        }
        else
        {
          if (Application.platform == RuntimePlatform.Android)
          {
            var p = Instantiate(localPlayerPrefabQuest);
            p.Setup(id, player->EntityRef, colorList.GetRobotColor(id));

            LocalPlayer = p;
          }
          else
          {
            var p = Instantiate(localPlayerPrefab);
            p.Setup(id, player->EntityRef, colorList.GetRobotColor(id));

            LocalPlayer = p;
          }
        }
      }
      else
      {
        var prefab = remotePlayerPrefabRobot;
        if (game.Frames.Current.Map.UserAsset.Instance is MapInfoData mapAsset && mapAsset.PlayerType == MapInfoData.TypeOfPlayer.Human)
        {
          prefab = remotePlayerPrefabHuman;
        }

        var p = Instantiate(prefab);
        p.Setup(game.Frames.Current, id, player->EntityRef, colorList.GetRobotColor(id));
      }

      spawnedPlayers.Add(player->Fields.Owner);
    }

    if (UnityEngine.Input.GetKeyDown(KeyCode.S))
    {
      GameStateFileManager.SaveGameState("save");
    }
  }
}
