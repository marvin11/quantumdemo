using UnityEngine;
using Valve.VR;

public class SandboxPlayerSteamVr : SandboxPlayer
{
	[Header("SteamVR Input")]
	public SteamVR_Action_Boolean grabAction;
	public SteamVR_Action_Vector2 teleportAction;
	public SteamVR_Action_Boolean pointerAction;
	public SteamVR_Action_Boolean openMenuAction;

	public SteamVR_Action_Vibration rumbleAction;
	public float vibrationFrequency = 32;

	public override Vector3 LeftHandPosition => leftHandBaseScript.Pivot.position;
	public override Quaternion LeftHandRotation => leftHandBaseScript.Pivot.rotation;

	public override Vector3 RightHandPosition => rightHandBaseScript.Pivot.position;
	public override Quaternion RightHandRotation => rightHandBaseScript.Pivot.rotation;

	public override bool GetMenuStateLeft()
	{
		return openMenuAction.GetStateDown(SteamVR_Input_Sources.LeftHand);
	}

	public override bool GetGrabStateLeft()
	{
		return grabAction.GetState(SteamVR_Input_Sources.LeftHand);
	}

	public override bool GetGrabStateRight()
	{
		return grabAction.GetState(SteamVR_Input_Sources.RightHand);
	}

	public override Vector2 GetTeleportStateLeft()
	{
		return teleportAction.GetAxis(SteamVR_Input_Sources.LeftHand);
	}

	public override Vector2 GetTeleportStateRight()
	{
		return teleportAction.GetAxis(SteamVR_Input_Sources.RightHand);
	}

	public override bool GetPointerToggleState()
	{
		return pointerAction.GetState(SteamVR_Input_Sources.Any);
	}

	public override void Rumble(bool left, float intensity, float duration)
	{
		rumbleAction.Execute(0, duration, vibrationFrequency, intensity,
			left ? SteamVR_Input_Sources.LeftHand : SteamVR_Input_Sources.RightHand);
	}
}