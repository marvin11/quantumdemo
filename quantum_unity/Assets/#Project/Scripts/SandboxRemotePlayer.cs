using System.Linq;
using Photon.Deterministic;
using Quantum;
using UnityEngine;
using Animator = UnityEngine.Animator;

public class SandboxRemotePlayer : MonoBehaviour
{
  
  public PlayerRef playerReference;

  public Transform headTransform;
  public Transform leftHandTransform;
  public Transform rightHandTransform;

  public Transform bodyTransform;

  public SpriteRenderer speakingFeedback;
  public Renderer[] bodyMeshes;

  public float torsoGap;

  [Header("Hands")]
  public VrHandsBase leftHandBaseScript;
  public VrHandsBase rightHandBaseScript;

  public EntityRef playerEntityRef;

  private EntityPrefabViewUpdater prefabViewUpdater;

  private EntityRef headEntity;
  private EntityRef leftHandEntity;
  private EntityRef rightHandEntity;

  private VoiceRecorder voiceRecorder;
  private PlayerColorList.RobotColorInfo robotColorInfo;

  public unsafe void Setup(Frame f, PlayerRef playerRef, EntityRef entityRef,
    PlayerColorList.RobotColorInfo color)
  {
    playerReference = playerRef;
    playerEntityRef = entityRef;
    robotColorInfo = color;

    var p = f.GetPlayer(entityRef);

    headEntity = p->Fields.Head;
    leftHandEntity = p->Fields.LeftHand;
    rightHandEntity = p->Fields.RightHand;

    prefabViewUpdater = FindObjectOfType<EntityPrefabViewUpdater>();

    TrySetupHand(leftHandBaseScript);
    TrySetupHand(rightHandBaseScript);

    if (f.Map.UserAsset.Instance is MapInfoData mapAsset && mapAsset.PlayerType == MapInfoData.TypeOfPlayer.Robot)
    {
      foreach (var bodyMesh in bodyMeshes)
        bodyMesh.material = color.robotMaterial;
    }

    FindVoiceRecorder();
  }

  private void FindVoiceRecorder()
  {
    voiceRecorder = FindObjectsOfType<VoiceRecorder>().FirstOrDefault(vr => vr.playerReference == playerReference);
  }

  unsafe void Update()
  {
    var game = QuantumRunner.Default.Game;
    if (game == null)
      return;

    var player = game.Frames.Current.GetPlayer(playerEntityRef);
    var head = game.Frames.Current.GetBodyPart(headEntity);
    var leftHand = game.Frames.Current.GetBodyPart(leftHandEntity);
    var rightHand = game.Frames.Current.GetBodyPart(rightHandEntity);

    transform.position = player->Transform3D.Position.ToUnityVector3();
    transform.rotation = player->Transform3D.Rotation.ToUnityQuaternion();

    headTransform.position = head->Transform3D.Position.ToUnityVector3();
    headTransform.rotation = head->Transform3D.Rotation.ToUnityQuaternion();

    if (bodyTransform != null)
    {
      bodyTransform.position = headTransform.position;
      bodyTransform.position -= Vector3.up * torsoGap;

      bodyTransform.rotation = Quaternion.Euler(0, headTransform.rotation.eulerAngles.y, 0);
    }

    leftHandTransform.position = leftHand->Transform3D.Position.ToUnityVector3();
    leftHandTransform.rotation = leftHand->Transform3D.Rotation.ToUnityQuaternion();

    rightHandTransform.position = rightHand->Transform3D.Position.ToUnityVector3();
    rightHandTransform.rotation = rightHand->Transform3D.Rotation.ToUnityQuaternion();

    if (prefabViewUpdater != null)
    {
      if (!leftHandBaseScript.IsSetup)
        SearchForHand(leftHandBaseScript, leftHandEntity);
      if (!rightHandBaseScript.IsSetup)
        SearchForHand(rightHandBaseScript, rightHandEntity);
    }

    var pos = player->Transform3D.Position.ToUnityVector2();

    if (voiceRecorder == null)
    {
      FindVoiceRecorder();
      speakingFeedback.enabled = false;
    }
    else
    {
      speakingFeedback.enabled = voiceRecorder.IsSpeaking;
    }

    var playerInputFlags = game.Frames.Verified.GetPlayerInputFlags(playerReference);
    var isPlayerActive =
      playerReference.IsValid &&
      (playerInputFlags & DeterministicInputFlags.PlayerNotPresent) != DeterministicInputFlags.PlayerNotPresent;

    foreach (var bodyMesh in bodyMeshes)
      bodyMesh.enabled = isPlayerActive;

    leftHandBaseScript.Visible = isPlayerActive;
    rightHandBaseScript.Visible = isPlayerActive;
  }

  private void SearchForHand(VrHandsBase handScript, EntityRef handEntity)
  {
    var hand = prefabViewUpdater.GetPrefab(handEntity);
    if (hand != null)
    {
      var anim = hand.GetComponentInChildren<Animator>();
      handScript.SetupHandModel(anim);

      TrySetupHand(handScript);
    }
  }

  void TrySetupHand(VrHandsBase handScript)
  {
    if (!handScript.IsSetup || robotColorInfo == null)
      return;

    var spaceHubHand = handScript.HandRoot.GetComponentInChildren<SpaceHubHands>();
    spaceHubHand.SetColor(robotColorInfo.handColor, robotColorInfo.rimColor);
  }
}