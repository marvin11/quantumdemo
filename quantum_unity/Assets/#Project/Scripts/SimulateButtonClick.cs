using UnityEngine;
using UnityEngine.UI;

public sealed class SimulateButtonClick : MonoBehaviour
{
  public Button button;

  public void Reset()
  {
    button = GetComponent<Button>();
  }

  [BitStrap.Button]
  public void Click()
  {
    button.onClick.Invoke();
  }
}
