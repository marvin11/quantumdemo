using BitStrap;
using UnityEngine;

public sealed class SpaceHubHands : MonoBehaviour, IVrHandsRender
{
	public Animator animator;

	public BoolAnimationParameter isDrawingParameter;

	public SkinnedMeshRenderer handMesh;
	public Renderer pencilObject;

	public string rimColorMaterialName;

	private bool isCurrentlyDrawing;
	private EntityPrefabRoot root;

	private VrHandsBase vrHands;
	
	void Start()
	{
		root = GetComponent<EntityPrefabRoot>();
	}

	void Update()
	{
		CheckDrawingPose();
		CheckRender();
	}

	public void SetColor(Color c, Color rim)
	{
		handMesh.material.color = c;
		handMesh.material.SetColor(rimColorMaterialName, rim);
	}
	
	private unsafe void CheckRender()
	{
		var game = QuantumRunner.Default.Game;
		if (game == null)
			return;

		var visible = vrHands == null || vrHands.Visible;		
		var hand = game.Frames.Current.GetBodyPart(root.EntityRef);

		handMesh.enabled = !hand->Fields.HoldingInfo.Holding && !isCurrentlyDrawing && visible;
	}

	private unsafe void CheckDrawingPose()
	{
		var game = QuantumRunner.Default.Game;
		if (game == null)
			return;

		var hand = game.Frames.Current.GetBodyPart(root.EntityRef);
		var player = game.Frames.Current.GetPlayer(hand->Fields.Owner);

		isCurrentlyDrawing = player->Fields.IsDrawing;
		isDrawingParameter.Set(animator, isCurrentlyDrawing);

		pencilObject.gameObject.SetActive(player->Fields.IsDrawing);
	}

	public void SetCollidersState(bool active)
	{
		var colliders = GetComponentsInChildren<Collider>();
		foreach (var c in colliders)
			c.enabled = active;
	}

	public void SetupHandBase(VrHandsBase handBase)
	{
		vrHands = handBase;
	}
}