using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class SplashScreen : MonoBehaviour
{
	public float splashScreenTime = 3.0f;
	public string nextSceneName;

	IEnumerator Start()
	{
		yield return new WaitForSeconds(splashScreenTime);
		SceneManager.LoadScene(nextSceneName);
	}
}