using System.Runtime.CompilerServices;
using UnityEngine;

public class TeleportFeedback : MonoBehaviour
{
	public LineRenderer lineFeedback;
	public Renderer teleportingFeedback;
	public Transform teleportDirectionFeedback;
	
	public Color teleportAllowedColor;
	public Color teleportBlockedColor;
	
	private SandboxPlayer sandboxPlayer;
	private SandboxRemotePlayer sandboxRemotePlayer;

	void Start()
	{
		sandboxPlayer = GetComponentInParent<SandboxPlayer>();
		sandboxRemotePlayer = GetComponentInParent<SandboxRemotePlayer>();
	}

	unsafe void Update()
	{
		var quantumGame = QuantumRunner.Default.Game;
		bool isTeleporting = false;
		bool validTeleport = false;
		var sourcePosition = Vector3.zero;
		var tpPosition = Vector3.zero;
		var sourceDirection = Vector3.forward;
		var tpAngle = 0f;
			
		if (sandboxPlayer != null)
		{
			var player = quantumGame.Frames.Current.GetPlayer(sandboxPlayer.playerEntityRef);
			var tpData = player->Fields.TeleportData;

			sourcePosition = tpData.Source == player->Fields.LeftHand
				? sandboxPlayer.LeftHandPosition
				: sandboxPlayer.RightHandPosition;
			
			isTeleporting = tpData.Teleporting;
			tpPosition = tpData.Position.ToUnityVector3();
			validTeleport = tpData.Valid;

			sourceDirection = sandboxPlayer.head.forward;
			sourceDirection.y = 0;

			tpAngle = tpData.Angle.AsFloat;
		}
		else if (sandboxRemotePlayer != null)
		{
			var player = quantumGame.Frames.Current.GetPlayer(sandboxRemotePlayer.playerEntityRef);
			var tpData = player->Fields.TeleportData;

			sourcePosition = sandboxRemotePlayer.headTransform.position;

			isTeleporting = tpData.Teleporting;
			tpPosition = tpData.Position.ToUnityVector3();
			validTeleport = tpData.Valid;

			sourceDirection = sandboxRemotePlayer.headTransform.forward;
			sourceDirection.y = 0;

			tpAngle = tpData.Angle.AsFloat;
		}

		lineFeedback.gameObject.SetActive(isTeleporting);
		teleportingFeedback.gameObject.SetActive(isTeleporting);
		
		lineFeedback.SetPosition(0, sourcePosition);
		lineFeedback.SetPosition(1, tpPosition);

		teleportingFeedback.transform.position = tpPosition + Vector3.up * 0.01f;
		teleportingFeedback.material.SetColor("_TintColor",
			validTeleport ? teleportAllowedColor : teleportBlockedColor);

		var quat = Quaternion.LookRotation(sourceDirection, Vector3.up) * Quaternion.AngleAxis(tpAngle, Vector3.up);
		teleportDirectionFeedback.eulerAngles = new Vector3(0, quat.eulerAngles.y, 0);
	}
}