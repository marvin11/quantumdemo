using System;
using UnityEngine;
using UnityEngine.Events;

public sealed class TriggerButton : MonoBehaviour
{
	private const float MinimumTimeToReceiveEvents = 0.5f;

	public LayerMask triggerMask;
	
	public UnityEvent onButtonTriggered;

	private float timeActive = 0;

	void Update()
	{
		timeActive += Time.deltaTime;
	}
	
	void OnEnable()
	{
		timeActive = 0;
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if (((1 << other.gameObject.layer) & triggerMask) == 0 || timeActive < MinimumTimeToReceiveEvents)
			return;

		onButtonTriggered.Invoke();
	}
}