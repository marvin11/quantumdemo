using BitStrap;
using UnityEngine;

public sealed class AppearingGazeUi : MonoBehaviour
{
	public GazeTrigger gazeTrigger;
	public Animator animator;
	
	public BoolAnimationParameter visibleParameter;

	void Update()
	{
		if (gazeTrigger == null)
			return;

		visibleParameter.Set(animator, gazeTrigger.isGazing);
	}
}