using UnityEngine;
using System.Collections;

public class HandPointer : MonoBehaviour
{
    // line renderer object
    private LineRenderer lr;
    // hand we're going to use for emiting
    private Transform handTransform;
    // line lenght when not touching an object
    [SerializeField]
    private float lineLength = 5.0f;
    // line width at the start
    [SerializeField]
    private float lineWidthStart = 0.03f;
    // line width at the end
    [SerializeField]
    private float lineWidthEnd = 0.03f;
    //turn on/off
    [SerializeField]
    private bool pointerIsOn = false;

    void Start()
    {
        handTransform = transform;
        lr = GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startWidth = lineWidthStart;
        lr.endWidth = lineWidthEnd;
        lr.enabled = true;
    }

    public void TogglePointer()
    {
        pointerIsOn = !pointerIsOn;
    }

    private void Update()
    {
        if (pointerIsOn)
        {
            RaycastHit hit;

            // initializing line positions and starting point
            Vector3[] positions = new Vector3[2];
            positions[0] = handTransform.position;

            // raycasting from the hand forward to see if we hit something
            if (Physics.Raycast(handTransform.position, handTransform.forward, out hit, Mathf.Infinity))
            {
                // did we hit something with the pointer?
                positions[1] = hit.point;
            }
            // nothing was hit so we propagate the ray for <var>lineLength</var>
            else
            {
                // set positions for line start and end
                positions[1] = handTransform.position + handTransform.forward * lineLength;
            }
            // let's update <var>lr</var> data
            lr.positionCount = positions.Length;
            lr.SetPositions(positions);
        }
        else lr.positionCount = 0;
    }
}
