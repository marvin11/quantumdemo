using System.Collections.Generic;
using Quantum.Commands;
using UnityEngine;

public class InteractionUi : MonoBehaviour
{
	public InteractionUiAnimatedWindow mainMenu;
	public InteractionUiAnimatedWindow videoMenu;
	public InteractionUiAnimatedWindow reactionsMenu;

	public List<ReactionDataAsset> reactionAssets;

	[Header("Audio")]
	public AudioSource audioSource;

	public AudioClip menuAppear;
	public AudioClip menuHide;
	public AudioClip menuClick;
	
	private IWindowOpener opener;

	void Start()
	{
		opener = (IWindowOpener) GetComponentInParent(typeof(IWindowOpener));

		opener.OnWindowOpen.Register(OnWindowOpened);
		opener.OnWindowClose.Register(OnWindowClosed);
	}

	private void OnWindowOpened()
	{
		mainMenu.Open();
		
		PlaySound(menuAppear);
	}

	private void OnWindowClosed()
	{
		mainMenu.Close();
		videoMenu.Close();
		reactionsMenu.Close();
		
		PlaySound(menuHide);
	}

	public void OpenVideoWindow()
	{
		videoMenu.Open();
		reactionsMenu.Close();
		
		PlaySound(menuClick);
	}

	public void OpenReactionWindow()
	{
		videoMenu.Close();
		reactionsMenu.Open();
		
		PlaySound(menuClick);
	}
	
	public void PlayVideo()
	{
		QuantumRunner.Default.Game.SendCommand(new PlayVideoCommand());
		PlaySound(menuClick);
	}

	public void StopVideo()
	{
		QuantumRunner.Default.Game.SendCommand(new StopVideoCommand());
		PlaySound(menuClick);
	}

	public void OnReactionChosen(int reactionNumber)
	{
		if (reactionNumber < reactionAssets.Count)
			PlayReaction(reactionAssets[reactionNumber]);
	}

	public void ToggleDrawingMode()
	{
		QuantumRunner.Default.Game.SendCommand(new ToggleDrawingModeCommand());
		PlaySound(menuClick);
	}
	
	private void PlayReaction(ReactionDataAsset reactionDataAsset)
	{
		QuantumRunner.Default.Game.SendCommand(new ReactionCommand
		{
			ReactionId = reactionDataAsset.Settings.Guid
		});
		opener.HideUi();
		PlaySound(menuClick);
	}

	void PlaySound(AudioClip clip)
	{
		audioSource.clip = clip;
		audioSource.Play();
	}

}