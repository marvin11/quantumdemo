using BitStrap;
using UnityEngine;

public class InteractionUiAnimatedWindow : MonoBehaviour
{
	public Animator animator;

	public BoolAnimationParameter windowStateParameter;

	public bool State => windowStateParameter.Get(animator);

	public void Open()
	{
		windowStateParameter.Set(animator, true);
	}

	public void Close()
	{
		windowStateParameter.Set(animator, false);
	}
}