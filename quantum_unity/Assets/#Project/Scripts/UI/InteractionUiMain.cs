using System;
using BitStrap;
using UnityEngine;

public class InteractionUiMain : InteractionUiAnimatedWindow
{
	[Serializable]
	public class MenuTracker
	{
		public InteractionUiAnimatedWindow windowToTrack;
		public Animator trackerIcon;

		[AnimatorField("trackerIcon")]
		public BoolAnimationParameter trackerAnimation;

		public void Update()
		{
			trackerAnimation.Set(trackerIcon, windowToTrack.State);
		}
	}

	public MenuTracker videoMenuTracker;
	public MenuTracker reactionsMenuTracker;

	void Update()
	{
		videoMenuTracker.Update();
		reactionsMenuTracker.Update();
	}
}