using UnityEngine;
using UnityEngine.UI;

public class InteractionUiVideoMenu : InteractionUiAnimatedWindow
{
	public Image playPauseButtonImage;

	public Sprite playSprite;
	public Sprite pauseSprite;

	void Update()
	{
		CheckVideoState();
	}

	private unsafe void CheckVideoState()
	{
		var videoPlayerState = QuantumRunner.Default.Game.Frames.Current.Global->VideoPlayer;
		playPauseButtonImage.sprite = videoPlayerState.Playing ? pauseSprite : playSprite;
	}
}