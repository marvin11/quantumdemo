using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class LeaveRoomUi : MonoBehaviour
{
	public void LeaveRoom()
	{
		StartCoroutine(DoLeave());
	}

	private IEnumerator DoLeave()
	{
		if (QuantumRunner.ShutdownAll(true)) {

			// leave room
			PhotonNetwork.LeaveRoom(false);

			// wait one second (or wait for LeaveRoom)
			var startWait = Time.realtimeSinceStartup;
			while (PhotonNetwork.room != null && (Time.realtimeSinceStartup - startWait) < 1f) {
				yield return null;
			}

			PhotonNetwork.Disconnect();
			SceneManager.LoadScene("Start");
		}		
	}
}