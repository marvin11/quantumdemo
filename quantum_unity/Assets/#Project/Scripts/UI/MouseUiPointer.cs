using UnityEngine;

public sealed class MouseUiPointer : MonoBehaviour
{
	public Camera cam;
	public Transform uiPointer;
	void Update()
	{
		uiPointer.transform.position = cam.transform.position;

		var mousePos = cam.ScreenPointToRay(Input.mousePosition);
		uiPointer.forward = mousePos.direction;
	}
}
