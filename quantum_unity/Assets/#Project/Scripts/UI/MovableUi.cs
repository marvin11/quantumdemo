using BitStrap;
using UnityEngine;

public interface IWindowOpener
{
	SafeAction OnWindowOpen { get; }
	SafeAction OnWindowClose { get; }

	void ShowUi();
	void HideUi();
}

public sealed class MovableUi : MonoBehaviour, IWindowOpener
{
	public bool IsOpen { get; set; }

	void Start()
	{
		HideUi();
	}
	
	public void ShowUi(Vector3 position, Vector3 facingDirection)
	{
		var t = transform;
		t.position = position;
		t.forward = facingDirection;

		IsOpen = true;
		OnWindowOpen.Call();
	}

	public void ShowUi()
	{
		ShowUi(Vector3.zero, Vector3.forward);
	}

	public void HideUi()
	{
		IsOpen = false;
		OnWindowClose.Call();
	}

	public SafeAction OnWindowOpen { get; private set; } = new SafeAction();
	public SafeAction OnWindowClose { get; private set; } = new SafeAction();
}