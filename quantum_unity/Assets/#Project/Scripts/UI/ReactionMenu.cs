using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public sealed class ReactionMenu : MonoBehaviour
{
	public ReactionMenuButton buttonPrefab;

	public SafeAction<ReactionDataAsset> onReactionChosen = new SafeAction<ReactionDataAsset>();
	
	void Start()
	{
		buttonPrefab.gameObject.SetActive(false);
	}
	
	public void Setup(List<ReactionDataAsset> reactions)
	{
		foreach (var dataAsset in reactions)
		{
			var button = Create.Behaviour(buttonPrefab, buttonPrefab.transform.parent);

			button.Menu = this;
			button.gameObject.SetActive(true);
			button.Setup(dataAsset);
		}
	}
	
	public void OnReactionChosen(ReactionDataAsset reactionAsset)
	{
		onReactionChosen.Call(reactionAsset);
	}
}
