using UnityEngine;
using UnityEngine.UI;

public sealed class ReactionMenuButton : MonoBehaviour
{
	public Image reactionImage;

	public ReactionMenu Menu { get; set; }
	private ReactionDataAsset reactionAsset;
	
	public void Setup(ReactionDataAsset reaction)
	{
		reactionAsset = reaction;

		reactionImage.sprite = reaction.reactionSprite;
	}

	public void OnClick()
	{
		Menu.OnReactionChosen(reactionAsset);
	}
}