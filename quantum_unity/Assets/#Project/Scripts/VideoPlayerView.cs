using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerView : MonoBehaviour
{
  public VideoPlayer videoPlayer;

  [Header("Video Sync Configurations")]
  public float timeDifferenceForForcedResync = 3.0f;

  unsafe void Update()
  {
    var f = QuantumRunner.Default.Game.Frames.Current;
    if (f == null)
      return;

    var videoPlayerState = f.Global->VideoPlayer;

    if (videoPlayerState.Playing && !videoPlayer.isPlaying)
      videoPlayer.Play();

    if (!videoPlayerState.Playing && videoPlayer.isPlaying)
    {
      if (videoPlayerState.Runtime > 0)
        videoPlayer.Pause();
      else
        videoPlayer.Stop();
    }

    var timeDiff = Mathf.Abs((float)(videoPlayer.time - videoPlayerState.Runtime.AsDouble));

    if (timeDiff >= timeDifferenceForForcedResync)
      videoPlayer.time = videoPlayerState.Runtime.AsDouble;
  }
}