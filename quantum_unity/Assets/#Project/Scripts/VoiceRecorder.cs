using System.Collections;
using System.Collections.Generic;
using BitStrap;
using ExitGames.Client.Photon;
using Quantum;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class VoiceRecorder : MonoBehaviour, IPunCallbacks
{
  public PhotonVoiceRecorder recorder;
  public PhotonVoiceSpeaker speaker;

  public PhotonView photonView;
  public PlayerRef playerReference;

  public AudioLevelWatcher audioLevelWatcher;
  [Range(0.0f, 1.0f)]
  public float isSpeakingThreshold = 0.3f;

  public bool debugEchoMode = false;

  public bool IsSpeaking
  {
    get { return audioLevelWatcher.level > isSpeakingThreshold; }
  }

  private void Awake()
  {
    photonView = GetComponent<PhotonView>();
  }

  private void Start()
  {
    recorder.enabled = false;
    speaker.enabled = true;

    if (photonView.isMine)
    {
      StartCoroutine(WaitAndCalibrate());
      recorder.DebugEchoMode = debugEchoMode;
    }

    DontDestroyOnLoad(gameObject);
  }

  [Button]
  public void CalibrateMicrophone()
  {
    recorder.enabled = true;
    recorder.VoiceDetectorCalibrate(500);
  }

  private IEnumerator WaitAndCalibrate()
  {
    yield return new WaitForSeconds(1.0f);
    recorder.enabled = true;
    recorder.VoiceDetectorCalibrate(500);

  }

  [PunRPC]
  private void SetupPlayerRef(int value)
  {
    playerReference = value;
  }

  public void OnPhotonInstantiate(PhotonMessageInfo info)
  {

  }

  public void OnConnectedToPhoton()
  {
  }

  public void OnLeftRoom()
  {
  }

  public void OnMasterClientSwitched(PhotonPlayer newMasterClient)
  {
  }

  public void OnPhotonCreateRoomFailed(object[] codeAndMsg)
  {
  }

  public void OnPhotonJoinRoomFailed(object[] codeAndMsg)
  {
  }

  public void OnCreatedRoom()
  {
  }

  public void OnJoinedLobby()
  {
  }

  public void OnLeftLobby()
  {
  }

  public void OnFailedToConnectToPhoton(DisconnectCause cause)
  {
  }

  public void OnConnectionFail(DisconnectCause cause)
  {
  }

  public void OnDisconnectedFromPhoton()
  {
  }

  public void OnReceivedRoomListUpdate()
  {
  }

  public void OnJoinedRoom()
  {
  }

  public void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
  {
  }

  public void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
  {
  }

  public void OnPhotonRandomJoinFailed(object[] codeAndMsg)
  {
  }

  public void OnConnectedToMaster()
  {
  }

  public void OnPhotonMaxCccuReached()
  {
  }

  public void OnPhotonCustomRoomPropertiesChanged(Hashtable propertiesThatChanged)
  {
  }

  public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
  {
  }

  public void OnUpdatedFriendList()
  {
  }

  public void OnCustomAuthenticationFailed(string debugMessage)
  {
  }

  public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
  {
  }

  public void OnWebRpcResponse(OperationResponse response)
  {
  }

  public void OnOwnershipRequest(object[] viewAndPlayer)
  {
  }

  public void OnLobbyStatisticsUpdate()
  {
  }

  public void OnPhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
  {
  }

  public void OnOwnershipTransfered(object[] viewAndPlayers)
  {
  }
}