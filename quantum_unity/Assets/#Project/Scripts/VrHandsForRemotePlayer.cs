using UnityEngine;

public sealed class VrHandsForRemotePlayer : VrHandsBase
{
	public override GameObject HandRoot => prefabRoot.gameObject;
	public override bool IsSetup => remoteHands != null;
	public override Transform Pivot { get; }

	private VrHandsRemote remoteHands;
	private EntityPrefabRoot prefabRoot;

	public override void SetupHandModel(Animator animator)
	{
		remoteHands = animator.GetComponentInChildren<VrHandsRemote>();
		prefabRoot = animator.GetComponentInChildren<EntityPrefabRoot>();

		var spaceHubHands = animator.GetComponent<SpaceHubHands>();
		spaceHubHands.SetCollidersState(false);
		spaceHubHands.SetupHandBase(this);
	}

	unsafe void Update()
	{
		if (remoteHands == null || QuantumRunner.Default.Game == null)
			return;

		var hand = QuantumRunner.Default.Game.Frames.Current.GetBodyPart(prefabRoot.EntityRef);

		remoteHands.UpdateHandAnimationData(
			hand->Fields.AnimationInfo.Thumb.AsFloat,
			hand->Fields.AnimationInfo.Index.AsFloat,
			hand->Fields.AnimationInfo.Middle.AsFloat,
			hand->Fields.AnimationInfo.Ring.AsFloat,
			hand->Fields.AnimationInfo.Pinky.AsFloat);
	}
}