﻿Shader "MeetingRoom/Scrolling"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] _Color ("Color", Color) = (1, 1, 1, 1)
		_Alpha ("Alpha (Transparency)", Range(0.0, 1.0)) = 1.0
		_Speed ("Speed", Float) = 1.0
	}
	SubShader
	{
		Tags{ "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }

		Pass
		{
			Blend One One
			ZWrite Off
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				half4 color : COLOR; 
				half2 uv : TEXCOORD0;
			};

			struct v2f
			{
				half2 uv : TEXCOORD0;
				half4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST;
			uniform fixed4 _Color;
			uniform fixed _Alpha;
			uniform fixed _Speed;

			v2f vert (appdata v)
			{
				v2f o;
				float4 vertex = v.vertex;
				o.vertex = UnityObjectToClipPos(vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, fixed2(_Speed*_Time.y + i.uv.x, i.uv.y));
				col.rgb *= i.color.rgb * _Color.rgb;
				col.a = _Alpha * _Color.a * i.color.a;
				col.rgb *= col.a;
				return col;
			}
			ENDCG
		}
	}
}
