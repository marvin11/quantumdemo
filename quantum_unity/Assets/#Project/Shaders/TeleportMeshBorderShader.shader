﻿Shader "DeMagnete/Teleport/TeleportMeshBorder"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		[HDR] _Color ("Color", Color) = (1, 1, 1, 1)
		_Alpha ("Alpha (Transparency)", Range(0.0, 1.0)) = 1.0
		_Speed ("Speed", Float) = 1.0
		[Header(_______________________________)]
		[Header(BH is base height)]
		[Header(SH is strip height)]
		[Header(V is height variation)]
		[Header(S is base strip sync)]
		_Values ("Values (BH, SH, V, S)", Vector) = (0.5, 0.5, 0.1, 1.5)
	}
	SubShader
	{
		Tags{ "Queue"="Overlay" "IgnoreProjector"="True" "RenderType"="Transparent" }

		Pass
		{
			Blend One One
			ZWrite Off
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			static const fixed PI = 3.1415926535897932384626433832795;

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform fixed4 _Color;
			uniform fixed _Alpha;
			uniform fixed _Speed;
			uniform fixed4 _Values;

			v2f vert (appdata v)
			{
				v2f o;
				float4 vertex = v.vertex;
				o.vertex = UnityObjectToClipPos(vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed time = _Time.y * _Speed;
				fixed strip = step(frac(i.uv.y - time), _Values.y * (1.0 - i.uv.y));
				fixed bounce = 1.0 - abs(sin(time * PI + PI * _Values.w));
				fixed base = step(i.uv.y, _Values.x + _Values.z * bounce);

				fixed4 col = tex2D(_MainTex, i.uv);
				col.rgb *= _Color.rgb;
				col.a = _Alpha * _Color.a * max(strip, base);
				col.rgb *= col.a;
				return col;
			}
			ENDCG
		}
	}
}
