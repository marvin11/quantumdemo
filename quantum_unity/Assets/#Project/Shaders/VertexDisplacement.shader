﻿Shader "BitCake/Vertex Displacement" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo", 2D) = "white" {}
		[NoScaleOffset] _WindNoise ("Wind Noise", 2D) = "white" {}
		_NoiseAmount ("Noise Amount", Vector) = (0,0,0,0)
		_NoiseScale ("Noise Texture Scale", Float) = 1.0
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		Cull Off

		CGPROGRAM
		#pragma surface surf Lambert noforwardadd vertex:vert
		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
			float4 vertCol : COLOR;
		};

		sampler2D _MainTex;
		sampler2D _WindNoise;
		fixed4 _Color;
		half4 _NoiseAmount;
		half _NoiseScale;

		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);

			float3 worldPos = mul (unity_ObjectToWorld, v.vertex).xyz;
			float3 offset = float3(0, 0.1, 0.2);

			float2 UV = worldPos.xz;
			float t = _Time.x;
			UV.xy += t*2;
			float3 windNoise = tex2Dlod(_WindNoise, float4(UV, 0, 0) * _NoiseScale).rgb;

			v.vertex.z += sin(t*20) * v.color.a * _NoiseAmount.z * v.color.r * windNoise.g;
			v.vertex.z += sin(t*15) * v.color.a * _NoiseAmount.z * v.color.g * windNoise.g;
			v.vertex.z += sin(t*25) * v.color.a * _NoiseAmount.z * v.color.b * windNoise.g;

			v.vertex.x += sin(t*20) * v.color.a * _NoiseAmount.x * v.color.r * windNoise.r;
			v.vertex.x += sin(t*15) * v.color.a * _NoiseAmount.x * v.color.g * windNoise.r;
			v.vertex.x += sin(t*25) * v.color.a * _NoiseAmount.x * v.color.b * windNoise.r;

			v.vertex.y += windNoise.r * _NoiseAmount.y * v.color.a * v.color.r;
			v.vertex.y += windNoise.g * _NoiseAmount.y * v.color.a * v.color.g;
			v.vertex.y += windNoise.b * _NoiseAmount.y * v.color.a * v.color.b;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;

			clip(c.a - 0.5);
		}
		ENDCG
	}

	FallBack "Custom/Mobile/DiffuseX"
}
