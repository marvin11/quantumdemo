﻿Shader "BitCake/VrHandOpaqueShader"
{
	Properties
	{
		[Enum(UnityEngine.Rendering.CullMode)] _Culling ("Cull Mode", Int) = 2
		[HDR] _Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		[HDR] _RimColor ("Rim Color", Color) = (1.0,1.0,1.0,1.0)
		_RimPower ("Rim Power", Range(0.1, 8.0)) = 3.0
	}
	SubShader
	{
		Tags { "Queue"="Geometry" "RenderType"="Opaque" "IgnoreProjector"="True" }
		Cull [_Culling]

		Pass
		{
			ZWrite On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				half3 normal: NORMAL;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				half3 normal : NORMAL;
				half3 viewDir : TEXCOORD0;
			};

			half4 _Color;
			half4 _RimColor;
			half _RimPower;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.viewDir = normalize(UnityWorldSpaceViewDir(worldPos));
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				half rim = 1.0 - saturate(dot(normalize(i.viewDir), i.normal));
				half4 c;
				c.rgb = lerp(_Color.rgb, _RimColor.rgb, pow(rim, _RimPower));
				c.a = lerp(_Color.a, _RimColor.a, pow(rim, _RimPower));
				c.rgb *= c.a;
				return c;
			}
			ENDCG
		}
	}
}
