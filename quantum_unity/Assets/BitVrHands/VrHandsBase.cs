using UnityEngine;

public interface IVrHandsRender
{
	void SetupHandBase(VrHandsBase handBase);
}

public abstract class VrHandsBase : MonoBehaviour
{
	public abstract GameObject HandRoot { get; }
	public abstract bool IsSetup { get; }
	public abstract Transform Pivot { get; }

	public bool Visible { get; set; } = true;
	
	public abstract void SetupHandModel(Animator animator);

	public virtual uint GetAnimationData()
	{
		return 0;
	}

	public static uint GetAnimationDataFromAnimator(Animator animator, int thumb, int index, int middle, int ring, int pinky)
	{
		uint Convert(float f)
		{
			return (uint) (f * 63);
		}

		uint bits = 0;

		// Ading bits to know it's an actual input value
		bits |= (uint) 3 << 30;
		bits |= Convert(animator.GetFloat(thumb)) << 24;
		bits |= Convert(animator.GetFloat(index)) << 18;
		bits |= Convert(animator.GetFloat(middle)) << 12;
		bits |= Convert(animator.GetFloat(ring)) << 6;
		bits |= Convert(animator.GetFloat(pinky));

		return bits;
	}
}