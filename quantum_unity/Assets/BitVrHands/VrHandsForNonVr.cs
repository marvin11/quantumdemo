using UnityEngine;

public sealed class VrHandsForNonVr : VrHandsBase
{
	public override GameObject HandRoot => gameObject;
	public override bool IsSetup => true;
	public override Transform Pivot => transform;
 
	public override void SetupHandModel(Animator animator)
	{
		return;
	}
}