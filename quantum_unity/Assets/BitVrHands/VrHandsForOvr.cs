using System;
using BitStrap;
using UnityEngine;

public sealed class VrHandsForOvr : VrHandsBase
{
    [Serializable]
    public class Finger
    {
        [AnimatorField("handAnimator")]
        public FloatAnimationParameter parameter;
        public OVRInput.NearTouch nearTouchInput;
        public OVRInput.Touch touchInput;
        public OVRInput.Button pressInput;
        
        public float animationSpeed;

        [Header("Targets")]
        public float closedTarget = 0.00f;
        public float nearTouchTarget = 0.25f;
        public float touchTarget = 0.75f;
        public float pressTarget = 1.00f;

        private float targetState;    
        private float fingerState;

        public void DoUpdate(Animator handAnimator, OVRInput.Controller controllerMask)
        {
            bool nearTouch = OVRInput.Get(nearTouchInput, controllerMask);
            bool touch = OVRInput.Get(touchInput, controllerMask);
            bool press = OVRInput.Get(pressInput, controllerMask);

            targetState = closedTarget;
            if (nearTouch)
                targetState = nearTouchTarget;
            if (touch)
                targetState = touchTarget;
            if (press)
                targetState = pressTarget;

            fingerState = Mathf.MoveTowards(fingerState, targetState, animationSpeed*Time.deltaTime);
            parameter.Set(handAnimator, fingerState);
        }
    }
    
    public OVRInput.Controller controllerMask;
    
    public Animator handAnimator;

    public Finger thumbFinger;
    public Finger indexFinger;
    public Finger middleFinger;
    public Finger ringFinger;
    public Finger pinkyFinger;

    public Transform handPivot;
    public string wristBoneName;
    
    public override GameObject HandRoot => handAnimator.gameObject;
    public override bool IsSetup => handAnimator != null;
    public override Transform Pivot => wristBone ? wristBone : handPivot;

    private Transform wristBone;
    
    public override void SetupHandModel(Animator animator)
    {
        handAnimator = animator;
        wristBone = handAnimator.transform.Find(wristBoneName);
        if (wristBone == null)
            Debug.LogErrorFormat("Could not find wrist bone on animator {0}", handAnimator);
        
        if (animator.GetComponentInChildren(typeof(IVrHandsRender)) is IVrHandsRender handRender)
            handRender.SetupHandBase(this);
    }

    private void Update()
    {
        thumbFinger.DoUpdate(handAnimator, controllerMask);
        indexFinger.DoUpdate(handAnimator, controllerMask);
        middleFinger.DoUpdate(handAnimator, controllerMask);
        ringFinger.DoUpdate(handAnimator, controllerMask);
        pinkyFinger.DoUpdate(handAnimator, controllerMask);

        if (handAnimator != null && wristBone != null)
        {
            var pivotTransform = handPivot.transform;
            
            wristBone.position = pivotTransform.position;
            wristBone.rotation = pivotTransform.rotation;
        }
    }

    public override uint GetAnimationData()
    {
        return GetAnimationDataFromAnimator(handAnimator,
            thumbFinger.parameter.Index,
            indexFinger.parameter.Index,
            middleFinger.parameter.Index,
            ringFinger.parameter.Index,
            pinkyFinger.parameter.Index);
    }
}
