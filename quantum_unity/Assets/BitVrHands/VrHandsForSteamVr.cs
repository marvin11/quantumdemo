using System;
using BitStrap;
using UnityEngine;
using Valve.VR;

public sealed class VrHandsForSteamVr : VrHandsBase
{
	public SteamVR_Action_Skeleton skeletonAction;
	
	public Animator handAnimator;

	[AnimatorField("handAnimator")]
	public FloatAnimationParameter thumbFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter indexFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter middleFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter ringFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter pinkyFinger;

	public bool mirrorBone;

	public string rootBoneName;
	public string wristBoneName;
	
	private Transform rootBone;
	private Transform wristBone;

	public override Transform Pivot => wristBone ? wristBone : transform;
	public override GameObject HandRoot => handAnimator.gameObject;
	public override bool IsSetup => handAnimator != null;
	public override void SetupHandModel(Animator animator)
	{
		handAnimator = animator;

		rootBone = handAnimator.transform.Find(rootBoneName);
		wristBone = handAnimator.transform.Find(wristBoneName);

		if (rootBone == null)
			Debug.LogErrorFormat("Could not find root bone on animator {0}", handAnimator);
		if (wristBone == null)
			Debug.LogErrorFormat("Could not find wrist bone on animator {0}", handAnimator);

		if (animator.GetComponentInChildren(typeof(IVrHandsRender)) is IVrHandsRender handRender)
			handRender.SetupHandBase(this);
	}

	private void Start()
	{
		if (handAnimator != null)
{			SetupHandModel(handAnimator);}
	}

	void Update()
	{
		if (handAnimator != null)
		{
			thumbFinger.Set(handAnimator, skeletonAction.thumbCurl);
			indexFinger.Set(handAnimator, skeletonAction.indexCurl);
			middleFinger.Set(handAnimator, skeletonAction.middleCurl);
			ringFinger.Set(handAnimator, skeletonAction.ringCurl);
			pinkyFinger.Set(handAnimator, skeletonAction.pinkyCurl);
		}

		if (wristBone != null)
			SetBone(wristBone, SteamVR_Skeleton_JointIndexes.wrist, mirrorBone);
		if (rootBone != null)
			SetBone(rootBone, SteamVR_Skeleton_JointIndexes.root, mirrorBone);
	}

	private void SetBone(Transform t, int boneIndex, bool shouldMirror)
	{
		var pos = skeletonAction.GetBonePositions()[boneIndex];
		var rot = skeletonAction.GetBoneRotations()[boneIndex];

		if (shouldMirror)
		{
			if (boneIndex == SteamVR_Skeleton_JointIndexes.wrist)
			{
				pos.Scale(new Vector3(-1, 1, 1));
				rot.y = rot.y * -1;
				rot.z = rot.z * -1;
			}
		}
		
		t.localPosition = pos;
		t.localRotation = rot;
	}
	
	public override uint GetAnimationData()
	{
		return GetAnimationDataFromAnimator(handAnimator, 
			thumbFinger.Index, 
			indexFinger.Index, 
			middleFinger.Index,
			ringFinger.Index,
			pinkyFinger.Index);
	}

}