using BitStrap;
using UnityEngine;

public sealed class VrHandsRemote : MonoBehaviour
{
	public Animator handAnimator;

	[AnimatorField("handAnimator")]
	public FloatAnimationParameter thumbFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter indexFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter middleFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter ringFinger;
	[AnimatorField("handAnimator")]
	public FloatAnimationParameter pinkyFinger;

	public void UpdateHandAnimationData(float thumb, float index, float middle, float ring, float pinky)
	{
		if (handAnimator == null)
			return;

		thumbFinger.Set(handAnimator, thumb);
		indexFinger.Set(handAnimator, index);
		middleFinger.Set(handAnimator, middle);
		ringFinger.Set(handAnimator, ring);
		pinkyFinger.Set(handAnimator, pinky);
	}
}
