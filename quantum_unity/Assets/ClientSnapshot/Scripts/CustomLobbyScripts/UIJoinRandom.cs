﻿using Quantum;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace QuantumReconnect{
public class UIJoinRandom : ConnectAndJoinRandom {
  public byte MaxPlayers = 1;
  public MapLink Map;
  public bool WaitForAll;

  public UnityEngine.UI.ScrollRect Console;
  public UnityEngine.UI.Text ConsoleText;
  public UnityEngine.UI.Button SkipWaitingButton;
  public UnityEngine.UI.Dropdown MapDropdown;

  private List<String> _mapGuids;

  public enum State {
    Connecting,
    Error,
    Joining,
    Creating,
    WaitingForPlayers,
    Starting
  }

      #region Properties

  State _State {
    get { return _state; }
    set {
      _state = value;
      Debug.Log("Setting UIJoinRandom state to " + _state.ToString());
    }
  }
  State _state;

  #endregion

  #region UnityCallbacks

  public override void Start() {
    base.Start();

    var maps = UnityEngine.Resources.LoadAll<MapAsset>(QuantumEditorSettings.Instance.ResourceDatabasePath);
    MapDropdown.AddOptions(maps.Select(m => m.name).ToList());

    _mapGuids = maps.Select(m => ((MapAsset)m).AssetObject.Guid).ToList();
    if (_mapGuids.Count == 1)
      Map.Guid = _mapGuids[0];

    Application.logMessageReceived += Log;
  }

  public void OnDestroy() {
    Application.logMessageReceived -= Log;
  }

  private void Log(string condition, string stackTrace, LogType type) {
    var color = type == LogType.Log ? "white" : type == LogType.Warning ? "yellow" : "red";
    ConsoleText.text += string.Format("<color={0}>[{1}]</color> {2}\n", color, type, condition);

    while (ConsoleText.preferredHeight > Console.content.sizeDelta.y) {
      var index = ConsoleText.text.IndexOf("\n");
      if (index < 0) break;
      ConsoleText.text = ConsoleText.text.Remove(0, index + 1);
    }

    if (ConsoleText.preferredHeight < Console.viewport.rect.height) {
      Console.verticalScrollbar.value = 1;
    }
    else {
      var scrollPosition = 
        (ConsoleText.preferredHeight - Console.viewport.rect.height) /
        (Console.content.sizeDelta.y - Console.viewport.rect.height);
      Console.verticalScrollbar.value = 1.0f - scrollPosition;
    }
  }

  public override void Update() {
    base.Update();

    if (UnityEngine.Input.GetKey(KeyCode.Space)) {
      Debug.Log("any key");
    }

    if (_State == State.Starting)
      return;

    var start = false;
    var map = default(String);

    if (PhotonNetwork.inRoom) {

      // Only admin posts properties into the room
      if (PhotonNetwork.isMasterClient) {
        var ht = new ExitGames.Client.Photon.Hashtable();
        if (!TryGetRoomProperty("MAP-GUID", out map)) {
          if (string.IsNullOrEmpty(Map.Guid)) {
            MapDropdown.gameObject.SetActive(true);
          }
          else {
            ht.Add("MAP", UnityDB.AllOf<MapAsset>().First(x => x.Settings.Guid == Map.Guid).Settings.Scene);
            ht.Add("MAP-GUID", Map.Guid);
          }
        }

        // Set START to true when we enough players joined or !WaitForAll
        if (!TryGetRoomProperty("START", out start) && (!WaitForAll || PhotonNetwork.room.PlayerCount >= MaxPlayers))
          ht.Add("START", true);
        else if (!TryGetRoomProperty("START", out start) && WaitForAll)
          SkipWaitingButton.gameObject.SetActive(true);

        if (ht.Count > 0)
          PhotonNetwork.room.SetCustomProperties(ht);
      }

      // Everyon is listening for map and start properties
      if (TryGetRoomProperty("MAP-GUID", out map)) {

        if (TryGetRoomProperty("START", out start)) {
          _State = State.Starting;

          Debug.LogFormat("### Starting game using map '{0}'", map);

          var config = new RuntimeConfig();
          config.Map.Guid = map;

          var param = new QuantumRunner.StartParameters {
            RuntimeConfig = config,
            DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
            GameMode = Photon.Deterministic.DeterministicGameMode.Multiplayer,
          };

          QuantumRunner.StartGame(Guid.NewGuid().ToString(), PhotonNetwork.room.MaxPlayers, param);
        }
      }
    }
  }

  Boolean TryGetRoomProperty<T>(String key, out T value) {
    System.Object v;
    if (PhotonNetwork.room.CustomProperties.TryGetValue(key, out v)) {
      if (v is T) {
        value = (T)v;
        return true;
      }
    }
    value = default(T);
    return false;
  }

  #endregion

  #region ConnectAndJoinRandom

  public override void OnConnectedToMaster() {
    _State = State.Joining;

    PhotonNetwork.JoinRandomRoom(null, 0, MatchmakingMode.FillRoom, null, null);
  }

  public override void OnPhotonRandomJoinFailed() {
    _State = State.Creating;

    RoomOptions roomOptions = new RoomOptions {
      IsVisible = true,
      IsOpen = true,
      MaxPlayers = MaxPlayers
    };

    PhotonNetwork.CreateRoom(null, roomOptions, null);

    Debug.LogFormat("Creating new room for '{0}' max players", MaxPlayers);
  }

  public override void OnFailedToConnectToPhoton(DisconnectCause cause) {
    _State = State.Error;

    base.OnFailedToConnectToPhoton(cause);
  }

  public new void OnJoinedRoom() {
    _State = State.WaitingForPlayers;

    UnityDB.Init();

    Debug.LogFormat("Connected to room '{0}' and waiting for other players (isMasterClient = {1})",
      PhotonNetwork.room.Name,
      PhotonNetwork.isMasterClient);

    if (PhotonNetwork.isMasterClient && string.IsNullOrEmpty(Map.Guid))
      Debug.Log("Select map manually");
  }

  #endregion

  #region UICallbacks

  public void OnUISkipWaitingButtonClicked() {
    if (_State == State.WaitingForPlayers) {
      var ht = new ExitGames.Client.Photon.Hashtable();
      ht.Add("START", true);
      PhotonNetwork.room.SetCustomProperties(ht);
      SkipWaitingButton.gameObject.SetActive(false);
      Debug.Log("Setting START on the Photon room");
    }
  }

  public void OnUIMapDropdownSelected() {
    Map.Guid = _mapGuids[MapDropdown.value];
    Debug.LogFormat("Selected map '{0}' with guid '{1}'", MapDropdown.options[MapDropdown.value].text, Map.Guid);
  }

  #endregion
}
}

