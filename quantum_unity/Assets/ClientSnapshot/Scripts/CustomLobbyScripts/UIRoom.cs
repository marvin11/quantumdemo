﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UI = UnityEngine.UI;
using Quantum;

namespace QuantumReconnect {
    public class UIRoom : UIScreen<UIRoom> {
        public UI.Text RoomName;
        public UI.Button StartButton;
        public UI.Text WaitingMessage;
        public UI.Dropdown MapSelect;

        public UI.GridLayoutGroup PlayerGrid;
        public UIRoomPlayer PlayerTemplate;
        public Boolean CloseRoomOnStart = true;

        

        Boolean _started;
        List<UIRoomPlayer> _players = new List<UIRoomPlayer>();

        const string MapSelectedKey = "Quantum_UIRoom_LastMapSelected";

        void Start() {
            PlayerTemplate.Hide();
        }

        void Update() {
            if (IsScreenVisible()) {
                if (PhotonNetwork.inRoom) {
                    // toggle start button state
                    StartButton.Toggle(PhotonNetwork.isMasterClient);

                    // toggle map select
                    MapSelect.Toggle(PhotonNetwork.isMasterClient);

                    // toggle
                    WaitingMessage.Toggle(PhotonNetwork.isMasterClient == false);

                    // update room name
                    RoomName.text = String.Format("{0} ({1}/{2})", PhotonNetwork.room.Name, PhotonNetwork.room.PlayerCount, PhotonNetwork.room.MaxPlayers);

                    // update players list
                    UpdatePlayerList();

                    //
                    CheckForGameStart();
                } else {
                    UIRoom.HideScreen();
                    UILobby.ShowScreen();
                }
            }
        }

        void CheckForGameStart() {
            if (_started) {
                return;
            }

            if (FrameSetup.Instance.SnapshotJoin || FrameSetup.Instance.IsLoadGame)
                return;
            

            var start = false;
            var map = default(String);

            if (TryGetRoomProperty<Boolean>("START", out start) && TryGetRoomProperty<String>("MAP", out map)) {
                if (start && String.IsNullOrEmpty(map) == false) {
                    _started = true;

                    RuntimeConfig config;
                    config = new RuntimeConfig();
                    config.RecordingFlags = RecordingFlags.Default;
                    config.Map.Guid = UnityDB.AllOf<MapAsset>().First(x => x.Settings.Scene == map).Settings.Guid;
          
                    var param = new QuantumRunner.StartParameters {
                        RuntimeConfig = config,
                        DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
                        ReplayProvider = null,
                        GameMode = Photon.Deterministic.DeterministicGameMode.Multiplayer,
                        InitialFrame = 0,
                    };

                    Debug.Log("QuantumRunner.StartGame From UIRoom");
                    QuantumRunner.StartGame(Guid.NewGuid().ToString(), PhotonNetwork.room.MaxPlayers, param);

                    UIRoom.HideScreen();
                    UILeaveGame.ShowScreen();
                }
            }
        }

        void UpdatePlayerList() {
            while (_players.Count < PhotonNetwork.room.MaxPlayers) {
                UIRoomPlayer instance;
                instance = Instantiate(PlayerTemplate);
                instance.transform.SetParent(PlayerGrid.transform, false);
                instance.transform.SetAsLastSibling();

                _players.Add(instance);
            }

            var i = 0;

            for (; i < PhotonNetwork.playerList.Length; ++i) {
                _players[i].Name.text = GetPlayerName(PhotonNetwork.playerList[i]);
                _players[i].Show();
            }

            for (; i < _players.Count; ++i) {
                _players[i].Hide();
            }

            WaitingMessage.transform.SetAsLastSibling();
        }

        String GetPlayerName(PhotonPlayer player) {
            String name = player.NickName;

            if (player.IsLocal) {
                name += " (You)";
            }

            if (player.IsMasterClient) {
                name += " (Room Owner)";
            }

            return name;
        }

        Boolean TryGetRoomProperty<T>(String key, out T value) {
            System.Object v;

            if (PhotonNetwork.room.CustomProperties.TryGetValue(key, out v)) {
                if (v is T) {
                    value = (T)v;
                    return true;
                }
            }

            value = default(T);
            return false;
        }

        public void OnLeaveClicked() {
            PhotonNetwork.LeaveRoom();
        }

        public void OnStartClicked() {
            if (PhotonNetwork.isMasterClient && PhotonNetwork.room.IsOpen) {
                var map = MapSelect.options[MapSelect.value].text;

#if UNITY_EDITOR
                PlayerPrefs.SetString(MapSelectedKey, map);
#endif

                var ht = new ExitGames.Client.Photon.Hashtable();
                ht.Add("MAP", map);
                ht.Add("MAP-GUID", UnityDB.AllOf<MapAsset>().First(x => x.Settings.Scene == map).Settings.Guid);
                ht.Add("START", true);

                if (CloseRoomOnStart) {
                    PhotonNetwork.room.IsOpen = false;
                }

                PhotonNetwork.room.SetCustomProperties(ht);
            }
        }

        public override void OnShowScreen(bool first) {
            _started = false;

            var list = UnityDB.AllOf<MapAsset>().Select(x => x.Settings.Scene).ToList();
            list.Sort();

            MapSelect.ClearOptions();
            MapSelect.AddOptions(list);
            MapSelect.value = 0;

#if UNITY_EDITOR
            if (PlayerPrefs.HasKey(MapSelectedKey)) {
                var map = PlayerPrefs.GetString(MapSelectedKey, string.Empty);
                var index = list.IndexOf(map);
                if (index >= 0)
                    MapSelect.value = index;
            }
#endif
        }
    }
}