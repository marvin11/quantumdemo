﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Deterministic.Protocol;
using Quantum;

namespace QuantumReconnect {
    public class FrameSetup : MonoBehaviour {
        // Further, all of the RPC Calls will be replaced by Photon Messages

        public static FrameSetup Instance;
        public byte[] Data;

        public string LastRoom;
        public bool IsLoadGame;
        public bool SnapshotJoin;
        public int FrameNumber;
        public bool LoadDone;
        public int ViewID = 1;

        private List<FrameSnapshot> _snaps = new List<FrameSnapshot>();
        private PhotonView _photonView;

        void Awake() {
            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);

                gameObject.AddComponent<PhotonView>();
                _photonView = PhotonView.Get(this);
                _photonView.viewID = ViewID;
            } else {
                DestroyObject(gameObject);
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        }

        public void ResetLoadGameSetup() {
            Data = null;
            SnapshotJoin = false;
            IsLoadGame = false;
            LoadDone = false;
            _snaps.Clear();
        }

        public void SetGameStateData(byte[] data) {
            Data = data;
            IsLoadGame = true;
            LoadDone = true;
            _photonView.RPC("SetLoadGame", PhotonTargets.Others, true);
            SendLoadedSnapshots(data, DeterministicSessionConfigAsset.Instance.Config.RollbackWindow);
            FrameNumber = DeterministicSessionConfigAsset.Instance.Config.RollbackWindow;
        }

        public void GetCurrentSnapshot(PhotonPlayer player) {
            _photonView.RPC("SnapshotRequested", PhotonTargets.MasterClient, player);
        }

        private void SendLoadedSnapshots(byte[] data, int rollBackWindow) {
            var allSnapshots = FrameSnapshot.Encode(rollBackWindow, data);
            foreach (var s in allSnapshots) {
                SendSnapshot(s);
            }
        }

        private void SendSnapshot(FrameSnapshot snap, PhotonPlayer player = null) {
            var b = new Photon.Deterministic.BitStream(50000);
            b.Writing = true;
            snap.Serialize(null, b);

            if (player == null) {
                _photonView.RPC("ReceiveSnap", PhotonTargets.Others, b.ToArray());
            } else {
                _photonView.RPC("ReceiveSnap", player, b.ToArray());
            }
        }

        public byte[] GetFrameData(out System.Int32 tick, bool isSavingGame) {
            Frame frame = (Frame)QuantumRunner.Default.Game.CreateFrame(QuantumRunner.Default.Game.CreateFrameContext());

            frame.CopyFrom(QuantumRunner.Default.Game.Frames.Verified);

            if (isSavingGame) {
                frame.Number = DeterministicSessionConfigAsset.Instance.Config.RollbackWindow;
            } else {
                frame.Number = QuantumRunner.Default.Game.Frames.Verified.Number;
            }

            tick = frame.Number;

            var frameData = frame.Serialize();
            return frameData;
        }

        [PunRPC]
        private void SnapshotRequested(PhotonPlayer player) {
            int tick;
            var frameData = GetFrameData(out tick, false);

            var allSnapshots = FrameSnapshot.Encode(tick, frameData);
            foreach (var s in allSnapshots) {
                SendSnapshot(s, player);
            }
        }

        [PunRPC]
        private void SetLoadGame(bool value) {
            IsLoadGame = value;
        }

        [PunRPC]
        private void ReceiveSnap(byte[] data) {
            var b = new Photon.Deterministic.BitStream();
            b.Reading = true;
            b.SetBuffer(data);

            FrameSnapshot snap = new FrameSnapshot();
            snap.Serialize(null, b);

            _snaps.Add(snap);
            if (snap.Last) {
                Debug.Log("Last snapshot received");
                FrameSnapshot.Decode(_snaps.ToArray(), ref Data, ref FrameNumber);

                LoadDone = true;
            }
        }
    }
}

