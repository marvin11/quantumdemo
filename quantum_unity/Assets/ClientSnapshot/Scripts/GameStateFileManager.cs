﻿using UnityEngine;
using Quantum;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace QuantumReconnect {
    unsafe public class GameStateFileManager : MonoBehaviour {
        private static Frame _frame;

        public static void SaveGameState(string fileName) {
            int tick;
            var frameData = FrameSetup.Instance.GetFrameData(out tick, true);
            SaveFile(fileName, frameData);
        }


        private static void SaveFile(string fileName, byte[] frameData) {
            string destination = Application.persistentDataPath + "/" + fileName + ".dat";
            FileStream file;

            if (File.Exists(destination)) file = File.OpenWrite(destination);
            else file = File.Create(destination);

            GameStateWrapper data = new GameStateWrapper(frameData);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(file, data);
            file.Close();
            Debug.Log("File saved on " + Application.persistentDataPath + "/" + fileName + ".dat");
        }

        public static GameStateWrapper LoadFile(string fileName) {

            string destination = Application.persistentDataPath + "/" + fileName + ".dat";
            FileStream file;

            if (File.Exists(destination)) file = File.OpenRead(destination);
            else {
                Debug.LogError("File not found");
                return null;
            }

            BinaryFormatter bf = new BinaryFormatter();
            GameStateWrapper data = (GameStateWrapper)bf.Deserialize(file);
            file.Close();

            if (data.FrameData.Length > 0) {
                Debug.Log("Load " + fileName + " successfully!");
            } else {
                Debug.Log("Nothing Loaded!");
            }

            return data;
        }
    }
}
