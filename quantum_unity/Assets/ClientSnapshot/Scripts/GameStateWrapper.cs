﻿namespace QuantumReconnect {
    [System.Serializable]
    public class GameStateWrapper {
        public byte[] FrameData;

        public GameStateWrapper(byte[] frameData) {
            FrameData = frameData;
        }
    }
}
