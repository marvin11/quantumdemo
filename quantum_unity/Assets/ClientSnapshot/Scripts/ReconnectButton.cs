﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuantumReconnect {
    public class ReconnectButton : MonoBehaviour {
        public void Reconnect() {
            SnapshotGameStarter.Instance.SnapshotJoinRoom();
        }
    }
}
