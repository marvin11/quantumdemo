﻿using UnityEngine;

namespace QuantumReconnect {

    public class ReconnectCallbacks : QuantumCallbacks {

        public override void OnGameStart(QuantumGame game) {
            FrameSetup.Instance.LastRoom = PhotonNetwork.room.Name;
        }

        public override void OnGameStartFromSnapshot(QuantumGame game, int frameNumber) {
            FrameSetup.Instance.LastRoom = PhotonNetwork.room.Name;
        }

        public override void OnGameDestroyed(QuantumGame game) {
            FrameSetup.Instance.ResetLoadGameSetup();
            SnapshotGameStarter.Instance.ResetStarter();
        }
    }
}
