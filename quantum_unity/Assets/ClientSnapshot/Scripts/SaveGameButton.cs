﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumReconnect {
    public class SaveGameButton : MonoBehaviour {
        private void Start() {
            if (!PhotonNetwork.isMasterClient) {
                gameObject.SetActive(false);
            }
        }

        public void SaveGameState(InputField inputField) {
            if (inputField.text == string.Empty) {
                inputField.text = "save";
            }
            GameStateFileManager.SaveGameState(inputField.text);
        }
    }
}
