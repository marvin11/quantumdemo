﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UI = UnityEngine.UI;
using Quantum;

namespace QuantumReconnect {
    public class SnapshotGameStarter : MonoBehaviour {
        public static SnapshotGameStarter Instance;

        private bool _isJoiningRoom;
        private bool _gameStarted;
        private bool _localGameStarted;

        private void Start() {
            if (Instance == null)
                Instance = this;

            _isJoiningRoom = false;
            _gameStarted = false;
            _localGameStarted = false;
        }

        public void ResetStarter() {
            _isJoiningRoom = false;
            _gameStarted = false;
            _localGameStarted = false;
        }

        public void SnapshotJoinRoom(string roomName = null) {
            FrameSetup.Instance.ResetLoadGameSetup();
            FrameSetup.Instance.SnapshotJoin = true;
            _isJoiningRoom = true;
            if (roomName == null) {
                PhotonNetwork.JoinRoom(FrameSetup.Instance.LastRoom);
            } else {
                PhotonNetwork.JoinRoom(roomName);
            }
        }

        public void LoadGameState(string fileName) {
            if (PhotonNetwork.isMasterClient) {
                if (fileName == string.Empty)
                    fileName = "save"; // default save file name
                var gameStateWrapper = GameStateFileManager.LoadFile(fileName);
                if (gameStateWrapper != null) {
                    FrameSetup.Instance.SetGameStateData(gameStateWrapper.FrameData);
                }
            }
        }

        private void Update() {
            if (_localGameStarted)
                return;

            if (!PhotonNetwork.inRoom) return;

            if (_isJoiningRoom) {
                _isJoiningRoom = false;
                _gameStarted = false;
                TryGetRoomProperty<Boolean>("START", out _gameStarted);
                if (_gameStarted) {
                    FrameSetup.Instance.GetCurrentSnapshot(PhotonNetwork.player);
                } else {
                    FrameSetup.Instance.SnapshotJoin = false;
                }
            }


            if (!FrameSetup.Instance.LoadDone) return;

            var map = default(String);

            if (TryGetRoomProperty<Boolean>("START", out _gameStarted) && TryGetRoomProperty<String>("MAP", out map)) {
                if (_gameStarted && String.IsNullOrEmpty(map) == false) {
                    RuntimeConfig config;
                    config = new RuntimeConfig();
                    config.RecordingFlags = RecordingFlags.Default;
                    config.Map.Guid = UnityDB.AllOf<MapAsset>().First(x => x.Settings.Scene == map).Settings.Guid;

                    var param = new QuantumRunner.StartParameters {
                        RuntimeConfig = config,
                        DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
                        ReplayProvider = null,
                        GameMode = Photon.Deterministic.DeterministicGameMode.Multiplayer,
                    };

                    Debug.Log("Start Snapshotgame Game!");
                    param.InitialFrame = FrameSetup.Instance.FrameNumber;
                    param.FrameData = FrameSetup.Instance.Data;

                    Debug.Log("QuantumRunner.StartGame " + param.InitialFrame);
                    _localGameStarted = true;
                    QuantumRunner.StartGame(Guid.NewGuid().ToString(), PhotonNetwork.room.MaxPlayers, param);

                    UIRoom.HideScreen();
                    UILeaveGame.ShowScreen();
                }
            }
        }

        Boolean TryGetRoomProperty<T>(String key, out T value) {
            System.Object v;

            if (PhotonNetwork.room.CustomProperties.TryGetValue(key, out v)) {
                if (v is T) {
                    value = (T)v;
                    return true;
                }
            }

            value = default(T);
            return false;
        }
    }
}
