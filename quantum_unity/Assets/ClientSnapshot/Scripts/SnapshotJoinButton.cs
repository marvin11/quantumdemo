﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QuantumReconnect {
    public class SnapshotJoinButton : MonoBehaviour {

        public InputField FileNameField;

        public void LoadGameStateFile() {
            SnapshotGameStarter.Instance.LoadGameState(FileNameField.text);
        }
    }
}

