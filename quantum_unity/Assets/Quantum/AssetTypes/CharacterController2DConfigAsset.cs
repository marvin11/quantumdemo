﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/Character Controller 2D", order = (00 * 26 * 26) + (15 * 26) + (2))]
public partial class CharacterController2DConfigAsset : AssetBase
{
  public Quantum.CharacterController2DConfig Settings;

  public override Quantum.AssetObject AssetObject
  {
    get { return Settings; }
  }

  public override void Reset()
  {
    if (Settings == null)
      Settings = new Quantum.CharacterController2DConfig();
    if (string.IsNullOrEmpty(Settings.Guid))
      Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class CharacterController3DConfigAssetExt
{
  public static CharacterController2DConfigAsset GetUnityAsset(this Quantum.CharacterController2DConfig data)
  {
    return data == null ? null : UnityDB.FindAsset<CharacterController2DConfigAsset>(data.Id);
  }
}
