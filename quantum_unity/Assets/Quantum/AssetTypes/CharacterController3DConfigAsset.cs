﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/Character Controller 3D", order = (00 * 26 * 26) + (15 * 26) + (2))]
public partial class CharacterController3DConfigAsset : AssetBase
{
  public Quantum.CharacterController3DConfig Settings;

  public override Quantum.AssetObject AssetObject
  {
    get { return Settings; }
  }

  public override void Reset()
  {
    if (Settings == null)
      Settings = new Quantum.CharacterController3DConfig();
    if (string.IsNullOrEmpty(Settings.Guid))
      Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class CharacterController3DConfigAssetExt
{
  public static CharacterController3DConfigAsset GetUnityAsset(this Quantum.CharacterController3DConfig data)
  {
    return data == null ? null : UnityDB.FindAsset<CharacterController3DConfigAsset>(data.Id);
  }
}
