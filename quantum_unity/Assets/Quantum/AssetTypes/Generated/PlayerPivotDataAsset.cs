/**
 * This code was auto-generated by a tool, every time
 * the tool executes this code will be reset.
 *
 * If you need to extend the classes generated to add 
 * fields or methods to them, please create partial 
 * declarations in another file.
 **/

using Quantum;
using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/PlayerPivotData", order = 390)]
public partial class PlayerPivotDataAsset : AssetBase {
  public PlayerPivotData Settings;

  public override AssetObject AssetObject {
    get { return Settings; }
  }

  public override void Reset() {
    if (Settings == null)
        Settings = new PlayerPivotData();
    if (string.IsNullOrEmpty(Settings.Guid))
        Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class PlayerPivotDataAssetExts {
  public static PlayerPivotDataAsset GetUnityAsset(this PlayerPivotData data) {
    return data == null ? null : UnityDB.FindAsset<PlayerPivotDataAsset>(data.Id);
  }
}
