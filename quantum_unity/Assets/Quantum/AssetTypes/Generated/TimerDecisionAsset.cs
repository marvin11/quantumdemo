/**
 * This code was auto-generated by a tool, every time
 * the tool executes this code will be reset.
 *
 * If you need to extend the classes generated to add 
 * fields or methods to them, please create partial 
 * declarations in another file.
 **/

using Quantum;
using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/HFSMDecision/TimerDecision", order = 201)]
public partial class TimerDecisionAsset : HFSMDecisionAsset {
  public TimerDecision Settings;

  public override AssetObject AssetObject {
    get { return Settings; }
  }

  public override void Reset() {
    if (Settings == null)
        Settings = new TimerDecision();
    if (string.IsNullOrEmpty(Settings.Guid))
        Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class TimerDecisionAssetExts {
  public static TimerDecisionAsset GetUnityAsset(this TimerDecision data) {
    return data == null ? null : UnityDB.FindAsset<TimerDecisionAsset>(data.Id);
  }
}
