using Quantum;
using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Map", order = (00 * 26 * 26) + (12 * 26) + (00))]
public partial class MapAsset : AssetBase {
  public Map Settings;

  public override AssetObject AssetObject {
    get { return Settings; }
  }
}

public static partial class MapAssetExts {
  public static MapAsset GetUnityAsset(this Map data) {
    return data == null ? null : UnityDB.FindAsset<MapAsset>(data.Id);
  }
}
