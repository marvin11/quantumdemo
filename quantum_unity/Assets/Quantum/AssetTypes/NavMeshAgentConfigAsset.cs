﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/NavMesh Agent Config", order = (00 * 26 * 26) + (15 * 26) + (13))]
public partial class NavMeshAgentConfigAsset : AssetBase {
  public Quantum.NavMeshAgentConfig Settings;

  public override Quantum.AssetObject AssetObject {
    get { return Settings; }
  }

  public override void Reset() {
    if (Settings == null)
      Settings = new Quantum.NavMeshAgentConfig();
    if (string.IsNullOrEmpty(Settings.Guid))
      Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class NavMeshAgentConfigAssetExts {
  public static NavMeshAgentConfigAsset GetUnityAsset(this Quantum.NavMeshAgentConfig data) {
    return data == null ? null : UnityDB.FindAsset<NavMeshAgentConfigAsset>(data.Id);
  }
}
