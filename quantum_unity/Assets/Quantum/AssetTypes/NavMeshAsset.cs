﻿using Quantum;
using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/NavMeshAsset", order = (00 * 26 * 26) + (13 * 26) + (00))]
public class NavMeshAsset : AssetBase {

  public NavMesh Settings;

  public override AssetObject AssetObject {
    get {
      return Settings;
    }
  }
}

public static partial class NavMeshAssetExts {
  public static NavMeshAsset GetUnityAsset(this NavMesh data) {
    return data == null ? null : UnityDB.FindAsset<NavMeshAsset>(data.Id);
  }
}
