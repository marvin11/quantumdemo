﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/Physics Material", order = (00 * 26 * 26) + (15 * 26) + (15))]
public partial class PhysicsMaterialAsset : AssetBase {
  public Quantum.PhysicsMaterial Settings;

  public override Quantum.AssetObject AssetObject {
    get { return Settings; }
  }

  public override void Reset() {
    if (Settings == null)
      Settings = new Quantum.PhysicsMaterial();
    if (string.IsNullOrEmpty(Settings.Guid))
      Settings.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();
  }
}

public static partial class PhysicsMaterialAssetExt {
  public static PhysicsMaterialAsset GetUnityAsset(this Quantum.PhysicsMaterial data) {
    return data == null ? null : UnityDB.FindAsset<PhysicsMaterialAsset>(data.Id);
  }
}
