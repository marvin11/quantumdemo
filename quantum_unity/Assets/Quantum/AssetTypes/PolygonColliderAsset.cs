﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/Polygon Collider", order = (00 * 26 * 26) + (15 * 26) + (15))]
public partial class PolygonColliderAsset : AssetBase {
  public Quantum.PolygonCollider Settings;

  public override Quantum.AssetObject AssetObject {
    get { return Settings; }
  }
}

public static partial class PolygonColliderAssetExt {
  public static PolygonColliderAsset GetUnityAsset(this Quantum.PolygonCollider data) {
    return data == null ? null : UnityDB.FindAsset<PolygonColliderAsset>(data.Id);
  }
}