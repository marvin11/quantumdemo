﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Assets/Physics/Terrain Collider 3D", order = (00 * 26 * 26) + (15 * 26) + (19))]
public partial class TerrainColliderAsset : AssetBase {
  public Quantum.TerrainCollider Settings;

  public override Quantum.AssetObject AssetObject {
    get { return Settings; }
  }
}

public static partial class TerrainColliderAssetExt {
  public static TerrainColliderAsset GetUnityAsset(this Quantum.TerrainCollider data) {
    return data == null ? null : UnityDB.FindAsset<TerrainColliderAsset>(data.Id);
  }
}
