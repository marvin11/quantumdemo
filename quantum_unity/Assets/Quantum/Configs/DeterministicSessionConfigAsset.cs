﻿using UnityEngine;
using Photon.Deterministic;

[CreateAssetMenu(menuName = "Quantum/Configurations/Deterministic", order = (02 * 26 * 26) + (03 * 26) + (00))]
public class DeterministicSessionConfigAsset : ScriptableObject {
  public DeterministicSessionConfig Config;

  public static DeterministicSessionConfigAsset Instance {
    get {
      return Resources.Load<DeterministicSessionConfigAsset>("DeterministicConfig");
    }
  }
}
