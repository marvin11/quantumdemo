﻿using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Configurations/SimulationConfig", fileName = "SimulationConfig", order = (02 * 26 * 26) + (18 * 26) + (00))]
public partial class SimulationConfigAsset : AssetBase {

  public static SimulationConfigAsset Instance {
    get {
      // Try not to use this anymore. 
      // You can get the SimulationConfig from the DB or use the actual asset in Unity.
      return Resources.LoadAll<SimulationConfigAsset>("DB")[0];
    }
  }

  public override void Reset() {
    Settings = new Quantum.SimulationConfig();
    Settings.Physics = new Quantum.SimulationConfig.SimulationConfigPhysics();
    Settings.NavMeshAgent = new Quantum.SimulationConfig.SimulationConfigNavMeshAgent();

    AssetObject.Guid = System.Guid.NewGuid().ToString().ToLowerInvariant();

    Settings.Physics.DefaultPhysicsMaterial.Guid = Quantum.PhysicsMaterial.DEFAULT_ID;
    Settings.Physics.DefaultCharacterController2D.Guid = Quantum.CharacterController2DConfig.DEFAULT_ID;
    Settings.Physics.DefaultCharacterController3D.Guid = Quantum.CharacterController3DConfig.DEFAULT_ID;
    Settings.NavMeshAgent.DefaultNavMeshAgent.Guid = Quantum.NavMeshAgentConfig.DEFAULT_ID;

    SimulationConfigAssetHelper.ImportLayersFromUnity(this);
  }
}
