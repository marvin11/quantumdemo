﻿using UnityEditor;

// Disable this class completely by setting this define in the Unity player settings
#if !DISABLE_QUANTUM_ASSET_INSPECTOR

namespace Quantum.Editor {
  [CustomEditor(typeof(AssetBase), true)]
  public class AssetBaseEditor : UnityEditor.Editor {

    public override void OnInspectorGUI() {
      if (!QuantumEditorSettings.Instance.UseQuantumAssetInspector) {
        // Soft deactivate the Quantum asset editor
        base.OnInspectorGUI();
        return;
      }

      // This draws all fields except the "Settings" property.
      CustomEditorsHelper.DrawDefaultInspector(serializedObject, new string[] { "Settings" });

      // As we show only the content of "Settings" add a headline here to make it clear that this is not the default asset layout.
      CustomEditorsHelper.DrawHeadline(target != null ? target.GetType().Name : "Quantum Asset Inspector");

      // We know the data is called "Settings" so we can go ahead and only render the content of "Settings".
      CustomEditorsHelper.BeginBox();
      CustomEditorsHelper.DrawDefaultInspector(serializedObject, "Settings", new string[] { }, false);
      CustomEditorsHelper.EndBox();
    }
  }
}

#endif