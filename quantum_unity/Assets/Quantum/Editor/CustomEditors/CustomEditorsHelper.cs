﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

public static class CustomEditorsHelper {
  public static void DrawHeadline(string header) {
    EditorGUILayout.Space();
    EditorGUILayout.LabelField(header, EditorStyles.boldLabel);
  }

  public static void DrawScriptFromScriptableObject(UnityEngine.Object obj) {
    MonoScript script = null;
    if (obj != null)
      script = MonoScript.FromScriptableObject((ScriptableObject)obj);
    GUI.enabled = false;
    script = EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false) as MonoScript;
    GUI.enabled = true;
  }

  public static void DrawScriptFromMonoBehaviour(UnityEngine.Object obj) {
    MonoScript script = null;
    if (obj != null)
      script = MonoScript.FromMonoBehaviour((MonoBehaviour)obj);
    GUI.enabled = false;
    script = EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false) as MonoScript;
    GUI.enabled = true;
  }

  public static bool DrawDefaultInspector(SerializedObject obj, string path, string[] filter, bool showFoldout = true) {
    bool foldout = false;
    return DrawDefaultInspector(obj, path, filter, showFoldout, ref foldout);
  }

  public static bool DrawDefaultInspector(SerializedObject obj, string path, string[] filter, bool showFoldout, ref bool foldout) {
    var property = obj.FindProperty(path);
    if (property == null)
      return false;

    if (showFoldout) {
      EditorStyles.foldout.fontStyle = FontStyle.Bold;
      var pathTokens = path.Split('.');
      property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, pathTokens[pathTokens.Length - 1], true);
      EditorStyles.foldout.fontStyle = FontStyle.Normal;
      foldout = property.isExpanded;
      if (!property.isExpanded)
        return false;

      EditorGUI.indentLevel++;
    }

    EditorGUI.BeginChangeCheck();
    obj.Update();

    bool expanded = true;
    while (property.NextVisible(expanded)) {
      if (filter.Any(f => property.propertyPath.Equals(f)))
        continue;
      if (!property.propertyPath.StartsWith(path))
        continue;

      using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath)) {
        EditorGUILayout.PropertyField(property, true);
      }

      expanded = false;
    }

    if (showFoldout)
      EditorGUI.indentLevel--;

    obj.ApplyModifiedProperties();
    return EditorGUI.EndChangeCheck();
  }

  public static bool DrawDefaultInspector(SerializedObject obj, string[] filter) {
    EditorGUI.BeginChangeCheck();
    obj.Update();

    SerializedProperty property = obj.GetIterator();
    bool expanded = true;
    while (property.NextVisible(expanded)) {
      if (filter.Any(f => property.propertyPath.StartsWith(f)))
        continue;

      using (new EditorGUI.DisabledScope("m_Script" == property.propertyPath)) {
        EditorGUILayout.PropertyField(property, true);
      }

      expanded = false;
    }

    obj.ApplyModifiedProperties();
    return EditorGUI.EndChangeCheck();
  }

  public static void BeginBox(string headline = null) {
#if !UNITY_2019_3_OR_NEWER
    if (EditorGUIUtility.isProSkin)
      GUI.backgroundColor = Color.grey;
#endif
    GUILayout.BeginVertical(EditorStyles.helpBox);
    if (!string.IsNullOrEmpty(headline))
      EditorGUILayout.LabelField(headline, EditorStyles.boldLabel);
    EditorGUI.indentLevel++;
  }

  public static void EndBox() {
    EditorGUI.indentLevel--;
    GUILayout.EndVertical();
  }

  public class BoxScope : IDisposable {
    public BoxScope(string headline = null) {
      BeginBox(headline);
    }

    public void Dispose() {
      EndBox();
    }
  }

  public class EditorFoldoutScope : IDisposable {
    public bool IsFoldout;
    public EditorFoldoutScope(string headline, string key) {
      BeginBox(null);
      EditorGUI.indentLevel--;

      IsFoldout = EditorPrefs.GetBool(key);
      var newIsFoldout = EditorGUILayout.Foldout(EditorPrefs.GetBool(key), headline);
      if (newIsFoldout != IsFoldout) {
        EditorPrefs.SetBool(key, newIsFoldout);
        IsFoldout = newIsFoldout;
      }

      EditorGUI.indentLevel++;
    }

    public void Dispose() {
      EndBox();
    }
  }
}
