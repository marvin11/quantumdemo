﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EntityPrefabRoot))]
public class EntityPrefabRootEditor : Editor {

  private DumpTree tree = new DumpTree();

  public unsafe override void OnInspectorGUI() {
    base.OnInspectorGUI();

    if (QuantumRunner.Default == null)
      return;

    var data = (EntityPrefabRoot)target;

    GUI.enabled = false;
    EditorGUILayout.TextField("Quantum Entity Id", data.EntityRef.ToString());
    GUI.enabled = true;

    GUILayout.BeginVertical();

    DataDumper.nextIndex = 0;
    DumpNode n = DataDumper.CreateNode(tree, 0);
    n.name = "Quantum Entity Root";
    n.value = null;
    n.initialized = true;
    n.nodeType = NodeType.Root;

    if (QuantumRunner.Default.Game.Frames.Current != null) {
      var entity = QuantumRunner.Default.Game.Frames.Current.GetEntity(data.EntityRef);
      var concreteEntity = Quantum.Entity.ToBoxedStruct(entity);
      if (concreteEntity != null) {
        DataDumper.Calculate(n, "Quantum Entity (" + concreteEntity.GetType().Name + ")", concreteEntity, 0, tree);
        QuantumStateInspector.DebugNode(n.children[0].children[0]);
      }
    }

    GUILayout.EndVertical();
  }
}
