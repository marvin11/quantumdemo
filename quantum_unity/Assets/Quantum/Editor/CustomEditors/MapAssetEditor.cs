﻿using UnityEditor;

namespace Quantum.Editor {
  [CustomEditor(typeof(MapAsset), true)]
  public class MapAssetEditor : AssetBaseEditor {
    public override void OnInspectorGUI() {
      if (!QuantumEditorSettings.Instance.UseQuantumAssetInspector) {
        // Soft deactivate the Quantum asset editor
        base.OnInspectorGUI();
        return;
      }

      // This draws all fields except the "Settings" and script.
      CustomEditorsHelper.DrawDefaultInspector(serializedObject, new string[] { "Settings", "m_Script" });

      // As we show only the content of "Settings" add a headline here to make it clear that this is not the default asset layout.
      CustomEditorsHelper.DrawHeadline(target != null ? target.GetType().Name : "Quantum Asset Inspector");

      using (new CustomEditorsHelper.BoxScope()) {
        CustomEditorsHelper.DrawDefaultInspector(serializedObject, "Settings", new string[] { "Settings.GridSize" }, false);
      }

      var data = target as MapAsset;

      // Convert from GridSize which became obsolete
      {
        if (data.Settings.GridSizeX == 0) {
          data.Settings.GridSizeX = data.Settings.GridSize;
        }
        if (data.Settings.GridSizeY == 0) {
          data.Settings.GridSizeY = data.Settings.GridSize;
        }
      }

      if (data.Settings.GridSizeX < 2) {
        data.Settings.GridSizeX = 2;
      }

      if (data.Settings.GridSizeY < 2) {
        data.Settings.GridSizeY = 2;
      }

      if ((data.Settings.GridSizeX & 1) == 1) {
        data.Settings.GridSizeX += 1;
      }

      if ((data.Settings.GridSizeY & 1) == 1) {
        data.Settings.GridSizeY += 1;
      }

      if (data.Settings.GridNodeSize < 2) {
        data.Settings.GridNodeSize = 2;
      }

      if ((data.Settings.GridNodeSize & 1) == 1) {
        data.Settings.GridNodeSize += 1;
      }
    }
  }
}