﻿using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomEditor(typeof(MapData), true)]
  public class MapDataEditor : UnityEditor.Editor {
    public override void OnInspectorGUI() {

      CustomEditorsHelper.DrawHeadline("MapData");

      var data = target as MapData;
      if (data) {

        // Never move the map center
        data.transform.position = Vector3.zero;

        using (new CustomEditorsHelper.BoxScope()) {
          base.DrawDefaultInspector();

          if (data.Asset) {
            EditorGUI.BeginDisabledGroup(EditorApplication.isPlayingOrWillChangePlaymode);

            var backgroundColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.green;

            if (GUILayout.Button("Bake Data", GUILayout.Height(EditorGUIUtility.singleLineHeight * 2))) {
              Undo.RecordObject(target, "Bake Data");
              MapDataBaker.BakeMapData(data, true);
              EditorUtility.SetDirty(data.Asset);
              AssetDatabase.Refresh();
            }

            if (GUILayout.Button("Bake Nav Meshes", GUILayout.Height(EditorGUIUtility.singleLineHeight * 2))) {
              Undo.RecordObject(target, "Bake Nav Meshes");
              MapDataBaker.BakeNavMeshes(data, true);
              EditorUtility.SetDirty(data.Asset);
              AssetDatabase.Refresh();
            }

            GUI.backgroundColor = backgroundColor;

            EditorGUI.EndDisabledGroup();
          }
        }

        if (data.Asset) {
          // Draw map asset inspector
          if (QuantumEditorSettings.Instance.UseQuantumAssetInspector) {
            var objectEditor = CreateEditor(data.Asset, typeof(MapAssetEditor));
            objectEditor.OnInspectorGUI();
          }
          else {
            CreateEditor(data.Asset).DrawDefaultInspector();
          }
        }
      }
    }
  }
}