﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEditor;

namespace Quantum.Editor {
  public partial class MapNavMeshDefinitionEditor {

    public static float FindSmallestAgentRadius(MapNavMeshDefinition data) {

      Type navMeshSurfaceType = TypeUtils.FindType("NavMeshSurface");
      if (navMeshSurfaceType != null && data.NavMeshSurfaces != null) {
        // Overide by Unity Navmesh Surface tool
        float agentRadius = float.MaxValue;
        foreach (var surface in data.NavMeshSurfaces) {
          var surfaceComponent = surface.GetComponent(navMeshSurfaceType);
          if (surfaceComponent == null) {
            Debug.LogErrorFormat("No NavMeshSurface found on '{0}'", surface.name);
          }
          else {
            var serializedObject = new SerializedObject(surfaceComponent);
            var agentTypeID = serializedObject.FindProperty("m_AgentTypeID").intValue;
            if (agentTypeID != -1) {
              var settings = UnityEngine.AI.NavMesh.GetSettingsByID(agentTypeID);
              if (settings.agentRadius < agentRadius) {
                agentRadius = settings.agentRadius;
              }
            }
          }
        }

        if (agentRadius < float.MaxValue) {
          return agentRadius;
        }
      }

      // Use regular Unity Navigation tool as default
      var settingsObject = new SerializedObject(UnityEditor.AI.NavMeshBuilder.navMeshSettingsObject);
      var radiusProperty = settingsObject.FindProperty("m_BuildSettings.agentRadius");
      return radiusProperty.floatValue;
    }

    public static void ImportFromUnity(MapNavMeshDefinition data) {

      data.InvalidateGizmoMesh();

      // If NavMeshSurface installed, this will deactivate non linked surfaces 
      // to make the CalculateTriangulation work only with the selected Unity nav mesh.
      List<GameObject> deactivatedObjects = new List<GameObject>();
      if (data.NavMeshSurfaces != null && data.NavMeshSurfaces.Length > 0) {
        Type navMeshSurfaceType = TypeUtils.FindType("NavMeshSurface");
        if (navMeshSurfaceType != null) {
          var surfaces = FindObjectsOfType(navMeshSurfaceType);
          foreach (MonoBehaviour surface in surfaces) {
            if (data.NavMeshSurfaces.Contains(surface.gameObject) == false) {
              surface.gameObject.SetActive(false);
              deactivatedObjects.Add(surface.gameObject);
            }
          }
        }
      }

      using (var progressBar = new ProgressBar("Importing Unity NavMesh")) {

        progressBar.Info = "Calculate Triangulation";
        var unityNavMeshTriangulation = UnityEngine.AI.NavMesh.CalculateTriangulation();

        if (unityNavMeshTriangulation.vertices.Length > 0) {

          progressBar.Info = "Loading Vertices";
          MapNavMeshVertex[] Vertices = new MapNavMeshVertex[unityNavMeshTriangulation.vertices.Length];
          for (int i = 0; i < Vertices.Length; ++i) {
            progressBar.Progress = i / (float)Vertices.Length;
            Vertices[i].Position = unityNavMeshTriangulation.vertices[i];
            Vertices[i].Neighbors = new List<int>();
            Vertices[i].Triangles = new List<int>();
          }

          progressBar.Info = "Loading Triangles";
          int triangleCount = unityNavMeshTriangulation.indices.Length / 3;
          MapNavMeshTriangle[] Triangles = new MapNavMeshTriangle[triangleCount];
          for (int i = 0; i < triangleCount; ++i) {
            progressBar.Progress = i / (float)triangleCount;
            int area = unityNavMeshTriangulation.areas[i];
            int baseIndex = i * 3;
            Triangles[i] = new MapNavMeshTriangle() {
              VertexIds2 = new int[] {
            unityNavMeshTriangulation.indices[baseIndex + 0],
            unityNavMeshTriangulation.indices[baseIndex + 1],
            unityNavMeshTriangulation.indices[baseIndex + 2] },
              Area = area,
              Regions = 0UL
            };
          }

          // Weld vertices
          if (data.WeldIdenticalVertices) {
            progressBar.Info = "Welding Intentical Vertices";
            WeldIdenticalVertices(ref Vertices, ref Triangles, data.WeldVertexEpsilon, p => progressBar.Progress = p);

            progressBar.Info = "Removing Unsued Vertices";
            RemoveUnusedVertices(ref Vertices, ref Triangles, p => progressBar.Progress = p);
          }

          // Merge vertices that ly on triangle edges
          if (data.FixTrianglesOnEdges) {
            progressBar.Info = "Fixing Triangles On Edges";
            for (int t = 0; t < Triangles.Length; ++t) {
              progressBar.Progress = t / (float)Triangles.Length;
              for (int v = 0; v < 3; ++v) {
                FixTrianglesOnEdges(ref Vertices, ref Triangles, t, v);
              }
            }

            progressBar.Info = "Removing Unsued Vertices";
            RemoveUnusedVertices(ref Vertices, ref Triangles, p => progressBar.Progress = p);
          }

          // Import regions
          List<string> regionMap = new List<string>();
          if (data.ImportRegions) {
            progressBar.Info = "Importing Regions";
            for (int t = 0; t < Triangles.Length; t++) {
              progressBar.Progress = t / (float)Triangles.Length;
              if (data.RegionAreaIds != null && data.RegionAreaIds.Contains(Triangles[t].Area) && Triangles[t].Regions <= 0) {
                ImportRegions(ref Vertices, ref Triangles, t, ref regionMap, data.RegionDetectionMargin);
              }
            }
          }

          // Set all vertex string ids (to work with manual editor)
          {
            progressBar.Info = "Finalizing Triangles";
            for (int t = 0; t < Triangles.Length; ++t) {
              Triangles[t].VertexIds = new string[3];
              for (int v = 0; v < 3; v++) {
                Triangles[t].VertexIds[v] = Triangles[t].VertexIds2[v].ToString();
              }
            }

            progressBar.Info = "Finalizing Vertices";
            progressBar.Progress = 0.5f;
            for (int v = 0; v < Vertices.Length; v++) {
              Vertices[v].Id = v.ToString();
            }
          }

          data.Vertices = Vertices.ToArray();
          data.Triangles = Triangles.ToArray();
          data.RegionMap = regionMap;

          if (regionMap.Count > 0) {
            Debug.LogFormat("Imported Unity NavMesh '{0}', cleaned up {1} vertices and found {2} regions", data.name, unityNavMeshTriangulation.vertices.Length - data.Vertices.Length, regionMap.Count);
          }
          else {
            Debug.LogFormat("Imported Unity NavMesh '{0}' and cleaned up {1} vertices", data.name, unityNavMeshTriangulation.vertices.Length - data.Vertices.Length);
          }
        }
        else {
          Debug.LogError("Unity NavMesh not found");
        }

        foreach (var go in deactivatedObjects) {
          go.SetActive(true);
        }
      }
    }

    static void WeldIdenticalVertices(ref MapNavMeshVertex[] vertices, ref MapNavMeshTriangle[] triangles, float cleanupEpsilon, Action<float> reporter) {
      int[] vertexRemapTable = new int[vertices.Length];
      for (int i = 0; i < vertexRemapTable.Length; ++i) {
        vertexRemapTable[i] = i;
      }

      for (int i = 0; i < vertices.Length; ++i) {
        reporter.Invoke(i /  (float)vertices.Length);
        Vector3 v = vertices[i].Position;

        for (int j = i + 1; j < vertices.Length; ++j) {
          if (j != vertexRemapTable[j]) {
            continue;
          }

          Vector3 v2 = vertices[j].Position;
          if (Mathf.Abs(Vector3.SqrMagnitude(v2 - v)) <= cleanupEpsilon) {
            vertexRemapTable[j] = i;
          }
        }
      }

      for (int i = 0; i < triangles.Length; ++i) {
        for (int v = 0; v < 3; v++) {
          triangles[i].VertexIds2[v] =
            vertexRemapTable[triangles[i].VertexIds2[v]];
        }
      }
    }

    static void RemoveUnusedVertices(ref MapNavMeshVertex[] vertices, ref MapNavMeshTriangle[] triangles, Action<float> reporter) {
      List<MapNavMeshVertex> newVertices = new List<MapNavMeshVertex>();
      int[] remapArray = new int[vertices.Length];
      for (int i = 0; i < remapArray.Length; ++i) {
        remapArray[i] = -1;
      }

      for (int t = 0; t < triangles.Length; ++t) {
        reporter.Invoke(t / (float)triangles.Length);
        for (int v = 0; v < 3; v++) {
          int newIndex = remapArray[triangles[t].VertexIds2[v]];
          if (newIndex < 0) {
            newIndex = newVertices.Count;
            remapArray[triangles[t].VertexIds2[v]] = newIndex;
            newVertices.Add(vertices[triangles[t].VertexIds2[v]]);
          }
          triangles[t].VertexIds2[v] = newIndex;
        }
      }

      //Debug.Log("Removed Unused Vertices: " + (vertices.Length - newVertices.Count));

      vertices = newVertices.ToArray();
    }

    static void ImportRegions(ref MapNavMeshVertex[] vertices, ref MapNavMeshTriangle[] triangles, int t, ref List<string> regionMap, float regionDetectionMargin) {
      // Expand the triangle until we have an isolated island containing all connected triangles of the same region
      HashSet<int> island = new HashSet<int>();
      HashSet<int> verticies = new HashSet<int>();
      island.Add(t);
      verticies.Add(triangles[t].VertexIds2[0]);
      verticies.Add(triangles[t].VertexIds2[1]);
      verticies.Add(triangles[t].VertexIds2[2]);
      bool isIslandComplete = false;
      while (!isIslandComplete) {
        isIslandComplete = true;
        for (int j = 0; j < triangles.Length; j++) {
          if (triangles[t].Area == triangles[j].Area && !island.Contains(j)) {
            for (int v = 0; v < 3; v++) {
              if (verticies.Contains(triangles[j].VertexIds2[v])) {
                island.Add(j);
                verticies.Add(triangles[j].VertexIds2[0]);
                verticies.Add(triangles[j].VertexIds2[1]);
                verticies.Add(triangles[j].VertexIds2[2]);
                isIslandComplete = false;
                break;
              }
            }
          }
        }
      }

      // Go through all MapNavMeshRegion scripts in the scene and check if all vertices of the islands
      // are within its bounds. Use the smallest possible bounds/region found. Use the RegionIndex from that for all triangles.
      if (island.Count > 0) {
        string regionId = string.Empty;
        float smallestRegionBounds = float.MaxValue;
        MapNavMeshRegion[] regions = GameObject.FindObjectsOfType<MapNavMeshRegion>();
        foreach (var region in regions) {
          Bounds bounds = region.gameObject.GetComponent<MeshRenderer>().bounds;
          // Grow the bounds, because the generated map is not exact
          bounds.Expand(regionDetectionMargin);
          bool isInsideBounds = true;
          foreach (var triangleIndex in island) {
            for (int v = 0; v < 3; v++) {
              Vector3 position = vertices[triangles[triangleIndex].VertexIds2[v]].Position;
              if (bounds.Contains(position) == false) {
                isInsideBounds = false;
                break;
              }
            }
          }

          if (isInsideBounds) {
            float size = bounds.extents.sqrMagnitude;
            if (size < smallestRegionBounds) {
              smallestRegionBounds = size;
              regionId = region.Id;
            }
          }
        }

        // Save the toggle region index on the triangles imported from Unity
        if (string.IsNullOrEmpty(regionId) == false) {
          if (regionMap.Contains(regionId) == false) {

            if (regionMap.Count >= NavMesh.MaxRegions) {
              // Still add to region map, but it won't be set on the triangles.
              Debug.LogErrorFormat("Failed to create region '{0}' because Quantum max region ({1}) reached. Reduce the number of regions.", regionId, NavMesh.MaxRegions);
            }

            regionMap.Add(regionId);
          }

          int toggleRegionIndex = regionMap.IndexOf(regionId);
          if (toggleRegionIndex < NavMesh.MaxRegions) {
            //Debug.LogFormat("Importing toggle region {0}({1}) with {2} triangles ", regionId, toggleRegionIndex, island.Count);

            foreach (var triangleIndex in island) {
              triangles[triangleIndex].Regions = 1UL << toggleRegionIndex;
            }
          }
        }
        else {
          Debug.LogWarningFormat("A triangle island (count = {0}) can not be matched with any region bounds, try to increase the RegionDetectionMargin.", island.Count);
        }
      }
    }

    static void FixTrianglesOnEdges(ref MapNavMeshVertex[] vertices, ref MapNavMeshTriangle[] triangles, int t, int v0) {
      int v1 = (v0 + 1) % 3;
      int vOther;
      int otherTriangle = FindTriangleOnEdge(ref vertices, ref triangles, t, triangles[t].VertexIds2[v0], triangles[t].VertexIds2[v1], out vOther);
      if (otherTriangle >= 0) {
        SplitTriangle(ref triangles, t, v0, triangles[otherTriangle].VertexIds2[vOther]);
        //Debug.LogFormat("Split triangle {0} at position {1}", t, vertices[triangles[otherTriangle].VertexIds2[vOther]].Position);
      }
    }

    static int FindTriangleOnEdge(ref MapNavMeshVertex[] vertices, ref MapNavMeshTriangle[] triangles, int tri, int v0, int v1, out int triangleVertexIndex) {
      triangleVertexIndex = -1;
      for (int t = 0; t < triangles.Length; ++t) {
        if (t == tri) {
          continue;
        }

        // Triangle shares at least one vertex?
        if (triangles[t].VertexIds2[0] == v0 || triangles[t].VertexIds2[1] == v0 || 
            triangles[t].VertexIds2[2] == v0 || triangles[t].VertexIds2[0] == v1 || 
            triangles[t].VertexIds2[1] == v1 || triangles[t].VertexIds2[2] == v1) {

          if (triangles[t].VertexIds2[0] == v0 || triangles[t].VertexIds2[1] == v0 || triangles[t].VertexIds2[2] == v0) {
            if (triangles[t].VertexIds2[0] == v1 || triangles[t].VertexIds2[1] == v1 || triangles[t].VertexIds2[2] == v1) {
              // Triangle shares two vertices, not interested in that
              return -1;
            }
          }

          if (IsPointBetween(vertices[triangles[t].VertexIds2[0]].Position, vertices[v0].Position, vertices[v1].Position)) {
            // Returns the triangle that has a vertex on the provided segment and the vertex index that lies on it
            triangleVertexIndex = 0;
            return t;
          }

          if (IsPointBetween(vertices[triangles[t].VertexIds2[1]].Position, vertices[v0].Position, vertices[v1].Position)) {
            triangleVertexIndex = 1;
            return t;
          }

          if (IsPointBetween(vertices[triangles[t].VertexIds2[2]].Position, vertices[v0].Position, vertices[v1].Position)) {
            triangleVertexIndex = 2;
            return t;
          }

        }
      }

      return -1;
    }

    static bool IsPointBetween(Vector3 p, Vector3 v0, Vector3 v1) {
      // We don't want to compare end points only is p is "really" in between
      if (p == v0 || p == v1 || v0 == v1)
        return false;

      Vector3 cross = Vector3.Cross(v1 - v0, p - v0);
      if (Mathf.Abs(cross.magnitude) > float.Epsilon)
        return false;

      float dot = Vector3.Dot(v1 - v0, p - v0);
      if (dot < 0.0f)
        return false;

      float length = (v1 - v0).sqrMagnitude;
      if (dot > length)
        return false;

      return true;
    }

    static void SplitTriangle(ref MapNavMeshTriangle[] triangles, int t, int v0, int vNew) {
      // Split edge is between vertex index 0 and 1
      int v1 = (v0 + 1) % 3;
      // Vertex index 2 is opposite of split edge
      int v2 = (v0 + 2) % 3;

      MapNavMeshTriangle newTriangle = new MapNavMeshTriangle {
        Area = triangles[t].Area,
        Regions = triangles[t].Regions,
        VertexIds2 = new int[3]
      };

      // Map new triangle
      newTriangle.VertexIds2[0] = vNew;
      newTriangle.VertexIds2[1] = triangles[t].VertexIds2[v1];
      newTriangle.VertexIds2[2] = triangles[t].VertexIds2[v2];
      ArrayUtils.Add(ref triangles, newTriangle);

      // Remap old triangle
      triangles[t].VertexIds2[v1] = vNew;
    }
  }
}
