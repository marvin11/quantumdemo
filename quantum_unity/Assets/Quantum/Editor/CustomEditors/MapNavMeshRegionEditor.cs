﻿using System;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomEditor(typeof(MapNavMeshRegion))]
  public class MapNavMeshRegionEditor : UnityEditor.Editor {
    public override void OnInspectorGUI() {
      EditorGUI.BeginChangeCheck();

      var data = (MapNavMeshRegion)target;

      data.Id = EditorGUILayout.TextField("Id", data.Id);

      if (string.IsNullOrEmpty(data.Id)) {
        EditorGUILayout.HelpBox("Id is not set", MessageType.Error);
      }
      else if (data.Id == "Default") {
        EditorGUILayout.HelpBox("'Default' is not allowed", MessageType.Error);
      }


      Type navMeshModifierType = TypeUtils.FindType("NavMeshModifier");
      if (navMeshModifierType != null) {
        EditorGUILayout.LabelField("NavMesh Area defined by NavMeshModifier");
      }
      else {
        var areaId = GameObjectUtility.GetNavMeshArea(data.gameObject);

        var map = GameObjectUtility.GetNavMeshAreaNames();
        var currentIndex = 0;
        for (currentIndex = 0; currentIndex < map.Length;) {
          if (GameObjectUtility.GetNavMeshAreaFromName(map[currentIndex]) == areaId)
            break;
          currentIndex++;
        }

        var newIndex = EditorGUILayout.Popup("Unity NavMesh Area", currentIndex, map);
        if (currentIndex != newIndex) {
          GameObjectUtility.SetNavMeshArea(data.gameObject, GameObjectUtility.GetNavMeshAreaFromName(map[newIndex]));
        }

        if (newIndex < 3 || newIndex >= map.Length) {
          EditorGUILayout.HelpBox("Unity NavMesh Area not valid", MessageType.Error);
        }

        var currentFlags = GameObjectUtility.GetStaticEditorFlags(data.gameObject);
        var currentNavigationStatic = (currentFlags & StaticEditorFlags.NavigationStatic) == StaticEditorFlags.NavigationStatic;
        var newNavigationStatic = EditorGUILayout.Toggle("Unity Navigation Static", currentNavigationStatic);
        if (currentNavigationStatic != newNavigationStatic) {
          if (newNavigationStatic)
            GameObjectUtility.SetStaticEditorFlags(data.gameObject, currentFlags | StaticEditorFlags.NavigationStatic);
          else
            GameObjectUtility.SetStaticEditorFlags(data.gameObject, currentFlags & ~StaticEditorFlags.NavigationStatic);
        }

        if (newNavigationStatic == false) {
          EditorGUILayout.HelpBox("Unity Navigation Static has to be enabled", MessageType.Error);
        }
      }

      if (EditorGUI.EndChangeCheck())
        EditorUtility.SetDirty(target);
    }
  }
}
