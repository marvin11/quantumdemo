﻿using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {

  [CustomEditor(typeof(NavMeshAgentConfigAsset))]
  public class NavMeshAgentConfigAssetEditor : UnityEditor.Editor {

    public override void OnInspectorGUI() {

      var data = (NavMeshAgentConfigAsset)target;

      CustomEditorsHelper.DrawScriptFromScriptableObject(target);

      CustomEditorsHelper.DrawHeadline("NavMeshAgentConfigAsset");

      CustomEditorsHelper.BeginBox();

      CustomEditorsHelper.DrawDefaultInspector(serializedObject, "Settings", new string[] {
        // avoidance
        "Settings.Priority",
        "Settings.ShowDebugSteering",
        "Settings.ShowDebugAvoidance",
        // avoidance (old)
        "Settings.AllowFlockBehavior",
        // avoidance (new)
        "Settings.ClampAgentToNavmesh",
        "Settings.ClampAgentToNavmeshRadiusThreshold",
        "Settings.ClampAgentToNavmeshCorrection",
        "Settings.AvoidanceQuality",
        "Settings.MaxAvoidanceCandidates",
        "Settings.ReduceAvoidanceAtWaypoints",
        "Settings.ReduceAvoidanceFactor",
        // target
        "Settings.FindValidTargetCellRange",
        // braking
        "Settings.BrakingDistance",
        "Settings.BrakingOnWaypoint",
        "Settings.BrakingOnWaypointFactor",
        "Settings.BrakingOnWaypointMinAngleRad",
      }, false);

      if (data.Settings.Epsilon < NavMeshAgentConfig.MinEpsilon) {
        // This is also performed when loading the config asset
        data.Settings.Epsilon = NavMeshAgentConfig.MinEpsilon;
      }

      serializedObject.Update();
      EditorGUI.BeginChangeCheck();

      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.FindValidTargetCellRange"), new GUIContent("Find Valid Target Cell Range", "Searches in cells around a off-navmesh target to find the closest valid  position on the navmesh.\n0 = deactivated, uses fallback cell triangle \nSearching too many cells can greatly implact the performance (max should be 3)."));
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ShowDebugSteering"), new GUIContent("ShowDebug", ""));

      CustomEditorsHelper.DrawHeadline("Avoidance");

      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.Priority"), new GUIContent("Priority", "Agent avoidance priority. Legacy avoidance: higher priority value = less avoidance (Unity inverted). Experimental avoidance: less priority value = less avoidance (Unity)"));
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ShowDebugAvoidance"), new GUIContent("ShowDebug", ""));

      CustomEditorsHelper.DrawHeadline("Avoidance Legacy");

      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.AllowFlockBehavior"), new GUIContent("Allow FlockBehavior", ""));

      CustomEditorsHelper.DrawHeadline("Avoidance Experimental (parameters may change)");

      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.AvoidanceQuality"), new GUIContent("Avoidance Quality", "None: avoidance is off\nLow: unused\nMedium = Good\nHigh = experimental (not recommended)"));
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.MaxAvoidanceCandidates"), new GUIContent("Max Avoidance Candidates", "Depending on the quality and number of agents that are influencing each other this value needs to be increased to maintain smooth avoidance."));
      if (data.Settings.UsePhysics == true) {
        // ClampAgentToNavmesh is not used when UsePhysics is active
        GUI.enabled = false;
      }
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ClampAgentToNavmesh"), new GUIContent("Clamp Agent To Navmesh", data.Settings.UsePhysics ? "Disable Use Physics to enable clamping" : "This setting makes sure that agents are pushed out of invalid nav mesh areas similar to what happens when UsePhysics is active and static colliders are located in non-navmesh areas."));
      if (data.Settings.ClampAgentToNavmesh) {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ClampAgentToNavmeshRadiusThreshold"), new GUIContent("Clamp Agent Radius Threshold", "The minimum radius of agents (subtract NavMesh.MinAgentRadius) to use optimized clamp computations. If small agents go outside the navmesh try increasing this to include them."));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ClampAgentToNavmeshCorrection"), new GUIContent("Clamp Agent Correction", "This is a percentage how much the agent is corrected each tick."));
      }
      GUI.enabled = true;
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ReduceAvoidanceAtWaypoints"), new GUIContent("Reduce Avoidance At Waypoints", "Activate this to reduce the agent avoidance when getting close to waypoints. Use this only if you have kinematic agents to mitigate agents going off the navmesh. An alternative to this is still under development. \nTry 'dynamic line of sight' when using physic bodies to mitigate stuck agents on waypoints. "));
      if (data.Settings.ReduceAvoidanceAtWaypoints) {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.ReduceAvoidanceFactor"), new GUIContent("Reduce Avoidance Factor", "This is multiplied with the agent radius and represents the distance in what the avoidance influence is reduced quadratically."));
      }

      CustomEditorsHelper.DrawHeadline("Braking");

      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.BrakingDistance"), new GUIContent("Braking Distance", "If set to 0 (default and recommended) the agent will use current velocity as reference to start braking. Set this value to > 0 to override the distance."));
      EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.BrakingOnWaypoint"), new GUIContent("Braking On Waypoint", "If enabled the agent will also apply braking when getting in range of the waypoints."));

      if (data.Settings.BrakingOnWaypoint) {
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.BrakingOnWaypointFactor"), new GUIContent("Braking On WaypointFactor", "Reduce the amount of braking applied on waypoints."));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Settings.BrakingOnWaypointMinAngleRad"), new GUIContent("Braking On Waypoint Min Angle Rad", "In Radians, set the minimum angle when braking on waypoint should be applied."));
      }

      if (EditorGUI.EndChangeCheck()) { 
        serializedObject.ApplyModifiedProperties();
      }

      CustomEditorsHelper.EndBox();
    }
  }
}
