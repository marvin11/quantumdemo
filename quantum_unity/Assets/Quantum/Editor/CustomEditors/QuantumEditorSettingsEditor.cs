﻿using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomEditor(typeof(QuantumEditorSettings), true)]
  public class QuantumEditorSettingsEditor : UnityEditor.Editor {

    public override void OnInspectorGUI() {
      base.OnInspectorGUI();

      var settings = target as QuantumEditorSettings;

      // Replace slashes, trim start and end
      settings.DatabasePath = Quantum.PathUtils.MakeSane(settings.DatabasePath);

      string pathRelativeToAssets;
      if (!Quantum.PathUtils.MakeRelativeToFolder(settings.DatabasePath, "Assets", out pathRelativeToAssets))
        EditorGUILayout.HelpBox("DatabasePath needs to be inside the Unity Assets folder.", MessageType.Error);

      string pathRelativeToResources;
      if (!Quantum.PathUtils.MakeRelativeToFolder(settings.DatabasePath, "Resources", out pathRelativeToResources))
        EditorGUILayout.HelpBox("DatabasePath needs to be inside a Resources folder.", MessageType.Error);

      if (!System.IO.Directory.Exists(System.IO.Path.Combine(Application.dataPath, pathRelativeToAssets)))
        EditorGUILayout.HelpBox("DatabasePath folder does not exist.", MessageType.Error);

      settings.QuantumProjectPath = Quantum.PathUtils.MakeSane(settings.QuantumProjectPath);

      if (!System.IO.File.Exists(System.IO.Path.Combine(Application.dataPath, settings.QuantumProjectPath)))
        EditorGUILayout.HelpBox("Quantum solution file not found at '" + System.IO.Path.Combine(Application.dataPath, settings.QuantumProjectPath + "'"), MessageType.Error);

      if (!settings.QuantumProjectPath.EndsWith(".sln"))
        EditorGUILayout.HelpBox("Quantum solution file path has to end with .sln", MessageType.Error);
    }
  }
}