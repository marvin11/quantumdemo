﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class QuantumFrameDifferWindow : EditorWindow {

  class QuantumFrameDifferGUIEditor : QuantumFrameDifferGUI {
    QuantumFrameDifferWindow _window;

    public override Rect Position {
      get { return _window.position; }
    }

    public override GUIStyle MiniButton {
      get { return EditorStyles.miniButton; }
    }

    public override GUIStyle MiniButtonLeft {
      get { return EditorStyles.miniButtonLeft; }
    }

    public override GUIStyle MiniButtonRight {
      get { return EditorStyles.miniButtonRight; }
    }

    public override GUIStyle BoldLabel {
      get { return EditorStyles.boldLabel; }
    }

    public override GUIStyle DiffHeaderError {
      get { return (GUIStyle)"flow node 6"; }
    }

    public override GUIStyle DiffHeader {
      get { return (GUIStyle)"flow node 1"; }
    }

    public override GUIStyle DiffBackground {
      get { return (GUIStyle)"CurveEditorBackground"; }
    }

    public override GUIStyle DiffLineOverlay {
      get { return (GUIStyle)"ProfilerTimelineBar"; }
    }

    public override bool IsEditor {
      get { return true; }
    }

    public override GUIStyle TextLabel {
      get { return EditorStyles.label; }
    }

    public QuantumFrameDifferGUIEditor(QuantumFrameDifferWindow window) {
      _window = window;
    }

    public override void Repaint() {
      _window.Repaint();
    }
  }

  [MenuItem("Quantum/Show Frame Differ")]
  public static void ShowWindow() {
    GetWindow(typeof(QuantumFrameDifferWindow));
  }

  QuantumFrameDifferGUIEditor _gui;

  void OnGUI() {
    titleContent = new GUIContent("Frame Differ");

    if(_gui == null) {
      _gui = new QuantumFrameDifferGUIEditor(this);
    }

    _gui.OnGUI();
  }
}
