﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomEditor(typeof(QuantumRunnerLocalReplay))]
  public class QuantumRunnerLocalReplayEditor : UnityEditor.Editor {

    public override void OnInspectorGUI() {
      var data = (QuantumRunnerLocalReplay)target;
      if (DrawDefaultInspector()) {      
        if (data.ReplayFile != null && data.DatabaseFile == null) {
          var assetPath = AssetDatabase.GetAssetPath(data.ReplayFile);
          var databaseFilepath = Path.Combine(Path.GetDirectoryName(assetPath), "db.json");
          data.DatabaseFile = AssetDatabase.LoadAssetAtPath<TextAsset>(databaseFilepath);
        }

        if (data.ReplayFile != null && data.ChecksumFile == null) {
          var assetPath = AssetDatabase.GetAssetPath(data.ReplayFile);
          var checksumFilepath = Path.Combine(Path.GetDirectoryName(assetPath), "checksum.json");
          data.ChecksumFile = AssetDatabase.LoadAssetAtPath<TextAsset>(checksumFilepath);
        }

        if (data.DatabaseFile != null) {
          var assetPath = AssetDatabase.GetAssetPath(data.DatabaseFile);
          data.DatabasePath = Path.GetDirectoryName(assetPath);
        }
        else
          data.DatabasePath = string.Empty;
      }

      // Toggle the debug runner if on the same game object.
      var debugRunner = data.gameObject.GetComponent<QuantumRunnerLocalDebug>();
      var debugSavegame = data.gameObject.GetComponent<QuantumRunnerLocalSavegame>();
      if (debugRunner != null) {
        var newIsEnabled = EditorGUILayout.Toggle(new GUIContent("Toggle Runners", "Enable this script and disable QuantumRunnerLocalDebug on the same game object."), data.enabled);
        if ((newIsEnabled == true && data.enabled == false) || 
            (newIsEnabled == false && data.enabled == true)) {
          data.enabled = newIsEnabled;
          debugRunner.enabled = !newIsEnabled;
          if (debugSavegame != null)
            debugSavegame.enabled = false;
        }
      }
    }
  }
}