﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomEditor(typeof(QuantumRunnerLocalSavegame))]
  public class QuantumRunnerLocalSavegameEditor : UnityEditor.Editor {

    public override void OnInspectorGUI() {
      var data = (QuantumRunnerLocalSavegame)target;
      if (DrawDefaultInspector()) {      
        if (data.SavegameFile != null && data.DatabaseFile == null) {
          var assetPath = AssetDatabase.GetAssetPath(data.SavegameFile);
          var databaseFilepath = Path.Combine(Path.GetDirectoryName(assetPath), "db.json");
          data.DatabaseFile = AssetDatabase.LoadAssetAtPath<TextAsset>(databaseFilepath);
        }

        if (data.DatabaseFile != null) {
          var assetPath = AssetDatabase.GetAssetPath(data.DatabaseFile);
          data.DatabasePath = Path.GetDirectoryName(assetPath);
        }
        else
          data.DatabasePath = string.Empty;
      }

      // Toggle the debug runner if on the same game object.
      var debugRunner = data.gameObject.GetComponent<QuantumRunnerLocalDebug>();
      var debugReplay = data.gameObject.GetComponent<QuantumRunnerLocalReplay>();
      if (debugRunner != null) {
        var newIsEnabled = EditorGUILayout.Toggle(new GUIContent("Toggle Runners", "Enable this script and disable QuantumRunnerLocalDebug on the same game object."), data.enabled);
        if ((newIsEnabled == true && data.enabled == false) ||
            (newIsEnabled == false && data.enabled == true)) {
          data.enabled = newIsEnabled;
          debugRunner.enabled = !newIsEnabled;
          if (debugReplay != null)
            debugReplay.enabled = false;
        }
      }
    }
  }
}