﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  [CustomPropertyDrawer(typeof(AssetLink))]
  public class AssetLinkDrawer : PropertyDrawer {
    static GUIStyle _overlay;

    static public GUIStyle OverlayStyle {
      get {
        if (_overlay == null) {
          _overlay = new GUIStyle(EditorStyles.miniLabel);
          _overlay.alignment = TextAnchor.MiddleRight;
          _overlay.contentOffset = new Vector2(-24, 0);
          _overlay.normal.textColor = Color.red.Alpha(0.9f);
        }

        return _overlay;
      }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      DrawAssetObjectSelector(position, property, label);
    }

    public static void DrawAssetObjectSelector(Rect position, SerializedProperty property, GUIContent label, Type type = null) {
      type = type ?? typeof(AssetBase);

      var all = UnityEngine.Resources.LoadAll<AssetBase>(QuantumEditorSettings.Instance.ResourceDatabasePath);

      var guidProperty = property.FindPropertyRelative("Guid");
      var guid = guidProperty.stringValue;

      EditorGUI.BeginChangeCheck();

      var currentObject = all.FirstOrDefault(ObjectFilter(guid, type));

      EditorGUI.BeginProperty(position, label, guidProperty);

      AssetBase selected;

      if (currentObject == null && string.IsNullOrEmpty(guid) && !type.IsAbstract) {
        position.width -= 25;
        selected = EditorGUI.ObjectField(position, label, currentObject, type, false) as AssetBase;
        position.x = position.width + 20;
        position.width = 20;
        if (GUI.Button(position, "+", EditorStyles.miniButton)) {
          selected = ScriptableObject.CreateInstance(type) as AssetBase;
          var assetPath = string.Format("{0}/{1}.asset", QuantumEditorSettings.Instance.DatabasePath, type.ToString());
          AssetDatabase.CreateAsset(selected, assetPath);
          AssetDatabase.SaveAssets();
        }
      }
      else {
        selected = EditorGUI.ObjectField(position, label, currentObject, type, false) as AssetBase;
      }

      EditorGUI.EndProperty();

      if (currentObject == null && guid != null && guid.Length > 0) {
        GUI.Label(position, "(asset missing)", OverlayStyle);
      }

      if (EditorGUI.EndChangeCheck()) {
        if (selected) {
          guidProperty.stringValue = selected.AssetObject.Guid;
        }
        else {
          guidProperty.stringValue = "";
        }
      }
    }

    static Func<AssetBase, Boolean> ObjectFilter(String guid, Type type) {
      return obj => obj && type.IsAssignableFrom(obj.GetType()) && obj.AssetObject != null && AssetObjectIdentifier.IsGuidValid(obj.AssetObject.Guid) && obj.AssetObject.Guid == guid;
    }
  }

  [CustomPropertyDrawer(typeof(MapLink))]
  public class MapLinkPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(MapAsset));
    }
  }

  [CustomPropertyDrawer(typeof(TerrainColliderLink))]
  public class TerrainColliderLinkPropertyDrawer : PropertyDrawer
  {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(TerrainColliderAsset));
    }
  }

  [CustomPropertyDrawer(typeof(NavMeshLink))]
  public class NavMeshLinkPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(NavMeshAsset));
    }
  }

  [CustomPropertyDrawer(typeof(PhysicsMaterialLink))]
  public class PhysicsMaterialPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(PhysicsMaterialAsset));
    }
  }

  [CustomPropertyDrawer(typeof(CharacterController3DConfigLink))]
  public class CharacterController3DConfigPropertyDrawer : PropertyDrawer
  {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(CharacterController3DConfigAsset));
    }
  }
  
  [CustomPropertyDrawer(typeof(CharacterController2DConfigLink))]
  public class CharacterController2DConfigPropertyDrawer : PropertyDrawer
  {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(CharacterController2DConfigAsset));
    }
  }

  [CustomPropertyDrawer(typeof(NavMeshAgentConfigLink))]
  public class NavMeshAgentConfigPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(NavMeshAgentConfigAsset));
    }
  }

  [CustomPropertyDrawer(typeof(AnimatorGraphLink))]
  public class AnimatorGraphPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(AnimatorGraphAsset));
    }
  }

  [CustomPropertyDrawer(typeof(PolygonColliderLink))]
  public class PolygonColliderDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(PolygonColliderAsset));
    }
  }

  [CustomPropertyDrawer(typeof(SimulationConfigLink))]
  public class SimulationConfigPropertyDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(SimulationConfigAsset));
    }
  }
}