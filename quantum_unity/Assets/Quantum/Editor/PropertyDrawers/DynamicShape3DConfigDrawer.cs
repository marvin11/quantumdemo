﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {

  [CustomPropertyDrawer(typeof(DynamicShape3DConfig))]
  public class DynamicShape3DConfigDrawer : PropertyDrawer {

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
      switch ((Quantum.Core.DynamicShape3DType)property.FindPropertyRelative("ColliderType").intValue) {
        case Core.DynamicShape3DType.Box: return 125;
        case Core.DynamicShape3DType.Sphere: return 65;
      }
      return 40;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      var p = position.SetHeight(20);

      EditorGUI.LabelField(p, label);

      try {
        EditorGUI.indentLevel += 1;
        EditorGUI.PropertyField(p.AddY(20), property.FindPropertyRelative("ColliderType"), new GUIContent("Type"));

        switch ((Quantum.Core.DynamicShape3DType)property.FindPropertyRelative("ColliderType").intValue) {
          case Core.DynamicShape3DType.None:
            break;
          
          case Core.DynamicShape3DType.Box:
            EditorGUI.PropertyField(p.AddY(40), property.FindPropertyRelative("BoxExtents"), new GUIContent("Extents"));
            break;

          case Core.DynamicShape3DType.Sphere:
            EditorGUI.PropertyField(p.AddY(40), property.FindPropertyRelative("SphereRadius"), new GUIContent("Radius"));
            break;
          
          default:
            EditorGUILayout.HelpBox("Shape type not supported for dynamic bodies.", MessageType.Warning);
            break;
        }
      }
      finally {
        EditorGUI.indentLevel -= 1;
      }
    }
  }
}