﻿using System;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Quantum.EntityTypeSelection))]
public class EntityTypeSelectionDrawer : PropertyDrawer {
  static String[] _stringValues;
  static String[] StringValues {
    get {
      if (_stringValues == null) {
        _stringValues = Enum.GetNames(typeof(Quantum.EntityTypes));
      }

      return _stringValues;
    }
  }

  public override void OnGUI(Rect p, SerializedProperty prop, GUIContent label) {
    var stringValueProperty = prop.FindPropertyRelative("StringValue");
    var stringValueIndex = Array.IndexOf(StringValues, stringValueProperty.stringValue);

    stringValueIndex = Mathf.Clamp(stringValueIndex, 0, StringValues.Length);

    var selectedIndex = EditorGUI.Popup(p, label.text, stringValueIndex, StringValues, EditorStyles.popup);
    if (selectedIndex != stringValueIndex || stringValueProperty.stringValue == null || stringValueProperty.stringValue == "") {
      stringValueProperty.stringValue = StringValues[selectedIndex];
    }
  }
}