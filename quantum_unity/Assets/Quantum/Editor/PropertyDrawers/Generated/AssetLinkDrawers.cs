using Quantum;
using UnityEngine;
using UnityEditor;
namespace Quantum.Editor {

[CustomPropertyDrawer(typeof(GOAPTaskLink))]
public class GOAPTaskLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(GOAPTaskAsset));
  }
}


[CustomPropertyDrawer(typeof(GOAPRootLink))]
public class GOAPRootLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(GOAPRootAsset));
  }
}


[CustomPropertyDrawer(typeof(HFSMDecisionLink))]
public class HFSMDecisionLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(HFSMDecisionAsset));
  }
}


[CustomPropertyDrawer(typeof(HFSMRootLink))]
public class HFSMRootLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(HFSMRootAsset));
  }
}


[CustomPropertyDrawer(typeof(HFSMStateLink))]
public class HFSMStateLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(HFSMStateAsset));
  }
}


[CustomPropertyDrawer(typeof(HFSMTransitionSetLink))]
public class HFSMTransitionSetLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(HFSMTransitionSetAsset));
  }
}


[CustomPropertyDrawer(typeof(AIBlackboardLink))]
public class AIBlackboardLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(AIBlackboardAsset));
  }
}


[CustomPropertyDrawer(typeof(AIBlackboardInitializerLink))]
public class AIBlackboardInitializerLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(AIBlackboardInitializerAsset));
  }
}


[CustomPropertyDrawer(typeof(AIActionLink))]
public class AIActionLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(AIActionAsset));
  }
}


[CustomPropertyDrawer(typeof(InteractableObjectDataLink))]
public class InteractableObjectDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(InteractableObjectDataAsset));
  }
}


[CustomPropertyDrawer(typeof(PlayerPivotDataLink))]
public class PlayerPivotDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(PlayerPivotDataAsset));
  }
}


[CustomPropertyDrawer(typeof(ReactionDataLink))]
public class ReactionDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(ReactionDataAsset));
  }
}


[CustomPropertyDrawer(typeof(DrawingBoardDataLink))]
public class DrawingBoardDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(DrawingBoardDataAsset));
  }
}


[CustomPropertyDrawer(typeof(BrushDataLink))]
public class BrushDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(BrushDataAsset));
  }
}


[CustomPropertyDrawer(typeof(MapInfoDataLink))]
public class MapInfoDataLinkPropertyDrawer : PropertyDrawer {
  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
    AssetLinkDrawer.DrawAssetObjectSelector(position, property, label, typeof(MapInfoDataAsset));
  }
}

}
