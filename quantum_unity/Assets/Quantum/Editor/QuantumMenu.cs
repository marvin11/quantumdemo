﻿using UnityEditor;
using UnityEngine;

namespace Quantum {
  public static class QuantumMenu {
    [MenuItem("Assets/Open Quantum Project")]
    private static void OpenQuantumProject1() {
      OpenQuantumProject2();
    }

    [MenuItem("Quantum/Open Quantum Project")]
    private static void OpenQuantumProject2() {
      var path = System.IO.Path.Combine(Application.dataPath, QuantumEditorSettings.Instance.QuantumProjectPath);
      if (!System.IO.File.Exists(path))
        EditorUtility.DisplayDialog("Open Quantum Project", "Solution file '" + path + "' not found. Check QuantumProjectPath in your QuantumEditorSettings.", "Ok");
      Application.OpenURL(string.Format("file://{0}", path));
    }
  }
}