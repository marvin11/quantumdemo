﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using Photon.Deterministic;

public unsafe class QuantumStateInspector : EditorWindow {
  private Vector2 scrollPos;
  private DumpTree tree = new DumpTree();

  [MenuItem("Quantum/State Inspector")]
  static void Init() {
    QuantumStateInspector window = (QuantumStateInspector)GetWindow(typeof(QuantumStateInspector), false, "Quantum State Inspector");
    window.Show();
  }

  public void OnGUI() {
    if (QuantumRunner.Default == null)
      return;

    var gameInstance = QuantumRunner.Default.Game;

    if (gameInstance == null || gameInstance.Frames.Current == null) {
      EditorGUILayout.LabelField("Simulation not running...");
      return;
    }

    GUILayout.BeginVertical();

    scrollPos = GUILayout.BeginScrollView(scrollPos, false, true);
    EditorGUILayout.LabelField("Tick: " + gameInstance.Frames.Current.Number);

    DataDumper.nextIndex = 0;
    DumpNode n = DataDumper.CreateNode(tree, 0);
    n.name = "State";
    n.value = null;
    n.initialized = true;
    DataDumper.Calculate(n, "Entities", *gameInstance.Frames.Current.Entities, 0, tree);
    DataDumper.Calculate(n, "Global", *gameInstance.Frames.Current.Global, 0, tree);
    DebugNode(n);

    GUILayout.EndScrollView();
    GUILayout.EndVertical();
  }

  public static void DebugNode(DumpNode node) {
    if (node.nodeType == NodeType.Entity && !node.active)
      return;

    if (node.name.Equals("_entity"))
      return;

    if (node.value != null) {
      EditorGUILayout.LabelField(node.name + ": " + node.value, node.style);
    }
    else {
      node.expanded = EditorGUILayout.Foldout(node.expanded, node.name, node.style);
      if (node.expanded) {
        foreach (DumpNode child in node.children) {
          DebugNode(child);
        }
      }
    }
  }

  public void OnInspectorUpdate() {
    this.Repaint();
  }
}

public class DumpTree
{
    public List<DumpNode> nodes = new List<DumpNode>();
    public Dictionary<Type, DumpNode> typeRoots = new Dictionary<Type, DumpNode>();
}

public enum NodeType
{
    Entity, Array, Property, AssetRef, EntityRef, MetaData, Root
}

// TODO expand this in a hierarchy (with more meta data)
public class DumpNode
{
    public NodeType nodeType = NodeType.Property;
    public NodeMetaData metaData;
    public bool active;
    public bool initialized = false;
    public bool expanded = false;
    public int index;
    public int lastIndex;
    public string name;
    public string value;
    public DumpNode parent;

    public GUIStyle style = new GUIStyle(EditorStyles.foldout);

    // offset + reflect type (we have)
    // cast contents as reflect type at runtime (so I can box and pass down to the data extractor)
    public List<DumpNode> children = new List<DumpNode>();
}

public class NodeMetaData
{
    public FieldInfo entityField;
    public FieldInfo activeField;
}

public static class DataDumper {
  public static int nextIndex = 0;

  class ReflectType {
    public FieldInfo[] Fields;

    static Dictionary<Type, ReflectType> lookup = new Dictionary<Type, ReflectType>();
    static HashSet<Type> primitiveTypes = new HashSet<Type>(new[] {
        typeof(FP), typeof(FPVector2), typeof(FPVector3), typeof(FPQuaternion)
      });

    public static ReflectType Create(Type t) {
      if (t.IsPrimitive) {
        return null;
      }

      if (t.IsEnum) {
        return null;
      }

      if (primitiveTypes.Contains(t)) {
        return null;
      }

      ReflectType rt;

      if (lookup.TryGetValue(t, out rt) == false) {
        rt = new ReflectType();
        rt.Fields = t.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

        lookup.Add(t, rt);
      }

      return rt;
    }
  }

  public static void Calculate(DumpNode node, String name, System.Object value, Int32 depth, DumpTree tree) {
    var rt = ReflectType.Create(value.GetType());
    DumpNode n = CreateNode(tree, ++nextIndex);
    n.parent = node;
    n.active = true;

    if (rt == null) {
      // volatile value, update
      n.value = value.ToString();
    }
    else {
      // update children on first traversal and when expanded only (reflection culling)
      if (n.expanded || !n.initialized) {
        foreach (var field in rt.Fields) {
          if (!n.initialized) {
            if (field.Name.Equals("_entity")) {
              n.nodeType = NodeType.Entity;
              n.metaData = new NodeMetaData();
              n.metaData.entityField = field;
              DumpNode typeRoot;
              if (tree.typeRoots.TryGetValue(value.GetType(), out typeRoot)) {

              }
              else {
                typeRoot = new DumpNode();
                typeRoot.name = value.GetType().Name;
                typeRoot.style = new GUIStyle(EditorStyles.foldout);
                typeRoot.style.margin = new RectOffset(10 + depth * 10, 0, 0, 0);
                tree.typeRoots.Add(value.GetType(), typeRoot);
                node.children.Add(typeRoot);
              }
              n.parent = typeRoot;
              typeRoot.children.Add(n);

              // finding the _flags field to be used for culling
              FieldInfo flagField = field.FieldType.GetField("_flags", BindingFlags.NonPublic | BindingFlags.Instance);
              n.metaData.activeField = flagField;
            }
          }
          Calculate(n, field.Name, field.GetValue(value), depth + 1, tree);
        }
      }
      else {
        // in case this is an entity, update active status based on precached field references
        if (n.nodeType == NodeType.Entity) {
          var flags = (Quantum.EntityFlags)n.metaData.activeField.GetValue(n.metaData.entityField.GetValue(value));
          n.active = (flags & Quantum.EntityFlags.Active) > 0;
        }
        nextIndex = n.lastIndex;
      }
    }

    if (!n.initialized) {
      n.name = name;
      if (n.nodeType != NodeType.Entity)
        node.children.Add(n);
      n.lastIndex = nextIndex;
      n.initialized = true;

      if (rt == null) {
        n.style = new GUIStyle(GUI.skin.label);
        n.style.contentOffset = new Vector2(10 + depth * 10, 0);
      }
      else {
        n.style = new GUIStyle(EditorStyles.foldout);
        n.style.margin = new RectOffset(10 + depth * 10, 0, 0, 0);
      }
    }
  }

  public static DumpNode CreateNode(DumpTree tree, int index) {

    if (tree.nodes.Count > index) {
      return tree.nodes[index];
    }

    DumpNode n = new DumpNode();
    n.index = index;
    tree.nodes.Add(n);
    return n;
  }
}