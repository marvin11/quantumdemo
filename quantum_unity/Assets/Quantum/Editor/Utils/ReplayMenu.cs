﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quantum.Editor {
  public class ReplayMenu {

    [MenuItem("Quantum/Replays/Save Replay And DB (JSON) %#r", true)]
    public static bool ExportReplayAndDBToJSONCheck() { return Application.isPlaying; }

    [MenuItem("Quantum/Replays/Save Replay And DB (JSON) %#r", false, 0)]
    public static void ExportReplayAndDBToJSON() {
      ExportDialogReplayAndDB(QuantumRunner.Default.Game, new JsonReplaySerializer(), "json");
    }

    [MenuItem("Quantum/Replays/Save Replay And DB (BSON)", true)]
    public static bool ExportReplayAndDBToBSONCheck() { return Application.isPlaying; }

    [MenuItem("Quantum/Replays/Save Replay And DB (BSON)", false, 1)]
    public static void ExportReplayAndDBToBSON() {
      ExportDialogReplayAndDB(QuantumRunner.Default.Game, new BsonReplaySerializer(), "bytes");
    }

    [MenuItem("Quantum/Replays/Save Replay (JSON)", true)]
    public static bool ExportReplayToJSONCheck() { return Application.isPlaying; }

    [MenuItem("Quantum/Replays/Save Replay (JSON)", false, 20)]
    public static void ExportReplayToJSON() {
      ExportDialogReplay(QuantumRunner.Default.Game, new JsonReplaySerializer(), "json");
    }

    [MenuItem("Quantum/Replays/Save Replay (BSON)", true)]
    public static bool ExportReplayToBSONCheck() { return Application.isPlaying; }

    [MenuItem("Quantum/Replays/Save Replay (BSON)", false, 21)]
    public static void ExportReplayToBSON() {
      ExportDialogReplay(QuantumRunner.Default.Game, new BsonReplaySerializer(), "bytes");
    }

    [MenuItem("Quantum/Replays/Save Game (JSON)", true)]
    public static bool SaveGameCheck() { return Application.isPlaying; }

    [MenuItem("Quantum/Replays/Save Game (JSON)", false, 60)]
    public static void SaveGame() {
      ExportDialogSavegame(QuantumRunner.Default.Game, new JsonReplaySerializer(), "json");
    }

    [MenuItem("Quantum/Replays/Save DB %#t", false, 80)]
    public static void ExportDB() {

      var folder = EditorUtility.SaveFolderPanel("Save db to..", "", "");
      if (folder.Length != 0) {

        if (!Application.isPlaying || DB.FastUnsafe == null) {
          // DB has not been initialized, yet.
          FileLoader.Init(new UnityFileLoader(QuantumEditorSettings.Instance.DatabasePath));
          UnityDB.Init();
#if UNITY_EDITOR
          AssetDatabase.Refresh();
#endif
        }

        using (var serializer = new JsonReplaySerializer())
        using (var stream = File.Create(Path.Combine(folder, "db.json")))
          QuantumGame.ExportDatabase(stream, serializer, folder, MapDataBaker.NavMeshSerializationBufferSize);
      }
    }

    [MenuItem("Quantum/Replays/Setup Debug Scene", true)]
    public static bool SetupSceneCheck() { return !Application.isPlaying; }

    [MenuItem("Quantum/Replays/Setup Debug Scene", false, 100)]
    public static void SetupScene() {
#pragma warning disable 0618
      if (GameObject.FindObjectOfType<QuantumInputRecorderStarter>() != null) {
        Debug.LogWarning("QuantumInputRecorderStarter found. Use QuantumInputRecorder instead.");
      }
#pragma warning restore 0618

      var runnerLocalDebug = GameObject.FindObjectOfType<QuantumRunnerLocalDebug>();
      if (runnerLocalDebug == null) {
        Debug.LogWarning("Can not setup the debug replay scene completely, because there is not QuantumRunnerLocalDebug.");
      }
      else {
        if (GameObject.FindObjectOfType<QuantumRunnerLocalReplay>() == null) {
          var runnerLocalReplay = runnerLocalDebug.gameObject.AddComponent<QuantumRunnerLocalReplay>();
          runnerLocalReplay.enabled = false;
        }

        if (GameObject.FindObjectOfType<QuantumRunnerLocalSavegame>() == null) {
          var runnerLocalReplay = runnerLocalDebug.gameObject.AddComponent<QuantumRunnerLocalSavegame>();
          runnerLocalReplay.enabled = false;
        }
      }
    }

    public static void ExportDialogReplayAndDB(QuantumGame game, IReplaySerializer serializer, string ext) {

      Assert.Check(serializer != null, "Serializer is invalid");

      var folder = EditorUtility.SaveFolderPanel("Save replay and db to..", Application.dataPath, "");
      if (folder.Length != 0) {

        using (var stream = File.Create(Path.Combine(folder, "replay." + ext)))
          QuantumGame.ExportRecordedReplay(game, stream, serializer);

        using (var stream = File.Create(Path.Combine(folder, "db.json")))
          QuantumGame.ExportDatabase(stream, serializer, folder, MapDataBaker.NavMeshSerializationBufferSize);

        if (game.RecordedChecksums != null) {
          using (var jsonSerializer = new JsonReplaySerializer())
          using (var stream = File.Create(Path.Combine(folder, "checksum.json")))
            QuantumGame.ExportRecordedChecksums(game, stream, jsonSerializer);
        }

        Debug.Log("Saved replay to " + folder);
        AssetDatabase.Refresh();
      }
    }

    public static void ExportDialogReplay(QuantumGame game, IReplaySerializer serializer, string ext) {

      Assert.Check(serializer != null, "Serializer is invalid");

      var datePostfix = string.Format("{0:yyyy-MM-dd--hh-mm-ss}", System.DateTime.Now);
      var replayFilename = string.Format("replay_{0}." + ext, datePostfix);
      var path = EditorUtility.SaveFilePanel("Save replay to..", "", replayFilename, ext);
      if (path.Length != 0) {

        using (var stream = File.Create(path))
          QuantumGame.ExportRecordedReplay(game, stream, serializer);

        if (game.RecordedChecksums != null) {
          var checksumPath = Path.Combine(Path.GetDirectoryName(path), string.Format("checksum_{0}.json", datePostfix));
          using (var jsonSerializer = new JsonReplaySerializer())
          using (var stream = File.Create(checksumPath))
            QuantumGame.ExportRecordedChecksums(game, stream, jsonSerializer);
        }

        Debug.Log("Saved replay to " + path);
        AssetDatabase.Refresh();
      }
    }

    public static void ExportDialogSavegame(QuantumGame game, IReplaySerializer serializer, string ext) {

      Assert.Check(serializer != null, "Serializer is invalid");

      var folder = EditorUtility.SaveFolderPanel("Save game to..", Application.dataPath, "");
      if (folder.Length != 0) {

        using (var stream = File.Create(Path.Combine(folder, "savegame." + ext)))
          QuantumGame.ExportSavegame(game, stream, serializer);

        using (var stream = File.Create(Path.Combine(folder, "db.json")))
          QuantumGame.ExportDatabase(stream, serializer, folder, MapDataBaker.NavMeshSerializationBufferSize);

        Debug.Log("Saved game to " + folder);
        AssetDatabase.Refresh();
      }
    }
  }
}