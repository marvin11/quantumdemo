﻿using Photon.Deterministic;
using Quantum;
using System;
using UnityEngine;

public unsafe class EntityPrefabRoot : MonoBehaviour {
  [NonSerialized]
  public String AssetGuid;

  [NonSerialized]
  public Quantum.EntityRef EntityRef;

  public EntityPrefabCreateBehaviour CreateBehaviour;

  public QuantumAnimator QuantumAnimator;

  [Tooltip("If enabled the EntityPrefabViewUpdater will not destroy this instance, and you are reponsible for removing it from the game world yourself.\n\nYou will still receive the OnEntityDestroyed callback.")]
  public Boolean ManualDestroy = false;

  [Header("Prediction Error Correction")]
  public Single ErrorCorrectionRateMin = 3.3f;
  public Single ErrorCorrectionRateMax = 10f;

  public Single ErrorPositionBlendStart = 0.25f;
  public Single ErrorPositionBlendEnd   = 1f;

  public Single ErrorRotationBlendStart    = 0.1f;
  public Single ErrorRotationBlendEnd      = 0.5f;
  public Single ErrorPositionMinCorrection = 0.025f;

  public Single ErrorPositionTeleportDistance = 2f;
  public Single ErrorRotationTeleportDistance = 0.5f;

  public bool blockEngineUpdate = false;

  [Header("Events")]
  public UnityEngine.Events.UnityEvent OnEntityInstantiated;

  public UnityEngine.Events.UnityEvent OnEntityDestroyed;

  FPVector2 _lastPredictedPosition2D;
  FPVector3 _lastPredictedPosition3D;

  FP           _lastPredictedRotation2D;
  FPQuaternion _lastPredictedRotation3D;

  Vector3    _errorVisualVector;
  Quaternion _errorVisualQuaternion;

  public void OnInstantiated() {
    _lastPredictedPosition2D = default(FPVector2);
    _lastPredictedRotation2D = default(FP);

    _lastPredictedPosition3D = default(FPVector3);
    _lastPredictedRotation3D = default(FPQuaternion);

    _errorVisualVector     = default(Vector3);
    _errorVisualQuaternion = Quaternion.identity;
  }

  public void UpdateFromTransform3D(QuantumGame game, Entity* entity, Boolean useClockAliasingInterpolation, Boolean useErrorCorrectionInterpolation) {
    var transform = Entity.GetTransform3D(entity);
    var newPosition = transform->Position.ToUnityVector3();
    var newRotation = transform->Rotation.ToUnityQuaternion();

    if (game != null) {
      var entityPrevious = game.Frames.PredictedPrevious.GetEntity(entity->EntityRef);
      if (entityPrevious != null && entity->Type == entityPrevious->Type) {
        var transformPrevious = Entity.GetTransform3D(entityPrevious);

        if (useClockAliasingInterpolation) {
          newPosition = Vector3.Lerp(transformPrevious->Position.ToUnityVector3(), newPosition, game.InterpolationFactor);
          newRotation = Quaternion.Slerp(transformPrevious->Rotation.ToUnityQuaternion(), newRotation, game.InterpolationFactor);
        }

        if (useErrorCorrectionInterpolation) {
          var oldEntity = game.Frames.PreviousUpdatePredicted.GetEntity(EntityRef);
          if (oldEntity != null) {
            var oldTransform = Entity.GetTransform3D(oldEntity);
            if (oldTransform != null) {
              var errorPosition = _lastPredictedPosition3D - oldTransform->Position;
              var errorRotation = _lastPredictedRotation3D * FPQuaternion.Inverse(oldTransform->Rotation);
              _errorVisualVector += errorPosition.ToUnityVector3();
              _errorVisualQuaternion *= errorRotation.ToUnityQuaternion();
            }
          }
        }
      }
    }

    // update rendered position
    if(!blockEngineUpdate)
      UpdateRenderPosition(newPosition, newRotation, useErrorCorrectionInterpolation);

    // store current prediction information
    _lastPredictedPosition3D = transform->Position;
    _lastPredictedRotation3D = transform->Rotation;
  }

  public void UpdateFromTransform2D(QuantumGame game, Entity* entity, Boolean useClockAliasingInterpolation, Boolean useErrorCorrectionInterpolation) {
    var transform = Entity.GetTransform2D(entity);
    var newPosition = transform->Position.ToUnityVector3();
    var newRotation = transform->Rotation.ToUnityQuaternion();

    if (game != null) {
      var entityPrevious = game.Frames.PredictedPrevious.GetEntity(entity->EntityRef);
      if (entityPrevious != null && entity->Type == entityPrevious->Type) {
        var transformPrevious = Entity.GetTransform2D(entityPrevious);

        if (useClockAliasingInterpolation) {
          newPosition = Vector3.Lerp(transformPrevious->Position.ToUnityVector3(), newPosition, game.InterpolationFactor);
          newRotation = Quaternion.Slerp(transformPrevious->Rotation.ToUnityQuaternion(), newRotation, game.InterpolationFactor);
        }

        if (useErrorCorrectionInterpolation) {
          var oldEntity = game.Frames.PreviousUpdatePredicted.GetEntity(EntityRef);
          if (oldEntity != null) {
            var oldTransform = Entity.GetTransform2D(oldEntity);
            if (oldTransform != null) {
              var errorPosition = _lastPredictedPosition2D - oldTransform->Position;
              var errorRotation = _lastPredictedRotation2D - oldTransform->Rotation;
              _errorVisualVector += errorPosition.ToUnityVector3();
              _errorVisualQuaternion *= errorRotation.ToUnityQuaternion();
            }
          }
        }
      }
    }

    // update rendered position
    UpdateRenderPosition(newPosition, newRotation, useErrorCorrectionInterpolation);

    // store current prediction information
    _lastPredictedPosition2D = transform->Position;
    _lastPredictedRotation2D = transform->Rotation;
  }

  void UpdateRenderPosition(Vector3 newPosition, Quaternion newRotation, Boolean useErrorCorrectionInterpolation) {
    var positionCorrectionRate = ErrorCorrectionRateMin;
    var rotationCorrectionRate = ErrorCorrectionRateMin;

    var positionTeleport = useErrorCorrectionInterpolation == false;
    var rotationTeleport = useErrorCorrectionInterpolation == false;

    // if we're going over teleport distance, we should just teleport
    var positionErrorMagnitude = _errorVisualVector.magnitude;
    if (positionErrorMagnitude > ErrorPositionTeleportDistance || positionTeleport) {
      positionTeleport = true;
      _errorVisualVector = default(Vector3);
    } else {
      var blendDiff = ErrorPositionBlendEnd - ErrorPositionBlendStart;
      var blendRate = Mathf.Clamp01((positionErrorMagnitude - ErrorPositionBlendStart) / blendDiff);
      positionCorrectionRate = Mathf.Lerp(ErrorCorrectionRateMin, ErrorCorrectionRateMax, blendRate);
    }

    var rotationErrorMagnitude = Mathf.Abs(Quaternion.Dot(_errorVisualQuaternion, Quaternion.identity));
    if (rotationErrorMagnitude > ErrorRotationTeleportDistance || rotationTeleport) {
      rotationTeleport = true;
      _errorVisualQuaternion = Quaternion.identity;
    } else {
      var blendDiff = ErrorRotationBlendEnd - ErrorRotationBlendStart;
      var blendRate = Mathf.Clamp01((rotationErrorMagnitude - ErrorRotationBlendStart) / blendDiff);
      rotationCorrectionRate = Mathf.Lerp(ErrorCorrectionRateMin, ErrorCorrectionRateMax, blendRate);
    }

    // apply new position (+ potential error correction)
    ApplyTransform(newPosition, newRotation, _errorVisualVector, _errorVisualQuaternion, positionTeleport, rotationTeleport);

    // reduce position error
    var positionCorrectionMultiplier = 1f - (Time.deltaTime * positionCorrectionRate);
    var positionCorrectionAmount     = _errorVisualVector * positionCorrectionMultiplier;
    if (positionCorrectionAmount.magnitude < ErrorPositionMinCorrection) {
      UpdateMinPositionCorrection(positionCorrectionMultiplier, positionCorrectionAmount);
    } else {
      _errorVisualVector *= positionCorrectionMultiplier;
    }

    // reduce rotation error
    _errorVisualQuaternion = Quaternion.Lerp(_errorVisualQuaternion, Quaternion.identity, Time.deltaTime * rotationCorrectionRate);
  }

  protected virtual void ApplyTransform(Vector3 newPosition, Quaternion newRotation, Vector3 errorVisualVector, Quaternion errorVisualQuaternion, bool positionTeleport, bool rotationTeleport) {
    // Override this in subclass to change how the new position is applied to the transform.
    transform.position = newPosition + errorVisualVector;
    transform.rotation = newRotation * errorVisualQuaternion;
  }

  void UpdateMinPositionCorrection(float positionCorrectionMultiplier, Vector3 positionCorrectionAmount) {
    if (_errorVisualVector.x == 0f && _errorVisualVector.y == 0f && _errorVisualVector.z == 0f) {
      return;
    }

    // calculate normalized vector
    var normalized = _errorVisualVector.normalized;

    // store signs so we know when we flip an axis
    var xSign = _errorVisualVector.x >= 0f;
    var ySign = _errorVisualVector.y >= 0f;
    var zSign = _errorVisualVector.z >= 0f;

    // subtract vector by normalized*ErrorPositionMinCorrection
    _errorVisualVector -= (normalized * ErrorPositionMinCorrection);

    // if sign flipped it means we passed zero
    if (xSign != (_errorVisualVector.x >= 0f)) {
      _errorVisualVector.x = 0f;
    }

    if (ySign != (_errorVisualVector.y >= 0f)) {
      _errorVisualVector.y = 0f;
    }

    if (zSign != (_errorVisualVector.z >= 0f)) {
      _errorVisualVector.z = 0f;
    }
  }
}