﻿using Photon.Deterministic;
using Quantum;
using System;
using System.Collections.Generic;
using UnityEngine;

public unsafe class EntityPrefabViewUpdater : QuantumCallbacks {

  // current set of entities that should be removed
  HashSet<EntityRef> _removeEntities = new HashSet<EntityRef>();

  // current set of active entities
  HashSet<EntityRef> _activeEntities = new HashSet<EntityRef>();

  // current set of active prefabs
  Dictionary<EntityRef, EntityPrefabRoot> _activePrefabs = new Dictionary<EntityRef, EntityPrefabRoot>(256);

  // teleport state variable
  Boolean _teleport;

  public EntityPrefabRoot GetPrefab(EntityRef entityRef) {
    EntityPrefabRoot root;

    if (_activePrefabs.TryGetValue(entityRef, out root)) {
      return root;
    }

    return null;
  }

  [Obsolete("Use TeleportAllEntities() instead")]
  public void SetTeleportOnce() {
    TeleportAllEntities();
  }

  public void TeleportAllEntities() {
    _teleport = true;
  }

  public override void OnGameStart(QuantumGame game) {
    // Add this on game start, but AutoLoadSceneFromMap could cause a scene reload (see OnUnitySceneLoadBegin)
    game.Callbacks.OnUpdateView += OnUpdateView;
  }

  public override void OnGameStartFromSnapshot(QuantumGame game, int frameNumber) {
    // Called as an replacement for OnGameStart when starting from a snapshot
    game.Callbacks.OnUpdateView += OnUpdateView;
  }

  public override void OnGameDestroyed(QuantumGame game) {
    game.Callbacks.OnUpdateView -= OnUpdateView;

    // Game and session are shutdown instantly -> delete the game objects right away and don't wait for a cleanup between the ticks (OnUpdateView).
    // If objects are not destroyed here scripts on them that access QuantumRunner.Default will throw.
    foreach (var prefab in _activePrefabs) {
      if (prefab.Value && prefab.Value.gameObject) {
        Destroy(prefab.Value.gameObject);
      }
    }
    _activePrefabs.Clear();
  }

  public override void OnUnitySceneLoadBegin(QuantumGame game) {
    // Map scene reload imminent. Remove because this component will be invalid.
    game.Callbacks.OnUpdateView -= OnUpdateView;
  }

  public override void OnUnitySceneLoadDone(QuantumGame game) {
    game.Callbacks.OnUpdateView += OnUpdateView;
  }

  public new void OnUpdateView(QuantumGame game) {

    if (game.Frames.Current != null) {

      _activeEntities.Clear();

      // Always use clock aliasing interpolation except during forced teleports.
      var useClockAliasingInterpolation = !_teleport;

      // Use error based interpolation only during multiplayer mode and when not forced teleporting.
      // For local games we want don't want error based interpolation as well as on forced teleports.
      var useErrorCorrection = game.Session.GameMode == DeterministicGameMode.Multiplayer && _teleport == false;

      // Go through all verified entities and create new prefab instances for new entities.
      // Checks information (CreateBehaviour) on each EntityPrefabRoot if it should be created/destroyed during Verified or Predicted frames.
      var prefabs = game.Frames.Verified.Filters.Prefab();
      while (prefabs.Next()) {
        CreateEntityIfNeeded(prefabs.Current, prefabs.Prefab, EntityPrefabCreateBehaviour.Verified);
      }

      // Go through all entities in the current predicted frame (predicted == verified only during lockstep).
      prefabs = game.Frames.Current.Filters.Prefab();
      while (prefabs.Next()) {
        CreateEntityIfNeeded(prefabs.Current, prefabs.Prefab, EntityPrefabCreateBehaviour.NonVerified);
      }

      // Sync the active prefab instances with the active entity list. Find outdated instances.
      _removeEntities.Clear();
      foreach (var key in _activePrefabs) {
        if (_activeEntities.Contains(key.Key) == false) {
          _removeEntities.Add(key.Key);
        }
      }

      // Destroy outdated prefab instances.
      foreach (var key in _removeEntities) {
        DestroyPrefab(key);
      }

      // Run over all prefab instances and update components using only entities from current frame.
      foreach (var kvp in _activePrefabs) {
        // grab instance
        var instance = kvp.Value;

        // make sure we do not try to update for an instance which doesn't exist.
        if (!instance) {
          continue;
        }

        var entity = game.Frames.Current.GetEntity(instance.EntityRef);
        if (entity != null) {
          // update 2d transform
          var transform2D = Entity.GetTransform2D(entity);
          if (transform2D != null) {
            instance.UpdateFromTransform2D(game, entity, useClockAliasingInterpolation, useErrorCorrection);
          }
          else {
            // update 3d transform
            var transform3D = Entity.GetTransform3D(entity);
            if (transform3D != null) {
              instance.UpdateFromTransform3D(game, entity, useClockAliasingInterpolation, useErrorCorrection);
            }
          }

          // Update quantum animator
          if (instance.QuantumAnimator) {
            var animator = Entity.GetAnimator(entity);
            if (animator != null) {
              instance.QuantumAnimator.Animate(animator);
            }
          }
        }
      }
    }

    // reset teleport to false always
    _teleport = false;
  }

  void CreateEntityIfNeeded(Entity* entity, Prefab* prefab, EntityPrefabCreateBehaviour createBehaviour) {
    var instance = default(EntityPrefabRoot);
    if (_activePrefabs.TryGetValue(entity->EntityRef, out instance)) {
      if (instance.CreateBehaviour == createBehaviour) {
        if (prefab->Current == null) {
          // Prefab has been revoked for this entity
          DestroyPrefab(entity->EntityRef);
        }
        else {
          if (instance.AssetGuid == prefab->Current.Guid) {
            _activeEntities.Add(entity->EntityRef);
          }
          else {
            // The Guid changed, recreate the prefab instance for this entity.
            DestroyPrefab(entity->EntityRef);
            if (CreatePrefab(entity, prefab->Current, createBehaviour) != null) {
              _activeEntities.Add(entity->EntityRef);
            }
          }
        }
      }
    }
    else {
      if (prefab->Current != null) {
        // Create a new prefab instance for this entity.
        if (CreatePrefab(entity, prefab->Current, createBehaviour) != null) {
          _activeEntities.Add(entity->EntityRef);
        }
      }
    }
  }

  EntityPrefabRoot CreatePrefab(Entity* entity, EntityPrefab prefab, EntityPrefabCreateBehaviour createBehaviour) {
    if (prefab == null) {
      return null;
    }

    var asset = UnityDB.FindAsset<EntityPrefabAsset>(prefab.Id);
    if (asset && asset.Prefab.CreateBehaviour == createBehaviour) {

      EntityPrefabRoot instance = default(EntityPrefabRoot);

      var transform2D = Entity.GetTransform2D(entity);
      var transform3D = Entity.GetTransform3D(entity);

      if (transform2D != null) {
        instance = CreateEntityPrefabInstance(asset, transform2D->Position.ToUnityVector3(), transform2D->Rotation.ToUnityQuaternion());
        instance.AssetGuid = prefab.Guid;
        instance.EntityRef = entity->EntityRef;
        instance.OnInstantiated();
        instance.UpdateFromTransform2D(null, entity, false, false);
        instance.OnEntityInstantiated.Invoke();
      }
      else if (transform3D != null) {
        instance = CreateEntityPrefabInstance(asset, transform3D->Position.ToUnityVector3(), transform3D->Rotation.ToUnityQuaternion());
        instance.AssetGuid = prefab.Guid;
        instance.EntityRef = entity->EntityRef;
        instance.OnInstantiated();
        instance.UpdateFromTransform3D(null, entity, false, false);
        instance.OnEntityInstantiated.Invoke();
      }
      else {
        instance = CreateEntityPrefabInstance(asset);
        instance.AssetGuid = prefab.Guid;
        instance.EntityRef = entity->EntityRef;
        instance.OnInstantiated();
        instance.OnEntityInstantiated.Invoke();
      }

      // add to lookup
      _activePrefabs.Add(entity->EntityRef, instance);

      // return instance
      return instance;
    }

    return null;
  }

  void DestroyPrefab(EntityRef entityRef) {
    EntityPrefabRoot prefab;

    if (_activePrefabs.TryGetValue(entityRef, out prefab)) {
      prefab.OnEntityDestroyed.Invoke();

      if (prefab.ManualDestroy == false) {
        DestroyEntityPrefabInstance(prefab);
      }
    }

    _activePrefabs.Remove(entityRef);
  }

  void OnDestroy() {
    foreach (var kvp in _activePrefabs) {
      if (kvp.Value && kvp.Value.gameObject) {
        Destroy(kvp.Value.gameObject);
      }
    }
  }

  protected virtual EntityPrefabRoot CreateEntityPrefabInstance(EntityPrefabAsset asset, Vector3? position = null, Quaternion? rotation = null) {
    if (position.HasValue && rotation.HasValue) {
      return GameObject.Instantiate(asset.Prefab, position.Value, rotation.Value);
    }

    return GameObject.Instantiate(asset.Prefab);
  }

  protected virtual void DestroyEntityPrefabInstance(EntityPrefabRoot instance) {
    GameObject.Destroy(instance.gameObject);
  }
}
