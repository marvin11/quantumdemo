﻿using System;

namespace Quantum {
  public static class ClientIdProvider {
    public enum Type {
      NewGuid,
      PhotonNickname,
      PhotonUserId
    }

    public static string CreateClientId(Type type) {
      switch (type) {
        case Type.NewGuid:
          return Guid.NewGuid().ToString();
        case Type.PhotonNickname:
          return PhotonNetwork.player.NickName;
        case Type.PhotonUserId:
          return PhotonNetwork.player.UserId;
      }
      return string.Empty;
    }
  }
}
