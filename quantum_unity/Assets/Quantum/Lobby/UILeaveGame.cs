﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace Quantum.Example {
  public class UILeaveGame : UIScreen<UILeaveGame> {
    public GameObject UICamera;
    public GameObject Background;

    Coroutine _leaveRoutine;

    public override void OnShowScreen(bool first) {
      UICamera.Hide();
      Background.Hide();
    }

    public override void OnHideScreen(bool first) {
      UICamera.Show();
      Background.Show();

      _leaveRoutine = null;
    }

    public void OnLeaveClicked() {
      if (_leaveRoutine != null) {
        return;
      }

      _leaveRoutine = StartCoroutine(OnLeaveRoutine());
    }

    IEnumerator OnLeaveRoutine() {
      if (QuantumRunner.ShutdownAll(true)) {

        // leave room
        PhotonNetwork.LeaveRoom(false);

        // wait one second (or wait for LeaveRoom)
        var startWait = Time.realtimeSinceStartup;
        while (PhotonNetwork.room != null && (Time.realtimeSinceStartup - startWait) < 1f) {
          yield return null;
        }

        // hide leave game button
        UILeaveGame.HideScreen();

        // are we still connected?
        if (PhotonNetwork.connected) {

          // goto lobby
          UILobby.ShowScreen();

        } else {

          // goto connect screen
          UIConnect.ShowScreen();
        }
      }

      _leaveRoutine = null;
    }
  }
}