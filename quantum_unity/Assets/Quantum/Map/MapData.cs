﻿using UnityEngine;
using Quantum;
using Photon.Deterministic;
using System.Collections.Generic;

[ExecuteInEditMode]
public class MapData : MonoBehaviour {
  public enum DrawMode { None, Grid, Area, All }

  public MapAsset Asset;
  public DrawMode DrawGridMode = DrawMode.All;

  // One-to-one mapping of Quantum static collider entries in MapAsset to their original source scripts. 
  // Purely for convenience to do post bake mappings and not required by the Quantum simulation.
  public List<MonoBehaviour> StaticCollider2DReferences = new List<MonoBehaviour>();
  public List<MonoBehaviour> StaticCollider3DReferences = new List<MonoBehaviour>();

  void Update() {
    transform.position = Vector3.zero;
  }

  void OnDrawGizmos() {
#if UNITY_EDITOR
    if (Asset) {

      if (DrawGridMode == DrawMode.Area || DrawGridMode == DrawMode.All) {
        GizmoUtils.DrawGizmosBox(
          transform,
          new FPVector2(Asset.Settings.WorldSizeX, Asset.Settings.WorldSizeY).ToUnityVector3(),
          QuantumEditorSettings.Instance.GridColor);
      }

      if (DrawGridMode == DrawMode.Grid || DrawGridMode == DrawMode.All) {
        var bottomLeft = transform.position - (-Asset.Settings.WorldOffset).ToUnityVector3();
        GizmoUtils.DrawGizmoGrid(
          bottomLeft,
          Asset.Settings.GridSizeX,
          Asset.Settings.GridSizeY,
          Asset.Settings.GridNodeSize,
          QuantumEditorSettings.Instance.GridColor.Alpha(0.4f)
        );
      }
    }
#endif
  }
}
