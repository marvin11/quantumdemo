﻿using Photon.Deterministic;
using Quantum;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public static class MapDataBaker
{

  public static int NavMeshSerializationBufferSize = 1024 * 1024 * 20;

  public static void BakeMapData(MapData data, Boolean inEditor)
  {
    FPMathUtils.LoadLookupTables();

    if (inEditor == false && !data.Asset)
    {
      data.Asset = ScriptableObject.CreateInstance<MapAsset>();
      data.Asset.Settings = new Quantum.Map();
    }

    BakeData(data, inEditor);
  }

  public static void BakeMeshes(MapData data, Boolean inEditor)
  {
    if (inEditor)
    {
#if UNITY_EDITOR
      var assetPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(data.Asset));
      var meshDataFilenname = data.Asset.name + "_mesh" + ".bytes";
      string dbAssetPath;
      if (!Quantum.PathUtils.MakeRelativeToFolder(assetPath, QuantumEditorSettings.Instance.DatabasePath, out dbAssetPath))
      {
        Debug.LogErrorFormat("Cannot convert '{0}' into a relative path using the db folder '{1}'.", assetPath, QuantumEditorSettings.Instance.DatabasePath);
        throw new Exception();
      }

      if (string.IsNullOrEmpty(dbAssetPath))
        data.Asset.Settings.MeshTrianglesDataPath = meshDataFilenname;
      else
        data.Asset.Settings.MeshTrianglesDataPath = Path.Combine(dbAssetPath, meshDataFilenname).Replace('\\', '/');

      // Serialize to binary some of the data (max 20 megabytes for now)
      var bytestream = new ByteStream(new Byte[NavMeshSerializationBufferSize]);
      data.Asset.Settings.Serialize(bytestream, true);
      File.WriteAllBytes(Path.Combine(assetPath, meshDataFilenname), bytestream.ToArray());
#endif
    }
  }

  public static IEnumerable<NavMesh> BakeNavMeshes(MapData data, Boolean inEditor)
  {
    FPMathUtils.LoadLookupTables();

    data.Asset.Settings.NavMeshLinks = new NavMeshLink[0];

    var navmeshes = BakeNavMeshesLoop(data).ToList();

    if (inEditor)
    {
#if UNITY_EDITOR
      var assetPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(data.Asset));
      foreach (var navmesh in navmeshes) {
        navmesh.Guid = data.Asset.name + "_" + navmesh.Name;
        var navmeshDataFilenname = navmesh.Guid + ".bytes";

        // Make navmesh data file path relative to the DB folder
        string dbAssetPath;
        if (!Quantum.PathUtils.MakeRelativeToFolder(assetPath, QuantumEditorSettings.Instance.DatabasePath, out dbAssetPath))
        {
          Debug.LogErrorFormat("Cannot convert '{0}' into a relative path using the db folder '{1}'.", assetPath, QuantumEditorSettings.Instance.DatabasePath);
          throw new Exception();
        }

        if (string.IsNullOrEmpty(dbAssetPath))
          navmesh.DataFilepath = navmeshDataFilenname;
        else
          navmesh.DataFilepath = Path.Combine(dbAssetPath, navmeshDataFilenname).Replace('\\', '/');

        // Serialize to binary some of the data (max 20 megabytes for now)
        var bytestream = new ByteStream(new Byte[NavMeshSerializationBufferSize]);
        navmesh.Serialize(bytestream, true);
        File.WriteAllBytes(Path.Combine(assetPath, navmeshDataFilenname), bytestream.ToArray());

        // Create scriptable object
        var navMeshAsset = ScriptableObject.CreateInstance<NavMeshAsset>();
        navMeshAsset.Settings = navmesh;
        navMeshAsset.Settings.mapLink.Guid = data.Asset.Settings.Guid;
        var navmeshAssetPath = Path.Combine(assetPath, navmesh.Guid + ".asset");
        AssetDatabase.CreateAsset(navMeshAsset, navmeshAssetPath);
        AssetDatabase.ImportAsset(navmeshAssetPath, ImportAssetOptions.ForceUpdate);
        ArrayUtils.Add(ref data.Asset.Settings.NavMeshLinks, new NavMeshLink() { Guid = navmesh.Guid });
      }
#endif
    }
    else {
      // When executing this during runtime the guids of the created navmesh are added to the map.
      // Binary navmesh files are not created because the fresh navmesh object has everyhing it needs.
      // Caveat: the returned navmeshes need to be added to the DB by either...
      // A) overwriting the navmesh inside an already existing NavMeshAsset ScriptableObject or
      // B) Creating new NavMeshAsset ScriptableObjects (see above) and inject them into the DB (use UnityDB.OnAssetLoad callback).
      foreach (var navmesh in navmeshes) {
        navmesh.Guid = data.Asset.name + "_" + navmesh.Name;
        navmesh.mapLink.Guid = data.Asset.Settings.Guid;
        ArrayUtils.Add(ref data.Asset.Settings.NavMeshLinks, new NavMeshLink() { Guid = navmesh.Guid });
      }
    }

    return navmeshes;
  }

  static StaticColliderData GetStaticData(GameObject gameObject, QuantumStaticColliderSettings settings)
  {
    return new StaticColliderData
    {
      Asset = settings.Asset,
      Name = gameObject.name,
      Tag = gameObject.tag
    };
  }

  static void BakeData(MapData data, Boolean inEditor)
  {
#if UNITY_EDITOR
    if (inEditor)
    {
      if (EditorSceneManager.loadedSceneCount != 1)
      {
        Debug.LogErrorFormat("Can't bake map data when more than one scene is open.");
        return;
      }

      // set scene name
      data.Asset.Settings.Scene = EditorSceneManager.GetActiveScene().name;
    }
#endif

    InvokeCallbacks("OnBeforeBake", data);

    // clear existing colliders
    data.Asset.Settings.StaticColliders = new MapStaticCollider[0];
    data.StaticCollider2DReferences = new List<MonoBehaviour>();
    data.StaticCollider3DReferences = new List<MonoBehaviour>();

    // circle colliders
    foreach (var collider in UnityEngine.Object.FindObjectsOfType<QuantumStaticCircleCollider2D>())
    {
      var s = collider.transform.localScale;
      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders, new MapStaticCollider
      {
        Position = collider.transform.position.ToFPVector2(),
        Rotation = collider.transform.rotation.ToFPRotation2D(),
#if QUANTUM_XY
        VerticalOffset = collider.transform.position.z.ToFP(),
        Height = collider.Height * s.z.ToFP(),
#else
        VerticalOffset = collider.transform.position.y.ToFP(),
        Height = collider.Height * s.y.ToFP(),
#endif
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = GetStaticData(collider.gameObject, collider.Settings),
        Layer = collider.gameObject.layer,

        // circle
        ShapeType = Quantum.Core.DynamicShapeType.Circle,
        CircleRadius = FP.FromFloat_UNSAFE(collider.Radius.AsFloat * s.x)
      });

      data.StaticCollider2DReferences.Add(collider);
    }

    // polygon colliders
    foreach (var collider in UnityEngine.Object.FindObjectsOfType<QuantumStaticPolygonCollider2D>())
    {
      var s = collider.transform.localScale;
      var vertices = collider.Vertices.Select(x => { var v = x.ToUnityVector3(); return new Vector3(v.x * s.x, v.y * s.y, v.z * s.z); }).Select(x => x.ToFPVector2()).ToArray();

      if (FPVector2.IsClockWise(vertices))
      {
        FPVector2.MakeCounterClockWise(vertices);
      }

      var normals = FPVector2.CalculatePolygonNormals(vertices);

      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders, new MapStaticCollider
      {
        Position = collider.transform.position.ToFPVector2(),
        Rotation = collider.transform.rotation.ToFPRotation2D(),
#if QUANTUM_XY
        VerticalOffset = collider.transform.position.z.ToFP(),
        Height = collider.Height * s.z.ToFP(),
#else
        VerticalOffset = collider.transform.position.y.ToFP(),
        Height = collider.Height * s.y.ToFP(),
#endif
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = GetStaticData(collider.gameObject, collider.Settings),
        Layer = collider.gameObject.layer,

        // polygon
        ShapeType = Quantum.Core.DynamicShapeType.Polygon,
        PolygonCollider = new PolygonCollider
        {
          Vertices = vertices,
          Normals = normals
        }
      });

      data.StaticCollider2DReferences.Add(collider);
    }

    // box colliders
    foreach (var collider in UnityEngine.Object.FindObjectsOfType<QuantumStaticBoxCollider2D>())
    {
      var e = collider.Size.ToUnityVector3();
      var s = collider.transform.localScale;
      e.x *= s.x;
      e.y *= s.y;
      e.z *= s.z;

      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders, new MapStaticCollider
      {
        Position = collider.transform.position.ToFPVector2(),
        Rotation = collider.transform.rotation.ToFPRotation2D(),
#if QUANTUM_XY
        VerticalOffset = collider.transform.position.z.ToFP(),
        Height = collider.Height * s.z.ToFP(),
#else
        VerticalOffset = collider.transform.position.y.ToFP(),
        Height = collider.Height * s.y.ToFP(),
#endif
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = GetStaticData(collider.gameObject, collider.Settings),
        Layer = collider.gameObject.layer,

        // polygon
        ShapeType = Quantum.Core.DynamicShapeType.Box,
        BoxExtents = e.ToFPVector2() * FP._0_50
      });

      data.StaticCollider2DReferences.Add(collider);
    }

    // 3D statics
    // clear existing colliders
    data.Asset.Settings.StaticColliders3D = new MapStaticCollider3D[0];
    // sphere colliders
    foreach (var collider in UnityEngine.Object.FindObjectsOfType<QuantumStaticSphereCollider3D>())
    {
      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders3D, new MapStaticCollider3D
      {
        Position = collider.transform.position.ToFPVector3(),
        Rotation = collider.transform.rotation.ToFPQuaternion(),
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = GetStaticData(collider.gameObject, collider.Settings),
        Layer = collider.gameObject.layer,

        // circle
        ShapeType = Quantum.Core.DynamicShape3DType.Sphere,
        SphereRadius = FP.FromFloat_UNSAFE(collider.Radius.AsFloat * collider.transform.localScale.x)
      });

      data.StaticCollider3DReferences.Add(collider);
    }

    // box3D colliders
    foreach (var collider in UnityEngine.Object.FindObjectsOfType<QuantumStaticBoxCollider3D>())
    {
      var e = collider.Size.ToUnityVector3();
      var s = collider.transform.localScale;
      e.x *= s.x;
      e.y *= s.y;
      e.z *= s.z;
      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders3D, new MapStaticCollider3D
      {
        Position = collider.transform.position.ToFPVector3(),
        Rotation = collider.transform.rotation.ToFPQuaternion(),
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = GetStaticData(collider.gameObject, collider.Settings),
        Layer = collider.gameObject.layer,

        // box
        ShapeType = Quantum.Core.DynamicShape3DType.Box,
        BoxExtents = e.ToFPVector3() * FP._0_50
      });

      data.StaticCollider3DReferences.Add(collider);
    }

    var meshes = UnityEngine.Object.FindObjectsOfType<QuantumStaticMeshCollider3D>();
    var terrains = UnityEngine.Object.FindObjectsOfType<TerrainColliderData>();

    Int32 staticIndex = data.Asset.Settings.StaticColliders3D.Length;
    data.Asset.Settings.MeshTriangles = new Quantum.Core.CCWTri[0];

    // static 3D mesh colliders
    foreach (var collider in meshes)
    {
      StaticColliderData staticData = new StaticColliderData()
      {
        ColliderId = staticIndex + 1,
        Asset = collider.Settings.Asset,
        IsTrigger = collider.Settings.Trigger,
        Name = collider.name,
        Layer = collider.gameObject.layer,
        Tag = collider.tag
      };

      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders3D, new MapStaticCollider3D
      {
        Position = collider.transform.position.ToFPVector3(),
        Rotation = collider.transform.rotation.ToFPQuaternion(),
        PhysicsMaterial = collider.Settings.PhysicsMaterial,
        Trigger = collider.Settings.Trigger,
        StaticData = staticData,
        Layer = collider.gameObject.layer,

        // mesh
        ShapeType = Quantum.Core.DynamicShape3DType.Mesh,
      });

      data.StaticCollider3DReferences.Add(collider);

      collider.Init(staticIndex);

      if (data.Asset.Settings.MeshTriangles == null)
        data.Asset.Settings.MeshTriangles = collider.Triangles.ToArray<Quantum.Core.CCWTri>();
      else
      {
        var oldSize = data.Asset.Settings.MeshTriangles.Length;
        var newTrinaglesCount = collider.Triangles.Length;
        Array.Resize<Quantum.Core.CCWTri>(ref data.Asset.Settings.MeshTriangles, oldSize + newTrinaglesCount);
        Array.Copy(collider.Triangles, 0, data.Asset.Settings.MeshTriangles, oldSize, newTrinaglesCount);
      }
      staticIndex++;
    }

    // terrain colliders
    foreach (var terrain in terrains)
    {
      terrain.Bake();
      StaticColliderData staticData = terrain.Asset.Settings.StaticData;
      staticData.Name = terrain.name;
      staticData.Layer = terrain.gameObject.layer;
      staticData.Tag = terrain.tag;
      staticData.ColliderId = staticIndex + 1;

      ArrayUtils.Add(ref data.Asset.Settings.StaticColliders3D, new MapStaticCollider3D
      {
        Position = default(FPVector3),
        Rotation = FPQuaternion.Identity,
        PhysicsMaterial = terrain.Asset.Settings.PhysicsMaterial,
        Trigger = staticData.IsTrigger,
        StaticData = staticData,
        Layer = terrain.gameObject.layer,

        // terrains are meshes
        ShapeType = Quantum.Core.DynamicShape3DType.Mesh,
      });

      data.StaticCollider3DReferences.Add(terrain);

      terrain.Asset.Settings.Loaded();
      var tris = terrain.Asset.Settings.Triangles.ToArray();
      for (int i = 0; i < tris.Length; i++)
      {
        tris[i].StaticDataIndex = staticIndex;
      }
      if (data.Asset.Settings.MeshTriangles == null)
        data.Asset.Settings.MeshTriangles = tris;
      else
      {
        var oldSize = data.Asset.Settings.MeshTriangles.Length;
        var newTrinaglesCount = tris.Length;
        Array.Resize<Quantum.Core.CCWTri>(ref data.Asset.Settings.MeshTriangles, oldSize + newTrinaglesCount);
        Array.Copy(tris, 0, data.Asset.Settings.MeshTriangles, oldSize, newTrinaglesCount);
      }
      staticIndex++;
    }
    
    BakeMeshes(data, inEditor);

    // invoke callbacks
    InvokeCallbacks("OnBake", data);

    if (inEditor)
    {
      Debug.LogFormat("Baked {0} 2D static colliders", data.Asset.Settings.StaticColliders.Length);
      Debug.LogFormat("Baked {0} 3D static primitive colliders", data.Asset.Settings.StaticColliders3D.Length);
      Debug.LogFormat("Baked {0} 3D static triangles", data.Asset.Settings.MeshTriangles.Length);
    }
  }

  private static void InvokeCallbacks(string callbackName, MapData data)
  {
    var bakeCallbacks = TypeUtils.GetSubClasses(typeof(MapDataBakerCallback), "Assembly-CSharp", "Assembly-CSharp-firstpass", "Assembly-CSharp-Editor", "Assembly-CSharp-Editor-firstpass")
      .OrderBy(t =>
      {
        var attr = TypeUtils.GetAttribute<MapDataBakerCallbackAttribute>(t);
        if (attr != null)
          return attr.InvokeOrder;
        return 0;
      });

    foreach (var callback in bakeCallbacks)
    {
      if (callback.IsAbstract == false)
      {
        try
        {
          switch (callbackName)
          {
            case "OnBeforeBake": (Activator.CreateInstance(callback) as MapDataBakerCallback).OnBeforeBake(data); break;
            case "OnBake": (Activator.CreateInstance(callback) as MapDataBakerCallback).OnBake(data); break;
          }
        }
        catch (Exception exn)
        {
          Debug.LogException(exn);
        }
      }
    }
  }

  static IEnumerable<NavMesh> BakeNavMeshesLoop(MapData data)
  {
    var navmeshDefinitions = data.GetComponentsInChildren<MapNavMeshDefinition>();
    for (int i = 0; i < navmeshDefinitions.Length; i++)
    {
      var navmesh = default(NavMesh);

      try
      {
        navmesh = MapNavMeshBaker.BakeNavMesh(data, navmeshDefinitions[i]);
        Debug.LogFormat("Baking Quantum NavMesh '{0}' complete ({1}/{2})", navmeshDefinitions[i].name, i + 1, navmeshDefinitions.Length);
      }
      catch (Exception exn)
      {
        Debug.LogException(exn);
      }

      if (navmesh != null)
      {
        yield return navmesh;
      }
      else
      {
        Debug.LogErrorFormat("Baking Quantum NavMesh '{0}' failed", navmeshDefinitions[i].name);
      }
    }
  }

}
