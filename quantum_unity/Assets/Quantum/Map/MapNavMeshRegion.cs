﻿using UnityEngine;
using Quantum;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(MeshRenderer))]
public class MapNavMeshRegion : MonoBehaviour {
  public string Id;

#if UNITY_EDITOR
  private void Reset() {
    GameObjectUtility.SetStaticEditorFlags(gameObject, GameObjectUtility.GetStaticEditorFlags(gameObject) | StaticEditorFlags.NavigationStatic);
  }
#endif

  private void OnDrawGizmos() {
    DrawGizmo(false);
  }

  private void OnDrawGizmosSelected() {
    DrawGizmo(true);
  }

  private void DrawGizmo(Boolean selected) {
    GizmoUtils.DrawGizmosBox(transform, new Vector3(1, 0, 1), selected, QuantumEditorSettings.Instance.NavMeshRegionColor, Vector3.zero);
  }
}
