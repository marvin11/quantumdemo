﻿using Photon.Deterministic;
using Quantum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuantumStaticMeshCollider3D : MonoBehaviour
{

  public QuantumStaticColliderSettings Settings;
  public Boolean UseColliderMesh;
  public Quantum.Core.CCWTri[] Triangles;

  void OnDrawGizmos()
  {
    DrawGizmo(false);
  }

  void OnDrawGizmosSelected()
  {
    DrawGizmo(true);
  }

  public void Init(Int32 index = 0)
  {
    FPMathUtils.LoadLookupTables(false);
    var meshFilter = GetComponent<MeshFilter>();
    var meshCollider = GetComponent<MeshCollider>();
    Mesh mesh = null;
    if (UseColliderMesh && meshCollider != null)
    {
      mesh = meshCollider.sharedMesh;
    }
    else {
      mesh = meshFilter.sharedMesh;
    }

    // game object transform data
    Vector3 position = transform.position;
    Quaternion rotation = transform.rotation;
    Vector3 scale = transform.lossyScale;

    Triangles = new Quantum.Core.CCWTri[mesh.triangles.Length / 3];
    //Debug.Log("Tri: " + mesh.triangles.Length / 3);
    for (int i = 0; i < mesh.triangles.Length; i += 3)
    {
      Quantum.Core.CCWTri tri = new Quantum.Core.CCWTri();
      var vertexA = mesh.triangles[i];
      var vertexB = mesh.triangles[i + 1];
      var vertexC = mesh.triangles[i + 2];

      //tri.C = mesh.vertices[vertexA].ToFPVector3() * 1000 + transform.position.ToFPVector3();
      //tri.B = mesh.vertices[vertexB].ToFPVector3() * 1000 + transform.position.ToFPVector3();
      //tri.A = mesh.vertices[vertexC].ToFPVector3() * 1000 + transform.position.ToFPVector3();

      tri.C = (rotation * GetScaled(mesh.vertices[vertexA], scale) + position).ToFPVector3();
      tri.B = (rotation * GetScaled(mesh.vertices[vertexB], scale) + position).ToFPVector3();
      tri.A = (rotation * GetScaled(mesh.vertices[vertexC], scale) + position).ToFPVector3();

      tri.ComputeNormal();
      tri.StaticDataIndex = index;
      //tri.Normal = -tri.Normal;
      Triangles[i / 3] = tri;
    }
  }

  private static Vector3 GetScaled(Vector3 vector, Vector3 scale)
  {
    vector.x *= scale.x;
    vector.y *= scale.y;
    vector.z *= scale.z;

    return vector;
  }

  private void Start()
  {
    Init();
  }

  void DrawGizmo(Boolean selected)
  {
    if (Triangles == null || Triangles.Length == 0)
      Init();
    foreach (Quantum.Core.CCWTri t in Triangles)
    {
      GizmoUtils.DrawGizmosTriangle(t.A.ToUnityVector3(), t.B.ToUnityVector3(), t.C.ToUnityVector3(), selected, QuantumEditorSettings.Instance.StaticColliderColor);
      Gizmos.color = Color.red;
      Gizmos.DrawLine(t.Center.ToUnityVector3(), t.Center.ToUnityVector3() + t.Normal.ToUnityVector3());
    }
  }
}
