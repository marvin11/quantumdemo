﻿using Photon.Deterministic;
using UnityEngine;
using System;
using Quantum;

public class QuantumStaticPolygonCollider2D : MonoBehaviour {

  public FPVector2[] Vertices = new FPVector2[3] {
    new FPVector2(0, 2),
    new FPVector2(-1, 0),
    new FPVector2(+1, 0)
  };
  public FP Height;
  public QuantumStaticColliderSettings Settings;

  void OnDrawGizmos() {
    DrawGizmo(false);
  }


  void OnDrawGizmosSelected() {
    DrawGizmo(true);
  }

  void DrawGizmo(Boolean selected) {
    GizmoUtils.DrawGizmoPolygon2D(transform, Vertices, Height.AsFloat, selected, selected, QuantumEditorSettings.Instance.StaticColliderColor);
  }
}
