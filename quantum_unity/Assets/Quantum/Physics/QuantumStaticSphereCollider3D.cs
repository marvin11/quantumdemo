﻿using Photon.Deterministic;
using Quantum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuantumStaticSphereCollider3D : MonoBehaviour {
  public FP Radius;
  public QuantumStaticColliderSettings Settings;

  void OnDrawGizmos() {
    DrawGizmo(false);
  }

  void OnDrawGizmosSelected() {
    DrawGizmo(true);
  }

  void DrawGizmo(Boolean selected) {
    GizmoUtils.DrawGizmosSphere(transform.position, Radius.AsFloat, selected, QuantumEditorSettings.Instance.StaticColliderColor);
  }
}
