﻿using Quantum;
using UnityEngine;
using Photon.Deterministic;

[ExecuteInEditMode]
public class TerrainColliderData : MonoBehaviour
{

  public TerrainColliderAsset Asset;

  public void Bake()
  {
    FPMathUtils.LoadLookupTables();
    Terrain t = GetComponent<Terrain>();
#if UNITY_2019_3_OR_NEWER
    Asset.Settings.gridSize = t.terrainData.heightmapResolution;
    Asset.Settings.Width = t.terrainData.heightmapResolution;
    Asset.Settings.Height = t.terrainData.heightmapResolution;
#else
    Asset.Settings.gridSize = t.terrainData.heightmapWidth;
    Asset.Settings.Width = t.terrainData.heightmapWidth;
    Asset.Settings.Height = t.terrainData.heightmapHeight;
#endif
    Asset.Settings.heightMap = new FP[Asset.Settings.gridSize * Asset.Settings.gridSize];
    Asset.Settings.Position = transform.position.ToFPVector3();
    Asset.Settings.Scale = t.terrainData.heightmapScale.ToFPVector3();
    for (int i = 0; i < Asset.Settings.gridSize; i++)
    {
      for (int j = 0; j < Asset.Settings.gridSize; j++)
      {
        Asset.Settings.heightMap[j + i * Asset.Settings.gridSize] = FP.FromFloat_UNSAFE(t.terrainData.GetHeight(i, j));
      }
    }
    Asset.Settings.Loaded();
  }

  void OnDrawGizmos()
  {
    if (Asset)
    {
      foreach (Quantum.Core.CCWTri tri in Asset.Settings.Triangles)
      {
        GizmoUtils.DrawGizmosTriangle(tri.A.ToUnityVector3(), tri.B.ToUnityVector3(), tri.C.ToUnityVector3(), false, QuantumEditorSettings.Instance.StaticColliderColor);
        Debug.DrawRay(tri.Center.ToUnityVector3(), tri.Normal.ToUnityVector3(), Color.red);
      }
    }
  }
}
