﻿using System;
using System.Collections.Generic;
using Photon.Deterministic;
using Quantum;
using UnityEngine;

public abstract class QuantumCallbacks : MonoBehaviour {
  public static readonly List<QuantumCallbacks> Instances = new List<QuantumCallbacks>();

  protected virtual void OnEnable() {
    Instances.Add(this);
  }

  protected virtual void OnDisable() {
    Instances.Remove(this);
  }

  [Obsolete("Use OnUpdateView(QuantumGame)")]
  public virtual void OnUpdateView() { }
  [Obsolete("Use OnGameStart(QuantumGame)")]
  public virtual void OnGameStart() { }
  [Obsolete("Use OnGameStartFromSnapshot(QuantumGame, int)")]
  public virtual void OnSnapshotLoaded() { }
  [Obsolete("Use OnUnitySceneLoadBegin(QuantumGame)")]
  public virtual void OnMapChangeBegin() { }
  [Obsolete("Use OnUnitySceneLoadDone(QuantumGame)")]
  public virtual void OnMapChangeDone() { }
  [Obsolete("Use OnChecksumError(QuantumGame, DeterministicTickChecksumError, Frame[])")]
  public virtual void OnChecksumError(DeterministicTickChecksumError error, Frame[] frames) { }

  public virtual void OnGameStart(QuantumGame game) { }
  public virtual void OnGameStartFromSnapshot(QuantumGame game, int frameNumber) { }
  public virtual void OnGameDestroyed(QuantumGame game) { }
  public virtual void OnUpdateView(QuantumGame game) { }
  public virtual void OnSimulateFinished(QuantumGame game, Frame frame) { }
  public virtual void OnUnitySceneLoadBegin(QuantumGame game) { }
  public virtual void OnUnitySceneLoadDone(QuantumGame game) { }
  public virtual void OnChecksumError(QuantumGame game, DeterministicTickChecksumError error, Frame[] frames) { }
}
