﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Quantum/Configurations/QuantumEditorSettings", fileName = "QuantumEditorSettings", order = (02 * 26 * 26) + (16 * 26) + (00))]
public class QuantumEditorSettings : ScriptableObject {

  private static QuantumEditorSettings instance;
  public static QuantumEditorSettings Instance {
    get {
      if (instance == null) {
        instance = Resources.Load<QuantumEditorSettings>("QuantumEditorSettings");
      }
      if (instance == null) {
        Debug.LogError("Can't find QuantumEditorSettings scriptable object in the Resource folders. Create a new instance via the create Asset menu.");
      }
      return instance;
    }
    private set { instance = value; }
  }

  [Tooltip("Overwrite to use a different folder to store Quantum assets. Must be somewhere under the Unity 'Assets' folder and under a 'Resources' folder.")]
  public string DatabasePath = "Assets/Resources/DB";

  [Tooltip("Overwrite to use a different path from the Unity project to the quantum code solution.")]
  public string QuantumProjectPath = "../../quantum_code/quantum_code.sln";

  [Header("Map")]
  public Color GridColor = new Color(0.0f, 0.7f, 1f);

  [Header("Collider Gizmos")]
  public Color StaticColliderColor = Quantum.ColorRGBA.ColliderGreen.ToColor();
  public Color DynamicColliderColor = Quantum.ColorRGBA.ColliderBlue.ToColor();
  public Color KinematicColliderColor = Quantum.ColorRGBA.White.ToColor();
  public Color CharacterControllerColor = Quantum.ColorRGBA.Yellow.ToColor();
  public Color AsleepColliderColor = Quantum.ColorRGBA.Gray.ToColor();
  public Color DisabledColliderColor = Quantum.ColorRGBA.Red.ToColor();

  [Header("Prediction Culling Gizmos")]
  public Boolean DrawPredictionArea = true;
  public Color PredictionAreaColor = new Color(1, 0, 0, 0.25f);

  [Header("Pathfinder Gizmos")]
  public Boolean DrawPathfinderRawPath = false;
  public Boolean DrawPathfinderRawTrianglePath = false;
  public Boolean DrawPathfinderFunnel = false;

  [Header("NavMesh Gizmos")]
  public Boolean DrawNavMesh = false;
  public Boolean DrawNavMeshBorders = false;
  public Boolean DrawNavMeshTriangleIds = false;
  public Boolean DrawNavMeshRegionIds = false;
  public Boolean DrawNavMeshVertexNormals = false;
  public Color NavMeshDefaultColor = new Color(0.0f, 0.75f, 1.0f, 0.5f);
  public Color NavMeshRegionColor = new Color(1.0f, 0.0f, 0.5f, 0.5f);

  [Header("NavMesh Editor")]
  public Boolean DrawNavMeshDefinitionAlways = false;
  public Boolean DrawNavMeshDefinitionMesh = false;
  public Boolean DrawNavMeshDefinitionOptimized = false;

  [Tooltip("Toggle the Quantum asset inspector. Completely disable it by defining DISABLE_QUANTUM_ASSET_EDITOR.")]
  public bool UseQuantumAssetInspector = true;

  [System.NonSerialized]
  private string resourceDatabasePath;

  public string ResourceDatabasePath {
    get {
      if (resourceDatabasePath == null || resourceDatabasePath.Length == 0) {
        // Make path relative to the Resource folder.
        if (!Quantum.PathUtils.MakeRelativeToFolder(DatabasePath, "Resources", out resourceDatabasePath)) {
          Debug.LogError("Failed to make folder relative to Resources/ " + DatabasePath);
        }
      }
      return resourceDatabasePath;
    }
  }

  public Color GetNavMeshColor(UInt64 regions) {
    if (regions == 0) {
      return NavMeshDefaultColor;
    }
    return NavMeshRegionColor;
  }
}
