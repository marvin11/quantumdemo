﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuantumFrameDiffer : MonoBehaviour {
  class QuantumFrameDifferGUIRuntime : QuantumFrameDifferGUI {
    public override int TextLineHeight {
      get { return 20; }
    }

    public override Rect Position {
      get { return new Rect(0, 0, Screen.width, Screen.height); }
    }
  }

  // gui instance
  QuantumFrameDifferGUI _gui;

  // draw stuff... lol
  void OnGUI() {
    if (_gui == null) {
      _gui = new QuantumFrameDifferGUIRuntime();
    }

    GUILayout.BeginArea(_gui.Position);

    _gui.OnGUI();

    GUILayout.EndArea();
  }

  public static QuantumFrameDiffer Show() {
    var instance = FindObjectOfType<QuantumFrameDiffer>();
    if (instance) {
      instance._gui.Show();
      return instance;
    }

    GameObject gameObject;
    gameObject = new GameObject(typeof(QuantumFrameDiffer).Name);

    var differ = gameObject.AddComponent<QuantumFrameDiffer>();
    if (differ._gui == null) {
      differ._gui = new QuantumFrameDifferGUIRuntime();
    }
    differ._gui.Show();

    return differ;
  }
}
