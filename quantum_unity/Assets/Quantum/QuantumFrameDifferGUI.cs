﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public abstract class QuantumFrameDifferGUI {

  String _search = "";
  String _gameId;
  Int32 _frameNumber;
  Int32 _scrollOffset;
  Boolean _hidden;

  public virtual Boolean IsEditor {
    get { return false; }
  }

  public virtual Int32 TextLineHeight {
    get { return 16; }
  }

  public virtual GUIStyle DiffBackground {
    get { return GUI.skin.box; }
  }

  public virtual GUIStyle DiffHeader {
    get { return GUI.skin.box; }
  }

  public virtual GUIStyle DiffHeaderError {
    get { return GUI.skin.box; }
  }

  public virtual GUIStyle DiffLineOverlay {
    get { return GUI.skin.textField; }
  }

  public virtual GUIStyle MiniButton {
    get { return GUI.skin.button; }
  }

  public virtual GUIStyle TextLabel {
    get { return GUI.skin.label; }
  }

  public virtual GUIStyle BoldLabel {
    get { return GUI.skin.label; }
  }

  public virtual GUIStyle MiniButtonLeft {
    get { return GUI.skin.button; }
  }

  public virtual GUIStyle MiniButtonRight {
    get { return GUI.skin.button; }
  }

  public abstract Rect Position {
    get;
  }

  public virtual void Repaint() {

  }

  public void Show() {
    _hidden = false;
  }

  public void OnGUI() {
    //SetupDebug();

    if (Event.current.type == EventType.ScrollWheel) {
      _scrollOffset += (int)(Event.current.delta.y * 1);
      Repaint();
    }

    if (QuantumGameCallback_FrameDiffer.Diffs == null) {
      DrawNoDumps();
      return;
    }

    DrawSelection();
    DrawDiff();
  }

  void DrawNoDumps() {
    GUILayout.BeginVertical();
    GUILayout.FlexibleSpace();
    GUILayout.BeginHorizontal();
    GUILayout.FlexibleSpace();
    GUILayout.Label("No currently active diffs");
    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();
    GUILayout.FlexibleSpace();
    GUILayout.EndVertical();
  }

  void DrawSelection() {
    GUILayout.Space(5);
    GUILayout.BeginHorizontal();

    if (IsEditor) {
      if (GUILayout.Button("Load Frame Dumps", MiniButton, GUILayout.Height(16))) {

      }

    }
    else {
      GUILayout.Space(5);

      if (_hidden) {
        if (GUILayout.Button("Show Quantum Frame Differ", MiniButton, GUILayout.Height(16))) {
          _hidden = false;
        }
      }
      else {
        if (GUILayout.Button("Hide", MiniButton, GUILayout.Height(16))) {
          _hidden = true;
        }
      }
    }

    if (_hidden) {
      GUILayout.FlexibleSpace();
      GUILayout.EndHorizontal();
      return;
    }

    GUILayout.Space(16);

    GUIStyle styleSelectedButton;
    styleSelectedButton = new GUIStyle(MiniButton);
    styleSelectedButton.normal = styleSelectedButton.active;

    foreach (var kvp in QuantumGameCallback_FrameDiffer.Diffs) {
      if (GUILayout.Button(kvp.Key, kvp.Key == _gameId ? styleSelectedButton : MiniButton, GUILayout.Height(16))) {
        _gameId = kvp.Key;
      }
    }

    // select a game if none is selected
    if (_gameId == null || QuantumGameCallback_FrameDiffer.Diffs.ContainsKey(_gameId) == false) {
      if (QuantumGameCallback_FrameDiffer.Diffs.Count > 0) {
        _gameId = QuantumGameCallback_FrameDiffer.Diffs.First().Key;
      }
    }

    // always auto select frame
    if (_gameId != null && QuantumGameCallback_FrameDiffer.Diffs.ContainsKey(_gameId)) {
      if (QuantumGameCallback_FrameDiffer.Diffs[_gameId].Count > 0) {
        _frameNumber = QuantumGameCallback_FrameDiffer.Diffs[_gameId].Keys.First();
      }
    }

    GUILayout.FlexibleSpace();
    GUILayout.EndHorizontal();

    Rect topBarRect;
    topBarRect = CalculateTopBarRect();
    topBarRect.x = (topBarRect.width - 200) - 3;
    topBarRect.width = 200;
    topBarRect.height = 18;
    topBarRect.y += 3;

    var currentSearch = _search;

    _search = GUI.TextField(topBarRect, _search ?? "");

    if (currentSearch != _search) {
      Search(GetSelectedFrameData().Values.FirstOrDefault(), 0, +1);
    }

    Rect prevButtonRect;
    prevButtonRect = topBarRect;
    prevButtonRect.height = 16;
    prevButtonRect.width = 50;
    prevButtonRect.x -= 102;
    prevButtonRect.y += 1;

    if (GUI.Button(prevButtonRect, "Prev", MiniButtonLeft)) {
      Search(GetSelectedFrameData().Values.FirstOrDefault(), _scrollOffset - 1, -1);
    }

    Rect nextButtonRect;
    nextButtonRect = prevButtonRect;
    nextButtonRect.x += 50;

    if (GUI.Button(nextButtonRect, "Next", MiniButtonRight)) {
      Search(GetSelectedFrameData().Values.FirstOrDefault(), _scrollOffset + 1, +1);
    }
  }

  void DrawDiff() {
    if (_hidden) {
      return;
    }

    var frameData = GetSelectedFrameData();
    if (frameData == null) {
      return;
    }

    // set of lines that are currently being drawn and have diffs
    HashSet<Int32> diffs;
    diffs = new HashSet<Int32>();

    // main background rect
    Rect mainRect;
    mainRect = CalculateMainRect(frameData.Count);

    // header rect for drawing title/prev/next background
    Rect headerRect;
    headerRect = Position;
    headerRect.x = 4;
    headerRect.y = 28;
    headerRect.width /= frameData.Count;
    headerRect.width -= 8;
    headerRect.height = 23;

    foreach (var kvp in frameData.OrderBy(x => x.Key)) {

      GUI.Box(mainRect, "", DiffBackground);

      // clamp scroll offset (yeah done every loop... w/e ;p)
      _scrollOffset = Mathf.Clamp(_scrollOffset, 0, Math.Max(0, kvp.Value.Lines.Length - 10));

      // draw lines
      for (Int32 i = 0; i < 100; ++i) {
        var line = _scrollOffset + i;
        if (line < kvp.Value.Lines.Length) {
          if (kvp.Value.Lines[line].Diff) {
            diffs.Add(i);
          }

          // label
          GUI.Label(CalculateLineRect(i, mainRect), kvp.Value.Lines[line].Line ?? "", TextLabel);
        }
      }

      // draw header background
      if (kvp.Value.Diffs > 0) {
        GUI.Box(headerRect, "", DiffHeaderError);
      }
      else {
        GUI.Box(headerRect, "", DiffHeader);
      }

      // titel label 
      Rect titleRect;
      titleRect = headerRect;
      titleRect.width = headerRect.width / 2;
      titleRect.y += 3;
      titleRect.x += 3;

      var title = String.Format("Client {0}, Diffs: {1}", kvp.Key, kvp.Value.Diffs);
      GUI.Label(titleRect, title, BoldLabel);

      // disable group for prev/next buttons
      GUI.enabled = kvp.Value.Diffs > 0;

      // next button
      Rect nextButtonRect;
      nextButtonRect = titleRect;
      nextButtonRect.height = 15;
      nextButtonRect.width = 75;
      nextButtonRect.x = headerRect.x + (headerRect.width - 78);

      if (GUI.Button(nextButtonRect, "Next Diff", MiniButton)) {
        SearchDiff(kvp.Value, _scrollOffset + 1, +1);
      }

      // prev button
      Rect prevButtonRect;
      prevButtonRect = nextButtonRect;
      prevButtonRect.x -= 77;

      if (GUI.Button(prevButtonRect, "Prev Diff", MiniButton)) {
        SearchDiff(kvp.Value, _scrollOffset - 1, -1);
      }

      GUI.enabled = true;

      mainRect.x += mainRect.width;
      headerRect.x += mainRect.width;
    }

    mainRect = CalculateMainRect(frameData.Count);

    // store gui color
    var c = GUI.color;

    // override with semi transparent red
    GUI.color = new Color(1, 0, 0, 0.25f);

    // draw diffing lines overlays
    foreach (var diff in diffs) {
      Rect r;
      r = CalculateLineRect(diff, mainRect);
      r.width = Position.width;

      GUI.Box(r, "", DiffLineOverlay);
    }

    // restore gui color
    GUI.color = c;
  }

  Rect CalculateLineRect(Int32 line, Rect mainRect) {
    Rect r = mainRect;
    r.height = TextLineHeight;
    r.y += 28;
    r.y += line * TextLineHeight;
    r.x += 4;
    r.width -= 8;

    return r;
  }

  Rect CalculateTopBarRect() {
    Rect mainRect;
    mainRect = Position;
    mainRect.x = 0;
    mainRect.y = 0;
    mainRect.height = 25;
    return mainRect;
  }

  Rect CalculateMainRect(Int32 frameDataCount) {
    Rect mainRect;
    mainRect = Position;
    mainRect.x = 0;
    mainRect.y = 25;
    mainRect.width /= frameDataCount;
    mainRect.height -= mainRect.y;
    return mainRect;
  }

  void SearchDiff(QuantumGameCallback_FrameDiffer.FrameData frameData, Int32 startIndex, Int32 searchDirection) {
    for (Int32 i = startIndex; i >= 0 && i < frameData.Lines.Length; i += searchDirection) {
      if (frameData.Lines[i].Diff) {
        _scrollOffset = i;
        break;
      }
    }
  }

  void Search(QuantumGameCallback_FrameDiffer.FrameData frameData, Int32 startIndex, Int32 searchDirection) {
    var term = _search ?? "";
    if (term.Length > 0) {
      for (Int32 i = startIndex; i >= 0 && i < frameData.Lines.Length; i += searchDirection) {
        if (frameData.Lines[i].Line.Contains(term)) {
          _scrollOffset = i;
          break;
        }
      }
    }
  }


  Dictionary<Int32, QuantumGameCallback_FrameDiffer.FrameData> GetSelectedFrameData() {
    Dictionary<Int32, Dictionary<Int32, QuantumGameCallback_FrameDiffer.FrameData>> games;

    if (QuantumGameCallback_FrameDiffer.Diffs.TryGetValue(_gameId, out games)) {

      Dictionary<Int32, QuantumGameCallback_FrameDiffer.FrameData> frames;

      if (games.TryGetValue(_frameNumber, out frames)) {

        if (frames.Count <= 1) {
          return null;
        }

        foreach (var frame in frames.Values) {
          if (frame.Initialized == false) {
            Diff(frames);
            break;
          }
        }

        return frames;
      }


    }

    return null;
  }

  void Diff(Dictionary<Int32, QuantumGameCallback_FrameDiffer.FrameData> frames) {
    // make sure all frames are split
    foreach (var frame in frames.OrderBy(x => x.Key).Select(x => x.Value)) {
      frame.Diffs = 0;

      if (frame.Initialized == false) {
        frame.Initialized = true;

        var split = frame.String.Split('\n');

        frame.Lines = new QuantumGameCallback_FrameDiffer.LineData[split.Length];

        for (Int32 i = 0; i < split.Length; ++i) {
          frame.Lines[i].Line = split[i];
        }
      }
    }

    Debug.Log("diffing frames count: " + frames.Values.Count);

    // make sure all lines arrays match in length
    var maxLength = frames.Values.Select(x => x.Lines.Length).Max();

    foreach (var frame in frames.Values) {
      if (frame.Lines.Length != maxLength) {
        Array.Resize(ref frame.Lines, maxLength);
      }
    }

    // diff all lines
    var framesArray = frames.OrderBy(x => x.Key).Select(x => x.Value).ToArray();

    for (Int32 i = 0; i < maxLength; ++i) {
      for (Int32 k = 1; k < framesArray.Length; ++k) {
        if (StringComparer.InvariantCulture.Compare(framesArray[k].Lines[i].Line, framesArray[0].Lines[i].Line) != 0) {
          framesArray[k].Lines[i].Diff = true;
          framesArray[k].Diffs += 1;
        }
        else {
          framesArray[0].Lines[i].Diff = false;
          framesArray[k].Lines[i].Diff = false;
        }
      }
    }
  }

  void SetupDebug() {
    var data = File.ReadAllText(@"C:\Users\Fredrik\Documents\GitHub\Quantum\quantum_code\PhotonDeterministic\PhotonDeterministic\Simulator\DeterministicSimulator.cs");
    var dataDiff = File.ReadAllText(@"C:\Users\Fredrik\Documents\GitHub\Quantum\quantum_code\PhotonDeterministic\PhotonDeterministic\Simulator\DeterministicSessionConfig.cs");

    QuantumGameCallback_FrameDiffer.InsertFrameDump("DEFAULT", 0, 100, data);
    QuantumGameCallback_FrameDiffer.InsertFrameDump("DEFAULT", 1, 100, data);
    QuantumGameCallback_FrameDiffer.InsertFrameDump("DEFAULT", 2, 100, dataDiff);

    QuantumGameCallback_FrameDiffer.InsertFrameDump("INSTANTREPLAY", 0, 102, data);
    QuantumGameCallback_FrameDiffer.InsertFrameDump("INSTANTREPLAY", 1, 102, data);
    QuantumGameCallback_FrameDiffer.InsertFrameDump("INSTANTREPLAY", 2, 102, data);
  }
}
