﻿using Quantum;
using System;

public unsafe class QuantumGameCallback_DebugDraw : IDisposable{

  QuantumGameCallbacks _target;

  public QuantumGameCallback_DebugDraw(QuantumGameCallbacks target) {
    _target = target;
    _target.OnUpdateView += OnUpdateView;
  }

  public void Dispose() {
    if (_target != null) {
      _target.OnUpdateView -= OnUpdateView;
      _target = null;
    }
  }

  private void OnUpdateView(QuantumGame game) {
    Quantum.Core.DebugDraw.DrawAll();
  }
}
