﻿using Quantum;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class QuantumGameCallback_FrameDiffer : IDisposable {
  public static Dictionary<String, Dictionary<Int32, Dictionary<Int32, FrameData>>> Diffs;

  public class FrameData {
    public String String;
    public Int32 Diffs;
    public LineData[] Lines;
    public Boolean Initialized;
  }

  public struct LineData {
    public String Line;
    public Boolean Diff;
  }

  QuantumGameCallbacks _target;

  public QuantumGameCallback_FrameDiffer(QuantumGameCallbacks target) {
    _target = target;
    _target.OnChecksumErrorFrameDump += OnChecksumErrorFrameDump;
  }

  public void Dispose() {
    if (_target != null) {
      _target.OnChecksumErrorFrameDump -= OnChecksumErrorFrameDump;
      _target = null;
    }
  }

  void OnChecksumErrorFrameDump(QuantumGame game, Int32 actorId, Int32 frameNumber, String frameData) {
    var gameRunner = QuantumRunner.ActiveRunners.FirstOrDefault(x => ReferenceEquals(x.Game, game));
    if (gameRunner == null) {
      Debug.LogError("Could not find runner for game");
      return;
    }

    InsertFrameDump(gameRunner.Id, actorId, frameNumber, frameData);

    if (Application.isEditor == false) {
      QuantumFrameDiffer.Show();
    }
  }

  public static void InsertFrameDump(String gameRunnerId, Int32 actorId, Int32 frameNumber, String frameData) {
    // init diffs
    if (Diffs == null) {
      Diffs = new Dictionary<String, Dictionary<Int32, Dictionary<Int32, FrameData>>>();
    }

    // init game  diffs
    Dictionary<Int32, Dictionary<Int32, FrameData>> gameDiffs;

    if (Diffs.TryGetValue(gameRunnerId, out gameDiffs) == false) {
      Diffs.Add(gameRunnerId, gameDiffs = new Dictionary<Int32, Dictionary<Int32, FrameData>>());
    }

    // init frame diffs
    Dictionary<Int32, FrameData> frameDiffs;

    if (gameDiffs.TryGetValue(frameNumber, out frameDiffs) == false) {
      gameDiffs.Add(frameNumber, frameDiffs = new Dictionary<Int32, FrameData>());
    }

    // add actor diff
    if (frameDiffs.ContainsKey(actorId) == false) {
      frameDiffs.Add(actorId, new FrameData { String = frameData });
    }
  }
}
