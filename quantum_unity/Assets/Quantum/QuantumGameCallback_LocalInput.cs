﻿using Photon.Deterministic;
using Quantum;
using System;
using UnityEngine;

public unsafe class QuantumGameCallback_LocalInput : IDisposable {

  QuantumGameCallbacks _target;

  public QuantumGameCallback_LocalInput(QuantumGameCallbacks target) {
    _target = target;
    _target.OnPollInput += OnPollInput;
    _target.OnPollInputLockstep += OnPollInputLockstep;
  }

  public void Dispose() {
    if (_target != null) {
      _target.OnPollInput -= OnPollInput;
      _target.OnPollInputLockstep -= OnPollInputLockstep;
      _target = null;
    }
  }

  private Photon.Deterministic.Tuple<Quantum.Input, DeterministicInputFlags> OnPollInput(QuantumGame game, int player) {
    if (QuantumInput.Instance != null)
      return QuantumInput.Instance.PollInput(player);

    Debug.LogWarning("No QuantumInput.Instance found.");
    return default(Photon.Deterministic.Tuple<Quantum.Input, DeterministicInputFlags>);
  }

  private Photon.Deterministic.Tuple<Quantum.Input, DeterministicInputFlags> OnPollInputLockstep(QuantumGame game, int player, int frame) {
    if (QuantumInput.Instance != null)
      return QuantumInput.Instance.PollInputLockstep(player, frame);

#if UNITY_EDITOR
    Debug.Log("No QuantumInput.Instance found.");
#endif

    return default(Photon.Deterministic.Tuple<Quantum.Input, DeterministicInputFlags>);
  }
}
