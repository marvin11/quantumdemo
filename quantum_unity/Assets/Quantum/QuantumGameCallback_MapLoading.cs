﻿using Quantum;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public unsafe class QuantumGameCallback_MapLoading : IDisposable{

  Map _map;
  AsyncOperation _mapLoad;
  QuantumGameCallbacks _target;

  public QuantumGameCallback_MapLoading(QuantumGameCallbacks target) {
    _target = target;
    _target.OnUpdateView += UpdateUnitySceneLoading;
    _target.OnGameDestroyed += OnGameDestroyed;
  }

  public void Dispose() {
    if (_target != null) {
      _target.OnUpdateView -= UpdateUnitySceneLoading;
      _target.OnGameDestroyed -= OnGameDestroyed;
      _target = null;
    }
    _mapLoad = null;
    _map = null;
  }

  private void OnGameDestroyed(QuantumGame game) {
    if (_mapLoad != null)
      Log.Warn("Map loading was still in progress when destroying the game");
    _mapLoad = null;
    _map = null;
  }

  private void UpdateUnitySceneLoading(QuantumGame game) {
    if (game.Configurations.Simulation.AutoLoadSceneFromMap) {
      if (_mapLoad == null) {
        var current = game.Frames.Verified.Global->Map.Asset;
        if (current != null && current != _map) {

          for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
            try {
              #pragma warning disable 0618
              QuantumCallbacks.Instances[i].OnMapChangeBegin();
              #pragma warning restore 0618
              QuantumCallbacks.Instances[i].OnUnitySceneLoadBegin(game);
            }
            catch (Exception exn) {
              Log.Exception(exn);
            }
          }

          _map = current;
          _mapLoad = SceneManager.LoadSceneAsync(current.Scene);
        }
      }
      else {
        if (_mapLoad.isDone) {
          _mapLoad = null;

          for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
            try {
              #pragma warning disable 0618
              QuantumCallbacks.Instances[i].OnMapChangeDone();
              #pragma warning restore 0618
              QuantumCallbacks.Instances[i].OnUnitySceneLoadDone(game);
            }
            catch (Exception exn) {
              Log.Exception(exn);
            }
          }
        }
      }
    }
  }
}
