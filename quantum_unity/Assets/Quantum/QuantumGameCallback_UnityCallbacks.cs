﻿using Photon.Deterministic;
using Quantum;
using System;

public unsafe class QuantumGameCallback_UnityCallbacks : IDisposable {

  QuantumGameCallbacks _target;

  public QuantumGameCallback_UnityCallbacks(QuantumGameCallbacks target) {
    _target = target;
    _target.OnChecksumError += OnChecksumError;
    _target.OnGameDestroyed += OnGameDestroyed;
    _target.OnGameStart += OnGameStart;
    _target.OnGameStartFromSnapshot += OnGameStartFromSnapshot;
    _target.OnSimulateFinished += OnSimulateFinished;
    _target.OnUpdateView += OnUpdateView;
  }

  public void Dispose() {
    if (_target != null) {
      _target.OnChecksumError -= OnChecksumError;
      _target.OnGameDestroyed -= OnGameDestroyed;
      _target.OnGameStart -= OnGameStart;
      _target.OnGameStartFromSnapshot -= OnGameStartFromSnapshot;
      _target.OnSimulateFinished -= OnSimulateFinished;
      _target.OnUpdateView -= OnUpdateView;
      _target = null;
    }
  }

#region Invoke Callbacks 

  private void OnGameStart(QuantumGame game) {
    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        var callback = QuantumCallbacks.Instances[i];
#pragma warning disable 0618
        callback.OnGameStart();
#pragma warning restore 0618
        callback.OnGameStart(game);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

  private void OnGameStartFromSnapshot(QuantumGame game, int frameNumber) {
    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        var callback = QuantumCallbacks.Instances[i];
#pragma warning disable 0618
        callback.OnSnapshotLoaded();
#pragma warning restore 0618
        callback.OnGameStartFromSnapshot(game, frameNumber);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

  private void OnGameDestroyed(QuantumGame game) {
    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        QuantumCallbacks.Instances[i].OnGameDestroyed(game);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

  private void OnUpdateView(QuantumGame game) {
    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        var callback = QuantumCallbacks.Instances[i];
#pragma warning disable 0618
        callback.OnUpdateView();
#pragma warning restore 0618
        callback.OnUpdateView(game);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

  private void OnSimulateFinished(QuantumGame game, Frame frame) {

    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        QuantumCallbacks.Instances[i].OnSimulateFinished(game, frame);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

  private void OnChecksumError(QuantumGame game, DeterministicTickChecksumError error, Frame[] frames) {
    for (Int32 i = QuantumCallbacks.Instances.Count - 1; i >= 0; --i) {
      try {
        var callback = QuantumCallbacks.Instances[i];
#pragma warning disable 0618
        callback.OnChecksumError(error, frames);
#pragma warning restore 0618
        callback.OnChecksumError(game, error, frames);
      }
      catch (Exception exn) {
        Log.Exception(exn);
      }
    }
  }

#endregion
}
