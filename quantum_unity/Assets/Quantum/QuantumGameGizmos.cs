﻿using Photon.Deterministic;
using Quantum;
using System;
using UnityEngine;

public static class QuantumGameGizmos
{
  public unsafe static void OnDrawGizmos(QuantumGame game)
  {
#if UNITY_EDITOR
    if (game.Frames.Current != null)
    {

      if (QuantumEditorSettings.Instance.DrawPredictionArea && game.Frames.Current.RuntimeConfig.SimulationConfig.Instance.UsePredictionArea)
      {
        var context = game.Frames.Current.Context;
        if (context.PredictionAreaRadius != FP.UseableMax)
        {
#if QUANTUM_XY
          // The Quantum simulation does not know about QUANTUM_XY and always keeps the vector2 Y component in the vector3 Z component.
          var predictionAreaCenter = new UnityEngine.Vector3(context.PredictionAreaCenter.X.AsFloat, context.PredictionAreaCenter.Z.AsFloat, 0);
#else
          var predictionAreaCenter = context.PredictionAreaCenter.ToUnityVector3();
#endif
          GizmoUtils.DrawGizmosSphere(predictionAreaCenter, context.PredictionAreaRadius.AsFloat, QuantumEditorSettings.Instance.PredictionAreaColor);
        }
      }

      var dynamics = game.Frames.Current.Filters.DynamicBody2D();
      while (dynamics.Next()) {
        var b = dynamics.DynamicBody;
        if (b->Enabled) {
          var e = dynamics.Current;
          var t = Entity.GetTransform2D(e);
          var tVertical = Entity.GetTransform2DVertical(e);
          var s = b->GetShape();
          var pos = t->Position.ToUnityVector3();
          var rot = t->Rotation.ToUnityQuaternion();
          var color = b->IsKinematic ? QuantumEditorSettings.Instance.KinematicColliderColor : QuantumEditorSettings.Instance.DynamicColliderColor;
          if (!b->Enabled)
            color = QuantumEditorSettings.Instance.DisabledColliderColor;
          if (b->IsSleeping)
            color = QuantumEditorSettings.Instance.AsleepColliderColor;

#if QUANTUM_XY
          if (tVertical != null)
            pos.z = tVertical->Position.AsFloat;
#else
          // Set 3d position of 2d object to simulate the vertical offset.
          if (tVertical != null)
            pos.y = tVertical->Position.AsFloat;
#endif

          switch (s.Type)
          {
            case Quantum.Core.DynamicShapeType.Circle:
              Quantum.GizmoUtils.DrawGizmosCircle(pos, s.Circle.Radius.AsFloat, tVertical != null ? tVertical->Height.AsFloat : 0.0f, color);
              break;

            case Quantum.Core.DynamicShapeType.Box:
              var size = s.Box.Extents.ToUnityVector3() * 2.0f;
#if QUANTUM_XY
              if (tVertical != null)
              {
                size.z = tVertical->Height.AsFloat;
                pos.z = tVertical->Position.AsFloat + tVertical->Height.AsFloat * 0.5f;
              }
#else
              if (tVertical != null)
              {
                size.y = tVertical->Height.AsFloat;
                pos.y = tVertical->Position.AsFloat + tVertical->Height.AsFloat * 0.5f;
              }

#endif

              Quantum.GizmoUtils.DrawGizmosBox(pos, rot, size, false, color);

              break;

            case Quantum.Core.DynamicShapeType.Polygon:
              var p = (PolygonCollider)Quantum.DB.FastUnsafe[s.Polygon.AssetId];
              if (p != null)
              {
                if (tVertical != null)
                  GizmoUtils.DrawGizmoPolygon2D(pos, rot, p.Vertices, tVertical->Height.AsFloat, color);
                else
                  GizmoUtils.DrawGizmoPolygon2D(pos, rot, p.Vertices, 0, color);
              }
              break;
          }
        }
      }

      var dynamics3D = game.Frames.Current.Filters.DynamicBody3D();
      while (dynamics3D.Next()) {
        var b = dynamics3D.DynamicBody3D;
        if (b->Enabled)
        {
          var t = Entity.GetTransform3D(dynamics3D.Current);
          var s = b->GetShape();

          var color = b->IsKinematic ? QuantumEditorSettings.Instance.KinematicColliderColor : QuantumEditorSettings.Instance.DynamicColliderColor;
          if (!b->Enabled)
            color = QuantumEditorSettings.Instance.DisabledColliderColor;
          if (b->IsSleeping)
            color = QuantumEditorSettings.Instance.AsleepColliderColor;

          switch (s.Type)
          {
            case Quantum.Core.DynamicShape3DType.Sphere:
              GizmoUtils.DrawGizmosSphere(t->Position.ToUnityVector3(), s.Sphere.Radius.AsFloat, false, color);
              break;
            case Quantum.Core.DynamicShape3DType.Box:
              GizmoUtils.DrawGizmosBox(t->Position.ToUnityVector3(), t->Rotation.ToUnityQuaternion(), s.Box.Extents.ToUnityVector3() * 2, false, color);
              break;
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawNavMesh) {
        var navmeshes = game.Frames.Current.Map.NavMeshes.Values;
        foreach (var navmesh in navmeshes) {
          for (Int32 i = 0; i < navmesh.Triangles.Length; i++) {
            var t = navmesh.Triangles[i];
            var color = QuantumEditorSettings.Instance.GetNavMeshColor(t.Regions);

            if (game.Frames.Current.IsNavMeshRegionActive(t.Regions) == false) {
              // Either grey out the color or continue
              var greyValue = (color.r + color.g + color.b) / 3.0f;
              color = new Color(greyValue, greyValue, greyValue, color.a);
            }

            // This is not very optimized, create and draw a whole Unity mesh here
            Gizmos.color = color;
            var vertex0 = ToUnityVector3_QUANTUM_XY(navmesh.Vertices[t.Vertex0].Point);
            var vertex1 = ToUnityVector3_QUANTUM_XY(navmesh.Vertices[t.Vertex1].Point);
            var vertex2 = ToUnityVector3_QUANTUM_XY(navmesh.Vertices[t.Vertex2].Point);

            Gizmos.DrawLine(vertex0, vertex1);
            Gizmos.DrawLine(vertex1, vertex2);
            Gizmos.DrawLine(vertex2, vertex0);

            UnityEditor.Handles.color = color;
            UnityEditor.Handles.lighting = true;
            UnityEditor.Handles.DrawAAConvexPolygon(vertex0, vertex1, vertex2);

            if (QuantumEditorSettings.Instance.DrawNavMeshRegionIds) {
              if (t.Regions > 0) {
                var s = string.Empty;
                for (int r = 0; r < navmesh.Regions.Length; r++) {
                  if ((t.Regions & navmesh.Regions[r].Flags) > 0) {
                    s += string.Format("{0}({1})", navmesh.Regions[r].Id, r);
                  }
                }
                UnityEditor.Handles.Label((vertex0 + vertex1 + vertex2) / 3.0f, s);
              }
            }
          }

          if (QuantumEditorSettings.Instance.DrawNavMeshVertexNormals) {
            Gizmos.color = Color.blue;
            for (Int32 v = 0; v < navmesh.Vertices.Length; ++v) {
              if (navmesh.Vertices[v].Borders.Length >= 2) {
                var normal = NavMeshVertex.CalculateNormal(v, navmesh, game.Frames.Current);
                if (normal != FPVector3.Zero) {
                  GizmoUtils.DrawGizmoVector(
                    ToUnityVector3_QUANTUM_XY(navmesh.Vertices[v].Point),
                    ToUnityVector3_QUANTUM_XY(navmesh.Vertices[v].Point) +
                    ToUnityVector3_QUANTUM_XY(normal) / 3.0f);
                }
              }
            }
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawPathfinderRawPath) {
        var pf = game.Frames.Current.Context.PathFinder;
        if (pf.RawPathSize >= 2) {
          var last = pf.RawPath[0].Point.XZ;
          for (Int32 i = 0; i < pf.RawPathSize; i++) {
            var point = pf.RawPath[i].Point.XZ;
            GizmoUtils.DrawGizmosCircle(point.ToUnityVector3(), 0.1f, Color.magenta);
            if (point != last) {
              Gizmos.color = Color.magenta;
              Gizmos.DrawLine(point.ToUnityVector3(), last.ToUnityVector3());
            }
            last = point;
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawPathfinderRawTrianglePath) {
        var pf = game.Frames.Current.Context.PathFinder;
        if (pf.RawPathSize >= 2) {
          NavMesh nm = null;
          var navmeshes = game.Frames.Current.Map.NavMeshes.Values;
          foreach (var navmesh in navmeshes) {
            nm = navmesh;
            break;
          }
          for (Int32 i = 0; i < pf.RawPathSize; i++) {
            var triangleIndex = pf.RawPath[i].Index;
            if (triangleIndex >= 0) {
              var vertex0 = ToUnityVector3_QUANTUM_XY(nm.Vertices[nm.Triangles[triangleIndex].Vertex0].Point);
              var vertex1 = ToUnityVector3_QUANTUM_XY(nm.Vertices[nm.Triangles[triangleIndex].Vertex1].Point);
              var vertex2 = ToUnityVector3_QUANTUM_XY(nm.Vertices[nm.Triangles[triangleIndex].Vertex2].Point);
              GizmoUtils.DrawGizmosTriangle(vertex0, vertex1, vertex2, true, Color.magenta);
              UnityEditor.Handles.color = Color.magenta.Alpha(0.25f);
              UnityEditor.Handles.lighting = true;
              UnityEditor.Handles.DrawAAConvexPolygon(vertex0, vertex1, vertex2);
            }
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawPathfinderFunnel) {
        var pf = game.Frames.Current.Context.PathFinder;
        if (pf.PathSize >= 2) {
          var last = pf.Path[0].Point.XZ;
          for (Int32 i = 0; i < pf.PathSize; i++) {
            var point = pf.Path[i].Point.XZ;
            GizmoUtils.DrawGizmosCircle(point.ToUnityVector3(), 0.05f, UnityEngine.Color.green);
            if (point != last) {
              Gizmos.color = UnityEngine.Color.green;
              Gizmos.DrawLine(point.ToUnityVector3(), last.ToUnityVector3());
            }
            last = point;
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawNavMeshBorders) {
        Gizmos.color = Color.blue;
        var navmeshes = game.Frames.Current.Map.NavMeshes.Values;
        foreach (var navmesh in navmeshes) {
          for (Int32 i = 0; i < navmesh.BorderGrid.Length; i++) {
            if (navmesh.BorderGrid[i].Borders != null) {
              for (int j = 0; j < navmesh.BorderGrid[i].Borders.Length; j++) {
                var b = navmesh.Borders[navmesh.BorderGrid[i].Borders[j]];
                if (b.Regions > 0 && game.Frames.Current.IsNavMeshRegionActive(b.Regions) == true) {
                  // grayed out?
                  continue;
                }

                Gizmos.color = Color.blue;
                Gizmos.DrawLine(ToUnityVector3_QUANTUM_XY(b.V0), ToUnityVector3_QUANTUM_XY(b.V1));

                //// How to do a thick line? Multiple GizmoDrawLine also possible.
                //var color = QuantumEditorSettings.Instance.GetNavMeshColor(b.Regions);
                //UnityEditor.Handles.color = color;
                //UnityEditor.Handles.lighting = true;
                //UnityEditor.Handles.DrawAAConvexPolygon(
                //  ToUnityVector3_QUANTUM_XY(b.V0), 
                //  ToUnityVector3_QUANTUM_XY(b.V1), 
                //  ToUnityVector3_QUANTUM_XY(b.V1) + Vector3.up * 0.05f,
                //  ToUnityVector3_QUANTUM_XY(b.V0) + Vector3.up * 0.05f);
              }
            }
          }
        }
      }

      if (QuantumEditorSettings.Instance.DrawNavMeshTriangleIds) {
        UnityEditor.Handles.color = Color.white;
        var navmeshes = game.Frames.Current.Map.NavMeshes.Values;
        foreach (var navmesh in navmeshes) {
          for (Int32 i = 0; i < navmesh.Triangles.Length; i++) {
            UnityEditor.Handles.Label(ToUnityVector3_QUANTUM_XY(navmesh.Triangles[i].Center), i.ToString());
          }
        }
      }

      var chars3D = game.Frames.Current.Filters.CharacterController3D();
      while (chars3D.Next())
      {
        //var e = chars3D.Current;
        
        var t = chars3D.Transform3D;
        var c = chars3D.CharacterController3D;

        var color = QuantumEditorSettings.Instance.CharacterControllerColor;
        var color2 = QuantumEditorSettings.Instance.AsleepColliderColor;
        GizmoUtils.DrawGizmosSphere(t->Position.ToUnityVector3() + c->Config.Offset.ToUnityVector3(), c->Config.Radius.AsFloat, false, color);
        GizmoUtils.DrawGizmosSphere(t->Position.ToUnityVector3() + c->Config.Offset.ToUnityVector3(), c->Config.Radius.AsFloat + c->Config.Extent.AsFloat, false, color2);
      }
      
      var chars2D = game.Frames.Current.Filters.CharacterController2D();
      while (chars2D.Next())
      {
        //var e = chars3D.Current;
        
        var t = chars2D.Transform2D;
        var c = chars2D.CharacterController2D;

        var color = QuantumEditorSettings.Instance.CharacterControllerColor;
        var color2 = QuantumEditorSettings.Instance.AsleepColliderColor;
        GizmoUtils.DrawGizmosCircle(t->Position.ToUnityVector3() + c->Config.Offset.ToUnityVector3(), c->Config.Radius.AsFloat, false, color);
        GizmoUtils.DrawGizmosCircle(t->Position.ToUnityVector3() + c->Config.Offset.ToUnityVector3(), c->Config.Radius.AsFloat + c->Config.Extent.AsFloat, false, color2);
      }

    }
#endif
  }

  private static Vector3 ToUnityVector3_QUANTUM_XY(Photon.Deterministic.FPVector3 v) {
#if QUANTUM_XY
    // Quantum NavMesh, although saving 3D vectors, is always in XZ layout. Adjust the gizmo rendering here for QUANTNUM_XY.
    return new Vector3(v.X.AsFloat, v.Z.AsFloat, v.Y.AsFloat);
#else
    return v.ToUnityVector3();
#endif
  }
}