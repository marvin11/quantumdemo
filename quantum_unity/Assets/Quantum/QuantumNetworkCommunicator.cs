﻿using ExitGames.Client.Photon;
using Photon.Deterministic;
using System;
using System.Collections.Generic;

namespace Quantum.Core {
  public class QuantumNetworkCommunicator : ICommunicator {
    public enum QuitBehaviour {
      LeaveRoom,
      LeaveRoomAndBecomeInactive,
      Disconnect,
      None
    }

    public QuitBehaviour ThisQuitBehaviour;

    RaiseEventOptions _eventOptions;
    LoadBalancingPeer _loadBalancingPeer;
    PhotonNetwork.EventCallback _lastEventCallback;
    SendOptions _sendOperationOptions;
    Dictionary<Byte, Object> _parameters;

    public Boolean IsConnected {
      get {
        return PhotonNetwork.connected;
      }
    }

    public Int32 RoundTripTime {
      get {
        return _loadBalancingPeer.RoundTripTime;
      }
    }

    public Byte LocalPLayerId {
      get {
        return (Byte)PhotonNetwork.player.ID;
      }
    }

    [Obsolete("Use QuantumNetworkCommunicator(LoadBalancingPeer, PhotonQuitBehavior) instead")]
    internal QuantumNetworkCommunicator(LoadBalancingPeer loadBalancingPeer, bool autoConnect) : 
      this(loadBalancingPeer, QuitBehaviour.LeaveRoom) {
    }

    internal QuantumNetworkCommunicator(LoadBalancingPeer loadBalancingPeer, QuitBehaviour quitBehavior) {
      ThisQuitBehaviour = quitBehavior;
      _loadBalancingPeer = loadBalancingPeer;
      _loadBalancingPeer.TimePingInterval = 50;

      _parameters = new Dictionary<Byte, Object>();
      _parameters[ParameterCode.ReceiverGroup] = (byte)ReceiverGroup.All;

      _eventOptions = new RaiseEventOptions();
      _sendOperationOptions = new SendOptions { DeliveryMode = DeliveryMode.UnreliableUnsequenced };
    }

    public void RaiseEvent(Byte eventCode, Object message, Boolean reliable, Int32[] toPlayers) {
      if (_loadBalancingPeer.PeerState != PeerStateValue.Connected) {
        return;
      }

      if (reliable) {
        _eventOptions.TargetActors = toPlayers;
        _loadBalancingPeer.OpRaiseEvent(eventCode, message, reliable, _eventOptions);

      } else {
        _parameters[ParameterCode.Code] = eventCode;
        _parameters[ParameterCode.Data] = message;
        _loadBalancingPeer.SendOperation(OperationCode.RaiseEvent, _parameters, _sendOperationOptions);

      }

      _loadBalancingPeer.SendOutgoingCommands();
    }

    public void AddEventListener(OnEventReceived onEventReceived) {
      RemoveEventListener();

      // save callback we know how to de-register it
      _lastEventCallback = (Byte eventCode, Object content, Int32 senderId) => onEventReceived(eventCode, content);

      // attach callback
      PhotonNetwork.OnEventCall += _lastEventCallback;
    }

    public void Service() {
      _loadBalancingPeer.Service();
    }

    public void RemoveEventListener() {
      if (_lastEventCallback != null) {
        PhotonNetwork.OnEventCall -= _lastEventCallback;
      }
    }

    public void OnDestroy() {
      RemoveEventListener();

      switch (ThisQuitBehaviour) {
        case QuitBehaviour.LeaveRoom:
        case QuitBehaviour.LeaveRoomAndBecomeInactive:
          if (PhotonNetwork.connected) {
            PhotonNetwork.LeaveRoom(ThisQuitBehaviour == QuitBehaviour.LeaveRoomAndBecomeInactive);
          }
          break;
        case QuitBehaviour.Disconnect:
          PhotonNetwork.Disconnect();
          break;
      }
    }
  }
}
