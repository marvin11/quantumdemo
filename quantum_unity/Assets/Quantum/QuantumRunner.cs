﻿using Photon.Deterministic;
using Quantum;
using Quantum.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using UnityEngine;

public sealed class QuantumRunner : MonoBehaviour, IDisposable {

  [Obsolete("Use QuantumRunner.Default instead")]
  public static QuantumRunner Current {
    get {
      // Always return the default runner.
      return Default;
    }
  }

  public static QuantumRunner Default {
    get { 
      if (_activeRunners.Count == 0)
        return null;
      return _activeRunners[0];
    }
  }

  public static IEnumerable<QuantumRunner> ActiveRunners {
    get {
      return _activeRunners;
    }
  }

  static List<QuantumRunner> _activeRunners = new List<QuantumRunner>();

  [HideInInspector]
  public float ReplayPlaybackSpeed = 1.0f;
  /// <summary>
  ///  Use this prevent the session from being update automatically in order to call Session.Update(DeltaTime) in your own code.
  ///  For example to inject custom delta time values.
  /// </summary>
  [HideInInspector]
  public bool OverrideUpdateSession = false;

  public struct StartParameters {
    public RuntimeConfig RuntimeConfig;
    public DeterministicSessionConfig DeterministicConfig;
    public QuantumGameCallbacks GameCallbacks;
    public IDeterministicReplayProvider ReplayProvider;
    public IDeterministicReplayProvider LocalInputProvider;
    public DeterministicGameMode GameMode;
    public Int32 InitialFrame;
    public Byte[] FrameData;
    public string RunnerId;
    public QuantumNetworkCommunicator.QuitBehaviour QuitBehaviour;
    public Int32 PlayerCount;
    public Int32 LocalPlayerCount;
  }

  public QuantumGame Game { get; private set; }
  public DeterministicSession Session { get; private set; }
  public string Id { get; private set; }
  public SimulationUpdateTime DeltaTimeType { get; set; }

  public float? DeltaTime {
    get {
      switch (DeltaTimeType) {
        case SimulationUpdateTime.EngineDeltaTime: return Time.deltaTime;
        case SimulationUpdateTime.EngineUnscaledDeltaTime: return Time.unscaledDeltaTime;
      }
      return null;
    }
  }

  bool _shutdownRequested;

  void Update() {
    if (Session != null && OverrideUpdateSession == false) {
      switch (Session.GameMode) {
        case DeterministicGameMode.Replay:
          Session.Update(Time.unscaledDeltaTime * ReplayPlaybackSpeed);
          break;
        case DeterministicGameMode.Multiplayer:
        case DeterministicGameMode.Local:
        case DeterministicGameMode.LocalDebug:
          Session.Update(DeltaTime);
          break;
      }
    }

    if (_shutdownRequested) {
      _shutdownRequested = false;
      Shutdown();
    }
  }

  void OnDisable() {
    //void OnDestroy() {
    // This was OnDestroy() before, which will postpone the removal until the next frame.
    // Changing this to OnDisable() will result in a more accurate OnDestroy event calling without calling WaitForSeconds().
    if (Session != null) {
      Session.Destroy();
      Session = null;
      Game = null;
    }
  }

  void OnDrawGizmos() {
#if UNITY_EDITOR
    if (Session != null) {
      var game = Session.Game as QuantumGame;
      if (game != null) {
        QuantumGameGizmos.OnDrawGizmos(game);
      }
    }
#endif
  }

  public void Shutdown() {
    // Runner is shut down, destroys its gameobject, will trigger OnDestroy() in next frame, will destroy the session, session will call dispose on the runner.
    Destroy(gameObject);
  }

  public void Dispose() {
    // Called by the Session.Destroy().
    _activeRunners.Remove(this);
  }

#if UNITY_SWITCH && !UNITY_EDITOR
  static unsafe class __Internal {
    [SuppressUnmanagedCodeSecurity]
    [DllImport("__Internal", EntryPoint = "egmemcpy", CallingConvention = CallingConvention.Cdecl, SetLastError = false)]
    public static unsafe extern void* memcpy(void* dest, void* src, UInt64 count);

    public static void* MemCpyCaller(void* dest, void* src, UInt64 count) {
      return __Internal.memcpy(dest, src, count);
    }
  }
#endif

  public static void Init(Boolean force = false) {

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
    Native.Impl = Native.NativeImpl.MSCVCRT;
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_IOS
    Native.Impl = Native.NativeImpl.LIBC;
#elif UNITY_ANDROID
    Native.Impl = Native.NativeImpl.C;
#elif UNITY_SWITCH && !UNITY_EDITOR
    unsafe {
      Native.Impl = Native.NativeImpl.DELEGATE;
      Native.MemCpyDelegate = __Internal.MemCpyCaller;
    }
#endif

    // load lookup table
    FPMathUtils.LoadLookupTables(force);

    // Init file loading from inside Quantum
    FileLoader.Init(new UnityFileLoader(QuantumEditorSettings.Instance.DatabasePath));

    // init profiler
    if (!DeterministicSessionConfigAsset.Instance.Config.RunInBackgroundThread)
      Profiler.Init(x => UnityEngine.Profiling.Profiler.BeginSample(x), () => UnityEngine.Profiling.Profiler.EndSample());

    // init debug draw functions
    Draw.Init(
      Quantum.Core.DebugDraw.Ray,
      Quantum.Core.DebugDraw.Ray3D,
      Quantum.Core.DebugDraw.Line,
      Quantum.Core.DebugDraw.Line3D,
      Quantum.Core.DebugDraw.Circle,
      Quantum.Core.DebugDraw.Sphere,
      Quantum.Core.DebugDraw.Rectangle
    );

    // init quantum logger
    Log.Init(
      UnityEngine.Debug.Log,
      UnityEngine.Debug.LogWarning,
      UnityEngine.Debug.LogError,
      UnityEngine.Debug.LogException
    );

    // init photon logger
    Photon.Deterministic.DeterministicLog.Init(
      UnityEngine.Debug.Log,
      UnityEngine.Debug.LogWarning,
      UnityEngine.Debug.LogError,
      UnityEngine.Debug.LogException
    );
  }

  // API compatibility, will be deprecated next release.
  [Obsolete("Use StartGame(String clientId, StartParameters param) instead")]
  public static QuantumRunner StartGame(String clientId, Int32 playerCount, DeterministicGameMode mode, RuntimeConfig runtimeConfig, IDeterministicReplayProvider replayProvider = null) {
    var param = new StartParameters {
      RuntimeConfig = runtimeConfig,
      DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
      ReplayProvider = replayProvider,
      GameMode = mode,
      InitialFrame = 0
    };
    return StartGame(clientId, playerCount, param);
  }

  // API compatibility, will be marked obsolete next release.
  public static QuantumRunner StartGame(String clientId, Int32 playerCount, StartParameters param) {
    param.PlayerCount = playerCount;
    param.LocalPlayerCount = 1;

    if (param.GameMode != DeterministicGameMode.Multiplayer) {
      // Always join all players as local players when not running default multiplayer.
      param.LocalPlayerCount = playerCount;
    }

    return StartGame(clientId, param);
  }

  public static QuantumRunner StartGame(String clientId, StartParameters param) {

    Log.Info("Starting Game");

    // set a default runner id if none is given
    if (param.RunnerId == null) {
      param.RunnerId = "DEFAULT";
    }

    // init debug
    Init();

    // initialize db
    UnityDB.Init();

    // Make sure the runtime config has a simulation config set.
    if (string.IsNullOrEmpty(param.RuntimeConfig.SimulationConfig.Guid)) {
      param.RuntimeConfig.SimulationConfig.Guid = DB.FindAllAssets<SimulationConfig>()[0].Guid;
    }

    // init layers
    var simulationConfig = param.RuntimeConfig.SimulationConfig.Instance;
    Layers.Init(simulationConfig.Physics.Layers, simulationConfig.Physics.LayerMatrix);

    if (param.GameMode == DeterministicGameMode.Multiplayer) {
      if (PhotonNetwork.connected == false) {
        throw new Exception("Not connected to photon");
      }

      if (PhotonNetwork.inRoom == false) {
        throw new Exception("Can't start networked game when not in a room");
      }
    }

    param.DeterministicConfig.PlayerCount = param.PlayerCount;

    // Add default game logic to callbacks when there is not callback object passed inside the params.
    var callbacks = param.GameCallbacks;
    if (callbacks == null) {
      callbacks = new QuantumGameCallbacks();
      // Calls QuantumInput.Instance for input polling
      callbacks.AddDisposableObject(new QuantumGameCallback_LocalInput(callbacks));
      // Forwards game callbacks to Unity scripts (QuantumCallbacks)
      callbacks.AddDisposableObject(new QuantumGameCallback_UnityCallbacks(callbacks));
      // Handles map loading
      callbacks.AddDisposableObject(new QuantumGameCallback_MapLoading(callbacks));
      // Calls debug draw
      callbacks.AddDisposableObject(new QuantumGameCallback_DebugDraw(callbacks));
      // Frame differ
      callbacks.AddDisposableObject(new QuantumGameCallback_FrameDiffer(callbacks));
    }

    // Create the runner
    var runner = CreateInstance();
    runner.Id = param.RunnerId;
    runner.DeltaTimeType = simulationConfig.DeltaTimeType;

    // Create the game
    runner.Game = new QuantumGame(callbacks);

    // new "local mode" runs as "replay" (with Game providing input polling), to avoid rollbacks of the local network debugger.
    // old Local mode can still be used for debug purposes (but RunnerLocalDebug now uses replay mode).
    if (param.LocalInputProvider == null && param.GameMode == DeterministicGameMode.Local)
      param.LocalInputProvider = runner.Game;

    // Create the session
    runner.Session = new DeterministicSession(
      param.GameMode, 
      RuntimeConfig.ToByteArray(param.RuntimeConfig), 
      param.DeterministicConfig,
      runner.Game,
      GetCommunicator(param.GameMode, param.QuitBehaviour), 
      param.ReplayProvider, 
      param.LocalInputProvider,
      param.InitialFrame,
      param.FrameData);

    // For convenience, to be able to access the runner by the session.
    runner.Session.Runner = runner;

    // Join local players
    runner.Session.Join(clientId, Math.Max(1, param.LocalPlayerCount), param.InitialFrame);

    return runner;
  }

  /// <summary>
  /// This cannot be called during the execution of Runner.Update() and Session.Update() methods.
  /// For this immediate needs to be false, which waits until the main thread is outside of Session.Update() to continue the shutdown of all runners.
  /// </summary>
  /// <param name="immediate">Destroy the sessions immediatly or wait to Session.Update to complete.</param>
  /// <returns>At least on runner is active and will shut down.</returns>
  public static bool ShutdownAll(bool immediate = false) {
    var result = _activeRunners.Count > 0;
    if (immediate) {
      while (_activeRunners.Count > 0) {
        _activeRunners.Last().Shutdown();
      }
    }
    else {
      foreach (var runner in _activeRunners)
        runner._shutdownRequested = true;
    }
    return result;
  }

  public static QuantumRunner FindRunner(string id) {
    return _activeRunners.Find(x => x.Id == id);
  }

  static QuantumNetworkCommunicator GetCommunicator(DeterministicGameMode mode, QuantumNetworkCommunicator.QuitBehaviour quitBehaviour) {
    if (mode != DeterministicGameMode.Multiplayer) {
      return null;
    }

    return new QuantumNetworkCommunicator(PhotonNetwork.networkingPeer, quitBehaviour);
  }

  static QuantumRunner CreateInstance() {
    GameObject go;

    go = new GameObject("QuantumRunner");
    var runner = go.AddComponent<QuantumRunner>();

    runner._shutdownRequested = false;

    _activeRunners.Add(runner);

    DontDestroyOnLoad(go);

    return runner;
  }
}
