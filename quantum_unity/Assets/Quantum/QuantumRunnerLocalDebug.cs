﻿using Quantum;
using System;
using UnityEngine;

public class QuantumRunnerLocalDebug : QuantumCallbacks {
  public Quantum.RuntimeConfig Config;
  public Quantum.RuntimePlayer[] Players;
  //public Int32 InitialFrame = 0;

  void Start() {
    if (PhotonNetwork.connected) {
      return;
    }

    if (QuantumRunner.Default != null)
      return;

    Debug.Log("### Starting quantum in local debug mode ###");

    var mapdata = FindObjectOfType<MapData>();
    if (mapdata) {

      // set map to this maps asset
      Config.Map.Guid = mapdata.Asset.AssetObject.Guid;

      var playerCount = Math.Max(1, Players.Length);

      // create start game parameter
      var param = new QuantumRunner.StartParameters {
        RuntimeConfig = Config,
        DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
        ReplayProvider = null,
        GameMode = Photon.Deterministic.DeterministicGameMode.Local,
        InitialFrame = 0,
        RunnerId = "LOCALDEBUG",
        PlayerCount = playerCount,
        LocalPlayerCount = playerCount
      };

      // start with debug config
      QuantumRunner.StartGame("LOCALDEBUG", param);
    }
    else {
      throw new Exception("No MapData object found, can't debug start scene");
    }
  }

  public override void OnGameStart(QuantumGame game) {
    for (Int32 i = 0; i < Players.Length; ++i) {
      game.SendPlayerData(i, Players[i]);
    }
  }
}
