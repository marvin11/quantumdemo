﻿using Newtonsoft.Json.Bson;
using System;
using System.IO;

namespace Quantum {
  public class BsonReplaySerializer : IReplaySerializer, IDisposable {
    public void Dispose() {
    }

    public T LoadFrom<T>(string text) {
      throw new NotImplementedException();
    }

    public T LoadFrom<T>(byte[] buffer) {
      using (var stream = new MemoryStream(buffer))
        return LoadFrom<T>(stream);
    }

    public T LoadFrom<T>(Stream stream) {
      using (var bsonReader = new BsonReader(stream)) {
        var serializer = NewtonsoftHelper.CreateSerializer(NewtonsoftHelper.CreateSettings());
        return serializer.Deserialize<T>(bsonReader);
      }
    }

    public void SaveTo(object obj, Stream stream) {
      using (var bsonWriter = new BsonWriter(stream)) {
        var serializer = NewtonsoftHelper.CreateSerializer(NewtonsoftHelper.CreateSettings());
        serializer.Serialize(bsonWriter, obj);
      }
    }
  }
}
