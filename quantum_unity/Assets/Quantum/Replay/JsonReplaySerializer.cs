﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace Quantum {
  public class JsonReplaySerializer : IReplaySerializer, IDisposable {
    public void Dispose() {
    }

    public T LoadFrom<T>(string text) {
      return JsonConvert.DeserializeObject<T>(text, NewtonsoftHelper.CreateSettings());
    }

    public T LoadFrom<T>(byte[] buffer) {
      throw new NotImplementedException();
    }

    public T LoadFrom<T>(Stream stream) {
      using (var streamReader = new StreamReader(stream))
      using (var jsonReader = new JsonTextReader(streamReader)) {
        var serializer = NewtonsoftHelper.CreateSerializer(NewtonsoftHelper.CreateSettings());
        return serializer.Deserialize<T>(jsonReader);
      }
    }

    public void SaveTo(object obj, Stream stream) {
      using (var streamWriter = new StreamWriter(stream))
      using (var jsonWriter = new JsonTextWriter(streamWriter)) {
        var serializer = NewtonsoftHelper.CreateSerializer(NewtonsoftHelper.CreateSettings());
        serializer.Serialize(jsonWriter, obj);
      }
    }
  }
}
