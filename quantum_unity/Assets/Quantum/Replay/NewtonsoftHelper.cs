﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Quantum {
  public static class NewtonsoftHelper {
    public static JsonSerializerSettings CreateSettings() {
      return new JsonSerializerSettings {
        ContractResolver = new WritablePropertiesOnlyResolver(),
        Formatting = Formatting.Indented,
        TypeNameHandling = TypeNameHandling.Auto,
        NullValueHandling = NullValueHandling.Ignore,
      };
    }

    public static JsonSerializer CreateSerializer(JsonSerializerSettings settings) {
      return JsonSerializer.Create(settings);
    }
  }

  class WritablePropertiesOnlyResolver : DefaultContractResolver {
    protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization) {
      IList<JsonProperty> props = base.CreateProperties(type, memberSerialization);
      return props.Where(p => p.Writable).ToList();
    }
  }
}
