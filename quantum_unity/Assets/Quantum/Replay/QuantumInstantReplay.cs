﻿using Photon.Deterministic;
using System;
using UnityEngine;

namespace Quantum{
  public class QuantumInstantReplay {

    public bool IsRunning { get; private set; }
    public float PlaybackSpeed {
      get {
        return _playbackSpeed;
      }
      set {
        _playbackSpeed = value;
        if (_replayRunner != null)
          _replayRunner.ReplayPlaybackSpeed = _playbackSpeed;
      }
    }
    public float ReplayLength { get; set; }
    public Action<QuantumGame> OnReplayStarted;
    public Action<QuantumGame> OnReplayStopped;

    // We need this to fast forward the simulation and wait until is fully initialized.
    public const int InitalFramesToSimulation = 4;

    private QuantumGame _liveGame;
    private QuantumRunner _replayRunner;
    private int _liveFrame;
    private float _playbackSpeed = 1.0f;

    public QuantumInstantReplay(QuantumGame game) {

      if (DeterministicSessionConfigAsset.Instance.Config.RunInBackgroundThread)
        Debug.LogError("QuantumInstantReplay is not thread-safe at the moment. Disable DeterministicSessionConfig.RunInBackgroundThread to test.");

      _liveGame = game;
    }

    public void Shutdown() {
      if (IsRunning)
        StopInstantReplay();

      OnReplayStarted = null;
      OnReplayStopped = null;

      _liveGame = null;
    }

    public void Update() {
      if (IsRunning) {
        // Stop the running instant replay.
        if (_replayRunner.Game.Frames.Verified != null &&
            _replayRunner.Game.Frames.Verified.Number >= _liveFrame) {
          StopInstantReplay();
        }
      }
    }

    public void StartInstantReplay() {

      if (DeterministicSessionConfigAsset.Instance.Config.RunInBackgroundThread) {
        Debug.LogError("Can't run instant replays with RunInBackgroundThread enabled.");
        return;
      }

      if (IsRunning) {
        Debug.LogError("Instant replay is already running.");
        return;
      }

      var inputProvider = _liveGame.Session.IsReplay ? _liveGame.Session.ReplayProvider : _liveGame.RecordedInputs;
      if (inputProvider == null) {
        Debug.LogError("Can't run instant replays without an input provider. Start the game with RecordingFlags.Input.");
        return;
      }

      IsRunning = true;
      _liveFrame = _liveGame.Frames.Verified.Number;

      var deterministicConfig = _liveGame.Session.SessionConfig;
      var desiredReplayFrame = _liveFrame - Mathf.FloorToInt(ReplayLength * deterministicConfig.UpdateFPS);
      var snapshot = _liveGame.GetRecordedSnapshot(desiredReplayFrame);

      // Create all required start parameters and serialize the snapshot as start data.
      var param = new QuantumRunner.StartParameters {
        RuntimeConfig = _liveGame.Configurations.Runtime,
        DeterministicConfig = deterministicConfig,
        GameCallbacks = new QuantumGameCallbacks(),
        ReplayProvider = inputProvider,
        GameMode = DeterministicGameMode.Replay,
        FrameData = snapshot.Serialize(),
        InitialFrame = snapshot.Number,
        RunnerId = "InstantReplay",
        PlayerCount = deterministicConfig.PlayerCount,
        LocalPlayerCount = deterministicConfig.PlayerCount
      };

      _replayRunner = QuantumRunner.StartGame("INSTANTREPLAY", param);
      _replayRunner.ReplayPlaybackSpeed = PlaybackSpeed;

      // Set the QuantumGame.Instance back to the live instance
#pragma warning disable CS0618 // Type or member is obsolete
      QuantumGame.Instance = _liveGame;
#pragma warning restore CS0618 // Type or member is obsolete

      // Run a couple of frames until fully initialized (replayRunner.Session.FrameVerified is set and session state isRunning).
      for (int i = 0; i < InitalFramesToSimulation; i++) { 
        _replayRunner.Session.Update(1.0f / deterministicConfig.UpdateFPS);
      }

      // Fast forward the session manually.
      while (_replayRunner.Session.FrameVerified.Number < desiredReplayFrame)
        _replayRunner.Session.Update(1.0f / deterministicConfig.UpdateFPS);

      if (OnReplayStarted != null)
        OnReplayStarted(_replayRunner.Game);
    }

    public void StopInstantReplay() {

      if (!IsRunning) {
        Debug.LogError("Instant replay is not running.");
        return;
      }

      IsRunning = false;

      if (OnReplayStopped != null)
        OnReplayStopped(_replayRunner.Game);

      if (_replayRunner != null)
        _replayRunner.Shutdown();

      _replayRunner = null;
    }
  }
}
