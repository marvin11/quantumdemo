﻿using UnityEngine;
using Quantum;

public class QuantumInstantReplayDemo : MonoBehaviour {

  public float PlaybackSpeed = 0.5f;
  [EditorDisabled]
  public bool IsReplayRunning;
  [InspectorButton("Editor_StartInstantReplay", "Start", true)]
  public bool Button_StartInstantReplay;
  [InspectorButton("Editor_StopInstantReplay", "Stop", true)]
  public bool Button_StopInstantReplay;
  public float BufferSizeSec = 3.0f;
  public int SnapshotFrequencyPerSec = 1;
  public float ReplayLengthSec = 2.0f;
  public bool ShowReplayLabel = true;
  public bool ShowFadingEffect = true;

  QuantumInstantReplay _instantReplay;
  bool _isFading;
  float _fadingAlpha = 1.0f;
  Texture2D _fadingTexture;
  float _fadingTime;

  #region Unity Callbacks

  public void Update() {

    // Lazy init the replay recorder.
    if (_instantReplay == null && QuantumRunner.Default != null) {
      // Create the instant replay object that takes care of creating new sessions.
      _instantReplay = new QuantumInstantReplay(QuantumRunner.Default.Game);
      _instantReplay.OnReplayStarted += OnReplayStarted;
      _instantReplay.OnReplayStopped += OnReplayStopped;

      // Tell the game to start capturing snapshots. This can be called at any point in the game.
      QuantumRunner.Default.Game.StartRecordingSnapshots(BufferSizeSec, SnapshotFrequencyPerSec);
      QuantumRunner.Default.Game.Callbacks.OnGameDestroyed += OnGameDestroyed;
    }

    if (_instantReplay != null) {
      _instantReplay.PlaybackSpeed = PlaybackSpeed;
      _instantReplay.ReplayLength = ReplayLengthSec;
      _instantReplay.Update();
    }

    Button_StartInstantReplay = _instantReplay != null && !_instantReplay.IsRunning;
    Button_StopInstantReplay = _instantReplay != null && _instantReplay.IsRunning;
    IsReplayRunning = _instantReplay != null && _instantReplay.IsRunning;
  }

  public void OnDisable() {
    if (_instantReplay != null && QuantumRunner.Default != null) {
      QuantumRunner.Default.Game.Callbacks.OnGameDestroyed -= OnGameDestroyed;
      _instantReplay.Shutdown();
      _instantReplay = null;
    }
  }

  void OnDestroy() {
    if (_fadingTexture != null)
      Destroy(_fadingTexture);
    _fadingTexture = null;
  }

  void OnGUI() {
    if (ShowReplayLabel && _instantReplay != null && _instantReplay.IsRunning) {
      GUI.contentColor = Color.red;
      GUI.Label(new Rect(10, 10, 200, 100), "INSTANT REPLAY");
    }

    if (_isFading) {
      _fadingTime += Time.deltaTime;
      _fadingAlpha = Mathf.Lerp(1.0f, 0.0f, _fadingTime);

      if (_fadingTexture == null)
        _fadingTexture = new Texture2D(1, 1);

      _fadingTexture.SetPixel(0, 0, new Color(0, 0, 0, _fadingAlpha));
      _fadingTexture.Apply();

      GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _fadingTexture);

      _isFading = _fadingAlpha > 0;
    }
  }

  #endregion

  #region QuantumRunner.Default.Game Callbacks

  public  void OnGameDestroyed(QuantumGame game) {
    // This is called when the default game runner is shut down.
    game.Callbacks.OnGameDestroyed -= OnGameDestroyed;
    
    if (_instantReplay != null)
      _instantReplay.Shutdown();
    _instantReplay = null;
  }

  #endregion

  #region Instant Replay Callbacks

  void OnReplayStarted(QuantumGame game) {
    game.Callbacks.OnGameDestroyed += OnReplayGameDestroyed;

    Debug.LogFormat("### Starting quantum instant replay at frame {0} ###", game.Frames.Current.Number);

    // FindObjectOfType is super slow, but it serves the demo purpose here.
    var entityPrefabViewUpdater = GameObject.FindObjectOfType<EntityPrefabViewUpdater>();
    if (entityPrefabViewUpdater != null) {
      // Switch the callbacks if you want to use the same view updater
      QuantumRunner.Default.Game.Callbacks.OnUpdateView -= entityPrefabViewUpdater.OnUpdateView;
      game.Callbacks.OnUpdateView += entityPrefabViewUpdater.OnUpdateView;
      entityPrefabViewUpdater.TeleportAllEntities();
    }

    StartFading();
  }

  void OnReplayStopped(QuantumGame game) {
    game.Callbacks.OnGameDestroyed -= OnReplayGameDestroyed;

    Debug.LogFormat("### Stopping quantum instant replay and resuming the live game ###");

    var entityPrefabViewUpdater = GameObject.FindObjectOfType<EntityPrefabViewUpdater>();
    if (entityPrefabViewUpdater != null) {
      QuantumRunner.Default.Game.Callbacks.OnUpdateView += entityPrefabViewUpdater.OnUpdateView;
      game.Callbacks.OnUpdateView -= entityPrefabViewUpdater.OnUpdateView;
      entityPrefabViewUpdater.TeleportAllEntities();
    }

    StartFading();
  }

  void OnReplayGameDestroyed(QuantumGame game) {
    // This will be called if the replay runner is shut down outside this class.
    // We can call shutdown() on the runner multiple times during the same frame.
    if (_instantReplay != null && _instantReplay.IsRunning)
      _instantReplay.StopInstantReplay();
  }

  void StartFading() {
    if (ShowFadingEffect) {
      _isFading = true;
      _fadingAlpha = 1.0f;
      _fadingTime = 0.0f;
    }
  }

  #endregion

  #region Editor Button

  public void Editor_StartInstantReplay() {
    if (_instantReplay != null && !_instantReplay.IsRunning)
      _instantReplay.StartInstantReplay();
  }

  public void Editor_StopInstantReplay() {
    if (_instantReplay != null && _instantReplay.IsRunning)
      _instantReplay.StopInstantReplay();
  }

  #endregion
}
