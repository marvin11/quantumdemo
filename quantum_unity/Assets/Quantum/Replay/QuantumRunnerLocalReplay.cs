﻿using Quantum;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class QuantumRunnerLocalReplay : MonoBehaviour {
  public TextAsset ReplayFile;
  public TextAsset DatabaseFile;
  public string DatabasePath;
  public TextAsset ChecksumFile;
  public bool ShowReplayLabel;

  private static InputProvider InputProvider;

  void Start() {
    if (PhotonNetwork.connected) {
      return;
    }

    if (QuantumRunner.Default != null)
      return;

    if (ReplayFile == null) {
      Debug.LogError("QuantumRunnerLocalReplay - not replay file selected.");
      return;
    }

    Debug.Log("### Starting quantum in local replay mode ###");

    var jsonSerializer = new JsonReplaySerializer();

    // Load replay file in json or bson
    ReplayFile replayFile = null;
    if (ReplayFile.text.Length > 0) {
      replayFile = jsonSerializer.LoadFrom<ReplayFile>(ReplayFile.text);
    }
    else {
      var bsonSerializer = new BsonReplaySerializer();
      replayFile = bsonSerializer.LoadFrom<ReplayFile>(ReplayFile.bytes);
    }

    // Create a new input provider from the replay file
    InputProvider = new InputProvider(replayFile.DeterministicConfig.PlayerCount);
    InputProvider.ImportFromList(replayFile.InputHistory);

    var param = new QuantumRunner.StartParameters {
      RuntimeConfig = replayFile.RuntimeConfig,
      DeterministicConfig = replayFile.DeterministicConfig,
      ReplayProvider = InputProvider,
      GameMode = Photon.Deterministic.DeterministicGameMode.Replay,
      RunnerId = "LOCALREPLAY",
      PlayerCount = replayFile.DeterministicConfig.PlayerCount,
      LocalPlayerCount = replayFile.DeterministicConfig.PlayerCount
    };

    var runner = QuantumRunner.StartGame("LOCALREPLAY", param);

    // Overwrite the Quantum database from file
    if (DatabaseFile != null) {
      // Add the database relative path to point to specific files to load during asset initialization.
      // Files must reside inside Resources folder to work outside Editor context.
      FileLoader.Init(new UnityFileLoader(DatabasePath));

      var databaseFile = jsonSerializer.LoadFrom<DatabaseFile>(DatabaseFile.text);
      var transformedAssetList = ArrayUtils.AddAtStart(databaseFile.Database, null);
      DB.Init(transformedAssetList);

      // Remap UnityDB to match the Quantum assets loaded from the file.
      // Careful: all calls to UnityDB.FindAsset(by asset or by index) performed before this will potentially break things.
      UnityDB.RemapDatabase(transformedAssetList);
    }

    if (ChecksumFile != null) {
      var checksumFile = new JsonReplaySerializer().LoadFrom<ChecksumFile>(ChecksumFile.text);
      runner.Game.StartVerifyingChecksums(checksumFile);
    }
  }
  
#if UNITY_EDITOR
  private void Update() {
    if (InputProvider != null && InputProvider.Finished) {
      EditorApplication.isPaused = true;
    }
  }
#endif

#if UNITY_EDITOR
  private float guiTimer;

  void OnGUI() {
    if (ShowReplayLabel && InputProvider != null) {
      if (InputProvider.Finished) {
        GUI.contentColor = Color.red;
        GUI.Label(new Rect(10, 10, 200, 100), "REPLAY COMPLETED");
      }
      else {
        guiTimer += Time.deltaTime;
        if (guiTimer % 2.0f > 1.0f) {
          GUI.contentColor = Color.red;
          GUI.Label(new Rect(10, 10, 200, 100), "REPLAY PLAYING");
        }
      }
    }
  }
#endif
}
