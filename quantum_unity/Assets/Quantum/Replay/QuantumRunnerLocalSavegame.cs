﻿using Quantum;
using UnityEngine;

public class QuantumRunnerLocalSavegame : MonoBehaviour {
  public TextAsset SavegameFile;
  public TextAsset DatabaseFile;
  public string DatabasePath;

  void Start() {
    if (PhotonNetwork.connected) {
      return;
    }

    if (QuantumRunner.Default != null)
      return;

    if (SavegameFile == null) {
      Debug.LogError("QuantumRunnerLocalSavegame - not savegame file selected.");
      return;
    }

    Debug.Log("### Starting quantum in local savegame mode ###");

    var jsonSerializer = new JsonReplaySerializer();

    // Load replay file in json or bson
    ReplayFile replayFile = null;
    if (SavegameFile.text.Length > 0) {
      replayFile = jsonSerializer.LoadFrom<ReplayFile>(SavegameFile.text);
    }
    else {
      var bsonSerializer = new BsonReplaySerializer();
      replayFile = bsonSerializer.LoadFrom<ReplayFile>(SavegameFile.bytes);
    }

    var param = new QuantumRunner.StartParameters {
      RuntimeConfig = replayFile.RuntimeConfig,
      DeterministicConfig = replayFile.DeterministicConfig,
      GameMode = Photon.Deterministic.DeterministicGameMode.Local,
      FrameData = replayFile.Frame,
      InitialFrame = replayFile.Length,
      RunnerId = "LOCALSAVEGAME",
      PlayerCount = replayFile.DeterministicConfig.PlayerCount,
      LocalPlayerCount = replayFile.DeterministicConfig.PlayerCount
    };

    QuantumRunner.StartGame("LOCALSAVEGAME", param);

    // Overwrite the Quantum database from file
    if (DatabaseFile != null) {
      // Add the database relative path to point to specific files to load during asset initialization.
      // Files must reside inside Resources folder to work outside Editor context.
      FileLoader.Init(new UnityFileLoader(DatabasePath));

      var databaseFile = jsonSerializer.LoadFrom<DatabaseFile>(DatabaseFile.text);
      var transformedAssetList = ArrayUtils.AddAtStart(databaseFile.Database, null);
      DB.Init(transformedAssetList);

      // Remap UnityDB to match the Quantum assets loaded from the file.
      // Careful: all calls to UnityDB.FindAsset(by asset or by index) performed before this will potentially break things.
      UnityDB.RemapDatabase(transformedAssetList);
    }
  }
}