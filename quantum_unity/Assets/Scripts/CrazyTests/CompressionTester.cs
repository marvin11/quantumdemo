using System;
using System.Diagnostics;
using BitStrap;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = System.Random;


// 2
// 0-10 
// 0-10
// 0-10

public sealed class CompressionTester : MonoBehaviour
{
	public Vector3 positionToCompress;

	public int positionToDecompress;

	
	[Header("Config")]
	public float upperBound = 1.0f;
	public int steps = 511;
	
	public int numberOfSimulationsToRun = 10000;
	public float simulationRadius = 2;

	int CompressFunction(Vector3 pos)
	{
		var norm = pos;

		var biggest = -2.0f;
		
		var firstComponent = 0;
		var secondComponent = 0;
		var sizeComponent = 0;
		var bigIndex = 0;
			
		for (int i = 0; i < 3; i++)
		{
			if (norm[i] > biggest)
			{
				biggest = norm[i];
				bigIndex = i;
			}
		}

		bigIndex = 1;
		
		switch (bigIndex)
		{
			case 0:
				firstComponent = CompressPolar(norm[1]);
				secondComponent = CompressPolar(norm[2]);
				break;
			case 1:
				firstComponent = CompressPolar(norm[0]);
				secondComponent = CompressPolar(norm[2]);
				break;
			case 2:
				firstComponent = CompressPolar(norm[0]);
				secondComponent = CompressPolar(norm[1]);
				break;
		}

		var size = positionToCompress.magnitude;
		var stepSize = 0.01f;

		sizeComponent = Mathf.Clamp(Mathf.FloorToInt(size / stepSize), 0, 1023);
		
		var final = (bigIndex << 30) | (firstComponent << 20) | (secondComponent << 10) | sizeComponent;
		return final;
	}

	Vector3 DecompressFunction(int compressed)
	{
		const uint BITMASK = ((1 << 10) - 1);
		var bigIndex = (positionToDecompress >> 30);
		var firstComponent = DecompressPolar((int) ((positionToDecompress >> 20) & BITMASK));
		var secondComponent = DecompressPolar((int) ((positionToDecompress >> 10) & BITMASK));
		var sizeComponent = positionToDecompress & BITMASK;

		var lastComponent = Mathf.Sqrt(1 - (firstComponent * firstComponent + secondComponent * secondComponent));
		
		var final = Vector3.zero;
		
		switch (bigIndex)
		{
			case 0:
				final = new Vector3(lastComponent, firstComponent, secondComponent);
				break;
			case 1:
				final = new Vector3(firstComponent, lastComponent, secondComponent);
				break;
			case 2:
				final = new Vector3(firstComponent, secondComponent, lastComponent);
				break;
		}

		final *= (sizeComponent * 0.01f);
		return final;
	}
	
	[Button]
	public void Compress()
	{
		var final = CompressFunction(positionToCompress);
		positionToDecompress = final;
	}

	[Button]
	public void Decompress()
	{
		var final = DecompressFunction(positionToDecompress);
		
		var error = (positionToCompress - final).magnitude;
	}

	[Button]
	public void Time()
	{
		double averageError = 0.0;

		for (int i = 0; i < numberOfSimulationsToRun; i++)
		{
			var randomPos = UnityEngine.Random.insideUnitSphere * UnityEngine.Random.Range(0, 2);

			if (randomPos.y < 0)
				randomPos.y = -randomPos.y;
			
			var compressed = CompressFunction(randomPos);
			var decompressed = DecompressFunction(compressed);

			var error = (randomPos - decompressed).magnitude;
			averageError += error;
		}

		Debug.LogFormat("Average error: {0}", averageError / numberOfSimulationsToRun);
	}
	
	private int CompressPolar(float f)
	{
		var r = f > 0 ? 0 : 512;
		var mask = ~512;

		var stepSize = upperBound / steps;

		var t = Mathf.FloorToInt(f / stepSize);

		var final = r | (t & mask);
		return final;
	}

	private float DecompressPolar(int v)
	{
		var mask = (1 << 9) - 1;
		var sign = (v & (1 << 9)) > 0 ? -1 : 1;
		
		var stepSize = upperBound / steps;

		var final = sign * ((v & mask) * stepSize);

		return final;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.DrawSphere(positionToCompress, 0.01f);
	}
}
