﻿using UnityEngine;

public class CustomCallbacks : QuantumCallbacks {

  public override void OnGameStart(QuantumGame game) {
    foreach (var lp in game.GetLocalPlayers()) {
      Debug.Log("CustomCallbacks - sending player: " + lp);
      game.SetPlayerData(lp, new Quantum.RuntimePlayer { });
    }
  }
}
