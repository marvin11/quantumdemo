﻿using Photon.Deterministic;
using UnityEngine;
using Input = Quantum.Input;

public class LocalInput : QuantumInput
{
  public override Photon.Deterministic.Tuple<Input, DeterministicInputFlags> PollInput(int player)
  {
    Input i = new Input();

    var p = FindObjectOfType<SandboxPlayer>();

    if (p != null)
    {
      var centerPos = p.CenterPosition;

      i.HeadPos = CalculatePositionBits(p.head.position, centerPos);
      i.HeadRotation = CalculateRotationBits(p.head.rotation);

      i.LeftHandPos = CalculatePositionBits(p.LeftHandPosition, centerPos);
      i.LeftHandRotation = CalculateRotationBits(p.LeftHandRotation);
      
      i.RightHandPos = CalculatePositionBits(p.RightHandPosition, centerPos);
      i.RightHandRotation = CalculateRotationBits(p.RightHandRotation);

      i.LeftHandAnimation = p.GetLeftHandAnimationData();
      i.RightHandAnimation = p.GetRightHandAnimationData();
      
      i.LeftButton = p.GetGrabStateLeft();
      i.RightButton = p.GetGrabStateRight();

      var lta = p.GetTeleportStateLeft();
      var rta = p.GetTeleportStateRight();

      i.LeftTeleport = lta.magnitude > 0.2f;
      i.RightTeleport = rta.magnitude > 0.2f;

      i.LeftTeleportAngle = TransformAngleToInput(lta);
      i.RightTeleportAngle = TransformAngleToInput(rta);

      i.LeftVelocity = p.LeftVelocity.ToFPVector3();
      i.RightVelocity = p.RightVelocity.ToFPVector3();

      // TODO: change Jump for controller button
      i.Pad = p.GetPointerToggleState();
    }

    return Tuple.Create(i, DeterministicInputFlags.Repeatable);
  }

  byte TransformAngleToInput(Vector2 dir)
  {
    var step = 360.0f / byte.MaxValue;
    float angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
    if (angle < 0f)
      angle += 360f;

    return (byte) (angle / step);
  }
  
  private uint CalculatePositionBits(Vector3 targetPosition, Vector3 centerPos)
  {
    var diff = targetPosition - centerPos;

    var x = Compress(diff.x);
    var y = Compress(diff.y);
    var z = Compress(diff.z);

    return (uint)(x << 20 | y << 10 | z);
  }

  private uint CalculateRotationBits(Quaternion targetRotation)
  {
    uint index = 0, x, y, z;

    float max = -2;
    for (int i = 0; i < 4; i++)
    {
      if (Mathf.Abs(targetRotation[i]) > max)
      {
        max = targetRotation[i];
        index = (uint)i;
      }
    }

    if (targetRotation[(int)index] < 0)
      for (int i = 0; i < 4; i++)
        targetRotation[i] = -targetRotation[i];

    CompressQuaternion(targetRotation, index, out x, out y, out z);

    return (uint)(index << 30 | x << 20 | y << 10 | z);
  }

  private void CompressQuaternion(Quaternion rot, uint drop, out uint x, out uint y, out uint z)
  {
    float tarX = 0, tarY = 0, tarZ = 0;

    switch (drop)
    {
    case 0:
      tarX = rot.y;
      tarY = rot.z;
      tarZ = rot.w;
      break;
    case 1:
      tarX = rot.x;
      tarY = rot.z;
      tarZ = rot.w;
      break;
    case 2:
      tarX = rot.x;
      tarY = rot.y;
      tarZ = rot.w;
      break;
    case 3:
      tarX = rot.x;
      tarY = rot.y;
      tarZ = rot.z;
      break;
    }

    x = CompressQuaternionComponent(tarX);
    y = CompressQuaternionComponent(tarY);
    z = CompressQuaternionComponent(tarZ);
  }

  private uint CompressQuaternionComponent(float target)
  {
    const uint BITMASK = ((1 << 9) - 1);

    uint sign = 0;
    if (target < 0)
    {
      sign = 1;
      target = -target;
    }

    ulong fpValue = (ulong)FP.FromFloat_UNSAFE(target).RawValue;
    fpValue = fpValue >> 7;

    return (uint)((fpValue & BITMASK) | (sign << 9));
  }

  private uint Compress(float val)
  {
    // Anything out of the domain stays at the edges of the domain
    val = Mathf.Clamp(val + 4, 0, 7.99999f);
    const uint BITMASK = ((1 << 10) - 1);
    ulong fpValue = (ulong)FP.FromFloat_UNSAFE(val).RawValue;

    fpValue = fpValue >> 9;

    return (uint)(fpValue & BITMASK);
  }
}
