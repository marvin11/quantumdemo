﻿using System;
using System.Collections;
using System.Linq;
using Photon;
using Photon.Deterministic;
using Quantum;
using QuantumReconnect;
using TMPro;
using UnityEngine;
using UnityEngine.XR;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public class MenuController : PunBehaviour
{
	private bool _started;

	[Header("Room Configuration")]
	public string singletonRoomName = "SpaceHub-Singleton";
	public int maxNumberOfPlayersPerRoom = 8;
	
	[Header("Rigs")]
	public GameObject mobileRig;
	public GameObject desktopRig;
	public GameObject pcClientRig;
	
	[Header("Region Maps")]
	public MapAsset usMap;
	public MapAsset saMap;
	public MapAsset euMap;
	
	[Header("Initial Menu")]
	public GameObject initialMenuObject;

	[Header("Room Menu")]
	public GameObject roomMenuObject;

	public TMP_Text roomStatus;
	public GameObject startButton;

	public TMP_Text uiStatus;

	private CloudRegionCode activeRegion;
	private MapAsset activeMap;
	
	private void ShowMenu()
	{
		initialMenuObject.SetActive(true);
		roomMenuObject.SetActive(false);
	}

	private void ShowRoom()
	{
		initialMenuObject.SetActive(false);
		roomMenuObject.SetActive(true);
	}

	private void Start()
	{
		var usePcClient = string.IsNullOrEmpty(XRSettings.loadedDeviceName);
		
		mobileRig.SetActive(!usePcClient && Application.platform == RuntimePlatform.Android);
		desktopRig.SetActive(!usePcClient && Application.platform != RuntimePlatform.Android);
		pcClientRig.SetActive(usePcClient);
		
		UnityDB.Init();
		ShowMenu();
	}

	public void ConnectToPhoton(string region)
	{
		switch (region)
		{
			case "us":
				activeRegion = CloudRegionCode.eu;
				activeMap = usMap;
				break;
			case "sa":
				activeRegion = CloudRegionCode.eu;
				activeMap = saMap;
				break;
			case "eu":
				activeRegion = CloudRegionCode.eu;
				activeMap = euMap;
				break;
		}

		ConnectToPhoton(activeRegion);
	}

	public void ConnectToPhoton(CloudRegionCode code)
	{
		uiStatus.text = "Connecting to Photon...";

		PhotonNetwork.player.NickName = Guid.NewGuid().ToString();
		PhotonNetwork.autoJoinLobby = true;

		PhotonNetwork.ConnectToRegion(activeRegion, "1.0");
	}

	public void JoinSingletonRoom()
	{
		uiStatus.text = "Joining room!";
		SnapshotGameStarter.Instance.SnapshotJoinRoom(singletonRoomName);
	}

	public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
	{
		var code = (short) codeAndMsg[0];

		switch (code)
		{
			case ErrorCode.GameFull:
				ShowAlert("The meeting you wanted to join\nis currently full...");
				break;
			case ErrorCode.GameDoesNotExist:
				CreateSingletonRoom();
				break;
			default:
				ShowAlert((string) codeAndMsg[1]);
				break;
		}
	}

	private void ShowAlert(string errorMessage)
	{
		ShowRoom();
		roomStatus.text = errorMessage;
		
		StartCoroutine(DisconnectAfterTime(5.0f));
	}

	private IEnumerator DisconnectAfterTime(float t)
	{
		yield return new WaitForSeconds(t);
		
		PhotonNetwork.Disconnect();
		ShowMenu();
	}

	void CreateSingletonRoom()
	{
		var roomOptions = new RoomOptions();
		roomOptions.IsVisible = true;
		roomOptions.MaxPlayers = (byte) maxNumberOfPlayersPerRoom;

		FrameSetup.Instance.ResetLoadGameSetup();
		PhotonNetwork.CreateRoom(singletonRoomName, roomOptions, TypedLobby.Default);
	}
	
	public override void OnJoinedRoom()
	{
		ShowRoom();
		if (PhotonNetwork.isMasterClient) 
			StartGame();
	}

	public override void OnLeftRoom()
	{
		ShowMenu();
	}

	private void Update()
	{
		if (PhotonNetwork.inRoom)
		{
			var r = PhotonNetwork.room;
			roomStatus.text = string.Format("Starting meeting...");


			// Only master client starts the room, other clients snapshot-join
			if (PhotonNetwork.isMasterClient)
				CheckForGameStart();
		}
		startButton.SetActive(false);
	}

	public void StartGame()
	{
		if (PhotonNetwork.isMasterClient && PhotonNetwork.room.IsOpen)
		{
			var map = activeMap.Settings.Scene;

			var ht = new Hashtable();
			ht.Add("MAP", map);
			ht.Add("START", true);

			PhotonNetwork.room.SetCustomProperties(ht);
		}
	}

	private void CheckForGameStart()
	{
		if (_started)
			return;

		if (FrameSetup.Instance.SnapshotJoin || FrameSetup.Instance.IsLoadGame)
			return;

		var start = false;
		var map = default(string);

		if (TryGetRoomProperty("START", out start) && TryGetRoomProperty("MAP", out map))
		{
			if (start && string.IsNullOrEmpty(map) == false)
			{
				_started = true;

				RuntimeConfig config;
				config = new RuntimeConfig();
				config.RecordingFlags = RecordingFlags.Default;
				config.Map.Guid = UnityDB.AllOf<MapAsset>().First(x => x.Settings.Scene == map).Settings.Guid;
				
				var param = new QuantumRunner.StartParameters
				{
					RuntimeConfig = config,
					DeterministicConfig = DeterministicSessionConfigAsset.Instance.Config,
					ReplayProvider = null,
					GameMode = DeterministicGameMode.Multiplayer,
					InitialFrame = 0
				};

				Debug.Log("QuantumRunner.StartGame");
				QuantumRunner.StartGame(Guid.NewGuid().ToString(), PhotonNetwork.room.MaxPlayers, param);

				UIRoom.HideScreen();
				UILeaveGame.ShowScreen();
			}
		}
	}

	private bool TryGetRoomProperty<T>(string key, out T value)
	{
		object v;

		if (PhotonNetwork.room.CustomProperties.TryGetValue(key, out v))
		{
			if (v is T)
			{
				value = (T) v;
				return true;
			}
		}

		value = default(T);
		return false;
	}

	public override void OnConnectedToMaster()
	{
		ConnectionSuccess();
	}

	public override void OnJoinedLobby()
	{
		ConnectionSuccess();
	}

	private void ConnectionSuccess()
	{
		uiStatus.text = "Connected and ready!";
		JoinSingletonRoom();
	}

	public void OnRegionSelected(bool v)
	{
		PhotonNetwork.Disconnect();
	}
}