﻿using UnityEngine;
using UnityEngine.Events;

public class VrSimpleButton : MonoBehaviour
{
	public UnityEvent onButtonTriggered;
	
	private void OnTriggerEnter(Collider other)
	{
		onButtonTriggered.Invoke();
	}
}