using UnityEngine;

public class OvrLinePointer : OVRCursor
{
	public LineRenderer lineRenderer;
	public float pointerSize;
	
	public override void SetCursorRay(Transform ray)
	{
		lineRenderer.SetPosition(0, ray.position);
		lineRenderer.SetPosition(1, ray.position + ray.forward * pointerSize);
	}

	public override void SetCursorStartDest(Vector3 start, Vector3 dest, Vector3 normal)
	{
		lineRenderer.SetPosition(0, start);
		lineRenderer.SetPosition(1, dest);
	}
}