using UnityEngine;
using Quantum;
using System;

public unsafe class PointerEventsHandler : MonoBehaviour
{
    private SandboxPlayer playerPrefab;
    private SandboxRemotePlayer playerPrefabRemote;
    private bool isRemote = false;

    private void Start()
    {
        playerPrefab = transform.root.gameObject.GetComponent<SandboxPlayer>();

        if (playerPrefab == null)
        {
            playerPrefabRemote = transform.root.gameObject.GetComponent<SandboxRemotePlayer>();
            isRemote = true;
        }

        EventTogglePointer.OnRaised += OnTogglePointer;
    }

    private void OnTogglePointer(EventTogglePointer obj)
    {
        var c = QuantumRunner.Default.Game.Frames.Current.GetPlayer(obj.Player);


        //Debug.Log(c->EntityRef.Index + " == " + (int)playerPrefab.playerReference);

        if (c->EntityRef.Index == (isRemote ? (int)playerPrefabRemote.playerReference : (int)playerPrefab.playerReference))
            BroadcastMessage("TogglePointer");
    }

    private void OnDestroy()
    {
        EventTogglePointer.OnRaised -= OnTogglePointer;
    }
}
