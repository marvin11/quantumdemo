﻿Shader "Custom/Mobile/DiffuseX Emissive"
{
	Properties
	{
		[HDR]_Color("Color",COLOR)=(0.5,0.5,0.5,1.0)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		[HDR]_EmissiveColor("Emissive Color",COLOR)=(0.5,0.5,0.5,1.0)
		_EmissiveTex ("Emissive (RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 150
		CGPROGRAM
		#pragma surface surf Lambert noforwardadd

		sampler2D _MainTex;
		sampler2D _EmissiveTex;
		fixed4 _Color;
		fixed4 _EmissiveColor;

		struct Input
		{
			float2 uv_MainTex;
			float2 uv_EmissiveTex;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			fixed3 e = tex2D(_EmissiveTex, IN.uv_EmissiveTex).rgb * _EmissiveColor.rgb;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			o.Emission = e;
		}
		ENDCG
	}
	Fallback "Mobile/VertexLit"
}